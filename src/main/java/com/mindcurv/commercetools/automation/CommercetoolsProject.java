package com.mindcurv.commercetools.automation;

import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.annotation.Configuration;

import com.mindcurv.commercetools.automation.services.ClientService;

import io.sphere.sdk.client.BlockingSphereClient;

@Configuration
public class CommercetoolsProject {

	private final static Logger LOG = LoggerFactory.getLogger(CommercetoolsProject.class);

	private String projectKey;
	private String dumpDirectory;
	private boolean dumpDrafts;
	private Properties connectionProperties;

	private String options = Constants.DEFAULT_OPTION;

	public CommercetoolsProject(final String projectKey) {
		LOG.info("initialized '{}'", projectKey);
		this.projectKey = projectKey;
	}

	public void setCommercertoolsProject(String projectKey) {
		this.projectKey = projectKey;
	}

	public String getProjectKey() {
		return projectKey;
	}

	public BlockingSphereClient getClient() {
		try {
			if (getConnectionProperties() != null) {
				return ClientService.createBlockingSphereClient(getConnectionProperties());
			} else {
				LOG.debug("Creating client for project {}", getProjectKey());
				return ClientService.createBlockingSphereClient(getProjectKey());
			}
		} catch (Exception e) {
			LOG.error("Exception", e);
		}
		return null;
	}

	public String getOptions() {
		return options;
	}

	@Required
	public void setOptions(String options) {
		this.options = options;
	}

	public String getDumpDirectory() {
		return dumpDirectory;
	}

	@Required
	public void setDumpDirectory(String dumpDirectory) {
		this.dumpDirectory = dumpDirectory;
	}

	public boolean isDumpDraftsEnabled() {
		if (dumpDrafts && StringUtils.isNotBlank(getDumpDirectory())) {
			return true;
		}
		return false;
	}

	@Required
	public void setDumpDrafts(boolean dumpDrafts) {
		this.dumpDrafts = dumpDrafts;
	}

	public Properties getConnectionProperties() {
		return connectionProperties;
	}

	public void setConnectionProperties(Properties connectionProperties) {
		this.connectionProperties = connectionProperties;
	}

	public String toString() {
		return new ToStringBuilder(this).append("projectKey", getProjectKey()).append("options", getOptions())
				.append("isDumpDraftsEnabled", isDumpDraftsEnabled()).append("dumpDirectory", getDumpDirectory())
				.toString();
	}

}

package com.mindcurv.commercetools.automation;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public interface Constants {

	public String ATT_PUBLICATIONID = "publicationId";

	public String ARG_CLEANUP = "cleanup";
	public String ARG_ESSENTIAL = "essential";
	public String ARG_SAMPLE = "sample";
	public String DEFAULT_OPTION = ARG_ESSENTIAL + "," + ARG_SAMPLE;

	public static final DateFormat DATEFORMAT = new SimpleDateFormat("yyyy-MM-dd");
	public static final DateFormat DATETIMEFORMAT = new SimpleDateFormat("YYYY-MM-DD'T'hh:mm:ss.sss'Z'");

	public static final String PROPS_PROJECTKEY = "projectKey";

	public static final String PROPS_SHEET_SETUP_OPTIONS = "sheet.setup.options";
	public static final String PROPS_SHEET_SETUP_EXCELFILE = "sheet.setup.excelfile";
	public static final String PROPS_DUMP_DIRECTORY ="dump.targetDirectory";
	public static final String PROPS_DUMP_DRAFTS = "dump.drafts";
			
	public static final String PROPS_SHEET_SETUP_GOOGLEID = "gdrive.fileId";

	public static final String PROPS_PRODUCTIMAGEDIR = "image.products.directory";

	public static final String MSG_SPACER = "######################################################";

}

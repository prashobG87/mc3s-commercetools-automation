package com.mindcurv.commercetools.automation.schedules.export;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;

import com.mindcurv.commercetools.automation.Constants;
import com.mindcurv.commercetools.automation.helper.files.ExportHelper;
import com.mindcurv.commercetools.automation.services.export.OrderImpExService;

import io.sphere.sdk.json.SphereJsonUtils;
import io.sphere.sdk.orders.Delivery;
import io.sphere.sdk.orders.Order;

public class ScheduledOrderExportTask extends BaseExportTask {

	private final static Logger LOG = LoggerFactory.getLogger(ScheduledOrderExportTask.class);

	private OrderImpExService orderExportService;

	public ScheduledOrderExportTask() {
		super();
	}

	@Required
	public void setOrderExportService(OrderImpExService orderExportService) {
		this.orderExportService = orderExportService;
	}

	@Async
	@Scheduled(cron = "0 */15 * * * *") // the top of every 15 min .
	public void export() {
		LOG.info(Constants.MSG_SPACER);
		LOG.info("planned order exports triggered at {}", new Date());
		final List<Map<String, Object>> splitResult = orderExportService.splitOrders();
		LOG.info("Store files for {} orders", splitResult.size());
		for (Map<String, Object> singleResult : splitResult) {
			final Order order = (Order) singleResult.get(OrderImpExService.KEY_ORDER);
			final String orderNumber = StringUtils.isBlank(order.getOrderNumber()) ? order.getId()
					: order.getOrderNumber();
			ExportHelper.storeToFile(getExportDirectory(), orderNumber + "-Order",
					SphereJsonUtils.toPrettyJsonString(order));
			int splitIndex = 0;
			for (Map.Entry<String, Object> entry : singleResult.entrySet()) {
				final String key = entry.getKey();
				if (key.equals(OrderImpExService.KEY_ORDER)) {
					continue; // already exported.
				}
				final Object value = entry.getValue();
				if (value instanceof Delivery) {
					final String prefix = orderNumber + "-Split-" + (splitIndex++) ;
					LOG.info("order export item '{}' {} -> '{}'", key, prefix, value);
					ExportHelper.storeToFile(getExportDirectory(), prefix,
							SphereJsonUtils.toPrettyJsonString(value));
				}
			}

		}
		LOG.debug("Exported {} orders", splitResult.size());
		
		final List<Order> updateOrders = orderExportService.updateOpenOrders(); 
		LOG.info( "orders to be updated:{}", updateOrders.size());
		
		LOG.info(Constants.MSG_SPACER);
	}
}

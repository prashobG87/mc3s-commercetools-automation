package com.mindcurv.commercetools.automation.schedules.e2o;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.mail.Flags.Flag;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;

import com.mindcurv.commercetools.automation.Constants;
import com.mindcurv.commercetools.automation.e2o.E2oConstants;
import com.mindcurv.commercetools.automation.e2o.E2oUtils;
import com.mindcurv.commercetools.automation.integration.email.EmailParserUtils;
import com.mindcurv.commercetools.automation.schedules.export.BaseExportTask;
import com.sun.mail.imap.IMAPFolder;

public class ScheduledE2oImportMailTask extends BaseExportTask {

	private final static Logger LOG = LoggerFactory.getLogger(ScheduledE2oImportMailTask.class);

	private Properties mailProperties;

	@Async
	@Scheduled(cron = "0 0/30 * * * *") // every 30 min
	public void export() {
		LOG.info(Constants.MSG_SPACER);
		LOG.info("planned email scheduler triggered at {}", new Date());
		try {
			Properties props = System.getProperties();
			props.setProperty("mail.imap.ssl.enable", "true");

			Session session = Session.getDefaultInstance(props, null);

			final String host = mailProperties.getProperty("e2o.mail.server");
			final String username = mailProperties.getProperty("e2o.mailboxusername").replaceAll("%40", "@");
			final String password = mailProperties.getProperty("e2o.mailboxpassword");

			LOG.info("try to connect to imap://{}:{}", host, username);

			final Store store = session.getStore("imap");
			store.connect(host, username, password);

			Map<String, IMAPFolder> folderMap = prepareFolders(store);
			final IMAPFolder inboxFolder = folderMap.get(E2oConstants.INBOX);

			// get a list of javamail messages as an array of messages
			Message[] messages = inboxFolder.getMessages();

			LOG.info("found {} unread messages in {}", messages.length, inboxFolder.getName());
			File exportDirectoryFile = getExportDirectoryAsFile();

			for (int i = 0; i < messages.length; i++) {
				final Message message = messages[i];
				try {
					
					message.setFlag(Flag.SEEN, true);
					message.setFlag(Flag.FLAGGED, true);
					
					boolean success = EmailParserUtils.handleMessage(exportDirectoryFile, message);
					
					message.setFlag(Flag.FLAGGED, !success);

					IMAPFolder targetFolder = success ? folderMap.get(E2oConstants.DONE_FOLDER_NAME)
							: folderMap.get(E2oConstants.ERROR_FOLDER_NAME);
					moveEmailToFolder(message, inboxFolder, targetFolder);
				} catch (Exception ex) {
					LOG.error(E2oConstants.E2OMAILBOX_INBOX_MESSAGE_ERROR);
				}
			}
			closeFoldersSafely(folderMap);
			store.close();
			LOG.info("closed connection to imap://{}:{}", host, username);
		} catch (NoSuchProviderException nspe) {
			LOG.error("invalid provider name", nspe);
		} catch (MessagingException me) {
			LOG.error("messaging exception", me);
		}
		LOG.info(Constants.MSG_SPACER);
	}

	private Map<String, IMAPFolder> prepareFolders(Store store) throws MessagingException {
		Map<String, IMAPFolder> folders = new HashMap<>();
		for (String folderName : E2oConstants.FOLDERNAMES) {
			final IMAPFolder folder = (IMAPFolder) store.getFolder(folderName);
			E2oUtils.openFolder(folder, Folder.READ_WRITE);
			folders.put(folderName, folder);
		}
		return folders;
	}

	private void closeFoldersSafely(Map<String, IMAPFolder> folderMap) {
		Map<String, IMAPFolder> folders = new HashMap<>();
		for (Map.Entry<String, IMAPFolder> entry : folders.entrySet()) {
			E2oUtils.closeFolderSafely(entry.getValue());
		}
	}

	public void moveEmailToFolder(final Message message, final IMAPFolder sourceFolder, final IMAPFolder targetFolder) {
		long uniqueId = 0;
		try {
			uniqueId = sourceFolder.getUID(message);
			if (targetFolder != null && targetFolder.exists()) {
				E2oUtils.moveEmailToFolder(message, sourceFolder, targetFolder);
				LOG.info(E2oConstants.MOVE_LOG_MESSAGE, uniqueId, sourceFolder.getName(), targetFolder.getName());
			} else {
				LOG.error(E2oConstants.ERROR_MOVING_LOG, uniqueId, sourceFolder.getName(), targetFolder.getName());
			}
		} catch (MessagingException ex) {
			LOG.error(E2oConstants.ERROR_MOVING_LOG, uniqueId, sourceFolder.getName(), targetFolder.getName(), ex);
		}
	}

	@Required
	public void setMailProperties(Properties mailProperties) {
		this.mailProperties = mailProperties;
	}
}

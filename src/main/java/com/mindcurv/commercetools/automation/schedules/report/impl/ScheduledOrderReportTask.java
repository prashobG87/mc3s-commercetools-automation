package com.mindcurv.commercetools.automation.schedules.report.impl;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;

import com.mindcurv.commercetools.automation.Constants;
import com.mindcurv.commercetools.automation.schedules.report.BaseScheduledReportTask;
import com.mindcurv.commercetools.automation.services.query.QueryDateRange;
import com.mindcurv.commercetools.automation.services.query.impl.OrderQueryService;
import com.mindcurv.commercetools.automation.services.reports.AbstractReportService;

import io.sphere.sdk.orders.Order;

public class ScheduledOrderReportTask extends BaseScheduledReportTask<Order>{

	private final static Logger LOG = LoggerFactory.getLogger(ScheduledOrderReportTask.class);

	public ScheduledOrderReportTask() {
	}

	@Async
	//@Scheduled(cron = "15/30 * * * * *") // every 30 sec
	@Scheduled(cron = "0 0 */1 * * *") // hourly
	public void createDailyOrderReport() {
		LOG.info(Constants.MSG_SPACER);
		LOG.info("planned order reports triggered at {}", new Date());

		final HashMap<String, Object> params = new HashMap<>();
		
		params.put(OrderQueryService.PARAM_DATERANGE, prepareQueryDateRange());
		
		params.put(AbstractReportService.PARAM_REPORTNAME, getReportTemplateName());
		params.put(AbstractReportService.PARAM_REPORTFORMAT, getReportFormat());
		params.put(AbstractReportService.PARAM_REPORTTARGETDIRECTORY, getExportDirectory());
		
		final File reportFile = getReportService().getReport(params);
		LOG.info("generated report {}", reportFile.getAbsolutePath());
		
		LOG.info(Constants.MSG_SPACER);
	}

	private QueryDateRange prepareQueryDateRange() {
		Calendar calendar = new GregorianCalendar();
		final int today = calendar.get(Calendar.DATE);
		calendar.set(Calendar.DATE, today - 1);
		calendar.set(Calendar.HOUR, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		Date startDate = calendar.getTime();

		calendar.set(Calendar.HOUR, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		Date endDate = calendar.getTime();
		return new QueryDateRange(startDate, endDate);
	}
	
}

package com.mindcurv.commercetools.automation.schedules.report;

import org.springframework.beans.factory.annotation.Required;

import com.mindcurv.commercetools.automation.schedules.export.BaseExportTask;
import com.mindcurv.commercetools.automation.services.reports.AbstractReportService;
import com.mindcurv.commercetools.automation.services.reports.ReportFormat;

public class BaseScheduledReportTask<T> extends BaseExportTask{
	
	private ReportFormat reportFormat = ReportFormat.PDF; 
	private String reportTemplateName ; 
	private AbstractReportService<T> reportService; 
	
	public ReportFormat getReportFormat() {
		return reportFormat;
	}

	@Required
	public void setReportFormat(final String reportFormatToBeSet) {
			reportFormat = ReportFormat.HTML.name().equalsIgnoreCase(reportFormatToBeSet) ? ReportFormat.HTML : ReportFormat.PDF; 
	}

	public String getReportTemplateName() {
		return reportTemplateName;
	}

	@Required
	public void setReportTemplateName(String reportTemplateName) {
		this.reportTemplateName = reportTemplateName;
	}

	public AbstractReportService<T> getReportService() {
		return reportService;
	}

	@Required
	public void setReportService(AbstractReportService<T> reportService) {
		this.reportService = reportService;
	}

}


package com.mindcurv.commercetools.automation.schedules.export;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;

import com.mindcurv.commercetools.automation.Constants;
import com.mindcurv.commercetools.automation.helper.VueHelper;
import com.mindcurv.commercetools.automation.pojo.vue.VueCategoryPojo;
import com.mindcurv.commercetools.automation.pojo.vue.VueProductPojo;
import com.mindcurv.commercetools.automation.pojo.vue.VueTaxCategoryPojo;
import com.mindcurv.commercetools.automation.services.query.impl.CategoryQueryService;
import com.mindcurv.commercetools.automation.services.query.impl.ProductQueryService;
import com.mindcurv.commercetools.automation.services.query.impl.ProductTypeQueryService;
import com.mindcurv.commercetools.automation.services.query.impl.TaxCategoryQueryService;

import io.sphere.sdk.categories.Category;
import io.sphere.sdk.products.Product;
import io.sphere.sdk.producttypes.ProductType;
import io.sphere.sdk.taxcategories.TaxCategory;

public class ScheduledVueExportTask extends BaseExportTask {

	private final static Logger LOG = LoggerFactory.getLogger(ScheduledVueExportTask.class);

	private ProductQueryService productQueryService;
	private CategoryQueryService categoryQueryService;
	private ProductTypeQueryService productTypeQueryService;
	private TaxCategoryQueryService taxCategoryQueryService;

	private List<String> productTypeList;

	private Map<String, Category> categoryCacheMap = new HashMap<>();
	private Map<String, ProductType> producttypeCacheMap = new HashMap<>();
	
	public ScheduledVueExportTask() {
		
	}

	@Async
	@Scheduled(cron = "0 */15 * * * *") // every 15m
	public void export() {
		LOG.info(Constants.MSG_SPACER);
		LOG.info("triggered job at {}", new Date());
		LOG.info("dumpSingleFile?{}", isDumpSingleFile() );

		setupCache();

		exportTax();
		exportProducts();
		exportCategories();
		
		LOG.info(Constants.MSG_SPACER);
	}

	private void exportTax() {
		LOG.info("Exporting Tax Categories");
		final List<VueTaxCategoryPojo> pojoList = new ArrayList<>();
		int counter = 0;
		for (TaxCategory taxCategory : getTaxCategoryQueryService().getAllEntries()) {
			VueTaxCategoryPojo pojo = new VueTaxCategoryPojo().withTaxCategory(taxCategory);
			pojo.setPosition(Integer.valueOf(counter++));
			pojoList.add(pojo);
		}
		storeResultList(pojoList);
		LOG.info("Finished Exporting {} Tax Categories", pojoList.size());
	}

	private Map<String, Category> exportProducts() {
		LOG.info("Exporting Products");
		LOG.info("Selected product types {}", productTypeList);
		final List<Product> result = productQueryService.getAllPublishedProductsByProductType(productTypeList);
		LOG.debug("Exported {} products", result.size());

		final List<VueProductPojo> pojoList = new ArrayList<>();
		for (Product product : result) {
			final VueProductPojo vueProductPojo = new VueProductPojo().withProduct(product, categoryCacheMap,
					producttypeCacheMap);
			pojoList.add(vueProductPojo);
			LOG.debug("Vue Product:{}", vueProductPojo.toJsonString());
		}
		boolean success = storeResultList(pojoList);
		LOG.info("Finished Exporting {} Products, success {} ", pojoList.size(), success);
		return VueHelper.extractRelevantCategories(categoryCacheMap, pojoList);
	}

	private void exportCategories() {
		LOG.info("Exporting Categories");
		final List<VueCategoryPojo> pojoList = new ArrayList<>();
		for (Category category : categoryQueryService.getAllEntries()) {
			final VueCategoryPojo pojo = new VueCategoryPojo().withCategory(category);
			pojoList.add(pojo);
			LOG.debug("category pojo {}", pojo.toJsonString());
		}
		boolean success = storeResultList(pojoList);
		LOG.info("Finished Exporting {} Categories, success {}", pojoList.size(), success);
	}

	private void setupCache() {
		categoryCacheMap.clear();
		for (Category category : getCategoryQueryService().getAllEntries()) {
			categoryCacheMap.put(VueHelper.replaceId(category.getId()), category);
		}
		producttypeCacheMap.clear();
		for (ProductType productType : getProductTypeQueryService().getAllEntries()) {
			producttypeCacheMap.put(VueHelper.replaceId(productType.getId()), productType);
		}
	}

	public ProductQueryService getProductQueryService() {
		return productQueryService;
	}

	@Required
	public void setProductQueryService(ProductQueryService productQueryService) {
		this.productQueryService = productQueryService;
	}

	public List<String> getProductTypeList() {
		return productTypeList;
	}

	@Required
	public void setProductTypeList(List<String> productTypeList) {
		this.productTypeList = productTypeList;
	}

	public CategoryQueryService getCategoryQueryService() {
		return categoryQueryService;
	}

	@Required
	public void setCategoryQueryService(CategoryQueryService categoryQueryService) {
		this.categoryQueryService = categoryQueryService;
	}

	public ProductTypeQueryService getProductTypeQueryService() {
		return productTypeQueryService;
	}

	@Required
	public void setProductTypeQueryService(ProductTypeQueryService productTypeQueryService) {
		this.productTypeQueryService = productTypeQueryService;
	}

	public TaxCategoryQueryService getTaxCategoryQueryService() {
		return taxCategoryQueryService;
	}

	@Required
	public void setTaxCategoryQueryService(TaxCategoryQueryService taxCategoryQueryService) {
		this.taxCategoryQueryService = taxCategoryQueryService;
	}

}

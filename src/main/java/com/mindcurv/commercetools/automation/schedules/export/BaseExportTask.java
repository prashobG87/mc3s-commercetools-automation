package com.mindcurv.commercetools.automation.schedules.export;

import java.io.File;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.mindcurv.commercetools.automation.helper.VueHelper;
import com.mindcurv.commercetools.automation.helper.files.ExportHelper;
import com.mindcurv.commercetools.automation.schedules.BaseCommercetoolsTask;

public class BaseExportTask extends BaseCommercetoolsTask {

	private final static Logger LOG = LoggerFactory.getLogger(BaseExportTask.class);

	private String exportDirectory;
	private boolean dumpSingleFile = true; 
	
	public BaseExportTask() {
	}

	protected File getExportDirectoryAsFile() {
		File exportDirectoryFile = new File(getExportDirectory());
		if ( !exportDirectoryFile.exists()) {
			exportDirectoryFile.mkdirs(); 
		}
		return exportDirectoryFile;
	}

	
	public boolean storeFileList(List<File> fileList) {
		boolean success = true; 
		File file = ExportHelper.storeFilesAsZip(exportDirectory, fileList);
		LOG.info("Stored {} - {} to {}", fileList.size(), file.getAbsolutePath());
		if ( !file.exists()) {
			success = false; 
		}
		return success; 
	}
	public boolean storeResultList(List<?> pojoList) {
		boolean success = true; 
		if (pojoList == null || pojoList.isEmpty()) {
			LOG.info("No items to be stored");
			return success;
		}
		String className = null;
		for (Object item : pojoList) {
			if (className == null) {
				className = item.getClass().getSimpleName();
			}
			if ( isDumpSingleFile()) {
				File file = ExportHelper.storeToFile(getExportDirectory(), item);
				LOG.debug("Stored {} - {} to {}", className, item, file.getAbsolutePath());
				if ( !file.exists()) {
					success = false; 
				}
			}
		}
		final String jsonString = VueHelper.listToJsonString(pojoList);
		File file = ExportHelper.storeToFile(getExportDirectory(), className, jsonString);
		if ( !file.exists()) {
			success = false; 
		}
		return success; 
	}

	public String getExportDirectory() {
		return exportDirectory;
	}

	@Required
	public void setExportDirectory(String exportDirectory) {
		this.exportDirectory = exportDirectory;
	}

	public boolean isDumpSingleFile() {
		return dumpSingleFile;
	}

	public void setDumpSingleFile(boolean dumpSingleFile) {
		this.dumpSingleFile = dumpSingleFile;
	}
}

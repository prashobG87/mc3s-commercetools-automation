package com.mindcurv.commercetools.automation.clients;

import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mindcurv.commercetools.automation.Constants;
import com.mindcurv.commercetools.automation.services.ClientService;
import com.mindcurv.commercetools.automation.services.setup.GoogleSheetService;

public class GDriveClient extends AbstractStandAloneClient {

	private final static Logger LOG = LoggerFactory.getLogger(GDriveClient.class);

	private String sheetId;

	public GDriveClient() throws IOException {
		super();
		Properties props = ClientService.getConnectionProperties();
		sheetId = props.getProperty(Constants.PROPS_SHEET_SETUP_GOOGLEID);
	}

	public static void main(String[] args) {

		boolean success = false;
		try {
			success = new GDriveClient().executeSync();
		} catch (Exception e) {
			LOG.error("Exception", e);
		}
		if (success) {
			System.exit(0);
		}
		System.exit(1);

	}

	@Override
	public boolean execute() {
		LOG.info(Constants.MSG_SPACER);
		boolean success = false;
		try {
			final GoogleSheetService googleSheetService = new GoogleSheetService(getCommercetoolsProject());
			googleSheetService.setSheetId(sheetId);
			googleSheetService.setSetupServices(getSetupServices());
			success = googleSheetService.executeSync();
		} catch (Exception e) {
			LOG.error("Exception", e);
		}
		LOG.info(Constants.MSG_SPACER);
		return success;
	}

	public void setSheetId(String sheetId) {
		this.sheetId = sheetId;
	}

}

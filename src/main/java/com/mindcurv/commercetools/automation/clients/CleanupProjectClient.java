package com.mindcurv.commercetools.automation.clients;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Configure sphere client and get project information.
 */
public class CleanupProjectClient extends AbstractStandAloneClient {
	private final static Logger LOG = LoggerFactory.getLogger(CleanupProjectClient.class);

	public CleanupProjectClient() throws IOException {
		super();
	}

	public static void main(String[] args) {
		boolean success = false;
		try {
			if (verifySecurityInput()) {
				success = new CleanupProjectClient().executeSync();
			}
		} catch (Exception e) {
			LOG.error("Exception", e);
		}
		if (success) {
			System.exit(0);
		}
		System.exit(1);
	}

	public static boolean verifySecurityInput() throws IOException {
		final String checkKey = UUID.randomUUID().toString();
		LOG.info("enter {} for verification", checkKey);
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		final String checkKeyVerification = br.readLine();
		
		LOG.info("entered value {} for verification", checkKeyVerification);
		br.close();
		
		if (checkKeyVerification.equals("FORCE")) {
			LOG.info("Forced input");
			return true;
		}
		
		if (checkKey.equals(checkKeyVerification)) {
			LOG.info("verficiation passed");
			return true;
		}
		
		LOG.error("Entered value '{}' differs: '{}'", checkKeyVerification, checkKey);
		return false;
	}

	@Override
	public boolean execute() {
		LOG.info("executing " + CleanupProjectClient.class.getSimpleName() + " for '{}'", getCommercetoolsProject());
		return cleanUp();
	}
}

package com.mindcurv.commercetools.automation.clients.boot;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mindcurv.commercetools.automation.services.ClientService;

public class BootClient {

	private final static Logger LOG = LoggerFactory.getLogger(BootClient.class);

	public static String[] prepareArgs(String[] args) throws IOException {
		final List<String> argList = new ArrayList<>();
		argList.addAll(Arrays.asList(args));
		File propsFile = ClientService.getConnectionPropertiesFile();
		if (propsFile != null) {
			final String propFileArg = "--spring.config.location=" + propsFile.getAbsolutePath();
			argList.add(propFileArg);
		}
		
		LOG.info("prepared {} argument(s)", argList.size());
		String[] newArgs = new String[argList.size()];
		argList.toArray(newArgs);
		if (LOG.isDebugEnabled()) {
			int counter = 0;
			for (String arg : newArgs) {
				LOG.debug("arg[{}] -> {}", counter++, arg);
			}
		}
		return newArgs;
	}

}

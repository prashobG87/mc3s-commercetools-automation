package com.mindcurv.commercetools.automation.clients.boot;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.mindcurv.commercetools.automation.integration.gdrive.GDriveConnection;

@SpringBootApplication
@EnableScheduling
@ComponentScan(basePackages = { "com.mindcurv.commercetools.automation.schedules" })
@ImportResource("application-context.xml")
@Configuration
public class CommercetoolsIntegrationClient extends BootClient {

	private static final String MIMETYPE_XLSX = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
	private final static Logger LOG = LoggerFactory.getLogger(CommercetoolsIntegrationClient.class);

	public static void main(String[] args) {
		try {
			final ConfigurableApplicationContext ctx = new SpringApplication(CommercetoolsIntegrationClient.class)
					.run(prepareArgs(args));
			LOG.info("Context loaded {}, waiting for files to be processed...", ctx);
			
			GDriveConnection gDriveConnection = (GDriveConnection) ctx.getBean("gDriveConnection");
			if ( gDriveConnection != null) {
				CommercetoolsIntegrationClient client = new CommercetoolsIntegrationClient();
				client.downloadInitSheet(gDriveConnection);
			}

		} catch (Exception e) {
			LOG.error("Exception", e);
		}
	}

	public CommercetoolsIntegrationClient() {

	}

	public void downloadInitSheet(GDriveConnection gDriveConnection) {
		Boolean download = gDriveConnection.getGdriveSettings().getDownload(); 
		LOG.info( "Initialize system by downloaded file? {}", download);
		if (download) {
			final String fileId = gDriveConnection.getGdriveSettings().getFileId();
			LOG.info("setting up system by downloading sheet {} from GDrive..." , fileId );
			final String targetDownloadFolderLocation = gDriveConnection.getGdriveSettings().getTargetDownloadFolder();
			final File targetDownloadFolder = new File(targetDownloadFolderLocation); 
			final File file = gDriveConnection.downloadGoogleDocument(fileId, targetDownloadFolder, MIMETYPE_XLSX);
			if ( LOG.isInfoEnabled()) {
				if ( file != null) {
					LOG.info("downloaded to {}", fileId, file.getAbsolutePath());
				} else {
					LOG.error( "could not download google file {}", fileId);
				}
			}
		}
	}

}

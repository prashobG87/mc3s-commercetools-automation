package com.mindcurv.commercetools.automation.clients;

import java.io.File;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;

import com.mindcurv.commercetools.automation.integration.email.EmailParserUtils;

public class TestMailDownload {

	public static void main(String[] args) {
		String host = "e2o-dev-webmail.mindcurv.com";
		String mailStoreType = "imap";
		String username = "dirk.paschke@e2o-dev-backoffice.mindcurv.com";
		String password = "eb3gSWMpA7";

		receiveEmail(host, mailStoreType, username, password);
	}

	private static void receiveEmail(String host, String provider, String username, String password) {
		try {
			Properties props = System.getProperties();
			props.setProperty("mail.imap.ssl.enable", "true");

			Session session = Session.getDefaultInstance(props, null);
			Store store = session.getStore(provider);
			store.connect(host, username, password);

			// open the inbox folder
			Folder inbox = store.getFolder("INBOX");
			inbox.open(Folder.READ_ONLY);
			
			// get a list of javamail messages as an array of messages
			Message[] messages = inbox.getMessages();

			for (int i = 0; i < messages.length; i++) {
				final Message message = messages[i];
				String from = getFrom(message);
				System.out.println("" + i + ":"+ from);
				
				EmailParserUtils.handleMessage(new File("."), message);
				
				
			}

			
			inbox.close(false);
			store.close();
		} catch (NoSuchProviderException nspe) {
			System.err.println("invalid provider name");
		} catch (MessagingException me) {
			System.err.println("messaging exception");
			me.printStackTrace();
		}
	}

	private static String getFrom(Message javaMailMessage) throws MessagingException {
		String from = "";
		Address a[] = javaMailMessage.getFrom();
		if (a == null)
			return null;
		for (int i = 0; i < a.length; i++) {
			Address address = a[i];
			from = from + address.toString();
		}

		return from;
	}

	

}

package com.mindcurv.commercetools.automation.clients;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.Constants;
import com.mindcurv.commercetools.automation.services.ClientService;
import com.mindcurv.commercetools.automation.services.setup.CleanupService;
import com.mindcurv.commercetools.automation.services.setup.SetupService;
import com.mindcurv.commercetools.automation.services.setup.impl.CartSetupService;
import com.mindcurv.commercetools.automation.services.setup.impl.CategorySetupService;
import com.mindcurv.commercetools.automation.services.setup.impl.ChannelSetupService;
import com.mindcurv.commercetools.automation.services.setup.impl.CustomObjectSetupService;
import com.mindcurv.commercetools.automation.services.setup.impl.CustomerGroupSetupService;
import com.mindcurv.commercetools.automation.services.setup.impl.CustomerSetupService;
import com.mindcurv.commercetools.automation.services.setup.impl.InventorySetupService;
import com.mindcurv.commercetools.automation.services.setup.impl.OrderSetupService;
import com.mindcurv.commercetools.automation.services.setup.impl.ProductSetupService;
import com.mindcurv.commercetools.automation.services.setup.impl.ProductTypeSetupService;
import com.mindcurv.commercetools.automation.services.setup.impl.ProjectSetupService;
import com.mindcurv.commercetools.automation.services.setup.impl.ShippingSetupService;
import com.mindcurv.commercetools.automation.services.setup.impl.TaxSetupService;
import com.mindcurv.commercetools.automation.services.setup.impl.TypeSetupService;
import com.mindcurv.commercetools.automation.services.setup.impl.ZoneSetupService;

public abstract class AbstractStandAloneClient {

	private final static Logger LOG = LoggerFactory.getLogger(AbstractStandAloneClient.class);

	private CommercetoolsProject commercetoolsProject;
	
	public abstract boolean execute(); 

	public AbstractStandAloneClient() throws IOException {
		Properties props = ClientService.getConnectionProperties();
		final String projectKey = props.getProperty(Constants.PROPS_PROJECTKEY);
		final String options = props.getProperty(Constants.PROPS_SHEET_SETUP_OPTIONS);
		final String dumpDirectory = props.getProperty(Constants.PROPS_DUMP_DIRECTORY);
		final boolean dumpDrafts = "true".equalsIgnoreCase(props.getProperty(Constants.PROPS_DUMP_DRAFTS));
		
		commercetoolsProject = new CommercetoolsProject(projectKey);
		commercetoolsProject.setOptions(options);
		commercetoolsProject.setDumpDirectory(dumpDirectory);
		commercetoolsProject.setDumpDrafts(dumpDrafts);
		
		LOG.info("CommercetoolsProject {}", commercetoolsProject);

	}

	public CommercetoolsProject getCommercetoolsProject() {
		return commercetoolsProject;
	}

	public boolean executeSync() {
		if (StringUtils.contains(getCommercetoolsProject().getOptions(), Constants.ARG_CLEANUP)) {
			boolean cleanupSuccess = cleanUp();
			LOG.info("Project is cleaned up? {}", cleanupSuccess);
		}
		execute(); 
		return true; 
	}
	protected boolean cleanUp() {

		final List<SetupService> setupServices = new ArrayList<>();
		setupServices.add(new InventorySetupService(commercetoolsProject));
		setupServices.add(new ProductSetupService(commercetoolsProject));
		setupServices.add(new CategorySetupService(commercetoolsProject));
		setupServices.add(new CartSetupService(commercetoolsProject));
		setupServices.add(new OrderSetupService(commercetoolsProject));
		setupServices.add(new CustomerSetupService(commercetoolsProject));
		setupServices.add(new CustomerGroupSetupService(commercetoolsProject));
		setupServices.add(new ChannelSetupService(commercetoolsProject));
		setupServices.add(new ProjectSetupService(commercetoolsProject));
		setupServices.add(new ProductTypeSetupService(commercetoolsProject));
		setupServices.add(new TypeSetupService(commercetoolsProject));
		setupServices.add(new ShippingSetupService(commercetoolsProject));
		setupServices.add(new TaxSetupService(commercetoolsProject));
		setupServices.add(new CustomObjectSetupService(commercetoolsProject));

		final CleanupService service = new CleanupService(commercetoolsProject);
		service.setSetupServices(setupServices);
		return service.executeCleanup();

	}

	protected List<SetupService> getSetupServices() {
		final List<SetupService> setupServices = new ArrayList<>();
		setupServices.add(new ProjectSetupService(commercetoolsProject));
		setupServices.add(new TaxSetupService(commercetoolsProject));
		setupServices.add(new ZoneSetupService(commercetoolsProject));
		setupServices.add(new ShippingSetupService(commercetoolsProject));
		setupServices.add(new TypeSetupService(commercetoolsProject));
		setupServices.add(new CustomObjectSetupService(commercetoolsProject));
		setupServices.add(new CategorySetupService(commercetoolsProject));
		setupServices.add(new ChannelSetupService(commercetoolsProject));
		setupServices.add(new CustomerGroupSetupService(commercetoolsProject));
		setupServices.add(new CustomerSetupService(commercetoolsProject));
		setupServices.add(new ProductSetupService(commercetoolsProject));
		setupServices.add(new InventorySetupService(commercetoolsProject));
		setupServices.add(new CartSetupService(commercetoolsProject));
		return setupServices;
	}
}

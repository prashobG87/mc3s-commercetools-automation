package com.mindcurv.commercetools.automation.clients;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mindcurv.commercetools.automation.Constants;
import com.mindcurv.commercetools.automation.services.ClientService;
import com.mindcurv.commercetools.automation.services.setup.ExcelService;

public class ExcelClient extends AbstractStandAloneClient {

	private final static Logger LOG = LoggerFactory.getLogger(ExcelClient.class);

	private File excelFile;

	public ExcelClient() throws IOException {
		super();
		Properties props = ClientService.getConnectionProperties();
		excelFile = new File(props.getProperty(Constants.PROPS_SHEET_SETUP_EXCELFILE));
		if (!excelFile.exists()) {
			throw new IOException("File '" + excelFile.getAbsolutePath() + "' doesn't exist");
		}

	}

	public static void main(String[] args) {
		boolean success = false;
		try {
			success = new ExcelClient().executeSync();
		} catch (Exception e) {
			LOG.error("Exception", e);
		}
		if (success) {
			System.exit(0);
		}
		System.exit(1);
	}

	@Override
	public boolean execute() {
		boolean success = false; 
		try {
			LOG.info(Constants.MSG_SPACER);
			final ExcelService service = new ExcelService(getCommercetoolsProject());
			service.setExcelFile(excelFile);
			service.setSetupServices(getSetupServices());
			success = service.executeSync();

		} catch (Exception e) {
			LOG.error("Exception", e);
		}
		return success;
	}

}

package com.mindcurv.commercetools.automation.integration.gdrive;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;

public class GDriveConnection {

	private final static Logger LOG = LoggerFactory.getLogger(GDriveConnection.class);

	GDriveSettings gdriveSettings;

	public GDriveConnection() {

	}

	public Drive getDriveService() {
		try {
			final Credential credential = authorizeM2M();

			Drive service = new Drive.Builder(GoogleNetHttpTransport.newTrustedTransport(),
					JacksonFactory.getDefaultInstance(), credential)
							.setApplicationName(gdriveSettings.getApplicationName()).build();

			return service;
		} catch (FileNotFoundException e) {
			LOG.error("FileNotFoundException during downloadGoogleDocument", e);
		} catch (GeneralSecurityException e) {
			LOG.error("GeneralSecurityException during downloadGoogleDocument", e);
		} catch (IOException e) {
			LOG.error("IOException during downloadGoogleDocument", e);
		} catch (Exception e) {
			LOG.error("Exception during downloadGoogleDocument", e);
		}
		return null;
	}

	public File downloadGoogleDocument(final String fileId, final File targetFolder, final String mimeType) {
		try {
			File tempFile = new File(targetFolder, fileId + "-" + System.currentTimeMillis());
			FileOutputStream outputStream = new FileOutputStream(tempFile);
			getDriveService().files().export(fileId, mimeType).executeMediaAndDownloadTo(outputStream);
			outputStream.close();
			final File targetFile = new File(tempFile.getParentFile(), "setup-" + tempFile.getName() + ".xlsx");
			FileUtils.moveFile(tempFile, targetFile);
			return targetFile;
		} catch (FileNotFoundException e) {
			LOG.error("FileNotFoundException during downloadGoogleDocument", e);
		} catch (IOException e) {
			LOG.error("IOException during downloadGoogleDocument", e);
		} catch (Exception e) {
			LOG.error("Exception during downloadGoogleDocument", e);
		}
		return null;
	}

	private Credential authorizeM2M() throws Exception {

		final File p12File = new File(gdriveSettings.getP12FileLocation());
		final Set<String> scopes = Collections.singleton(DriveScopes.DRIVE);

		GoogleCredential credential = new GoogleCredential.Builder()
				.setTransport(GoogleNetHttpTransport.newTrustedTransport())
				.setJsonFactory(JacksonFactory.getDefaultInstance())
				.setServiceAccountId(gdriveSettings.getServiceAccountId())
				.setServiceAccountPrivateKeyFromP12File(p12File).setServiceAccountScopes(scopes)
				.build();

		if (!credential.refreshToken()) {
			throw new RuntimeException("Failed OAuth to refresh the token");
		}

		return credential;
	}

	public GDriveSettings getGdriveSettings() {
		return gdriveSettings;
	}

	@Required
	public void setGdriveSettings(GDriveSettings gdriveSettings) {
		this.gdriveSettings = gdriveSettings;
	}
	
	

}

package com.mindcurv.commercetools.automation.integration.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.Message;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.Constants;
import com.mindcurv.commercetools.automation.integration.DraftHandler;
import com.mindcurv.commercetools.automation.pojo.types.product.TypeAttribute;
import com.mindcurv.commercetools.automation.services.setup.SetupService;
import com.mindcurv.commercetools.automation.services.setup.impl.TypeSetupService;

@Configuration
public class ExcelSetupHandler extends DraftHandler {
	
	private final static Logger LOG = LoggerFactory.getLogger(ExcelSetupHandler.class);
	
	private List<SetupService> setupServices ; 

	public ExcelSetupHandler(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	public File handleFile(Message<?> message) {
		LOG.info("Processing Message {}", message);
		
		LOG.info( "SetupServices loaded: {}", setupServices.size());

		final File input = (File) message.getPayload();
		boolean success = true;
		try {
			LOG.info("processing setup excel file {}", input.getName());

			final FileInputStream excelFile = new FileInputStream(input);
			final Workbook workbook = new XSSFWorkbook(excelFile);

			final Date startDate = new Date();
			Map<String, List<TypeAttribute>> attributesMap = Collections.emptyMap(); 
			
			for ( SetupService setupService : setupServices) {
				LOG.info(Constants.MSG_SPACER);
				if ( setupService instanceof TypeSetupService) {
					 attributesMap = ((TypeSetupService) setupService).setup(workbook);
				}
				setupService.setup(workbook, attributesMap);
			}
			
			final Date endDate = new Date();
			LOG.info("Setup time: {}s", (endDate.getTime() - startDate.getTime()) / 1000);
			LOG.info(Constants.MSG_SPACER);

			workbook.close();

		} catch (FileNotFoundException e) {
			LOG.error("FileNotFoundException for file " + input.getName(), e);
		} catch (IOException e) {
			LOG.error("IOException for file " + input.getName(), e);
		} catch (Exception e) {
			LOG.error("Exception for file " + input.getName(), e);
		}
		if (success) {
			LOG.info("processed {}", input.getAbsolutePath());
		} else {
			LOG.error("could not process" + input.getAbsolutePath());
		}
		return input;

	}

	public List<SetupService> getSetupServices() {
		return setupServices;
	}
	@Required
	public void setSetupServices(List<SetupService> setupServices) {
		this.setupServices = setupServices;
	}
}

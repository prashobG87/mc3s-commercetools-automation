package com.mindcurv.commercetools.automation.integration.file;

import java.io.File;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.integration.DraftHandler;

public class MediaFileHandler extends DraftHandler {

	private final static Logger LOG = LoggerFactory.getLogger(MediaFileHandler.class);

	public MediaFileHandler(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}
	
	public File handleFile(Message<?> message) {
		LOG.info("Processing Message {}", message);
		final File input = (File) message.getPayload();
		boolean success = true;
		try {
			LOG.info( "processsing media file {}", input.getName());
			processTypeRelatedMessages("media", Arrays.asList(input.getAbsolutePath())); 
		} catch (Exception e) {
			LOG.error("Exception for file " + input.getName(), e);
		}
		if (success) {
			LOG.info("processed {}", input.getAbsolutePath());
		} else {
			LOG.error("could not process" + input.getAbsolutePath());
		}
		return input;
	}

	
}

package com.mindcurv.commercetools.automation.integration.file;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.file.DefaultFileNameGenerator;
import org.springframework.messaging.Message;

import com.mindcurv.commercetools.automation.Constants;

public class ProcessedFileNameGenerator extends DefaultFileNameGenerator {
	private final static Logger LOG = LoggerFactory.getLogger(ProcessedFileNameGenerator.class);

	@Override
	public String generateFileName(Message<?> message) {
		String filename = super.generateFileName(message);
		if ( LOG.isDebugEnabled()) {
			LOG.debug(Constants.MSG_SPACER);
			for (Map.Entry<String, Object> header : message.getHeaders().entrySet()) {
				LOG.debug("{} -> {}", header.getKey(), header.getValue());
			}
			LOG.debug("payload: {}", message.getPayload());
			LOG.debug(Constants.MSG_SPACER);
		}
		filename += ".processed"; 
		filename += message.getHeaders().getTimestamp();
		LOG.info("generated file name {}", filename);
		return filename;
	}
}

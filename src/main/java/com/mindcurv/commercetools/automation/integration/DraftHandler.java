package com.mindcurv.commercetools.automation.integration;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.services.load.AbstractLoadService;

public class DraftHandler {

	private final static Logger LOG = LoggerFactory.getLogger(DraftHandler.class);

	private Map<String, AbstractLoadService<?>> typeConfigurationMap;

	private CommercetoolsProject commercetoolsProjectBean;
	
	public DraftHandler(CommercetoolsProject commercetoolsProject) {
		this.commercetoolsProjectBean = commercetoolsProject; 
	}

	protected SyncStatistics processTypeRelatedMessages(String messageType, final List<String> messages) {
		final AbstractLoadService<?> service = getTypeConfigurationMap().get(messageType);
		if (service == null) {
			LOG.error("unsupported message type {}", messageType);
			if ( LOG.isInfoEnabled()) {
				for ( Map.Entry<String, AbstractLoadService<?>> entry : getTypeConfigurationMap().entrySet() ) {
					LOG.info( "supported: '{}' -> '{}'", entry.getKey(), entry.getValue());
				}
			}
			return null;
		}
		if (service.getCommercetoolsProject() == null) {
			service.setCommercetoolsProject(commercetoolsProjectBean);
		}
		final SyncStatistics syncStatistics = service.syncStrings(messages);
		LOG.info("processing messageType {}: {}", messageType, syncStatistics.toJsonString());
		return syncStatistics;

	}

	public Map<String, AbstractLoadService<?>> getTypeConfigurationMap() {
		return typeConfigurationMap;
	}

	public CommercetoolsProject getCommercetoolsProjectBean() {
		return commercetoolsProjectBean;
	}

	@Required
	public Map<String, AbstractLoadService<?>> setTypeConfigurationMap(Map<String, AbstractLoadService<?>> typeConfigurationMap) {
		this.typeConfigurationMap = typeConfigurationMap;
		return this.typeConfigurationMap; 
	}

}


package com.mindcurv.commercetools.automation.integration.talend;

import java.io.File;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.beans.factory.annotation.Value;

import routines.system.api.TalendJob;

public class TalendJobHandler {
	private final static Logger LOG = LoggerFactory.getLogger(TalendJobHandler.class);

	@Value("${hotfolder.basedirectory}")
	private String hotfolderBasedirectory;
	
	@Value("${hotfolder.draftfolder}")
	private String draftfolder;
	
	private Map<String, TalendJob> talendConfigurationMap; 

	public File handleFile(File input) {

		LOG.info("file: {}", input.getAbsolutePath());
		final String targetFolder = hotfolderBasedirectory + draftfolder; 
		
		final String idocType = extractJobfromFileName(input); 
		
		final String key = "talend" + idocType + "Process";
		final TalendJob job = talendConfigurationMap.get(key);
		LOG.info( "IDOC {} -> job: {}", idocType, job);
		if ( job == null) {
			LOG.error( "no TalendJob for {}({}) configured", idocType, key);
		} else {
			final String[] args = {
				"--context_param idocfile=" + input.getAbsolutePath(), 
				"--context_param baseFolder=" + input.getParentFile().getAbsolutePath(), 
				"--context_param hotfolder=" + targetFolder, 
			};
			
			executeJob(job, args);
		}
		
		return input;
	}

	public void executeJob(final TalendJob job, String[] args) {
		
		final String[][] results = job.runJob(args);
		boolean success = false; 
		if (LOG.isInfoEnabled()) {
			for (String[] result : results) {
				for (String resultLine : result) {
					LOG.info("Talend transformation Result: {} ", resultLine);
					success =  "0".equals(resultLine); 
				}
			}
		}
		LOG.info( "Talend job {} finished. Success?{}", job, success);
	}
	
	

	private String extractJobfromFileName(File input) {
		if ( input.getName().toLowerCase().contains( "clsmas")) {
			return "CLSMAS";
		}
		return null; 
	}

	@Required
	public void setTalendConfigurationMap(Map<String, TalendJob> talendConfigurationMap) {
		this.talendConfigurationMap = talendConfigurationMap;
	}
	
	
}

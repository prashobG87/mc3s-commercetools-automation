package com.mindcurv.commercetools.automation.integration.file;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.integration.DraftHandler;

public class DraftJsonHandler extends DraftHandler {

	private final static Logger LOG = LoggerFactory.getLogger(DraftJsonHandler.class);

	public DraftJsonHandler(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}
	
	public File handleFile(Message<?> message) {
		LOG.info("Processing Message {}", message);
		final File input = (File) message.getPayload();
		boolean success = false;
		try {
			final String fileContent = FileUtils.readFileToString(input, StandardCharsets.UTF_8);
			final String messageType = extractMessageTypeFromFilename(input); 
			success = loadDraft(messageType, fileContent);
		} catch (Exception e) {
			LOG.error("Exception for file " + input.getName(), e);
		}
		if (success) {
			LOG.info("processed {}", input.getAbsolutePath());
		} else {
			LOG.error("could not process" + input.getAbsolutePath());
		}
		return input;
	}
	public boolean loadDraft(String messageType, String input) {
		if (getCommercetoolsProjectBean() == null) {
			LOG.error("No CommercetoolsProject available");
			return false; 
		} 
		final List<String> messages = Arrays.asList(input); 
		processTypeRelatedMessages(messageType, messages);
		return true;
	}
	private String extractMessageTypeFromFilename(File input) {
		final String fileName = input.getName();
		return fileName.substring(0, fileName.indexOf("-")); 
	}
	
}

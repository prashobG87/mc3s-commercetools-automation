package com.mindcurv.commercetools.automation.integration.email;

import java.io.File;

import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;

public class EmailHandler implements MessageHandler {

	private final static Logger LOG = LoggerFactory.getLogger(EmailHandler.class);

	@Override
	public void handleMessage(Message<?> message) throws MessagingException {
		LOG.info( "processing message {}", message);
		MimeMessage mailMessage = (MimeMessage) message.getPayload(); 
		LOG.info( "mailMessage {}", mailMessage);
		EmailParserUtils.download(new File("."), mailMessage);
		//BUG in spring integration mail 
		//https://stackoverflow.com/questions/44640449/spring-integration-imap-folder-closed-exception
		//https://jira.spring.io/browse/INT-4299
	}	

}

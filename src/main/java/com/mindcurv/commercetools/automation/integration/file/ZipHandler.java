
package com.mindcurv.commercetools.automation.integration.file;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.helper.MediaHelper;
import com.mindcurv.commercetools.automation.integration.DraftHandler;

public class ZipHandler extends DraftHandler {

	private final static Logger LOG = LoggerFactory.getLogger(ZipHandler.class);

	public ZipHandler(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	public File handleFile(Message<?> message) {
		LOG.info("Processing Message {}", message);
		final File input = (File) message.getPayload();
		boolean success = true;
		try {
			ZipFile zipFile = new ZipFile(input);
			Enumeration<? extends ZipEntry> entries = zipFile.entries();

			while (entries.hasMoreElements()) {
				ZipEntry entry = entries.nextElement();

				if (!entry.isDirectory() && MediaHelper.isSupportedMedia(entry.getName())) {
					LOG.info("supported file {}", entry.getName());

					try {
						extractFileToHotFolder(zipFile, entry, input.getParentFile());
					} catch (IOException e) {
						LOG.error("Error during unzipping {} in file {}", entry.getName(), input.getName());
						success = false;
					}

				} else {
					LOG.debug("unsupported file {}", entry.getName());
				}
			}
			zipFile.close();
		} catch (Exception e) {
			LOG.error("Exception for file " + input.getName(), e);
			success = false;
		}
		if (success) {
			LOG.info("processed {}", input.getAbsolutePath());
		} else {
			LOG.error("could not process" + input.getAbsolutePath());
		}
		return input;
	}

	private void extractFileToHotFolder(ZipFile zipFile, ZipEntry entry, File unzipDir) throws IOException {

		final File tmpFile = new File(unzipDir , entry.getName() + ".tmp");
		final File destFile = new File(unzipDir , entry.getName() );

		try (InputStream inputStream = zipFile.getInputStream(entry);
				FileOutputStream outputStream = new FileOutputStream(tmpFile);) {
			int data = inputStream.read();
			while (data != -1) {
				outputStream.write(data);
				data = inputStream.read();
			}
		}
		LOG.debug("rename {} to {}", tmpFile.getName(), destFile.getName());
		tmpFile.renameTo(destFile); 
	}
}

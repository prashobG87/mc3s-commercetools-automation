package com.mindcurv.commercetools.automation.integration;

import com.commercetools.sync.commons.helpers.BaseSyncStatistics;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "processed", "failed", "created", "updated" })

public class SyncStatistics {
	private int failed = 0;
	private int created = 0;
	private int processed = 0;
	private int updated = 0;

	public SyncStatistics() {

	}

	public SyncStatistics setValues(BaseSyncStatistics baseSyncStatistics) {
		failed = baseSyncStatistics.getFailed().get();
		created = baseSyncStatistics.getCreated().get();
		processed = baseSyncStatistics.getProcessed().get();
		updated = baseSyncStatistics.getUpdated().get();
		return this;
	}

	public void reset() {
		failed = 0;
		created = 0;
		processed = 0;
		updated = 0;
	}

	@JsonProperty("created")
	public int getProcessed() {
		return processed;
	}

	@JsonProperty("failed")
	public int getFailed() {
		return failed;
	}

	@JsonProperty("created")
	public int getCreated() {
		return created;
	}

	@JsonProperty("updated")
	public int getUpdated() {
		return updated;
	}

	public void incrementFailed() {
		failed++;
	}

	public void incrementCreated() {
		created++;
	}

	public void incrementProcessed() {
		processed++;
	}

	public void incrementUpdated() {
		updated++;
	}

	public String toString() {
		StringBuffer stringBuffer = new StringBuffer("Sync Result: ");
		stringBuffer.append("processed = ").append(processed);
		stringBuffer.append(" (created = ").append(created);
		stringBuffer.append(", updated = ").append(updated);
		stringBuffer.append(", failed = ").append(failed).append(")");
		return stringBuffer.toString();
	}
	public String toJsonString() {
		final Gson gsonBuilder = new GsonBuilder().create();
		return gsonBuilder.toJson(this);
	}
}

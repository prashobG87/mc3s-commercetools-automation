package com.mindcurv.commercetools.automation.integration.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.integration.DraftHandler;

public class DraftDbHandler extends DraftHandler {

	private final static Logger LOG = LoggerFactory.getLogger(DraftDbHandler.class);
	
	public DraftDbHandler(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}
	
	public void handleJdbcMessage(List<Map<Object, Object>> message) {
		final Map<String, List<String>> draftMap = new HashMap<>();
		for (Map<Object, Object> map : message) {
			final String id = (String) map.get("id");
			final String type = (String) map.get("type");
			final String draftdocument = (String) map.get("draftdocument");
			LOG.info("{}({}) -> payload: {}", id, type, draftdocument.length());

			final List<String> draftList = getDraftListForType(draftMap, type);
			draftList.add(draftdocument);
		}
		loadDraftsFromMap(draftMap);
	}

	private boolean loadDraftsFromMap(Map<String, List<String>> draftMap) {
		for (Map.Entry<String, List<String>> entry : draftMap.entrySet()) {
			final String messageType = entry.getKey();
			final List<String> messages = entry.getValue();
			processTypeRelatedMessages(messageType, messages);
		}
		return true;

	}

	private List<String> getDraftListForType(final Map<String, List<String>> draftMap, final String type) {
		List<String> draftList = draftMap.get(type);
		if (draftList == null) {
			draftList = new ArrayList<>();
			draftMap.put(type, draftList);
		}
		return draftList;
	}

}

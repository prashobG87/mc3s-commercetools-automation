package com.mindcurv.commercetools.automation.integration.gdrive;

import org.springframework.beans.factory.annotation.Required;

public class GDriveSettings {

	public static final String DEFAULT_APPLICATION_NAME = "GDrive Client for commercetools";
	public static final String DEFAULT_GDRIVE_OAUTH_SECRETS_PATH = "secrets/google-sheets-client-secret.json";

	private String fileId = "undefined";
	private String serviceAccountId;
	private String p12FileLocation;
	private String oauthSecretsLocation = DEFAULT_GDRIVE_OAUTH_SECRETS_PATH;
	private String applicationName = DEFAULT_APPLICATION_NAME;
	
	private Boolean download = Boolean.valueOf(false);
	private String targetDownloadFolder; 
	
	public String getFileId() {
		return fileId;
	}
	@Required
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	public String getServiceAccountId() {
		return serviceAccountId;
	}
	@Required
	public void setServiceAccountId(String serviceAccountId) {
		this.serviceAccountId = serviceAccountId;
	}
	public String getP12FileLocation() {
		return p12FileLocation;
	}
	@Required
	public void setP12FileLocation(String p12FileLocation) {
		this.p12FileLocation = p12FileLocation;
	}
	public String getOauthSecretsLocation() {
		return oauthSecretsLocation;
	}

	public void setOauthSecretsLocation(String oauthSecretsLocation) {
		this.oauthSecretsLocation = oauthSecretsLocation;
	}
	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	public String getTargetDownloadFolder() {
		return targetDownloadFolder;
	}
	public void setTargetDownloadFolder(String targetDownloadFolder) {
		this.targetDownloadFolder = targetDownloadFolder;
	}
	public Boolean getDownload() {
		return download;
	}
	
	public void setDownload(Boolean download) {
		this.download = download;
	}
	
}

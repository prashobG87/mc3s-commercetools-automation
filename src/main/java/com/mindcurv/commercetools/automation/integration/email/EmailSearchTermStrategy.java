package com.mindcurv.commercetools.automation.integration.email;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.search.FlagTerm;
import javax.mail.search.NotTerm;
import javax.mail.search.SearchTerm;

import org.springframework.integration.mail.SearchTermStrategy;

public class EmailSearchTermStrategy implements SearchTermStrategy {

	@Override
	public SearchTerm generateSearchTerm(Flags supportedFlags, Folder folder) {
		SearchTerm searchTerm = null;
		if (supportedFlags != null) {
			if (supportedFlags.contains(Flags.Flag.SEEN)) {
				NotTerm notSeen = new NotTerm(new FlagTerm(new Flags(Flags.Flag.SEEN), true));
				if (searchTerm == null) {
					searchTerm = notSeen;
				}
			}
		}
		return searchTerm; 
	}
	
	

}

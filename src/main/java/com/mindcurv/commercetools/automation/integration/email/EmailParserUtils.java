package com.mindcurv.commercetools.automation.integration.email;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.BodyPart;
import javax.mail.Header;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.ContentType;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.ParseException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.mail.support.MailUtils;
import org.springframework.util.Assert;

import com.mindcurv.commercetools.automation.e2o.E2oUtils;

public final class EmailParserUtils {

	public final static Logger LOG = LoggerFactory.getLogger(EmailParserUtils.class);

	private EmailParserUtils() {
		throw new AssertionError();
	}

	public static void download(final File directory, final Message mailMessage) {

		Assert.notNull(mailMessage, "The mail message to be parsed must not be null.");
		final Object content;
		try {
			content = mailMessage.getContent();
		} catch (IOException e) {
			throw new IllegalStateException("Error while retrieving the email contents.", e);
		} catch (MessagingException e) {
			throw new IllegalStateException("Error while retrieving the email contents.", e);
		}
		final File directoryToUse = E2oUtils.prepareDownloadDirectory(directory, mailMessage);

		if (content instanceof String) {
			String contentString = (String) content;
			handleString(directoryToUse, contentString, mailMessage);
		} else if (content instanceof Multipart) {
			Multipart multipart = (Multipart) content;
			handleMultipart(directoryToUse, multipart, mailMessage);
		} else {
			throw new IllegalStateException("This content type is not handled - " + content.getClass().getSimpleName());
		}
	}

	/**
	 * Parses a mail message. The respective message can either be the root
	 * message or another message that is attached to another message.
	 *
	 * If the mail message is an instance of {@link String}, then a
	 * {@link EmailFragment} is being created using the email message's subject
	 * line as the file name, which will contain the mail message's content.
	 *
	 * If the mail message is an instance of {@link Multipart} then we delegate
	 * to {@link #handleMultipart(File, Multipart, javax.mail.Message, List)}.
	 *
	 * @param directory
	 *            The directory for storing the message. If null this is the
	 *            root message.
	 * @param mailMessage
	 *            The mail message to be parsed. Must not be null.
	 * @param emailFragments
	 *            Must not be null.
	 */
	public static boolean handleMessage(final File directory, final javax.mail.Message mailMessage) {

		Assert.notNull(mailMessage, "The mail message to be parsed must not be null.");

		final Object content;

		try {
			content = mailMessage.getContent();
		} catch (IOException e) {
			LOG.error("Error while retrieving the email contents.", e);
			return false;
		} catch (MessagingException e) {
			LOG.error("Error while retrieving the email contents.", e);
			return false;
		}

		final String emailIdFromMessage = getEmailIdFromMessage(mailMessage);
		LOG.info("Processing mail {}", emailIdFromMessage);

		final File intermediateDirectory = E2oUtils.prepareDownloadDirectory(directory, mailMessage);

		final List<File> createdFiles = new ArrayList<>();
		
		try {
			final File propsFile = new File(intermediateDirectory, emailIdFromMessage + "properties"); 
			for (Map.Entry<String, Object> entry : EmailParserUtils.processHeaders(mailMessage).entrySet()) {
				final String line = entry.getKey() + "=" + entry.getValue() + "\n";
				FileUtils.write(propsFile, line, true);
			}
			createdFiles.add(propsFile); 
		} catch (IOException e1) {
			LOG.error("IOException during mail properties file");
		}
		
		if (content instanceof String) {
			String contentString = (String) content;
			createdFiles.add(handleString(intermediateDirectory, contentString, mailMessage));
		} else if (content instanceof Multipart) {
			Multipart multipart = (Multipart) content;
			createdFiles.addAll(handleMultipart(intermediateDirectory, multipart, mailMessage));
		} else {
			LOG.error("This content type is not handled - " + content.getClass().getSimpleName());
			return false;
		}

		try {
			LOG.info("Created {} file(s) from mail {}", createdFiles.size(), emailIdFromMessage);
			if (!createdFiles.isEmpty()) {
				File srcFile = E2oUtils.storeFilesToZip(createdFiles);
				final File destFile = new File(directory, emailIdFromMessage + ".zip");
				if ( destFile.exists()) {
					destFile.delete(); 
				}
				FileUtils.moveFile(srcFile, destFile);
				LOG.info( "Created zip file {}" , destFile.getAbsolutePath());
				FileUtils.forceDelete(intermediateDirectory);
			}
		} catch (FileNotFoundException e) {
			LOG.error( "FileNotFoundException", e);
		} catch (IOException e) {
			LOG.error( "IOException", e);
		}
		
		return true;
	}

	public static File handleString(File directory, String contentString, Message mailMessage) {
		try {
			final String emailIdFromMessage = getEmailIdFromMessage(mailMessage);
			final String fileName = emailIdFromMessage + ".e2o";
			File targetDirectory = E2oUtils.prepareDownloadDirectory(directory, mailMessage);
			File tempFile = new File(targetDirectory, fileName);
			FileUtils.writeStringToFile(tempFile, contentString);
			LOG.info("Stored String from email {} to {}", emailIdFromMessage, tempFile.getAbsolutePath());
			return tempFile;
		} catch (IOException e) {
			LOG.error("IOException", e);
		}
		return null;
	}

	public static List<File> handleMultipart(File directory, Multipart multipart, Message mailMessage) {

		Assert.notNull(directory, "The directory must not be null.");
		Assert.notNull(multipart, "The multipart object to be parsed must not be null.");
		Assert.notNull(mailMessage, "The mail message to be parsed must not be null.");
		final List<File> files = new ArrayList<>();
		final int count;

		try {
			count = multipart.getCount();

			LOG.info("Number of enclosed BodyPart objects: {}.", count);

			for (int i = 0; i < count; i++) {

				final BodyPart bodypart = multipart.getBodyPart(i);

				final String contentType = bodypart.getContentType();
				final String disposition = bodypart.getDisposition();
				final String subject = mailMessage.getSubject();
				final String filename = getFilename(bodypart);

				if (Part.ATTACHMENT.equalsIgnoreCase(disposition)) {
					LOG.info("{} - Store {} '{}' to '{}'", i, disposition, filename, directory.getAbsolutePath());
					LOG.info("Content Type: '{}', subject: '{}'", contentType, subject);
					InputStream is = bodypart.getInputStream();
					File tempFile = new File(directory, bodypart.getFileName());
					FileOutputStream fileOutputStream = new FileOutputStream(tempFile);
					byte[] buf = new byte[4096];
					int bytesRead;
					while ((bytesRead = is.read(buf)) != -1) {
						fileOutputStream.write(buf, 0, bytesRead);
					}
					fileOutputStream.close();
					files.add(tempFile);

				} else {

					final ContentType ct;
					try {
						ct = new ContentType(contentType);
						final String textFilename;
						if ("text/plain".equalsIgnoreCase(ct.getBaseType())) {
							textFilename = "message.txt";
						} else if ("text/html".equalsIgnoreCase(ct.getBaseType())) {
							textFilename = "message.html";
						} else {
							textFilename = "message.other";
						}
						LOG.info("textFilename {}", textFilename);
					} catch (ParseException e) {
						LOG.error("Error while parsing content {}", e);

					}
					// else {
					// final Object content = getContentFromBodyPart(bodypart);
					// if ( content == null) {
					// continue;
					// }
					//
					// if (content instanceof String) {
					//
					// if (Part.ATTACHMENT.equalsIgnoreCase(disposition)) {
					// emailFragments.add(new EmailFragment(directory, i + "-" +
					// filename, content));
					// LOG.info(String.format("Handling attachment '%s', type:
					// '%s'", filename, contentType));
					// } else {
					//
					// final String textFilename;
					// final ContentType ct;
					//
					// try {
					// ct = new ContentType(contentType);
					// } catch (ParseException e) {
					// throw new IllegalStateException("Error while parsing
					// content
					// type '" + contentType + "'.",
					// e);
					// }
					//
					// if ("text/plain".equalsIgnoreCase(ct.getBaseType())) {
					// textFilename = "message.txt";
					// } else if
					// ("text/html".equalsIgnoreCase(ct.getBaseType())) {
					// textFilename = "message.html";
					// } else {
					// textFilename = "message.other";
					// }
					//
					// emailFragments.add(new EmailFragment(directory,
					// textFilename,
					// content));
					// }
					//
					// } else if (content instanceof InputStream) {
					//
					// final InputStream inputStream = (InputStream) content;
					// final ByteArrayOutputStream bis = new
					// ByteArrayOutputStream();
					//
					// try {
					// IOUtils.copy(inputStream, bis);
					// } catch (IOException e) {
					// throw new IllegalStateException(
					// "Error while copying input stream to the
					// ByteArrayOutputStream.", e);
					// }
					//
					// emailFragments.add(new EmailFragment(directory, filename,
					// bis.toByteArray()));
					//
					// } else if (content instanceof Message) {
					// handleMessage(directory, (Message) content,
					// emailFragments);
					// } else if (content instanceof Multipart) {
					// final Multipart mp2 = (Multipart) content;
					// handleMultipart(directory, mp2, mailMessage,
					// emailFragments);
					// } else {
					// throw new IllegalStateException("Content type not
					// handled: "
					// + content.getClass().getSimpleName());
					// }
					// }

				}
			}
		} catch (MessagingException e) {
			throw new IllegalStateException("Error while retrieving message content.", e);
		} catch (IOException e) {
			throw new IllegalStateException("Error while retrieving message content.", e);
		} catch (Exception e) {
			throw new IllegalStateException("Error while retrieving message content.", e);
		}
		return files;
	}

	public List<InputStream> getAttachments(Message message) throws Exception {
		Object content = message.getContent();
		if (content instanceof String)
			return null;

		if (content instanceof Multipart) {
			Multipart multipart = (Multipart) content;
			List<InputStream> result = new ArrayList<InputStream>();

			for (int i = 0; i < multipart.getCount(); i++) {
				result.addAll(getAttachmentsAsInputStreams(multipart.getBodyPart(i)));
			}
			return result;

		}
		return null;
	}

	public static String getEmailIdFromMessage(final Message mailMessage) {
		final Map<String, Object> map = processHeaders(mailMessage);
		final String emailId = (String) map.get("MAIL_UUID");
		return emailId;
	}

	private static List<InputStream> getAttachmentsAsInputStreams(BodyPart part) throws Exception {
		List<InputStream> result = new ArrayList<InputStream>();
		Object content = part.getContent();
		if (content instanceof InputStream || content instanceof String) {
			if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition()) || StringUtils.isNotBlank(part.getFileName())) {
				result.add(part.getInputStream());
				return result;
			} else {
				return new ArrayList<InputStream>();
			}
		}
		return result;
	}

	public static String getFilename(final BodyPart bodypart) throws MessagingException {
		String filename = bodypart.getFileName();
		if (filename == null && bodypart instanceof MimeBodyPart) {
			filename = ((MimeBodyPart) bodypart).getContentID();
		}
		return filename;
	}

	public static Object getContentFromBodyPart(final BodyPart bodyPart) throws IOException, MessagingException {
		try {
			final Object content = bodyPart.getContent();
			return content;
		} catch (Exception e) {
			LOG.error("Exception while getting content from bodyPart ", e);
		}
		return null;
	}

	public static Map<String, Object> processHeaders(final Message message) {
		Map<String, Object> headerValueMap = new HashMap<>();
		headerValueMap.putAll(MailUtils.extractStandardHeaders(message));
		try {
			final Enumeration<?> allHeaders = message.getAllHeaders();
			while (allHeaders.hasMoreElements()) {
				final Header headerAttribute = (Header) allHeaders.nextElement();
				headerValueMap.put(headerAttribute.getName(), headerAttribute.getValue());
				if (headerAttribute.getName().equals("Message-Id")) {
					final String messageId = headerAttribute.getValue();
					if (StringUtils.isNotBlank(messageId)) {
						headerValueMap.put("MAIL_UUID",
								messageId.replaceAll("<", "").replaceAll(">", "").replaceAll("@", "_"));
					}
				}
			}
		} catch (MessagingException e) {
			LOG.error("MessagingException while extracting headers", e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("-------");
			for (Map.Entry<String, Object> header : headerValueMap.entrySet()) {
				LOG.debug("header {} ->{}", header.getKey(), header.getValue());
			}
			LOG.debug("-------");
		}
		return headerValueMap;
	}
}
package com.mindcurv.commercetools.automation.e2o;

import java.util.Arrays;
import java.util.List;

public interface E2oConstants {
	public static final String INBOX                  = "INBOX";
    public static final String PROCESSING_FOLDER_NAME = "PROCESSING";
    public static final String DONE_FOLDER_NAME       = "DONE";
    public static final String ERROR_FOLDER_NAME      = "ERROR";
    public static final List<String> FOLDERNAMES = Arrays.asList(INBOX, PROCESSING_FOLDER_NAME, DONE_FOLDER_NAME, ERROR_FOLDER_NAME); 
    
    public static final String MOVE_LOG_MESSAGE                 = "Moved email {} from {} to {} folder.";
    public static final String ERROR_MOVING_LOG                 = "Failed to move email {} in mailbox {} from {} to {} folder.Aborting processing of this email.";
    public static final String E2OMAILBOX_STATUS                = "Found {} mailbox/es ({} active).";
    public static final String E2OMAILBOX_INBOX_MESSAGE         = "Found {} email/s in in INBOX folder.";
    public static final String E2OMAILBOX_INBOX_MESSAGE_ERROR   = "Failed to retrieve list of emails in INBOX folder.";
    public static final String E2OMAILBOX_INVALID_CREDENTIALS   = "Failed to connect to mailbox {}. Aborting processing of this mailbox.";
    public static final String OPENING_FOLDER_MSG               = "Opened {} folder.";
    public static final String ERROR_OPENING_FOLDER_MSG         = "Failed to open {} folder. The folder may not exist. Aborting processing of this mailbox.";
    public static final String CLOSED_FOLDER_MSG                = "Closed {} folder.";
    public static final String ERROR_CLOSING_FOLDER_MSG         = "Unexpected exception while closing {} folder. Ignoring.";
    public static final String E2OEMAIL_PERSIST_SUCCESS_MESSAGE = "Persisted email {} in mailbox {} as E2oEmail instance {}.";
    public static final String E2OEMAIL_PERSIST_FAILURE_MESSAGE = "Failed to persist email {} in mailbox {} as E2oEMail instance";
    public static final String CART_METRICS_INFO_MSG            = "Found {} E2O carts ({} initializing, {} failed initialization, {} pending, {} processing, {} aborted, {} done).";
    public static final String WARN_CART_INITIALIZING_STATE     = "Noticed {} E2O cart/s with status {}, while 0 were expected. Possibly multiple cron jobs are processing E2O carts at the same time, or perhaps the status needs correction.";
    public static final String WARN_CART_PROCESSING_STATE       = "Noticed {} E2O cart/s with status {}, while 0 were expected. Possibly multiple cron jobs are processing E2O carts at the same time, or perhaps the status needs correction.";
    public static final String PROCESSING_CART_ENTRY            = "Processing cart {} entry {} (mailbox {}, customer {}).";
    public static final String SKU_ID_VALIDATION                = "SKU ID {} is considered valid in terms of length and format.";
    public static final String SKU_ID_VALIDATION_ERROR          = "SKU ID {} is considered invalid in terms of length and/or format.";
    public static final String QUANTITY_VALIDATION_ERROR        = "Cart {} entry {}: Failed to parse {} as an acceptable quantity.";
    public static final String PRICING_CART_SUCCESS             = "Calculated price for cart {} (currency {}): subtotal={}; totalPrice={}; totalTax={}; paymentCost={}; deliveryCost={}.";
    public static final String PRICING_CART_ERROR               = "Calculating price for cart {} failed.";
    public static final String E2OMAILBOX_RUNNING               = "Running E2oMailboxMonitorJob.";
    public static final String E2OMAILBOX_IMAP_SERVICE          = "Mail server IMAP service is at {} port {}.";
    public static final String E2OMAILBOX_ERROR                 = "Aborting E2oMailboxMonitorJob due to invalid mail server settings. Mail server IMAP service is at {} port {}. Aborting E2oMailboxMonitorJob.";
    public static final String E2OMAILBOX_CONNECTED             = "Connected to mailbox {}.";
    public static final String E2OMAILBOX_DISCONNECTED          = "Disconnected from mailbox {}.";
    public static final String E2OMAILBOX_DISCONNECTED_ERROR    = "Unexpected exception while disconnecting from mailbox {}. Ignoring.";
    public static final String E2OMAILBOX_PROCESSED_EMAILS      = "Processed {} email/s in mailbox {}: {} moved to DONE, {} moved to ERROR folder, {} failed to be moved.";
    public static final String E2OMAILBOX_FINISHED              = "Finished E2oMailboxMonitorJob in {} second/s. Found {} email/s in {} active mailbox/es. {} email/s successfully persisted, {} email/s failed to be persisted.";

}

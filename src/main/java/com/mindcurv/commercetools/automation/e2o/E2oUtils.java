package com.mindcurv.commercetools.automation.e2o;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mindcurv.commercetools.automation.integration.email.EmailParserUtils;
import com.sun.mail.imap.IMAPFolder;

public final class E2oUtils {

	private static final Logger LOG = LoggerFactory.getLogger(E2oUtils.class);

	private E2oUtils() {
	}

	public static void openFolder(Folder folder, int mode) {
		try {
			boolean folderExists = checkOrCreateMailFolder(folder);
			if (folderExists && !folder.isOpen()) {
				folder.open(mode);
				LOG.debug("Opening folder {} - {}", folder.getName());
			}
		} catch (MessagingException exception) {
			LOG.error(E2oConstants.ERROR_OPENING_FOLDER_MSG, folder.getName(), exception);
		}
	}
	
	public static void closeFolderSafely(Folder folder) {
		try {
			if (folder.isOpen()) {
				boolean expunge = true;
				folder.close(expunge);
				LOG.debug(E2oConstants.CLOSED_FOLDER_MSG, folder.getName());
			}
		} catch (MessagingException exception) {
			LOG.error(E2oConstants.ERROR_CLOSING_FOLDER_MSG, folder.getName(), exception);
		}
	}

	private static boolean checkOrCreateMailFolder(Folder folder) {
		boolean folderExists = false;
		try {
			folderExists = folder.exists();
			if (!folderExists) {
				LOG.info( "create folder {}", folder.getName());
				
				folderExists = folder.create(Folder.HOLDS_MESSAGES);
			}
		} catch (MessagingException e) {
			LOG.debug("error during mailbox folder creation", e);
		}
		return folderExists;
	}

	public static void writeToZipFile(File file, ZipOutputStream zipStream) throws FileNotFoundException, IOException {
		EmailParserUtils.LOG.info("adding {} to zip file", file.getAbsolutePath());
		FileInputStream fis = new FileInputStream(file);
		ZipEntry zipEntry = new ZipEntry(file.getName());
		zipStream.putNextEntry(zipEntry);
		byte[] bytes = new byte[1024];
		int length;
		while ((length = fis.read(bytes)) >= 0) {
			zipStream.write(bytes, 0, length);
		}
		zipStream.closeEntry();
		fis.close();
	}

	public static File storeFilesToZip(final List<File> createdFiles)
			throws FileNotFoundException, IOException {
		final File zipFile = File.createTempFile("e2o", "temp");
		EmailParserUtils.LOG.info( "Preparing temporary zip file {}", zipFile.getAbsolutePath());
		FileOutputStream outputStream = new FileOutputStream(zipFile);
		ZipOutputStream zipOS = new ZipOutputStream(outputStream);
		for (File file : createdFiles) {
			writeToZipFile(file, zipOS);
			FileUtils.deleteQuietly(file); 
		}
		zipOS.close();
		return zipFile; 
	}

	public static File prepareDownloadDirectory(final File directory, final Message mailMessage) {
		final String emailId = EmailParserUtils.getEmailIdFromMessage(mailMessage);
		final File directoryToUse = new File(directory, emailId);
		if (!directoryToUse.exists()) {
			directoryToUse.mkdirs();
		}
		LOG.info("Download files to {}", directoryToUse.getAbsolutePath());
		return directoryToUse;
	}
	
	public static void moveEmailsToFolder(final Message[] emails, final IMAPFolder srcFolder, final Folder destFolder)
			throws MessagingException {
		if (checkOrCreateMailFolder(srcFolder)) {
			srcFolder.copyMessages(emails, destFolder);
			srcFolder.setFlags(emails, new Flags(Flags.Flag.DELETED), true);
			srcFolder.expunge(emails);
		} else {
			LOG.error("could not move to folder " + srcFolder.getName());
		}
	}

	public static void moveEmailToFolder(Message message, IMAPFolder sourceFolder, Folder targetFolder) throws MessagingException {
		final Message currentEmail = sourceFolder.getMessage(message.getMessageNumber());
		final Message[] emails = { currentEmail };
		moveEmailsToFolder(emails, sourceFolder, targetFolder);
	}
}

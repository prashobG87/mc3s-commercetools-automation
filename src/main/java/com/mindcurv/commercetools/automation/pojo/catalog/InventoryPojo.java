
package com.mindcurv.commercetools.automation.pojo.catalog;

import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mindcurv.commercetools.automation.Constants;
import com.mindcurv.commercetools.automation.helper.spreadsheet.ValueHelper;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;
import com.mindcurv.commercetools.automation.pojo.GenericTypeReferencePojo;
import com.mindcurv.commercetools.automation.pojo.DraftPojo;

import io.sphere.sdk.inventory.InventoryEntryDraft;
import io.sphere.sdk.json.SphereJsonUtils;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "sku", "supplyChannel", "quantityOnStock", "availableQuantity" })
public class InventoryPojo extends DraftPojo {

	private final static Logger LOG = LoggerFactory.getLogger(InventoryPojo.class);

	public static final String MESSAGETYPE = "inventory";

	@JsonProperty("sku")
	private String sku;
	@JsonProperty("supplyChannel")
	private GenericTypeReferencePojo supplyChannel;
	@JsonProperty("quantityOnStock")
	private Integer quantityOnStock;
	@JsonProperty("restockableInDays")
	private Integer restockableInDays;
	@JsonProperty("expectedDelivery")
	private String expectedDelivery;

	public InventoryPojo() {
		super(); 
		setMessageType(MESSAGETYPE);
	}
	public InventoryPojo(Map<String, Object> value) {
		super(value);
		setMessageType(MESSAGETYPE);
		
		setSku(ValueHelper.getValueAsString(CsvConstants.Inventory.SKU_KEY, value));
		setQuantityOnStock(ValueHelper.getValueAsInteger(CsvConstants.Inventory.QUANTITYON_STOCK_KEY, value));
		final String channelId = ValueHelper.getValueAsString(CsvConstants.Inventory.SUPPLY_CHANNEL_KEY, value);
		if (StringUtils.isNotBlank(channelId)) {
			setSupplyChannel(new GenericTypeReferencePojo().withId(channelId));
		}
		setRestockableInDays(ValueHelper.getValueAsInteger(CsvConstants.Inventory.RESTOCKABLE_IN_DAYS_KEY, value));
		final Date expectedDeliveryDate = ValueHelper.getValueAsDate(CsvConstants.Inventory.EXPECTED_DELIVERY_KEY,
				value);
		if (expectedDeliveryDate != null) {
			final String dateString = Constants.DATETIMEFORMAT.format(expectedDeliveryDate);
			setExpectedDelivery(dateString);
		}

	}

	@JsonProperty("sku")
	public String getSku() {
		return sku;
	}

	@JsonProperty("sku")
	public void setSku(String sku) {
		this.sku = sku;
	}

	public InventoryPojo(final String sku) {
		this.sku = sku;
	}

	@JsonProperty("supplyChannel")
	public GenericTypeReferencePojo getSupplyChannel() {
		return supplyChannel;
	}

	@JsonProperty("supplyChannel")
	public void setSupplyChannel(GenericTypeReferencePojo supplyChannel) {
		this.supplyChannel = supplyChannel;
	}

	public InventoryPojo withSupplyChannel(String supplyChannelId) {
		GenericTypeReferencePojo supplyChannel = new GenericTypeReferencePojo();
		supplyChannel.setId(supplyChannelId);
		this.supplyChannel = supplyChannel;
		return this;
	}

	@JsonProperty("quantityOnStock")
	public Integer getQuantityOnStock() {
		return quantityOnStock;
	}

	@JsonProperty("quantityOnStock")
	public void setQuantityOnStock(Integer quantityOnStock) {
		this.quantityOnStock = quantityOnStock;
	}

	public InventoryPojo withQuantityOnStock(Integer quantityOnStock) {
		this.quantityOnStock = quantityOnStock;
		return this;
	}

	@JsonProperty("restockableInDays")
	public Integer getRestockableInDays() {
		return restockableInDays;
	}

	@JsonProperty("restockableInDays")
	public void setRestockableInDays(Integer restockableInDays) {
		this.restockableInDays = restockableInDays;
	}

	@JsonProperty("expectedDelivery")
	public String getExpectedDelivery() {
		return expectedDelivery;
	}

	@JsonProperty("expectedDelivery")
	public void setExpectedDelivery(String expectedDelivery) {
		this.expectedDelivery = expectedDelivery;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("sku", sku).append("supplyChannel", supplyChannel)
				.append("quantityOnStock", quantityOnStock).toString();
	}

	public String getUniqueKey() {
		final StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(sku.replace("://", "").replaceAll(" ", ""));
		if (supplyChannel != null) {
			stringBuffer.append("_ch").append(getSupplyChannel().getId());
		}
		return stringBuffer.toString();
	}

	public InventoryEntryDraft toDraft() {
		final InventoryEntryDraft draft = SphereJsonUtils.readObject(toJsonString(), InventoryEntryDraft.class);
		LOG.debug("InventoryEntryDraft - {} / {}", draft.getSku(), draft.getSupplyChannel());
		LOG.debug("{}", SphereJsonUtils.toPrettyJsonString(draft));
		return draft;
	}
}

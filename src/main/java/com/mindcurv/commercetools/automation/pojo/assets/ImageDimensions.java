package com.mindcurv.commercetools.automation.pojo.assets;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "h",
    "w"
})
public class ImageDimensions {

    @JsonProperty("h")
    private Integer h;
    @JsonProperty("w")
    private Integer w;
 
    /**
     * No args constructor for use in serialization
     * 
     */
    public ImageDimensions() {
    }

    /**
     * 
     * @param w
     * @param h
     */
    public ImageDimensions(Integer h, Integer w) {
        super();
        this.h = h;
        this.w = w;
    }

    @JsonProperty("h")
    public Integer getH() {
        return h;
    }

    @JsonProperty("h")
    public void setH(Integer h) {
        this.h = h;
    }

    @JsonProperty("w")
    public Integer getW() {
        return w;
    }

    @JsonProperty("w")
    public void setW(Integer w) {
        this.w = w;
    }
    @Override
   	public String toString() {
   		return new ToStringBuilder(this).append("h", getH()).append("w", getW())
   				.toString();
   	}

}

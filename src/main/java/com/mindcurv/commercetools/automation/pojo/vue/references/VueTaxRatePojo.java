
package com.mindcurv.commercetools.automation.pojo.vue.references;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mindcurv.commercetools.automation.helper.VueHelper;
import com.mindcurv.commercetools.automation.pojo.BasePojo;

import io.sphere.sdk.taxcategories.TaxCategory;
import io.sphere.sdk.taxcategories.TaxRate;

import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "tax_country_id",
    "tax_region_id",
    "tax_postcode",
    "code",
    "rate",
    "zip_is_range",
    "zip_from",
    "zip_to",
    "id"
})
public class VueTaxRatePojo extends BasePojo{

    @JsonProperty("tax_country_id")
    private String tax_country_id;
    @JsonProperty("tax_region_id")
    private String tax_region_id;
    @JsonProperty("code")
    private String code; 
    @JsonProperty("tax_postcode")
    private String tax_postcode = "*";
    @JsonProperty("rate")
    private Double rate;
    @JsonProperty("zip_is_range")
    private Boolean zip_is_range = Boolean.FALSE;
    @JsonProperty("zip_from")
    private String zip_from;
    @JsonProperty("zip_to")
    private String zip_to;
    
    public VueTaxRatePojo withTaxRate(TaxRate taxRate, TaxCategory taxCategory) {
    	setId( VueHelper.replaceId(taxRate.getId()));
    	setTaxCountryId(taxRate.getCountry().getAlpha2());
    	setTaxRegionId(taxRate.getState());
    	setRate(taxRate.getAmount());
    	
    	final StringBuffer stringBuffer = new StringBuffer(); 
    	stringBuffer.append(taxCategory.getKey());
    	stringBuffer.append(getTaxCountryId());
    	stringBuffer.append(getTaxRegionId());
    	stringBuffer.append(taxRate.isIncludedInPrice());
    	setCode(stringBuffer.toString());
    	
//    	zip_is_range : Boolean
//    	zip_from : String
//    	zip_to : String
		return this;
	}

	@JsonProperty("tax_country_id")
    public String getTaxCountryId() {
        return tax_country_id;
    }

    @JsonProperty("tax_country_id")
    public void setTaxCountryId(String taxCountryId) {
        this.tax_country_id = taxCountryId;
    }

    public VueTaxRatePojo withTaxCountryId(String taxCountryId) {
        this.tax_country_id = taxCountryId;
        return this;
    }

    @JsonProperty("tax_region_id")
    public String getTaxRegionId() {
        return tax_region_id;
    }

    @JsonProperty("tax_region_id")
    public void setTaxRegionId(String taxRegionId) {
        this.tax_region_id = taxRegionId;
    }

    public VueTaxRatePojo withTaxRegionId(String taxRegionId) {
        this.tax_region_id = taxRegionId;
        return this;
    }

    @JsonProperty("tax_postcode")
    public String getTaxPostcode() {
        return tax_postcode;
    }

    @JsonProperty("tax_postcode")
    public void setTaxPostcode(String taxPostcode) {
        this.tax_postcode = taxPostcode;
    }

    public VueTaxRatePojo withTaxPostcode(String taxPostcode) {
        this.tax_postcode = taxPostcode;
        return this;
    }
    @JsonProperty("code")
    public String getCode() {
    	return code;
    }

    @JsonProperty("code")
    public void setCode(String code) {
        this.code = code;
    }

    public VueTaxRatePojo withCode(String code) {
        this.code = code;
        return this;
    }
    @JsonProperty("rate")
    public Double getRate() {
        return rate;
    }

    @JsonProperty("rate")
    public void setRate(Double rate) {
        this.rate = rate;
    }

    public VueTaxRatePojo withRate(Double rate) {
        this.rate = rate;
        return this;
    }

    @JsonProperty("zip_is_range")
    public Boolean getZipIsRange() {
        return zip_is_range;
    }

    @JsonProperty("zip_is_range")
    public void setZipIsRange(Boolean zipIsRange) {
        this.zip_is_range = zipIsRange;
    }

    public VueTaxRatePojo withZipIsRange(Boolean zipIsRange) {
        this.zip_is_range = zipIsRange;
        return this;
    }

    @JsonProperty("zip_from")
    public String getZipFrom() {
        return zip_from;
    }

    @JsonProperty("zip_from")
    public void setZipFrom(String zipFrom) {
        this.zip_from = zipFrom;
    }

    public VueTaxRatePojo withZipFrom(String zipFrom) {
        this.zip_from = zipFrom;
        return this;
    }

    @JsonProperty("zip_to")
    public String getZipTo() {
        return zip_to;
    }

    @JsonProperty("zip_to")
    public void setZipTo(String zipTo) {
        this.zip_to = zipTo;
    }

    public VueTaxRatePojo withZipTo(String zipTo) {
        this.zip_to = zipTo;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("taxCountryId", tax_country_id).append("taxRegionId", tax_region_id).append("taxPostcode", tax_postcode).append("code", getCode()).append("rate", rate).append("zipIsRange", zip_is_range).append("zipFrom", zip_from).append("zipTo", zip_to).append("id", getId()).toString();
    }

}

package com.mindcurv.commercetools.automation.pojo;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mindcurv.commercetools.automation.helper.spreadsheet.ValueHelper;
import com.mindcurv.commercetools.automation.pojo.types.product.TypeAttribute;

public class DraftPojo extends BasePojo {

	private final static Logger LOG = LoggerFactory.getLogger(DraftPojo.class);

	private boolean essential;

	@JsonProperty("key")
	private String key;
	
	private String messageType; 

	public DraftPojo() {
		super(); 
	}
	
	@JsonProperty("key")
	public String getKey() {
		return key;
	}

	@JsonProperty("key")
	public void setKey(String key) {
		this.key = key;
	}

	public boolean isEssential() {
		return essential;
	}

	public void setEssential(boolean essential) {
		this.essential = essential;
	}

	
	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public DraftPojo(Map<String, Object> value) {
		setEssential(ValueHelper.getValueAsBoolean(CsvConstants.ESSENTIAL, value));
		setId(ValueHelper.getValueAsString(CsvConstants.ID_KEY, value));
		setKey(ValueHelper.getValueAsString(CsvConstants.KEY_KEY, value));
	}

	public void addAttributes(final Map<String, Object> value, final Map<String, List<TypeAttribute>> attributesMap) {
		LOG.warn("no pojo method implemented");
	}
	
}


package com.mindcurv.commercetools.automation.pojo.shipping;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mindcurv.commercetools.automation.pojo.PriceValue;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "price",
    "freeAbove",
    "tiers"
})
public class ShippingRate {

    @JsonProperty("price")
    private PriceValue price;
    @JsonProperty("freeAbove")
    private PriceValue freeAbove;
    @JsonProperty("tiers")
    private List<Object> tiers = new ArrayList<Object>();

    @JsonProperty("price")
    public PriceValue getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(PriceValue price) {
        this.price = price;
    }

    public ShippingRate withPrice(PriceValue price) {
        this.price = price;
        return this;
    }

    @JsonProperty("freeAbove")
    public PriceValue getFreeAbove() {
        return freeAbove;
    }

    @JsonProperty("freeAbove")
    public void setFreeAbove(PriceValue freeAbove) {
        this.freeAbove = freeAbove;
    }

    public ShippingRate withFreeAbove(PriceValue freeAbove) {
        this.freeAbove = freeAbove;
        return this;
    }

    @JsonProperty("tiers")
    public List<Object> getTiers() {
        return tiers;
    }

    @JsonProperty("tiers")
    public void setTiers(List<Object> tiers) {
        this.tiers = tiers;
    }

    public ShippingRate withTiers(List<Object> tiers) {
        this.tiers = tiers;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("price", price).append("freeAbove", freeAbove).append("tiers", tiers).toString();
    }

}

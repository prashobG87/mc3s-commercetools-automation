package com.mindcurv.commercetools.automation.pojo.types.status;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.mindcurv.commercetools.automation.helper.spreadsheet.ValueHelper;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;
import com.mindcurv.commercetools.automation.pojo.DraftPojo;

import io.sphere.sdk.states.StateType;

public class StatusPojo extends DraftPojo {

	public StateType type; 
	public List<String> transistions = new ArrayList<>();
	
	public StatusPojo(Map<String, Object> value) {
		super(value); 
		final StateType stateType = "lineitem".equals(ValueHelper.getValueAsString(CsvConstants.TYPE_KEY, value)) ? StateType.LINE_ITEM_STATE : StateType.ORDER_STATE; 
		setType(stateType);
		final String transitionsString = ValueHelper.getValueAsString(CsvConstants.Status.TRANSITIONS_KEY, value);
		if ( StringUtils.isNotBlank(transitionsString)) {
			for ( String transitionKey : transitionsString.split(CsvConstants.SPLITTOKEN)) {
				transistions.add( transitionKey.trim()); 
			}
		}
	}
	
	public StateType getType() {
		return type;
	}
	public void setType(StateType type) {
		this.type = type;
	}
	public List<String> getTransistions() {
		return transistions;
	}
	public void setTransistions(List<String> transistions) {
		this.transistions = transistions;
	}
	
	
}


package com.mindcurv.commercetools.automation.pojo.shipping;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mindcurv.commercetools.automation.helper.spreadsheet.ValueHelper;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;
import com.mindcurv.commercetools.automation.pojo.GenericTypeReferencePojo;
import com.mindcurv.commercetools.automation.pojo.PriceValue;
import com.mindcurv.commercetools.automation.pojo.ReferenceType;
import com.mindcurv.commercetools.automation.pojo.types.product.TypeAttribute;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "zone", "shippingRates" })
public class ShippingZoneRate {

	@JsonProperty("zone")
	private GenericTypeReferencePojo zone;
	@JsonProperty("shippingRates")
	private List<ShippingRate> shippingRates;

	private String shippingMethod;

	public ShippingZoneRate withSheetData(final Map<String, Object> value, final List<TypeAttribute> attributes) {
		setShippingRates(new ArrayList<ShippingRate>());
		setShippingMethod(ValueHelper.getValueAsString(CsvConstants.ShippingZoneRates.SHIPPING_METHOD_KEY, value));
		setZone(new GenericTypeReferencePojo().withTypeId(ReferenceType.ZONE)
				.withId(ValueHelper.getValueAsString(CsvConstants.ShippingZoneRates.ZONE_KEY, value)));
		addPrice(value);
		return this;
	}

	public String getShippingMethod() {
		return shippingMethod;
	}

	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}

	@JsonProperty("zone")
	public GenericTypeReferencePojo getZone() {
		return zone;
	}

	@JsonProperty("zone")
	public void setZone(GenericTypeReferencePojo zone) {
		this.zone = zone;
	}

	public ShippingZoneRate withZone(GenericTypeReferencePojo zone) {
		this.zone = zone;
		return this;
	}

	@JsonProperty("shippingRates")
	public List<ShippingRate> getShippingRates() {
		return shippingRates;
	}

	@JsonProperty("shippingRates")
	public void setShippingRates(List<ShippingRate> shippingRates) {
		this.shippingRates = shippingRates;
	}

	public ShippingZoneRate withShippingRates(List<ShippingRate> shippingRates) {
		this.shippingRates = shippingRates;
		return this;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("zone", zone).append("shippingRates", shippingRates).toString();
	}

	private void addPrice(final Map<String, Object> value) {
		final String currencyCode = ValueHelper.getValueAsString(CsvConstants.ShippingZoneRates.CURRENCY_CODE_KEY,
				value);
		final Integer priceValue = ValueHelper.getValueAsCentAmount(CsvConstants.ShippingZoneRates.PRICE_KEY, value);
		if (priceValue != null) {

			final PriceValue price = new PriceValue().withCurrencyCode(currencyCode).withCentAmount(priceValue);
			final ShippingRate priceRate = new ShippingRate()
					.withPrice(price);

			final Integer freeAboveCentAmount = ValueHelper
					.getValueAsCentAmount(CsvConstants.ShippingZoneRates.FREE_ABOVE_KEY, value);

			if (freeAboveCentAmount != null) {
				final PriceValue freeAbove = new PriceValue().withCurrencyCode(currencyCode).withCentAmount(freeAboveCentAmount);
				priceRate.withFreeAbove(freeAbove);
			}
			shippingRates.add(priceRate);
		}
	}

}

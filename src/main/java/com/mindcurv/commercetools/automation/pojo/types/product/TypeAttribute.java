
package com.mindcurv.commercetools.automation.pojo.types.product;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mindcurv.commercetools.automation.pojo.LocalizedStringPojo;
import com.mindcurv.commercetools.automation.pojo.ReferenceType;

import io.sphere.sdk.products.attributes.AttributeConstraint;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "name", "label", "isRequired", "required", "type", "attributeConstraint", "isSearchable", "inputHint",
		"inputTip", "displayGroup" })
public class TypeAttribute {

	@JsonProperty("name")
	private String name;
	@JsonProperty("label")
	private LocalizedStringPojo label;
	@JsonProperty("isRequired")
	private Boolean isRequired = Boolean.FALSE;
	@JsonProperty("required")
	private Boolean required = Boolean.FALSE;;
	
	@JsonProperty("type")
	private ReferenceType type;
	@JsonProperty("attributeConstraint")
	private String attributeConstraint = AttributeConstraint.NONE.toSphereName();
	@JsonProperty("isSearchable")
	private Boolean isSearchable = Boolean.FALSE;;
	@JsonProperty("inputHint")
	private String inputHint = "SingleLine";
	@JsonProperty("inputTip")
	private LocalizedStringPojo inputTip;
	@JsonProperty("displayGroup")
	private String displayGroup;

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	public TypeAttribute withName(String name) {
		this.name = name;
		return this;
	}

	@JsonProperty("label")
	public LocalizedStringPojo getLabel() {
		return label;
	}

	@JsonProperty("label")
	public void setLabel(LocalizedStringPojo label) {
		this.label = label;
	}

	public TypeAttribute withLabel(LocalizedStringPojo label) {
		this.label = label;
		return this;
	}

	@JsonProperty("isRequired")
	public Boolean getIsRequired() {
		return isRequired;
	}

	@JsonProperty("required")
	public Boolean getRequired() {
		return required;
	}

	@JsonProperty("isRequired")
	public void setIsRequired(Boolean isRequired) {
		this.isRequired = isRequired;
		this.required = isRequired; 
	}
	
	@JsonProperty("required")
	public void setRequired(Boolean isRequired) {
		this.isRequired = isRequired;
		this.required = isRequired;
	}

	public TypeAttribute withIsRequired(Boolean isRequired) {
		this.isRequired = isRequired;
		this.required = isRequired;
		return this;
	}

	@JsonProperty("type")
	public ReferenceType getType() {
		return type;
	}

	@JsonProperty("type")
	public void setType(ReferenceType type) {
		this.type = type;
	}

	public TypeAttribute withType(ReferenceType type) {
		this.type = type;
		return this;
	}

	@JsonProperty("attributeConstraint")
	public String getAttributeConstraint() {
		return attributeConstraint;
	}

	@JsonProperty("attributeConstraint")
	public void setAttributeConstraint(String attributeConstraint) {
		if (StringUtils.isNotBlank(attributeConstraint)) {
			this.attributeConstraint = attributeConstraint;
		}
	}

	public TypeAttribute withAttributeConstraint(String attributeConstraint) {
		if (StringUtils.isNotBlank(attributeConstraint)) {
			setAttributeConstraint(attributeConstraint);
		}
		return this;
	}

	@JsonProperty("isSearchable")
	public Boolean getIsSearchable() {
		return isSearchable;
	}

	@JsonProperty("isSearchable")
	public void setIsSearchable(Boolean isSearchable) {
		this.isSearchable = isSearchable;
	}

	public TypeAttribute withIsSearchable(Boolean isSearchable) {
		this.isSearchable = isSearchable;
		return this;
	}

	@JsonProperty("inputHint")
	public String getInputHint() {
		return inputHint;
	}

	@JsonProperty("inputHint")
	public void setInputHint(String inputHint) {
		this.inputHint = inputHint;
	}

	public TypeAttribute withInputHint(final String inputHint) {
		
		final String valueToSet = StringUtils.isBlank(inputHint) ? "SingleLine" : inputHint; 
		this.inputHint = valueToSet;
		return this;
	}

	@JsonProperty("inputTip")
	public LocalizedStringPojo getInputTip() {
		return inputTip;
	}

	@JsonProperty("inputTip")
	public void setInputTip(LocalizedStringPojo inputTip) {
		this.inputTip = inputTip;
	}

	public TypeAttribute withInputTip(LocalizedStringPojo inputTip) {
		this.inputTip = inputTip;
		return this;
	}

	@JsonProperty("displayGroup")
	public String getDisplayGroup() {
		return displayGroup;
	}

	@JsonProperty("displayGroup")
	public void setDisplayGroup(String displayGroup) {
		this.displayGroup = displayGroup;
	}

	public TypeAttribute withDisplayGroup(String displayGroup) {
		this.displayGroup = displayGroup;
		return this;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("name", name).append("label", label).append("isRequired", isRequired)
				.append("type", type).append("attributeConstraint", attributeConstraint)
				.append("isSearchable", isSearchable).append("inputHint", inputHint).append("inputTip", inputTip)
				.append("displayGroup", displayGroup).toString();
	}

}

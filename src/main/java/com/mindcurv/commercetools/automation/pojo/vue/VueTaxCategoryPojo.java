
package com.mindcurv.commercetools.automation.pojo.vue;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mindcurv.commercetools.automation.helper.VueHelper;
import com.mindcurv.commercetools.automation.pojo.BasePojo;
import com.mindcurv.commercetools.automation.pojo.vue.references.VueTaxRatePojo;

import io.sphere.sdk.taxcategories.TaxCategory;
import io.sphere.sdk.taxcategories.TaxRate;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "code", "priority", "position", "calculate_subtotal", "id", "tax_rates_ids",
		"product_tax_class_ids", "customer_tax_class_ids", "rates" })
public class VueTaxCategoryPojo extends BasePojo {

	@JsonProperty("code")
	private String code;
	@JsonProperty("priority")
	private Integer priority = Integer.valueOf(1);
	@JsonProperty("position")
	private Integer position = Integer.valueOf(0);
	@JsonProperty("calculate_subtotal")
	private Integer calculate_subtotal = Integer.valueOf(0);;
	@JsonProperty("tax_rates_ids")
	private List<String> tax_rates_ids = new ArrayList<String>();
	@JsonProperty("product_tax_class_ids")
	private List<String> product_tax_class_ids = new ArrayList<String>();
	@JsonProperty("customer_tax_class_ids")
	private List<String> customer_tax_class_ids = new ArrayList<String>();
	@JsonProperty("rates")
	private List<VueTaxRatePojo> rates = new ArrayList<VueTaxRatePojo>();

	public VueTaxCategoryPojo withTaxCategory(TaxCategory taxCategory) {

		setId(VueHelper.replaceId(taxCategory.getId()));
		setCode(taxCategory.getName());
		boolean includedInPrice = false; 
		for ( TaxRate taxRate : taxCategory.getRates()) {
			VueTaxRatePojo taxRatePojo = new VueTaxRatePojo().withTaxRate(taxRate, taxCategory);
			rates.add( taxRatePojo); 
			includedInPrice = taxRatePojo.getCode().endsWith(String.valueOf(true));
		}
		Integer calculateSubtotal = includedInPrice ? Integer.valueOf(0) : Integer.valueOf(1); 
		setCalculateSubtotal(calculateSubtotal);
		
		// priority : Integer
		// position : Integer
		// tax_rates_ids : List<String>
		// product_tax_class_ids : List<String>
		// customer_tax_class_ids : List<String>
		return this;
	}

	@JsonProperty("code")
	public String getCode() {
		return code;
	}

	@JsonProperty("code")
	public void setCode(String code) {
		this.code = code;
	}

	public VueTaxCategoryPojo withCode(String code) {
		this.code = code;
		return this;
	}

	@JsonProperty("priority")
	public Integer getPriority() {
		return priority;
	}

	@JsonProperty("priority")
	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public VueTaxCategoryPojo withPriority(Integer priority) {
		this.priority = priority;
		return this;
	}

	@JsonProperty("position")
	public Integer getPosition() {
		return position;
	}

	@JsonProperty("position")
	public void setPosition(Integer position) {
		this.position = position;
	}

	public VueTaxCategoryPojo withPosition(Integer position) {
		this.position = position;
		return this;
	}

	@JsonProperty("calculate_subtotal")
	public Integer getCalculateSubtotal() {
		return calculate_subtotal;
	}

	@JsonProperty("calculate_subtotal")
	public void setCalculateSubtotal(Integer calculateSubtotal) {
		this.calculate_subtotal = calculateSubtotal;
	}

	public VueTaxCategoryPojo withCalculateSubtotal(Integer calculateSubtotal) {
		this.calculate_subtotal = calculateSubtotal;
		return this;
	}

	@JsonProperty("tax_rates_ids")
	public List<String> getTaxRatesIds() {
		return tax_rates_ids;
	}

	@JsonProperty("tax_rates_ids")
	public void setTaxRatesIds(List<String> taxRatesIds) {
		this.tax_rates_ids = taxRatesIds;
	}

	public VueTaxCategoryPojo withTaxRatesIds(List<String> taxRatesIds) {
		this.tax_rates_ids = taxRatesIds;
		return this;
	}

	@JsonProperty("product_tax_class_ids")
	public List<String> getProductTaxClassIds() {
		return product_tax_class_ids;
	}

	@JsonProperty("product_tax_class_ids")
	public void setProductTaxClassIds(List<String> productTaxClassIds) {
		this.product_tax_class_ids = productTaxClassIds;
	}

	public VueTaxCategoryPojo withProductTaxClassIds(List<String> productTaxClassIds) {
		this.product_tax_class_ids = productTaxClassIds;
		return this;
	}

	@JsonProperty("customer_tax_class_ids")
	public List<String> getCustomerTaxClassIds() {
		return customer_tax_class_ids;
	}

	@JsonProperty("customer_tax_class_ids")
	public void setCustomerTaxClassIds(List<String> customerTaxClassIds) {
		this.customer_tax_class_ids = customerTaxClassIds;
	}

	public VueTaxCategoryPojo withCustomerTaxClassIds(List<String> customerTaxClassIds) {
		this.customer_tax_class_ids = customerTaxClassIds;
		return this;
	}

	@JsonProperty("rates")
	public List<VueTaxRatePojo> getRates() {
		return rates;
	}

	@JsonProperty("rates")
	public void setRates(List<VueTaxRatePojo> rates) {
		this.rates = rates;
	}

	public VueTaxCategoryPojo withRates(List<VueTaxRatePojo> rates) {
		this.rates = rates;
		return this;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("code", code).append("priority", priority).append("position", position)
				.append("calculateSubtotal", calculate_subtotal).append("id", getId())
				.append("taxRatesIds", tax_rates_ids).append("productTaxClassIds", product_tax_class_ids)
				.append("customerTaxClassIds", customer_tax_class_ids).append("rates", rates).toString();
	}

}


package com.mindcurv.commercetools.automation.pojo.types;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mindcurv.commercetools.automation.pojo.LocalizedStringPojo;
import com.mindcurv.commercetools.automation.pojo.ReferenceType;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "name",
    "type",
    "required",
    "label",
    "inputHint"
})
public class FieldDefinition {

    @JsonProperty("name")
    private String name;
    @JsonProperty("type")
    private ReferenceType type;
    @JsonProperty("required")
    private Boolean required;
    @JsonProperty("label")
    private LocalizedStringPojo label;
    @JsonProperty("inputHint")
    private String inputHint;
 
    

    public FieldDefinition withName(String name) {
	    this.name = name;
	    return this;
	}

	public FieldDefinition withType(ReferenceType type) {
	    this.type = type;
	    return this;
	}

	public FieldDefinition withRequired(Boolean required) {
	    this.required = required;
	    return this;
	}

	public FieldDefinition withLabel(LocalizedStringPojo label) {
	    this.label = label;
	    return this;
	}

	@JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("type")
    public ReferenceType getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(ReferenceType type) {
        this.type = type;
    }

    @JsonProperty("required")
    public Boolean getRequired() {
        return required;
    }

    @JsonProperty("required")
    public void setRequired(Boolean required) {
        this.required = required;
    }

    @JsonProperty("label")
    public LocalizedStringPojo getLabel() {
        return label;
    }

    @JsonProperty("label")
    public void setLabel(LocalizedStringPojo label) {
        this.label = label;
    }

    @JsonProperty("inputHint")
    public String getInputHint() {
        return inputHint;
    }

    @JsonProperty("inputHint")
    public void setInputHint(String inputHint) {
        this.inputHint = inputHint;
    }

    public FieldDefinition withInputHint(String inputHint) {
        this.inputHint = inputHint;
        return this;
    }

 

	@Override
    public String toString() {
        return new ToStringBuilder(this).append("name", name).append("type", type).append("required", required).append("label", label).append("inputHint", inputHint).toString();
    }

}

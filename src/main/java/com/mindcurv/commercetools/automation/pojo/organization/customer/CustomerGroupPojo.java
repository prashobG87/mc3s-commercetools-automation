
package com.mindcurv.commercetools.automation.pojo.organization.customer;

import java.util.Map;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mindcurv.commercetools.automation.helper.spreadsheet.ValueHelper;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;
import com.mindcurv.commercetools.automation.pojo.Custom;
import com.mindcurv.commercetools.automation.pojo.types.BaseTypePojo;

import io.sphere.sdk.customergroups.CustomerGroupDraft;
import io.sphere.sdk.json.SphereJsonUtils;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "typeId", "id", "key", "groupName" })
public class CustomerGroupPojo extends BaseTypePojo {

	private final static Logger LOG = LoggerFactory.getLogger(CustomerGroupPojo.class);

	public static final String MESSAGETYPE = "customergroup";

	@JsonProperty("groupName")
	private String groupName;

	public CustomerGroupPojo() {
		super();
		setMessageType(MESSAGETYPE);
	}

	public CustomerGroupPojo(Map<String, Object> value) {
		super(value);
		setId(ValueHelper.getValueAsString(CsvConstants.CustomerGroup.ID_KEY, value));
		setGroupName(ValueHelper.getValueAsString(CsvConstants.CustomerGroup.GROUPNAME_KEY, value));
		Custom custom = getCustom();
		if (custom != null && custom.getType() != null) {
			custom.getType().setKey(custom.getType().getId());
			custom.getType().setId(null);
		}
	}

	@JsonProperty("groupName")
	public String getGroupName() {
		return groupName;
	}

	@JsonProperty("groupName")
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("key", getKey()).append("groupName", groupName).append("id", getId())
				.toString();
	}

	public CustomerGroupDraft toDraft() {
		final CustomerGroupDraft draft = SphereJsonUtils.readObject(toJsonString(), CustomerGroupDraft.class);
		LOG.info("CustomerGroupDraft - {}", draft.getKey());
		LOG.debug("{}", SphereJsonUtils.toPrettyJsonString(draft));
		return draft;
	}
}

package com.mindcurv.commercetools.automation.pojo;

public class CsvConstants {

	public class Address {

		public static final String SHEET = "address";
		public static final String SHEETNAME = SHEET + SHEET_RANGE;

		public static final String REFERENCE_TYPE_CHANNEL = "channel";
		public static final String REFERENCE_TYPE_CUSTOMER = "customer";

		public static final String TYPE_SHIPPING = "shipping";
		public static final String TYPE_BILLING = "billing";
		public static final String TYPE_SHIPPING_AND_BILLING = "shipping_billing";

		public static final String REFERENCE_TYPE_KEY = "referenceType";
		public static final String REFERENCE_ID_KEY = "referenceId";
		public static final String TYPE_KEY = "type";
		public static final String DEFAULT_KEY = "default";
		public static final String DISPLAYNAME_KEY = "displayName";
		public static final String COMPANY_KEY = "company";
		public static final String CITY_KEY = "city";
		public static final String FAX_KEY = "fax";
		public static final String TELEPHONE_KEY = "phone";
		public static final String COUNTRY_KEY = "country";
		public static final String EMAIL_KEY = "email";
		public static final String GEOCOORDINATES_KEY = "geocoordinates";
		public static final String STREETNAME_KEY = "streetName";
		public static final String STREETNUMBER_KEY = "streetNumber";
		public static final String POSTALCODE_KEY = "postalCode";
		public static final String TITLE_KEY = "title";
		public static final String ADDITIONALSTREETINFO_KEY = "additionalAddressInfo";
		public static final String SALUTATION_KEY = "salutation";
		public static final String ADDITIONALADDRESSINFO_KEY = "additionalStreetInfo";
		public static final String FIRSTNAME_KEY = "firstName";
		public static final String LASTNAME_KEY = "lastName";
		public static final String REGION_KEY = "region";
		public static final String STATE_KEY = "state";
		public static final String DEPARTMENT_KEY = "department";
		public static final String MOBILE_KEY = "mobile";
		public static final String EXTERNALID_KEY = "externalId";
		public static final String BUILDING_KEY = "building";
		public static final String APARTMENT_KEY = "apartment";
		public static final String POBOX_KEY = "pOBox";
	}

	public class Attributes {
		public static final String SHEET = "Attributes";
		public static final String SHEETNAME = SHEET + SHEET_RANGE;
		public static final String NAME = "name";
		public static final String DISPLAY_GROUP = "displayGroup";
		public static final String TYPE = "type";
		public static final String SET = "set";
		public static final String REFERENCE_TYPE_ID = "referenceTypeId";
		public static final String ENUM_VALUES = "enumValues";
		public static final String ATTRIBUTE_CONTRAINTS = "attributeConstraint";
		public static final String REQUIRED = "isRequired";
		public static final String SEARCHABLE = "isSearchable";
		public static final String LABEL_EN = "label.en";
		public static final String LABEL_DE = "label.de";
		public static final String TEXT_INPUT_HINT = "textInputHint";
		public static final String TYPES = "Types";
	}

	public class Category {
		public static final String SHEET = "category";
		public static final String SHEETNAME = SHEET + SHEET_RANGE;
		public static final String EXTERNALID_KEY = "externalId";
		public static final String PARENT_KEY = "parent";
		public static final String TYPE_KEY = "type";
		public static final String ORDERHINT_KEY = "orderHint";
		public static final String RELEASEENDDATE_KEY = "releaseEndDate";
		public static final String RELEASESTARTDATE_KEY = "releaseStartDate";

	}

	public static class Channel {
		public static final String SHEET = "channel";
		public static final String SHEETNAME = SHEET + SHEET_RANGE;
		public static final String ROLES_KEY = "roles";
	}

	public static class Customer {
		public static final String SHEET = "customer";
		public static final String SHEETNAME = SHEET + SHEET_RANGE;
		public static final String CUSTOMER_NUMBER_KEY = "customerNumber";
		public static final String FIRST_NAME_KEY = "firstName";
		public static final String LAST_NAME_KEY = "lastName";
		public static final String EXTERNAL_ID_KEY = "externalId";
		public static final String PASSWORD_KEY = "password";
		public static final String VAT_ID_KEY = "vatId";
		public static final String DATE_OF_BIRTH_KEY = "dateOfBirth";
		public static final String GROUP_KEY = "group";
		public static final String COMPANYNAME_KEY = "companyName";
		public static final String EMAIL_KEY = "email";
	}

	public static class CustomerGroup {
		public static final String SHEET = "customergroup";
		public static final String SHEETNAME = SHEET + SHEET_RANGE;
		public static final String ID_KEY = "id";
		public static final String KEY_KEY = "key";
		public static final String GROUPNAME_KEY = "groupName";
	}

	public static class CustomObject {
		public static final String SHEET = "customobject";
		public static final String SHEETNAME = SHEET + SHEET_RANGE;
		public static final String CONTAINER_KEY = "container";
		public static final String KEY_KEY = "key";
		public static final String CLASS_KEY = "class";
		public static final String VALUE_KEY = "value";
	}

	public static class Inventory {
		public static final String SHEET = "inventory";
		public static final String SHEETNAME = SHEET + SHEET_RANGE;
		public static final String SKU_KEY = "sku";
		public static final String SUPPLY_CHANNEL_KEY = "supplyChannel";
		public static final String QUANTITYON_STOCK_KEY = "quantityOnStock";
		public static final String RESTOCKABLE_IN_DAYS_KEY = "restockableInDays";
		public static final String EXPECTED_DELIVERY_KEY = "expectedDelivery";
	}

	public static class Price {
		public static final String SHEET = "price";
		public static final String SHEETNAME = SHEET + SHEET_RANGE;
		public static final String SKU_KEY = "sku";
		public static final String VARIANTID_KEY = "variantId";
		public static final String CURRENCY_KEY = "currency";
		public static final String PRICE_KEY = "price";
		public static final String COUNTRY_KEY = "country";
		public static final String CHANNEL_KEY = "channel";
		public static final String CUSTOMER_GROUP_KEY = "customerGroup";
		public static final String VALID_FROM_KEY = "validFrom";
		public static final String VALID_UNTIL_KEY = "validUntil";
	}

	public static class Product {
		public static final String SHEET = "product";
		public static final String SHEETNAME = SHEET + SHEET_RANGE;
		public static final String PRODUCTTYPE_KEY = "productType";
		public static final String VARIANTKEY_KEY = "variantKey";
		public static final String SKU_KEY = "sku";
		public static final String TAX_KEY = "tax";
		public static final String CATEGORIES_KEY = "categories";
	}

	public static class Variant {
		public static final String SHEET = "variant";
		public static final String SHEETNAME = SHEET + SHEET_RANGE;
		public static final String BASE_PRODUCT_KEY = "baseProduct";
		public static final String SKU_KEY = "sku";
		public static final String VARIANT_ID_KEY = "variantId";
		public static final String KEY_KEY = "key";
	}

	public static class ProductImages {
		public static final String SHEET = "productImage";
		public static final String SHEETNAME = SHEET + SHEET_RANGE;
		public static final String BASE_PRODUCT_KEY = "baseProduct";
		public static final String VARIANTKEY_KEY = "variantKey";
		public static final String URL_KEY = "url";
		public static final String LABEL_KEY = "label";
		public static final String WIDTH_KEY = "width";
		public static final String HEIGHT_KEY = "width";
	}

	public class Status {
		public static final String SHEET = "status";
		public static final String SHEETNAME = SHEET + SHEET_RANGE;
		public static final String TRANSITIONS_KEY = "transitions";
	}

	public class TaxCategory {
		public static final String SHEET = "taxCategory";
		public static final String SHEETNAME = SHEET + SHEET_RANGE;
		public static final String NAME_KEY = "name";
		public static final String DESCRIPTION_KEY = "description";

	}

	public class TaxRate {
		public static final String SHEET = "taxRate";
		public static final String SHEETNAME = SHEET + SHEET_RANGE;
		public static final String TAXKEY_KEY = "taxKey";
		public static final String NAME_KEY = "name";
		public static final String COUNTRY_KEY = "country";
		public static final String STATE_KEY = "state";
		public static final String AMOUNT_KEY = "amount";
		public static final String INCLUDED_IN_PRICE_KEY = "includedInPrice";
	}

	public class Type {
		public static final String SHEET = "Types";
		public static final String SHEETNAME = SHEET + SHEET_RANGE;
	}

	public class ShippingMethod {
		public static final String SHEET = "shippingMethods";
		public static final String SHEETNAME = SHEET + SHEET_RANGE;
		public static final String KEY_KEY = "key";
		public static final String NAME_KEY = "name";
		public static final String DESCRIPTION_KEY = "description";
		public static final String TAX_CATEGORY_KEY = "taxCategory";
		public static final String DEFAULT_KEY = "isDefault";
	}

	public class ShippingZone {
		public static final String SHEET = "shippingZones";
		public static final String SHEETNAME = SHEET + SHEET_RANGE;
		public static final String NAME_KEY = "name";
		public static final String DESCRIPTION_KEY = "description";
		public static final String LOCATIONS_KEY = "locations";
	}

	public class ShippingZoneRates {
		public static final String SHEET = "shippingZoneRates";
		public static final String SHEETNAME = SHEET + SHEET_RANGE;
		public static final String ZONE_KEY = "zone";
		public static final String SHIPPING_METHOD_KEY = "shippingMethod";
		public static final String CURRENCY_CODE_KEY = "currencyCode";
		public static final String PRICE_KEY = "price";
		public static final String FREE_ABOVE_KEY = "freeAbove";

	}

	public class Order {
		public static final String SHEET = "order";
		public static final String SHEETNAME = SHEET + SHEET_RANGE;
		public static final String CUSTOMER_NUMBER_KEY = "customerNumber";
		public static final String CURRENCY_KEY = "currency";
		public static final String SHIPPING_METHOD_KEY = "shippingMethod";
		public static final String ENTRIES_KEY = "entries";
		public static final String STATUS_KEY = "status";
		public static final String TYPE_KEY = "type";
		public static final String INVENTORY_MODE_KEY = "inventoryMode";
		public static final String TAX_CALCULATION_MODE_KEY = "taxCalculationMode";
	}

	public static final String ESSENTIAL = "essential";
	public static final String ID_KEY = "id";
	public static final String KEY_KEY = "key";

	private static final String METATITLE = "metaTitle";
	private static final String METADESCRIPTION = "metaDescription";
	private static final String METAKEYWORDS = "metaKeywords";
	private static final String SLUG = "slug";
	private static final String NAME = "name";
	private static final String DESCRIPTION = "description";

	private static final String DE = ".de";
	private static final String EN = ".en";

	public static final String NAME_DE_KEY = NAME + DE;
	public static final String SLUG_DE_KEY = SLUG + DE;
	public static final String DESCRIPTION_DE_KEY = DESCRIPTION + DE;
	public static final String METATITLE_DE_KEY = METATITLE + DE;
	public static final String METADESCRIPTION_DE_KEY = METADESCRIPTION + DE;
	public static final String METAKEYWORDS_DE_KEY = METAKEYWORDS + DE;

	public static final String NAME_EN_KEY = NAME + EN;
	public static final String SLUG_EN_KEY = SLUG + EN;
	public static final String DESCRIPTION_EN_KEY = DESCRIPTION + EN;
	public static final String METATITLE_EN_KEY = METATITLE + ".en";
	public static final String METADESCRIPTION_EN_KEY = METADESCRIPTION + ".en";
	public static final String METAKEYWORDS_EN_KEY = METAKEYWORDS + ".en";

	public static final String TYPE_KEY = "type";
	public static final String SHEET_RANGE = "!A:ZZ";
	public static final String SPLITTOKEN = ",";

}


package com.mindcurv.commercetools.automation.pojo.organization;

import java.util.Map;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mindcurv.commercetools.automation.helper.spreadsheet.ValueHelper;
import com.mindcurv.commercetools.automation.pojo.BasePojo;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "key", "streetName", "streetNumber", "postalCode", "city", "country", "company", "phone", "email",
		"fax" })
public class AddressPojo extends BasePojo {

	private String referenceTypeKey;
	private String referenceId;
	private boolean shipping;
	private boolean billing;
	private boolean isdefault;

	@JsonProperty("key")
	private String key;
	@JsonProperty("streetName")
	private String streetName;
	@JsonProperty("streetNumber")
	private String streetNumber;
	@JsonProperty("postalCode")
	private String postalCode;
	@JsonProperty("city")
	private String city;
	@JsonProperty("country")
	private String country;
	@JsonProperty("company")
	private String company;
	@JsonProperty("phone")
	private String phone;
	@JsonProperty("email")
	private String email;
	@JsonProperty("fax")
	private String fax;

	@JsonProperty("title")
	private String title;
	@JsonProperty("salutation")
	private String salutation;
	@JsonProperty("firstName")
	private String firstName;
	@JsonProperty("lastName")
	private String lastName;
	@JsonProperty("additionalStreetInfo")
	private String additionalStreetInfo;
	@JsonProperty("region")
	private String region;
	@JsonProperty("state")
	private String state;
	@JsonProperty("department")
	private String department;
	@JsonProperty("building")
	private String building;
	@JsonProperty("apartment")
	private String apartment;
	@JsonProperty("pOBox")
	private String pOBox;
	@JsonProperty("mobile")
	private String mobile;
	@JsonProperty("additionalAddressInfo")
	private String additionalAddressInfo;
	@JsonProperty("externalId")
	private String externalId;

	public AddressPojo withSheetData(final Map<String, Object> value) {
		setKey(ValueHelper.getValueAsString(CsvConstants.KEY_KEY, value));
		setReferenceTypeKey(ValueHelper.getValueAsString(CsvConstants.Address.REFERENCE_TYPE_KEY, value));
		setReferenceId(ValueHelper.getValueAsString(CsvConstants.Address.REFERENCE_ID_KEY, value));
		setIsdefault(ValueHelper.getValueAsBoolean(CsvConstants.Address.DEFAULT_KEY, value));
		final String type = ValueHelper.getValueAsString(CsvConstants.Address.TYPE_KEY, value);
		if (CsvConstants.Address.TYPE_SHIPPING.equals(type)
				|| CsvConstants.Address.TYPE_SHIPPING_AND_BILLING.equals(type)) {
			setShipping(true);
		}
		if (CsvConstants.Address.TYPE_BILLING.equals(type)
				|| CsvConstants.Address.TYPE_SHIPPING_AND_BILLING.equals(type)) {
			setBilling(true);
		}
		setCountry(ValueHelper.getValueAsString(CsvConstants.Address.COUNTRY_KEY, value));
		setEmail(ValueHelper.getValueAsString(CsvConstants.Address.EMAIL_KEY, value));
		setPhone(ValueHelper.getValueAsString(CsvConstants.Address.TELEPHONE_KEY, value));
		setFax(ValueHelper.getValueAsString(CsvConstants.Address.FAX_KEY, value));
		setCity(ValueHelper.getValueAsString(CsvConstants.Address.CITY_KEY, value));
		setCompany(ValueHelper.getValueAsString(CsvConstants.Address.COMPANY_KEY, value));
		setStreetName(ValueHelper.getValueAsString(CsvConstants.Address.STREETNAME_KEY, value));
		setStreetNumber(ValueHelper.getValueAsString(CsvConstants.Address.STREETNUMBER_KEY, value));
		setPostalCode(ValueHelper.getValueAsString(CsvConstants.Address.POSTALCODE_KEY, value));

		setTitle(ValueHelper.getValueAsString(CsvConstants.Address.TITLE_KEY, value));
		setSalutation(ValueHelper.getValueAsString(CsvConstants.Address.SALUTATION_KEY, value));
		setFirstName(ValueHelper.getValueAsString(CsvConstants.Address.FIRSTNAME_KEY, value));
		setLastName(ValueHelper.getValueAsString(CsvConstants.Address.LASTNAME_KEY, value));
		setAdditionalAddressInfo(ValueHelper.getValueAsString(CsvConstants.Address.ADDITIONALADDRESSINFO_KEY, value));
		setAdditionalStreetInfo(ValueHelper.getValueAsString(CsvConstants.Address.ADDITIONALSTREETINFO_KEY, value));
		setRegion(ValueHelper.getValueAsString(CsvConstants.Address.REGION_KEY, value));
		setState(ValueHelper.getValueAsString(CsvConstants.Address.STATE_KEY, value));
		setDepartment(ValueHelper.getValueAsString(CsvConstants.Address.DEPARTMENT_KEY, value));
		setBuilding(ValueHelper.getValueAsString(CsvConstants.Address.BUILDING_KEY, value));
		setApartment(ValueHelper.getValueAsString(CsvConstants.Address.APARTMENT_KEY, value));
		setpOBox(ValueHelper.getValueAsString(CsvConstants.Address.POBOX_KEY, value));
		setMobile(ValueHelper.getValueAsString(CsvConstants.Address.MOBILE_KEY, value));
		setExternalId(ValueHelper.getValueAsString(CsvConstants.Address.EXTERNALID_KEY, value));

		return this;
	}

	public String getReferenceTypeKey() {
		return referenceTypeKey;
	}

	public void setReferenceTypeKey(String referenceTypeKey) {
		this.referenceTypeKey = referenceTypeKey;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public boolean isShipping() {
		return shipping;
	}

	public void setShipping(boolean shipping) {
		this.shipping = shipping;
	}

	public boolean isBilling() {
		return billing;
	}

	public void setBilling(boolean billing) {
		this.billing = billing;
	}

	public boolean isIsdefault() {
		return isdefault;
	}

	public void setIsdefault(boolean isdefault) {
		this.isdefault = isdefault;
	}

	@JsonProperty("key")
	public String getKey() {
		return key;
	}

	@JsonProperty("key")
	public void setKey(String key) {
		this.key = key;
	}

	public AddressPojo withKey(String key) {
		this.key = key;
		return this;
	}

	@JsonProperty("streetName")
	public String getStreetName() {
		return streetName;
	}

	@JsonProperty("streetName")
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public AddressPojo withStreetName(String streetName) {
		this.streetName = streetName;
		return this;
	}

	@JsonProperty("streetNumber")
	public String getStreetNumber() {
		return streetNumber;
	}

	@JsonProperty("streetNumber")
	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}

	public AddressPojo withStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
		return this;
	}

	@JsonProperty("postalCode")
	public String getPostalCode() {
		return postalCode;
	}

	@JsonProperty("postalCode")
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public AddressPojo withPostalCode(String postalCode) {
		this.postalCode = postalCode;
		return this;
	}

	@JsonProperty("city")
	public String getCity() {
		return city;
	}

	@JsonProperty("city")
	public void setCity(String city) {
		this.city = city;
	}

	public AddressPojo withCity(String city) {
		this.city = city;
		return this;
	}

	@JsonProperty("country")
	public String getCountry() {
		return country;
	}

	@JsonProperty("country")
	public void setCountry(String country) {
		this.country = country;
	}

	public AddressPojo withCountry(String country) {
		this.country = country;
		return this;
	}

	@JsonProperty("company")
	public String getCompany() {
		return company;
	}

	@JsonProperty("company")
	public void setCompany(String company) {
		this.company = company;
	}

	public AddressPojo withCompany(String company) {
		this.company = company;
		return this;
	}

	@JsonProperty("phone")
	public String getPhone() {
		return phone;
	}

	@JsonProperty("phone")
	public void setPhone(String phone) {
		this.phone = phone;
	}

	public AddressPojo withPhone(String phone) {
		this.phone = phone;
		return this;
	}

	@JsonProperty("email")
	public String getEmail() {
		return email;
	}

	@JsonProperty("email")
	public void setEmail(String email) {
		this.email = email;
	}

	public AddressPojo withEmail(String email) {
		this.email = email;
		return this;
	}

	@JsonProperty("fax")
	public String getFax() {
		return fax;
	}

	@JsonProperty("fax")
	public void setFax(String fax) {
		this.fax = fax;
	}

	public AddressPojo withFax(String fax) {
		this.fax = fax;
		return this;
	}

	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

	@JsonProperty("title")
	public void setTitle(String title) {
		this.title = title;
	}

	@JsonProperty("salutation")
	public String getSalutation() {
		return salutation;
	}

	@JsonProperty("salutation")
	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	@JsonProperty("firstName")
	public String getFirstName() {
		return firstName;
	}

	@JsonProperty("firstName")
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@JsonProperty("lastName")
	public String getLastName() {
		return lastName;
	}

	@JsonProperty("lastName")
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@JsonProperty("additionalStreetInfo")
	public String getAdditionalStreetInfo() {
		return additionalStreetInfo;
	}

	@JsonProperty("additionalStreetInfo")
	public void setAdditionalStreetInfo(String additionalStreetInfo) {
		this.additionalStreetInfo = additionalStreetInfo;
	}

	@JsonProperty("region")
	public String getRegion() {
		return region;
	}

	@JsonProperty("region")
	public void setRegion(String region) {
		this.region = region;
	}

	@JsonProperty("state")
	public String getState() {
		return state;
	}

	@JsonProperty("state")
	public void setState(String state) {
		this.state = state;
	}

	@JsonProperty("department")
	public String getDepartment() {
		return department;
	}

	@JsonProperty("department")
	public void setDepartment(String department) {
		this.department = department;
	}

	@JsonProperty("building")
	public String getBuilding() {
		return building;
	}

	@JsonProperty("building")
	public void setBuilding(String building) {
		this.building = building;
	}

	@JsonProperty("apartment")
	public String getApartment() {
		return apartment;
	}

	@JsonProperty("apartment")
	public void setApartment(String apartment) {
		this.apartment = apartment;
	}

	@JsonProperty("pOBox")
	public String getpOBox() {
		return pOBox;
	}

	@JsonProperty("pOBox")
	public void setpOBox(String pOBox) {
		this.pOBox = pOBox;
	}

	@JsonProperty("mobile")
	public String getMobile() {
		return mobile;
	}

	@JsonProperty("mobile")
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	@JsonProperty("additionalAddressInfo")
	public String getAdditionalAddressInfo() {
		return additionalAddressInfo;
	}

	@JsonProperty("additionalAddressInfo")
	public void setAdditionalAddressInfo(String additionalAddressInfo) {
		this.additionalAddressInfo = additionalAddressInfo;
	}

	@JsonProperty("externalId")
	public String getExternalId() {
		return externalId;
	}

	@JsonProperty("externalId")
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("key", key).append("streetName", streetName)
				.append("streetNumber", streetNumber).append("postalCode", postalCode).append("city", city)
				.append("country", country).append("company", company).append("phone", phone).append("email", email)
				.append("fax", fax).toString();
	}

}

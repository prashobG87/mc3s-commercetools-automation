
package com.mindcurv.commercetools.automation.pojo.shipping;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mindcurv.commercetools.automation.helper.spreadsheet.ValueHelper;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;
import com.mindcurv.commercetools.automation.pojo.DraftPojo;
import com.mindcurv.commercetools.automation.pojo.GenericTypeReferencePojo;
import com.mindcurv.commercetools.automation.pojo.ReferenceType;
import com.mindcurv.commercetools.automation.pojo.types.product.TypeAttribute;

import io.sphere.sdk.json.SphereJsonUtils;
import io.sphere.sdk.shippingmethods.ShippingMethodDraft;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "version", "name", "description", "taxCategory", "zoneRates", "isDefault", "createdAt",
		"lastModifiedAt" })
public class ShippingMethodPojo extends DraftPojo {

	private final static Logger LOG = LoggerFactory.getLogger(ShippingMethodPojo.class);

	public static final String MESSAGETYPE = "shippingmethod";

	@JsonProperty("name")
	private String name;
	@JsonProperty("description")
	private String description;
	@JsonProperty("taxCategory")
	private GenericTypeReferencePojo taxCategory;
	@JsonProperty("zoneRates")
	private List<ShippingZoneRate> zoneRates = new ArrayList<ShippingZoneRate>();
	@JsonProperty("isDefault")
	private Boolean isDefault;

	public ShippingMethodPojo() {
		super();
		
		setMessageType(MESSAGETYPE);

	}

	public ShippingMethodPojo withSheetData(final Map<String, Object> value, final List<TypeAttribute> attributes) {

		setKey(ValueHelper.getValueAsString(CsvConstants.ShippingMethod.KEY_KEY, value));
		setName(ValueHelper.getValueAsString(CsvConstants.ShippingMethod.NAME_KEY, value));
		setDescription(ValueHelper.getValueAsString(CsvConstants.ShippingMethod.DESCRIPTION_KEY, value));
		setTaxCategory(new GenericTypeReferencePojo()
				.withTypeId(ReferenceType.TAX_CATEGORY)
				.withKey(ValueHelper.getValueAsString(CsvConstants.ShippingMethod.TAX_CATEGORY_KEY, value)));
		setIsDefault(ValueHelper.getValueAsBoolean(CsvConstants.ShippingMethod.DEFAULT_KEY, value));
		return this;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	public ShippingMethodPojo withName(String name) {
		this.name = name;
		return this;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("description")
	public void setDescription(String description) {
		this.description = description;
	}

	public ShippingMethodPojo withDescription(String description) {
		this.description = description;
		return this;
	}

	@JsonProperty("taxCategory")
	public GenericTypeReferencePojo getTaxCategory() {
		return taxCategory; 
	}

	@JsonProperty("taxCategory")
	public void setTaxCategory(GenericTypeReferencePojo taxCategory) {
		this.taxCategory = taxCategory;
	}

	public ShippingMethodPojo withTaxCategory(GenericTypeReferencePojo taxCategory) {
		this.taxCategory = taxCategory;
		return this;
	}

	@JsonProperty("zoneRates")
	public List<ShippingZoneRate> getZoneRates() {
		return zoneRates;
	}

	@JsonProperty("zoneRates")
	public void setZoneRates(List<ShippingZoneRate> zoneRates) {
		this.zoneRates = zoneRates;
	}

	public ShippingMethodPojo withZoneRates(List<ShippingZoneRate> zoneRates) {
		this.zoneRates = zoneRates;
		return this;
	}

	@JsonProperty("isDefault")
	public Boolean getIsDefault() {
		return isDefault;
	}

	@JsonProperty("isDefault")
	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	public ShippingMethodPojo withIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
		return this;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("name", name).append("description", description)
				.append("taxCategory", taxCategory).append("zoneRates", zoneRates).append("isDefault", isDefault)
				.toString();
	}

	public ShippingMethodDraft toDraft() {
		final ShippingMethodDraft draft = SphereJsonUtils.readObject(toJsonString(), ShippingMethodDraft.class);
		LOG.info("ShippingMethodDraft - name:{}", draft.getName());
		LOG.debug("{}", SphereJsonUtils.toPrettyJsonString(draft));
		return draft;
	}

}

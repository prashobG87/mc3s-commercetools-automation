
package com.mindcurv.commercetools.automation.pojo.types.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "key",
    "label"
})
public class ProductTypeValue {

    @JsonProperty("key")
    private String key;
    @JsonProperty("label")
    private Object label;

    @JsonProperty("key")
    public String getKey() {
        return key;
    }

    @JsonProperty("key")
    public void setKey(String key) {
        this.key = key;
    }

    public ProductTypeValue withKey(String key) {
        this.key = key;
        return this;
    }

    @JsonProperty("label")
    public Object getLabel() {
        return label;
    }

    @JsonProperty("label")
    public void setLabel(Object label) {
        this.label = label;
    }

    public ProductTypeValue withLabel(Object label) {
        this.label = label;
        return this;
    }
    
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("key", key).append("label", label).toString();
    }

	

}

package com.mindcurv.commercetools.automation.pojo.catalog.products;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mindcurv.commercetools.automation.helper.TypeHelper;
import com.mindcurv.commercetools.automation.helper.spreadsheet.ValueHelper;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;
import com.mindcurv.commercetools.automation.pojo.DraftPojo;
import com.mindcurv.commercetools.automation.pojo.GenericTypeReferencePojo;
import com.mindcurv.commercetools.automation.pojo.LocalizedStringPojo;
import com.mindcurv.commercetools.automation.pojo.ReferenceType;
import com.mindcurv.commercetools.automation.pojo.types.product.TypeAttribute;

import io.sphere.sdk.json.SphereJsonUtils;
import io.sphere.sdk.products.ProductDraft;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "key", "slug", "productType", "name", "masterVariant", "variants", "publish", "categories",
		"description", "taxCategory" })
public class ProductPojo extends DraftPojo {

	public static final String MESSAGETYPE = "product";

	private final static Logger LOG = LoggerFactory.getLogger(ProductPojo.class);

	@JsonProperty("productType")
	private ReferenceType productType;
	@JsonProperty("slug")
	private LocalizedStringPojo slug;
	@JsonProperty("name")
	private LocalizedStringPojo name;
	@JsonProperty("description")
	private LocalizedStringPojo description;

	@JsonProperty("metaDescription")
	private LocalizedStringPojo metaDescription;
	@JsonProperty("metaTitle")
	private LocalizedStringPojo metaTitle;
	@JsonProperty("searchKeywords")
	private SearchKeywords searchKeywords;

	@JsonProperty("masterVariant")
	private VariantPojo masterVariant;

	@JsonProperty("variants")
	private List<VariantPojo> variants = new ArrayList<>();

	@JsonProperty("publish")
	private Boolean publish;
	@JsonProperty("categories")
	private List<GenericTypeReferencePojo> categories;
	@JsonProperty("taxCategory")
	private GenericTypeReferencePojo taxCategory = new GenericTypeReferencePojo();

	
	private Map<String, VariantPojo> variantMap = new HashMap<>();

	public ProductPojo(Map<String, Object> value) {
		super(value);

		setMessageType(MESSAGETYPE);

		setName(TypeHelper.getNameObject(value, CsvConstants.NAME_DE_KEY, CsvConstants.NAME_EN_KEY));
		setSlug(TypeHelper.getNameObject(value, CsvConstants.SLUG_DE_KEY, CsvConstants.SLUG_EN_KEY));
		setDescription(
				TypeHelper.getNameObject(value, CsvConstants.DESCRIPTION_DE_KEY, CsvConstants.DESCRIPTION_EN_KEY));

		setMetaTitle(TypeHelper.getNameObject(value, CsvConstants.METATITLE_DE_KEY, CsvConstants.METATITLE_EN_KEY));
		setMetaDescription(TypeHelper.getNameObject(value, CsvConstants.METADESCRIPTION_DE_KEY,
				CsvConstants.METADESCRIPTION_EN_KEY));

		final String keywordsDeString = ValueHelper.getValueAsString(CsvConstants.METAKEYWORDS_DE_KEY, value);
		final String keywordsEnString = ValueHelper.getValueAsString(CsvConstants.METAKEYWORDS_EN_KEY, value);
		if (StringUtils.isNotBlank(keywordsDeString) || StringUtils.isNotBlank(keywordsEnString)) {
			SearchKeywords searchKeywords = new SearchKeywords();
			searchKeywords.setDe(getKeywordList(keywordsDeString));
			searchKeywords.setEn(getKeywordList(keywordsEnString));
			setSearchKeywords(searchKeywords);
		}
		// processMasterVariant(value);

		final String productTypeId = ValueHelper.getValueAsString(CsvConstants.Product.PRODUCTTYPE_KEY, value);
		if (StringUtils.isNotBlank(productTypeId)) {
			setProductTypeId(productTypeId);
		}
		taxCategory.setId(ValueHelper.getValueAsString(CsvConstants.Product.TAX_KEY, value));
		final String categoryIds = ValueHelper.getValueAsString(CsvConstants.Product.CATEGORIES_KEY, value);
		if (StringUtils.isNotBlank(categoryIds)) {
			categories = new ArrayList<>();
			for (String categoryId : categoryIds.split(",")) {
				categories.add(new GenericTypeReferencePojo().withId(categoryId.trim()));
			}
		}
		// custom fields

	}

	protected List<KeywordText> getKeywordList(final String keywordsString) {
		List<KeywordText> keywords = new ArrayList<>();
		if (StringUtils.isNotBlank(keywordsString)) {
			for (String keyword : keywordsString.split(",")) {
				keywords.add(new KeywordText().withText(keyword.trim()));
			}
		}
		return keywords;
	}

	@JsonProperty("slug")
	public LocalizedStringPojo getSlug() {
		return slug;
	}

	@JsonProperty("slug")
	public void setSlug(LocalizedStringPojo slug) {
		this.slug = slug;
	}

	@JsonProperty("productType")
	public ReferenceType getProductType() {
		return productType;
	}

	@JsonProperty("productType")
	public void setProductType(ReferenceType productType) {
		this.productType = productType;
	}

	@JsonProperty("name")
	public LocalizedStringPojo getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(LocalizedStringPojo name) {
		this.name = name;
	}

	@JsonProperty("masterVariant")
	public VariantPojo getMasterVariant() {
		return masterVariant;
	}

	@JsonProperty("masterVariant")
	public void setMasterVariant(VariantPojo masterVariant) {
		this.masterVariant = masterVariant;
	}

	@JsonProperty("variants")
	public List<VariantPojo> getVariants() {
		return variants;
	}

	@JsonProperty("variants")
	public void setVariants(List<VariantPojo> variants) {
		this.variants = variants;
		variantMap.clear();
		for (VariantPojo variantPojo : variants) {
			variantMap.put(variantPojo.getKey(), variantPojo);
		}
	}

	@JsonProperty("publish")
	public Boolean getPublish() {
		return publish;
	}

	@JsonProperty("publish")
	public void setPublish(Boolean publish) {
		this.publish = publish;
	}

	@JsonProperty("categories")
	public List<GenericTypeReferencePojo> getCategories() {
		return categories;
	}

	@JsonProperty("categories")
	public void setCategories(List<GenericTypeReferencePojo> categories) {
		this.categories = categories;
	}

	@JsonProperty("description")
	public LocalizedStringPojo getDescription() {
		return description;
	}

	@JsonProperty("description")
	public void setDescription(LocalizedStringPojo description) {
		this.description = description;
	}

	@JsonProperty("taxCategory")
	public GenericTypeReferencePojo getTaxCategory() {
		return taxCategory;
	}

	@JsonProperty("metaDescription")
	public LocalizedStringPojo getMetaDescription() {
		return metaDescription;
	}

	@JsonProperty("metaDescription")
	public void setMetaDescription(LocalizedStringPojo metaDescription) {
		this.metaDescription = metaDescription;
	}

	@JsonProperty("metaTitle")
	public LocalizedStringPojo getMetaTitle() {
		return metaTitle;
	}

	@JsonProperty("metaTitle")
	public void setMetaTitle(LocalizedStringPojo metaTitle) {
		this.metaTitle = metaTitle;
	}

	@JsonProperty("searchKeywords")
	public SearchKeywords getSearchKeywords() {
		return searchKeywords;
	}

	@JsonProperty("searchKeywords")
	public void setSearchKeywords(SearchKeywords searchKeywords) {
		this.searchKeywords = searchKeywords;
	}

	public ProductPojo withSearchKeywords(SearchKeywords searchKeywords) {
		this.searchKeywords = searchKeywords;
		return this;
	}

	@JsonProperty("taxCategory")
	public void setTaxCategory(GenericTypeReferencePojo taxCategory) {
		this.taxCategory = taxCategory;
	}

	public void setNameValues(String nameValue) {
		if (name == null) {
			name = new LocalizedStringPojo();
		}
		name.setDe(nameValue);
		name.setEn(nameValue);
	}

	public void setDescriptionValues(String descValue) {
		if (description == null) {
			description = new LocalizedStringPojo();
		}
		description.setDe(descValue);
		description.setEn(descValue);
	}

	public void setProductTypeId(final String productTypeId) {
		if (productType == null) {
			productType = new ReferenceType();
		}
		productType.setId(productTypeId);
	}

	public void setTaxCategoryId(final String taxCategoryId) {
		if (taxCategory == null) {
			taxCategory = new GenericTypeReferencePojo();
		}
		taxCategory.setId(taxCategoryId);
		taxCategory.setTypeId(ReferenceType.TAX_CATEGORY);
	}

	public String getRawKey() {
		return getKey().replaceAll("://", "_");
	}

	public void addVariant(VariantPojo variantPojo) {
		if (variantPojo.isMasterVariant()) {
			setMasterVariant(variantPojo);
		} else {
			variants.add(variantPojo);
		}
		variantMap.put(variantPojo.getKey(), variantPojo);
	}

	public VariantPojo getVariantByKey(final String key) {
		if (key == null) {
			return null;
		}
		if (getMasterVariant() != null && key.equals(getMasterVariant().getKey())) {
			return getMasterVariant();
		}
		return variantMap.get(key);
	}

	@Override
	public void addAttributes(Map<String, Object> value, Map<String, List<TypeAttribute>> attributesMap) {

		if (isCustomType()) {
			final String typeKey = getProductType().getId();
			final List<TypeAttribute> fullAttributeList = attributesMap.get(typeKey);
			final List<TypeAttribute> sameForAllList = fillSameForAllList(value, fullAttributeList);
			if (!sameForAllList.isEmpty()) {
				LOG.debug("Found {} attributes for type {} and 'SameForAll'", sameForAllList.size(), typeKey);

				for (VariantPojo variantPojo : getVariants()) {
					setVariantAttributes(value, variantPojo, sameForAllList);
				}
				setVariantAttributes(value, getMasterVariant(), sameForAllList);
			} else {
				LOG.debug("no additional attributes for type {}", typeKey);
			}
		} else {
			LOG.debug("no custom Type configured for Product {}", getKey());
		}

	}

	private List<TypeAttribute> fillSameForAllList(Map<String, Object> value,
			final List<TypeAttribute> fullAttributeList) {
		final List<TypeAttribute> sameForAllList = new ArrayList<>();
		if (fullAttributeList != null) {
			for (final TypeAttribute typeAttribute : fullAttributeList) {
				if ("SameForAll".equals(typeAttribute.getAttributeConstraint())
						&& value.keySet().contains(typeAttribute.getName())) {
					sameForAllList.add(typeAttribute);
				} else {
					LOG.debug("Skipping attribute {}", typeAttribute.getName());
				}
			}
		}
		return sameForAllList;
	}

	public void setVariantAttributes(Map<String, Object> value, final VariantPojo variantPojo,
			final List<TypeAttribute> attributeList) {
		if (variantPojo == null) {
			LOG.info("no VariantPojo provided");
			return;
		}
		if (attributeList == null || attributeList.isEmpty()) {
			LOG.debug("no attribute list for variant {} '{}' available", variantPojo.getVariantId(),
					variantPojo.getKey());
			return;
		}
		LOG.debug("processing attribute list ({}) for variant {} '{}' available", attributeList.size(),
				variantPojo.getVariantId(), variantPojo.getKey());

		final List<AttributePojo> attributes = new ArrayList<>();
		for (TypeAttribute typeAttribute : attributeList) {

			final String attributeName = typeAttribute.getName();

			if (!value.keySet().contains(attributeName)) {
				LOG.debug("Skipping {}, not present in sheet", typeAttribute.getName());
				continue;
			}

			final String attributeType = typeAttribute.getType().getName();
			Object valueFromSheet = null;
			if (TypeHelper.TYPE_BOOLEAN.equalsIgnoreCase(attributeType)) {
				valueFromSheet = ValueHelper.getValueAsBoolean(attributeName, value);
			} else if (TypeHelper.TYPE_NUMBER.equalsIgnoreCase(attributeType)) {
				valueFromSheet = ValueHelper.getValueAsDouble(attributeName, value);
			} else {
				valueFromSheet = ValueHelper.getValue(attributeName, value);
			}

			if (valueFromSheet != null) {
				LOG.debug("attribute {} / {} -> {}", attributeName, attributeType, valueFromSheet);
				final AttributePojo attributePojo = new AttributePojo();
				attributePojo.setName(attributeName);
				attributePojo.setValue(valueFromSheet);
				attributes.add(attributePojo);
			}
		}
		if (!attributes.isEmpty()) {
			variantPojo.addAttributesList(attributes);
		}
	}

	public boolean isCustomType() {
		return (getProductType() != null && StringUtils.isNotBlank(getProductType().getId()));
	}

	public ProductDraft toDraft() {
		final ProductDraft draft = SphereJsonUtils.readObject(toJsonString(), ProductDraft.class);
		LOG.debug("ProductDraft - {} - masterVariant: {} variants:{}", draft.getKey(),
				(draft.getMasterVariant() != null), draft.getVariants().size());
		LOG.debug("{}", SphereJsonUtils.toPrettyJsonString(draft));
		return draft;
	}

}

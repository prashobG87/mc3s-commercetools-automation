
package com.mindcurv.commercetools.automation.pojo.types.product;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mindcurv.commercetools.automation.helper.TypeHelper;
import com.mindcurv.commercetools.automation.helper.spreadsheet.ValueHelper;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;
import com.mindcurv.commercetools.automation.pojo.DraftPojo;
import com.mindcurv.commercetools.automation.pojo.LocalizedStringPojo;
import com.mindcurv.commercetools.automation.pojo.ReferenceType;

import io.sphere.sdk.json.SphereJsonUtils;
import io.sphere.sdk.products.attributes.AttributeConstraint;
import io.sphere.sdk.producttypes.ProductTypeDraft;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "name", "key", "description", "attributes" })
public class ProductTypePojo extends DraftPojo {
	private final static Logger LOG = LoggerFactory.getLogger(ProductTypePojo.class);

	public static final String MESSAGETYPE = "producttype";

	@JsonProperty("name")
	private String name;
	@JsonProperty("description")
	private String description;
	@JsonProperty("attributes")
	private List<TypeAttribute> attributes = new ArrayList<TypeAttribute>();

	public ProductTypePojo() {
		super();
		setMessageType(MESSAGETYPE);

	}

	public ProductTypePojo withSheetData(final Map<String, Object> value, final List<TypeAttribute> attributes) {
		setKey(ValueHelper.getValueAsString(CsvConstants.KEY_KEY, value));
		setName(ValueHelper.getValueAsString(CsvConstants.NAME_EN_KEY, value));
		setDescription(ValueHelper.getValueAsString(CsvConstants.DESCRIPTION_EN_KEY, value));
		setAttributes(attributes);
		return this;
	}

	public ProductTypePojo withSapDefaults() {
		
		final String[] textAttributeNames = {
				"sapIdocNum", "sapBaseUnit","sapAlternativeUnit","sapSalesOrg", "sapDistributionChannel", "sapDivision"
		};
		
		prepareAttributeArray(textAttributeNames, new ReferenceType().withName(TypeHelper.PRODUCTTYPE_STRING));
		
		final String[] dateAttributeNames = {
				"sapCreatedAt", "sapModifiedAt"
		};
		
		prepareAttributeArray(dateAttributeNames, new ReferenceType().withName(TypeHelper.PRODUCTTYPE_DATE));
		
		final String[] numberAttributeNames = {
				"sapUnitConversionNumerator", "sapUnitConversionDenominator"
		};
		prepareAttributeArray(numberAttributeNames, new ReferenceType().withName(TypeHelper.PRODUCTTYPE_NUMBER));
		
		return this;
	}

	public void prepareAttributeArray(final String[] attributeNames, final ReferenceType referenceType) {
		final String constraint = AttributeConstraint.SAME_FOR_ALL.toSphereName(); 
		for (String attributeName : attributeNames) {
			final TypeAttribute typeAttribute = new TypeAttribute()
					.withName(attributeName)
					.withLabel(new LocalizedStringPojo(attributeName, attributeName))
					.withAttributeConstraint(constraint)
					.withInputTip( new LocalizedStringPojo(attributeName, attributeName))
				;
			typeAttribute.setType(referenceType);
			addAttribute(typeAttribute);
		}
	}

	public ProductTypePojo withKey(String key) {
		setKey(key);
		return this;
	}

	public ProductTypePojo withName(String name) {
		this.name = name;
		return this;
	}

	public ProductTypePojo withDescription(String description) {
		this.description = description;
		return this;
	}

	public ProductTypePojo withAttributes(List<TypeAttribute> attributes) {
		this.attributes = attributes;
		return this;
	}

	public ProductTypePojo addAttribute(TypeAttribute attribute) {
		for (TypeAttribute existingAttribute : attributes) {
			if (existingAttribute.getName().equals(attribute.getName())) {
				updateAttribute(attribute, existingAttribute);
				return this;
			}
		}
		LOG.info("Adding new Attribute {}", attribute);
		attributes.add(attribute);
		return this;
	}

	public void updateAttribute(TypeAttribute newAttribute, TypeAttribute existingAttribute) {
		LOG.info("Updating existing Attribute {}", existingAttribute);
		existingAttribute.setAttributeConstraint(newAttribute.getAttributeConstraint());
		existingAttribute.setDisplayGroup(newAttribute.getDisplayGroup());
		existingAttribute.setInputHint(newAttribute.getInputHint());
		existingAttribute.setInputTip(newAttribute.getInputTip());
		existingAttribute.setIsRequired(newAttribute.getIsRequired());
		existingAttribute.setIsSearchable(newAttribute.getIsSearchable());
		existingAttribute.setLabel(newAttribute.getLabel());
		existingAttribute.setRequired(newAttribute.getIsRequired());
		existingAttribute.setType(newAttribute.getType());
		LOG.info("Updated Attribute {}", existingAttribute);
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("description")
	public void setDescription(String description) {
		this.description = description;
	}

	@JsonProperty("attributes")
	public List<TypeAttribute> getAttributes() {
		return attributes;
	}

	@JsonProperty("attributes")
	public void setAttributes(List<TypeAttribute> attributes) {
		this.attributes = attributes;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("name", name).append("key", getKey()).append("description", description)
				.append("attributes", attributes).toString();
	}

	public ProductTypeDraft toDraft() {
		// LOG.debug("ProductTypeDraft - {}", toJsonString());
		final ProductTypeDraft draft = SphereJsonUtils.readObject(toJsonString(), ProductTypeDraft.class);
		final int attributesSize = draft.getAttributes() == null ? 0 : draft.getAttributes().size();
		LOG.info("ProductTypeDraft - {} - {} attribute(s)", draft.getKey(), attributesSize);
		if (LOG.isDebugEnabled()) {
			LOG.debug("----");
			LOG.debug("{}", SphereJsonUtils.toPrettyJsonString(draft));
			LOG.debug("----");
		}
		return draft;
	}

}

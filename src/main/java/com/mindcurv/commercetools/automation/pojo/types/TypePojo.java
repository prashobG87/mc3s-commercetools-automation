
package com.mindcurv.commercetools.automation.pojo.types;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mindcurv.commercetools.automation.helper.spreadsheet.ValueHelper;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;
import com.mindcurv.commercetools.automation.pojo.types.product.TypeAttribute;

import io.sphere.sdk.json.SphereJsonUtils;
import io.sphere.sdk.types.TypeDraft;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "key", "name", "description", "resourceTypeIds", "fieldDefinitions" })
public class TypePojo extends BaseTypePojo {

	private final static Logger LOG = LoggerFactory.getLogger(TypePojo.class);

	public static final String MESSAGETYPE = "type";

	@JsonProperty("resourceTypeIds")
	private List<String> resourceTypeIds = null;
	@JsonProperty("fieldDefinitions")
	private List<TypeAttribute> fieldDefinitions = null;

	public TypePojo(final Map<String, Object> value) {
		super(value);
		
		setMessageType(MESSAGETYPE);

		setResourceTypeIds(ValueHelper.getValueAsList(CsvConstants.TYPE_KEY, value));

	}

	public TypePojo withKey(String key) {
		setKey(key);
		return this;
	}

	public TypePojo withResourceTypeIds(List<String> resourceTypeIds) {
		this.resourceTypeIds = resourceTypeIds;
		return this;
	}

	public TypePojo withFieldDefinitions(List<TypeAttribute> fieldDefinitions) {
		this.fieldDefinitions = fieldDefinitions;
		return this;
	}

	@JsonProperty("resourceTypeIds")
	public List<String> getResourceTypeIds() {
		return resourceTypeIds;
	}

	@JsonProperty("resourceTypeIds")
	public void setResourceTypeIds(List<String> resourceTypeIds) {
		this.resourceTypeIds = resourceTypeIds;
	}

	@JsonProperty("fieldDefinitions")
	public List<TypeAttribute> getFieldDefinitions() {
		return fieldDefinitions;
	}

	@JsonProperty("fieldDefinitions")
	public void setFieldDefinitions(List<TypeAttribute> fieldDefinitions) {
		this.fieldDefinitions = fieldDefinitions;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("key", getKey()).append("name", getName())
				.append("resourceTypeIds", resourceTypeIds).append("fieldDefinitions", fieldDefinitions).toString();
	}

	public TypeDraft toDraft() {
		final TypeDraft draft = SphereJsonUtils.readObject(toJsonString(), TypeDraft.class);
		LOG.info("TypeDraft - {}", draft.getKey());
		LOG.debug("TypeDraft - {}", SphereJsonUtils.toPrettyJsonString(draft));
		return draft;
	}
}

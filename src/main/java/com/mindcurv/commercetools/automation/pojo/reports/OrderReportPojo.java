package com.mindcurv.commercetools.automation.pojo.reports;

import java.util.Date;

import com.mindcurv.commercetools.automation.pojo.BasePojo;

public class OrderReportPojo extends BasePojo {
	private Date startDate;
	private Date endDate;

	
	public OrderReportPojo() {
		setStartDate(new Date()); 
		setEndDate(new Date());
		
	}


	public Date getStartDate() {
		return startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	

}

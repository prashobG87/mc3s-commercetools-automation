
package com.mindcurv.commercetools.automation.pojo.vue;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mindcurv.commercetools.automation.helper.VueHelper;
import com.mindcurv.commercetools.automation.pojo.BasePojo;

import io.sphere.sdk.categories.Category;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "entity_type_id",
    "attribute_set_id",
    "parent_id",
    "created_at",
    "updated_at",
    "position",
    "level",
    "children_count",
    "request_path",
    "is_active",
    "name",
    "url_key",
    "is_anchor",
    "id",
    "children_data"
})
public class VueCategoryPojo extends BasePojo{

    @JsonProperty("entity_type_id")
    private String entity_type_id = VueConstants.ENTITY_TYPE_ID_CATEGORY;
    @JsonProperty("attribute_set_id")
    private String attribute_set_id;
    @JsonProperty("parent_id")
    private String parent_id;
    @JsonProperty("created_at")
    private String created_at;
    @JsonProperty("updated_at")
    private String updated_at;
    @JsonProperty("position")
    private Integer position = Integer.valueOf(0);
    @JsonProperty("level")
    private Integer level = Integer.valueOf(1);
    @JsonProperty("request_path")
    private String request_path;
    @JsonProperty("is_active")
    private Boolean isActive = Boolean.TRUE;
    @JsonProperty("name")
    private String name;
    @JsonProperty("url_key")
    private String url_key;
    @JsonProperty("is_anchor")
    private Boolean is_anchor = Boolean.FALSE;
    @JsonProperty("children_data")
    private List<VueCategoryPojo> children_data = new ArrayList<VueCategoryPojo>();

    public VueCategoryPojo withCategory(Category category) {
    	
    	
		setId(VueHelper.replaceId(category.getId()));
    	setCreatedAt(VueConstants.DATE_FORMAT.format(category.getCreatedAt()));
		setUpdatedAt(VueConstants.DATE_FORMAT.format(category.getLastModifiedAt()));
    	if ( category.getParent() != null) {
    		setParentId(VueHelper.replaceId(category.getParent().getId()));
    	}
    	final Locale currentLocale = Locale.GERMAN;
		setName(category.getName().get(currentLocale));
		setRequestPath(category.getSlug().get(currentLocale));
		setUrlKey(getRequestPath());
    	
//    	position : Integer
//    	level : Integer
    	return this;
    }
    
    @JsonProperty("entity_type_id")
    public String getEntityTypeId() {
        return entity_type_id;
    }

    @JsonProperty("entity_type_id")
    public void setEntityTypeId(String entityTypeId) {
        this.entity_type_id = entityTypeId;
    }

    public VueCategoryPojo withEntityTypeId(String entityTypeId) {
        this.entity_type_id = entityTypeId;
        return this;
    }

    @JsonProperty("attribute_set_id")
    public String getAttributeSetId() {
        return attribute_set_id;
    }

    @JsonProperty("attribute_set_id")
    public void setAttributeSetId(String attributeSetId) {
        this.attribute_set_id = attributeSetId;
    }

    public VueCategoryPojo withAttributeSetId(String attributeSetId) {
        this.attribute_set_id = attributeSetId;
        return this;
    }

    @JsonProperty("parent_id")
    public String getParentId() {
        return parent_id;
    }

    @JsonProperty("parent_id")
    public void setParentId(String parentId) {
        this.parent_id = parentId;
    }

    public VueCategoryPojo withParentId(String parentId) {
        this.parent_id = parentId;
        return this;
    }

    @JsonProperty("created_at")
    public String getCreatedAt() {
        return created_at;
    }

    @JsonProperty("created_at")
    public void setCreatedAt(String createdAt) {
        this.created_at = createdAt;
    }

    public VueCategoryPojo withCreatedAt(String createdAt) {
        this.created_at = createdAt;
        return this;
    }

    @JsonProperty("updated_at")
    public String getUpdatedAt() {
        return updated_at;
    }

    @JsonProperty("updated_at")
    public void setUpdatedAt(String updatedAt) {
        this.updated_at = updatedAt;
    }

    public VueCategoryPojo withUpdatedAt(String updatedAt) {
        this.updated_at = updatedAt;
        return this;
    }

    @JsonProperty("position")
    public Integer getPosition() {
        return position;
    }

    @JsonProperty("position")
    public void setPosition(Integer position) {
        this.position = position;
    }

    public VueCategoryPojo withPosition(Integer position) {
        this.position = position;
        return this;
    }

    @JsonProperty("level")
    public Integer getLevel() {
        return level;
    }

    @JsonProperty("level")
    public void setLevel(Integer level) {
        this.level = level;
    }

    public VueCategoryPojo withLevel(Integer level) {
        this.level = level;
        return this;
    }

    @JsonProperty("children_count")
    public Integer getChildrenCount() {
    	if ( getChildrenData() == null) {
    		return Integer.valueOf(0); 
    	}
        return Integer.valueOf(getChildrenData().size());
    }

    @JsonProperty("request_path")
    public String getRequestPath() {
        return request_path;
    }

    @JsonProperty("request_path")
    public void setRequestPath(String requestPath) {
        this.request_path = requestPath;
    }

    public VueCategoryPojo withRequestPath(String requestPath) {
        this.request_path = requestPath;
        return this;
    }

    @JsonProperty("is_active")
    public Boolean getIsActive() {
        return isActive;
    }

    @JsonProperty("is_active")
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public VueCategoryPojo withIsActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public VueCategoryPojo withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("url_key")
    public String getUrlKey() {
        return url_key;
    }

    @JsonProperty("url_key")
    public void setUrlKey(String urlKey) {
        this.url_key = urlKey;
    }

    public VueCategoryPojo withUrlKey(String urlKey) {
        this.url_key = urlKey;
        return this;
    }

    @JsonProperty("is_anchor")
    public Boolean getIsAnchor() {
        return is_anchor;
    }

    @JsonProperty("is_anchor")
    public void setIsAnchor(Boolean isAnchor) {
        this.is_anchor = isAnchor;
    }

    public VueCategoryPojo withIsAnchor(Boolean isAnchor) {
        this.is_anchor = isAnchor;
        return this;
    }

   @JsonProperty("children_data")
    public List<VueCategoryPojo> getChildrenData() {
        return children_data;
    }

    @JsonProperty("children_data")
    public void setChildrenData(List<VueCategoryPojo> childrenData) {
        this.children_data = childrenData;
    }

    public VueCategoryPojo withChildrenData(List<VueCategoryPojo> childrenData) {
        this.children_data = childrenData;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("entityTypeId", entity_type_id).append("attributeSetId", attribute_set_id).append("parentId", parent_id).append("createdAt", created_at).append("updatedAt", updated_at).append("position", position).append("level", level).append("childrenCount", getChildrenCount()).append("requestPath", request_path).append("isActive", isActive).append("name", name).append("urlKey", url_key).append("isAnchor", is_anchor).append("id", getId()).append("childrenData", children_data).toString();
    }

}

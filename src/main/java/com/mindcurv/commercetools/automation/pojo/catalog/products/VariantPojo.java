
package com.mindcurv.commercetools.automation.pojo.catalog.products;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mindcurv.commercetools.automation.Constants;
import com.mindcurv.commercetools.automation.helper.spreadsheet.ValueHelper;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;
import com.mindcurv.commercetools.automation.pojo.ReferenceType;
import com.mindcurv.commercetools.automation.pojo.assets.Image;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "sku", "key", "attributes", "images" })
public class VariantPojo {

	private boolean essential;
	private String baseProduct;
	private Long variantId;

	@JsonProperty("sku")
	private String sku;
	@JsonProperty("key")
	private String key;
	@JsonProperty("prices")
	private List<PricePojo> prices = new ArrayList<>();
	@JsonProperty("attributes")
	private List<AttributePojo> attributes = new ArrayList<>();
	@JsonProperty("images")
	private List<Image> images = new ArrayList<>();

	public VariantPojo(Map<String, Object> value) {
		setEssential(ValueHelper.getValueAsBoolean(CsvConstants.ESSENTIAL, value));
		setSku(ValueHelper.getValueAsString(CsvConstants.Variant.SKU_KEY, value));
		setVariantId(ValueHelper.getValueAsLong(CsvConstants.Variant.VARIANT_ID_KEY, value));
		setKey(ValueHelper.getValueAsString(CsvConstants.Variant.KEY_KEY, value));
		setBaseProduct(ValueHelper.getValueAsString(CsvConstants.Variant.BASE_PRODUCT_KEY, value));
	}

	public boolean isEssential() {
		return essential;
	}

	public void setEssential(boolean essential) {
		this.essential = essential;
	}

	public String getBaseProduct() {
		return baseProduct;
	}

	public void setBaseProduct(String baseProduct) {
		this.baseProduct = baseProduct;
	}

	public Long getVariantId() {
		return variantId;
	}

	public void setVariantId(Long variantId) {
		this.variantId = variantId;
	}

	public boolean isMasterVariant() {
		return ( Long.valueOf(1).equals(getVariantId())) ; 
	}

	@JsonProperty("sku")
	public String getSku() {
		return sku;
	}

	@JsonProperty("sku")
	public void setSku(String sku) {
		this.sku = sku;
	}

	@JsonProperty("key")
	public String getKey() {
		return key;
	}

	@JsonProperty("key")
	public void setKey(String key) {
		this.key = key;
	}

	@JsonProperty("attributes")
	public List<AttributePojo> getAttributes() {
		return attributes;
	}

	@JsonProperty("attributes")
	public void setAttributes(List<AttributePojo> attributes) {
		this.attributes = attributes;
	}

	@JsonProperty("images")
	public List<Image> getImages() {
		return images;
	}

	@JsonProperty("images")
	public void setImages(List<Image> images) {
		this.images = images;
	}

	@JsonProperty("prices")
	public List<PricePojo> getPrices() {
		return prices;
	}

	@JsonProperty("prices")
	public void setPrices(List<PricePojo> prices) {
		this.prices = prices;
	}

	public void addAttribute(String name, Object value) {
		if (!Objects.isNull(value)) {
			AttributePojo attribute = new AttributePojo(name, value);
			attributes.add(attribute);
		}
	}

	public void addAttributeAsDate(String name, Long value) {
		if (!Objects.isNull(value)) {
			addAttribute(name, Constants.DATEFORMAT.format(new Date(value)));
		}
	}

	public void addAttributeAsEnumValue(String name, Object value) {
		if (!Objects.isNull(value)) {
			final Map<String, Object> map = new HashMap<>(2);
			map.put("label", value);
			map.put("key", value);
			addAttribute(name, map);
		}
	}

	public void addAttributeAsProductRelation(String name, Object value) {
		if (!Objects.isNull(value) ) {
			addRelationAttribute(name, value, ReferenceType.PRODUCT);
		}
	}

	public void addAttributeAsChannelRelation(String name, Object value) {
		if (!Objects.isNull(value)) {
			addRelationAttribute(name, value, ReferenceType.CHANNEL);
		}
	}

	public void addAttributeAsKeyValueRelation(String name, Object value) {
		if (!Objects.isNull(value)) {
			addRelationAttribute(name, value, ReferenceType.KEY_VALUE_DOCUMENT);
		}
	}

	private void addRelationAttribute(String name, Object value, final String typeId) {
		final Map<String, Object> enumMap = new HashMap<>(2);
		enumMap.put("id", value);
		enumMap.put("typeId", typeId);
		addAttribute(name, enumMap);
	}

	public void addImage(Image image) {
		if (image != null && !images.contains(image)) {
			images.add(image);
		}
	}

	public void addAttributesList(List<AttributePojo> additionalAttributes) {
		if (additionalAttributes != null && !additionalAttributes.isEmpty()) {
			this.attributes.addAll(additionalAttributes);
		}
	}

}

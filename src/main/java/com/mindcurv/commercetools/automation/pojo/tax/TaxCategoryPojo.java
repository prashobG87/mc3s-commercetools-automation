
package com.mindcurv.commercetools.automation.pojo.tax;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mindcurv.commercetools.automation.pojo.DraftPojo;
import com.mindcurv.commercetools.automation.helper.spreadsheet.ValueHelper;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;
import com.mindcurv.commercetools.automation.pojo.types.product.TypeAttribute;

import io.sphere.sdk.json.SphereJsonUtils;
import io.sphere.sdk.taxcategories.TaxCategoryDraft;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "name",
    "key",
    "description",
    "rates"
})
public class TaxCategoryPojo extends DraftPojo {

	private final static Logger LOG = LoggerFactory.getLogger(TaxCategoryPojo.class);
	
	public static final String MESSAGETYPE = "tax";

    @JsonProperty("name")
    private String name;
    @JsonProperty("description")
    private String description;
    @JsonProperty("rates")
    private List<TaxRatePojo> rates = new ArrayList<TaxRatePojo>();

    public TaxCategoryPojo() {
    	super(); 
    	setMessageType(MESSAGETYPE);
    }
    public TaxCategoryPojo withSheetData(final Map<String, Object> value, final List<TypeAttribute> attributes) {
		setKey(ValueHelper.getValueAsString(CsvConstants.KEY_KEY, value));
		setName(ValueHelper.getValueAsString(CsvConstants.TaxCategory.NAME_KEY, value));
		setDescription(ValueHelper.getValueAsString(CsvConstants.TaxCategory.DESCRIPTION_KEY, value));
		return this; 
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public TaxCategoryPojo withName(String name) {
        this.name = name;
        return this;
    }

    public TaxCategoryPojo withKey(String key) {
        setKey(key);
        return this;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public TaxCategoryPojo withDescription(String description) {
        this.description = description;
        return this;
    }

    @JsonProperty("rates")
    public List<TaxRatePojo> getRates() {
        return rates;
    }

    @JsonProperty("rates")
    public void setRates(List<TaxRatePojo> rates) {
        this.rates = rates;
    }

    public TaxCategoryPojo withRates(List<TaxRatePojo> rates) {
        this.rates = rates;
        return this;
    }

    public void addRate(TaxRatePojo rate) {
        if ( rates == null) {
        	rates = new ArrayList<>(); 
        }
        if (rate != null) {
        	rates.add(rate); 
        }
    }
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("name", name).append("key", getKey()).append("description", description).append("rates", rates).toString();
    }

    public TaxCategoryDraft toDraft() {
		final TaxCategoryDraft draft = SphereJsonUtils.readObject(toJsonString(), TaxCategoryDraft.class);
		LOG.info("TaxCategoryDraft - {}", draft.getKey());
		LOG.debug("{}", SphereJsonUtils.toPrettyJsonString(draft));
		return draft;
	}

}


package com.mindcurv.commercetools.automation.pojo.organization.customer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mindcurv.commercetools.automation.helper.spreadsheet.ValueHelper;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;
import com.mindcurv.commercetools.automation.pojo.organization.AddressPojo;
import com.mindcurv.commercetools.automation.pojo.types.BaseTypePojo;

import io.sphere.sdk.customers.CustomerDraft;
import io.sphere.sdk.json.SphereJsonUtils;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "customerNumber", "email", "firstName", "lastName", "middleName", "title", "password",
		"addresses", "defaultShippingAddressId", "defaultBillingAddressId", "shippingAddressIds", "billingAddressIds",
		"isEmailVerified", "customerGroup", "externalId", "dateOfBirth", "companyName", "vatId" })
public class CustomerPojo extends BaseTypePojo {

	private final static Logger LOG = LoggerFactory.getLogger(CustomerPojo.class);
	
	public static final String MESSAGETYPE = "customer";

	@JsonProperty(CsvConstants.Customer.CUSTOMER_NUMBER_KEY)
	private String customerNumber;
	@JsonProperty("email")
	private String email;
	@JsonProperty(CsvConstants.Customer.FIRST_NAME_KEY)
	private String firstName;
	@JsonProperty(CsvConstants.Customer.LAST_NAME_KEY)
	private String lastName;
	@JsonProperty("middleName")
	private String middleName;
	@JsonProperty("title")
	private String title;
	@JsonProperty(CsvConstants.Customer.PASSWORD_KEY)
	private String password;
	@JsonProperty("addresses")
	private List<AddressPojo> addresses = new ArrayList<AddressPojo>();
	@JsonProperty("defaultShippingAddressId")
	private String defaultShippingAddressId;
	@JsonProperty("defaultBillingAddressId")
	private String defaultBillingAddressId;
	@JsonProperty("shippingAddressIds")
	private List<String> shippingAddressIds = new ArrayList<String>();
	@JsonProperty("billingAddressIds")
	private List<String> billingAddressIds = new ArrayList<String>();
	@JsonProperty("isEmailVerified")
	private Boolean isEmailVerified = Boolean.FALSE;
	@JsonProperty("customerGroup")
	private CustomerGroupPojo customerGroup;
	@JsonProperty(CsvConstants.Customer.EXTERNAL_ID_KEY)
	private String externalId;
	@JsonProperty(CsvConstants.Customer.DATE_OF_BIRTH_KEY)
	private String dateOfBirth;
	@JsonProperty(CsvConstants.Customer.COMPANYNAME_KEY)
	private String companyName;
	@JsonProperty(CsvConstants.Customer.VAT_ID_KEY)
	private String vatId;

	public CustomerPojo(Map<String, Object> value) {
		super(value);
		
		setMessageType(MESSAGETYPE);
		
		//addresses.add(new AddressPojo().withSheetData(value));

		customerNumber = ValueHelper.getValueAsString(CsvConstants.Customer.CUSTOMER_NUMBER_KEY, value);
		firstName = ValueHelper.getValueAsString(CsvConstants.Customer.FIRST_NAME_KEY, value);
		lastName = ValueHelper.getValueAsString(CsvConstants.Customer.LAST_NAME_KEY, value);
		externalId = ValueHelper.getValueAsString(CsvConstants.Customer.EXTERNAL_ID_KEY, value);
		password = ValueHelper.getValueAsString(CsvConstants.Customer.PASSWORD_KEY, value);
		vatId = ValueHelper.getValueAsString(CsvConstants.Customer.VAT_ID_KEY, value);
		email = ValueHelper.getValueAsString(CsvConstants.Customer.EMAIL_KEY, value);
		companyName = ValueHelper.getValueAsString(CsvConstants.Customer.COMPANYNAME_KEY, value);
		dateOfBirth = ValueHelper.getValueAsString(CsvConstants.Customer.DATE_OF_BIRTH_KEY, value);

		final String customerGroupKey = ValueHelper.getValueAsString(CsvConstants.Customer.GROUP_KEY, value);
		if (StringUtils.isNotBlank(customerGroupKey)) {
			customerGroup = new CustomerGroupPojo();
			customerGroup.setKey(customerGroupKey);
		}
	}

	@JsonProperty(CsvConstants.Customer.CUSTOMER_NUMBER_KEY)
	public String getCustomerNumber() {
		return customerNumber;
	}

	@JsonProperty(CsvConstants.Customer.CUSTOMER_NUMBER_KEY)
	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public CustomerPojo withCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
		return this;
	}

	@JsonProperty("email")
	public String getEmail() {
		return email;
	}

	@JsonProperty("email")
	public void setEmail(String email) {
		this.email = email;
	}

	public CustomerPojo withEmail(String email) {
		this.email = email;
		return this;
	}

	@JsonProperty(CsvConstants.Customer.FIRST_NAME_KEY)
	public String getFirstName() {
		return firstName;
	}

	@JsonProperty(CsvConstants.Customer.FIRST_NAME_KEY)
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public CustomerPojo withFirstName(String firstName) {
		this.firstName = firstName;
		return this;
	}

	@JsonProperty(CsvConstants.Customer.LAST_NAME_KEY)
	public String getLastName() {
		return lastName;
	}

	@JsonProperty(CsvConstants.Customer.LAST_NAME_KEY)
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public CustomerPojo withLastName(String lastName) {
		this.lastName = lastName;
		return this;
	}

	@JsonProperty("middleName")
	public String getMiddleName() {
		return middleName;
	}

	@JsonProperty("middleName")
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public CustomerPojo withMiddleName(String middleName) {
		this.middleName = middleName;
		return this;
	}

	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

	@JsonProperty("title")
	public void setTitle(String title) {
		this.title = title;
	}

	public CustomerPojo withTitle(String title) {
		this.title = title;
		return this;
	}

	@JsonProperty(CsvConstants.Customer.PASSWORD_KEY)
	public String getPassword() {
		return password;
	}

	@JsonProperty(CsvConstants.Customer.PASSWORD_KEY)
	public void setPassword(String password) {
		this.password = password;
	}

	public CustomerPojo withPassword(String password) {
		this.password = password;
		return this;
	}

	@JsonProperty("addresses")
	public List<AddressPojo> getAddresses() {
		return addresses;
	}

	@JsonProperty("addresses")
	public void setAddresses(List<AddressPojo> addresses) {
		this.addresses = addresses;
	}

	public CustomerPojo withAddresses(List<AddressPojo> addresses) {
		this.addresses = addresses;
		return this;
	}

	@JsonProperty("defaultShippingAddressId")
	public String getDefaultShippingAddressId() {
		return defaultShippingAddressId;
	}

	@JsonProperty("defaultShippingAddressId")
	public void setDefaultShippingAddressId(String defaultShippingAddressId) {
		this.defaultShippingAddressId = defaultShippingAddressId;
	}

	public CustomerPojo withDefaultShippingAddressId(String defaultShippingAddressId) {
		this.defaultShippingAddressId = defaultShippingAddressId;
		return this;
	}

	@JsonProperty("defaultBillingAddressId")
	public String getDefaultBillingAddressId() {
		return defaultBillingAddressId;
	}

	@JsonProperty("defaultBillingAddressId")
	public void setDefaultBillingAddressId(String defaultBillingAddressId) {
		this.defaultBillingAddressId = defaultBillingAddressId;
	}

	public CustomerPojo withDefaultBillingAddressId(String defaultBillingAddressId) {
		this.defaultBillingAddressId = defaultBillingAddressId;
		return this;
	}

	@JsonProperty("shippingAddressIds")
	public List<String> getShippingAddressIds() {
		return shippingAddressIds;
	}

	@JsonProperty("shippingAddressIds")
	public void setShippingAddressIds(List<String> shippingAddressIds) {
		this.shippingAddressIds = shippingAddressIds;
	}

	public CustomerPojo withShippingAddressIds(List<String> shippingAddressIds) {
		this.shippingAddressIds = shippingAddressIds;
		return this;
	}

	@JsonProperty("billingAddressIds")
	public List<String> getBillingAddressIds() {
		return billingAddressIds;
	}

	@JsonProperty("billingAddressIds")
	public void setBillingAddressIds(List<String> billingAddressIds) {
		this.billingAddressIds = billingAddressIds;
	}

	public CustomerPojo withBillingAddressIds(List<String> billingAddressIds) {
		this.billingAddressIds = billingAddressIds;
		return this;
	}

	@JsonProperty("isEmailVerified")
	public Boolean getIsEmailVerified() {
		return isEmailVerified;
	}

	@JsonProperty("isEmailVerified")
	public void setIsEmailVerified(Boolean isEmailVerified) {
		this.isEmailVerified = isEmailVerified;
	}

	public CustomerPojo withIsEmailVerified(Boolean isEmailVerified) {
		this.isEmailVerified = isEmailVerified;
		return this;
	}

	@JsonProperty("customerGroup")
	public CustomerGroupPojo getCustomerGroup() {
		return customerGroup;
	}

	@JsonProperty("customerGroup")
	public void setCustomerGroup(CustomerGroupPojo customerGroup) {
		this.customerGroup = customerGroup;
	}

	public CustomerPojo withCustomerGroup(CustomerGroupPojo customerGroup) {
		this.customerGroup = customerGroup;
		return this;
	}

	@JsonProperty(CsvConstants.Customer.EXTERNAL_ID_KEY)
	public String getExternalId() {
		return externalId;
	}

	@JsonProperty(CsvConstants.Customer.EXTERNAL_ID_KEY)
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public CustomerPojo withExternalId(String externalId) {
		this.externalId = externalId;
		return this;
	}

	@JsonProperty(CsvConstants.Customer.DATE_OF_BIRTH_KEY)
	public String getDateOfBirth() {
		return dateOfBirth;
	}

	@JsonProperty(CsvConstants.Customer.DATE_OF_BIRTH_KEY)
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public CustomerPojo withDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
		return this;
	}

	@JsonProperty(CsvConstants.Customer.COMPANYNAME_KEY)
	public String getCompanyName() {
		return companyName;
	}

	@JsonProperty(CsvConstants.Customer.COMPANYNAME_KEY)
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public CustomerPojo withCompanyName(String companyName) {
		this.companyName = companyName;
		return this;
	}

	@JsonProperty(CsvConstants.Customer.VAT_ID_KEY)
	public String getVatId() {
		return vatId;
	}

	@JsonProperty(CsvConstants.Customer.VAT_ID_KEY)
	public void setVatId(String vatId) {
		this.vatId = vatId;
	}

	public CustomerPojo withVatId(String vatId) {
		this.vatId = vatId;
		return this;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", getId())
				.append(CsvConstants.Customer.CUSTOMER_NUMBER_KEY, customerNumber).append("email", email)
				.append(CsvConstants.Customer.FIRST_NAME_KEY, firstName)
				.append(CsvConstants.Customer.LAST_NAME_KEY, lastName).append("middleName", middleName)
				.append("title", title).append(CsvConstants.Customer.PASSWORD_KEY, password)
				.append("addresses", addresses).append("defaultShippingAddressId", defaultShippingAddressId)
				.append("defaultBillingAddressId", defaultBillingAddressId)
				.append("shippingAddressIds", shippingAddressIds).append("billingAddressIds", billingAddressIds)
				.append("isEmailVerified", isEmailVerified).append("customerGroup", customerGroup)
				.append(CsvConstants.Customer.EXTERNAL_ID_KEY, externalId)
				.append(CsvConstants.Customer.DATE_OF_BIRTH_KEY, dateOfBirth)
				.append(CsvConstants.Customer.COMPANYNAME_KEY, companyName).append(CsvConstants.Customer.VAT_ID_KEY, vatId)
				.toString();
	}

	public CustomerDraft toDraft() {
		final CustomerDraft draft = SphereJsonUtils.readObject(toJsonString(), CustomerDraft.class);
		LOG.info("CustomerDraft - {}", draft.getEmail());
		LOG.debug("{}", SphereJsonUtils.toPrettyJsonString(draft));
		return draft;
	}
}

package com.mindcurv.commercetools.automation.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class BasePojo {

	@JsonProperty("id")
	private String id;

	public BasePojo() {

	}
	@JsonProperty("id")
	public String getId() {
		return id;
	}
	@JsonProperty("id")
	public void setId(String id) {
		this.id = id;
	}
	public BasePojo withId(String id) {
		setId(id);
		return this;
	}
	
	public String toJsonString() {
		final Gson gsonBuilder = new GsonBuilder().setPrettyPrinting().create();
		return gsonBuilder.toJson(this);
	}
}


package com.mindcurv.commercetools.automation.pojo.catalog.products;

import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mindcurv.commercetools.automation.Constants;
import com.mindcurv.commercetools.automation.helper.spreadsheet.ValueHelper;
import com.mindcurv.commercetools.automation.pojo.DraftPojo;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;
import com.mindcurv.commercetools.automation.pojo.GenericTypeReferencePojo;
import com.mindcurv.commercetools.automation.pojo.PriceValue;

import io.sphere.sdk.json.SphereJsonUtils;
import io.sphere.sdk.products.PriceDraft;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "value", "country", "channel", "customerGroup", "validFrom", "validUntil" })
public class PricePojo extends DraftPojo {

	private final static Logger LOG = LoggerFactory.getLogger(PricePojo.class);
	
	private static final String MESSAGETYPE = "price"; 

	@JsonProperty("sku")
	private String sku;
	@JsonProperty("value")
	private PriceValue value = new PriceValue();
	@JsonProperty("country")
	private String country;
	@JsonProperty("channel")
	private GenericTypeReferencePojo channel;
	@JsonProperty("customerGroup")
	private GenericTypeReferencePojo customerGroup;
	@JsonProperty("validFrom")
	private String validFrom;
	@JsonProperty("validUntil")
	private String validUntil;

	public PricePojo() {
	}

	public PricePojo(Map<String, Object> values) {
		super(values);
		
		setMessageType(MESSAGETYPE);
		
		setSku(ValueHelper.getValueAsString(CsvConstants.Price.SKU_KEY, values));

		final Integer valueAsCentAmount = ValueHelper.getValueAsCentAmount(CsvConstants.Price.PRICE_KEY, values);
		if (valueAsCentAmount != null) {
			PriceValue priceValue = new PriceValue().withCentAmount(valueAsCentAmount)
					.withCurrencyCode(ValueHelper.getValueAsString(CsvConstants.Price.CURRENCY_KEY, values));
			setValue(priceValue);
		}

		final String countryCode = ValueHelper.getValueAsString(CsvConstants.Price.COUNTRY_KEY, values);
		if (StringUtils.isNotBlank(countryCode)) {
			setCountry(countryCode);
		}
		final String channelId = ValueHelper.getValueAsString(CsvConstants.Price.CHANNEL_KEY, values);
		if (StringUtils.isNotBlank(channelId)) {
			setChannel(new GenericTypeReferencePojo().withId(channelId));
		}
		final String customerGroupId = ValueHelper.getValueAsString(CsvConstants.Price.CUSTOMER_GROUP_KEY, values);
		if (StringUtils.isNotEmpty(customerGroupId)) {
			setCustomerGroup(new GenericTypeReferencePojo().withId(customerGroupId).withTypeId("customer-group"));
		}
		final Date validFromDate = ValueHelper.getValueAsDate(CsvConstants.Price.VALID_FROM_KEY, values);
		if (validFromDate != null) {
			setValidFrom(Constants.DATETIMEFORMAT.format(validFromDate));
		}
		final Date validUntilDate = ValueHelper.getValueAsDate(CsvConstants.Price.VALID_UNTIL_KEY, values);
		if (validUntilDate != null) {
			setValidUntil(Constants.DATETIMEFORMAT.format(validUntilDate));
		}
	}

	public PricePojo withSku(String sku) {
		this.sku = sku;
		return this;
	}

	public PricePojo withCountry(String country) {
		this.country = country;
		return this;
	}

	public PricePojo withChannel(GenericTypeReferencePojo channel) {
		this.channel = channel;
		return this;
	}

	public PricePojo withChannel(String channelId) {
		GenericTypeReferencePojo channel = new GenericTypeReferencePojo();
		channel.setId(channelId);
		this.channel = channel;
		return this;
	}

	@JsonProperty("value")
	public PriceValue getValue() {
		return value;
	}

	@JsonProperty("value")
	public void setValue(PriceValue value) {
		this.value = value;
	}

	@JsonProperty("sku")
	public String getSku() {
		return sku;
	}

	@JsonProperty("sku")
	public void setSku(String sku) {
		this.sku = sku;
	}

	@JsonProperty("country")
	public String getCountry() {
		return country;
	}

	@JsonProperty("country")
	public void setCountry(String country) {
		this.country = country;
	}

	@JsonProperty("channel")
	public GenericTypeReferencePojo getChannel() {
		return channel;
	}

	@JsonProperty("channel")
	public void setChannel(GenericTypeReferencePojo channel) {
		this.channel = channel;
	}

	@JsonProperty("customerGroup")
	public GenericTypeReferencePojo getCustomerGroup() {
		return customerGroup;
	}

	@JsonProperty("customerGroup")
	public void setCustomerGroup(GenericTypeReferencePojo customerGroup) {
		this.customerGroup = customerGroup;
	}

	@JsonProperty("validFrom")
	public String getValidFrom() {
		return validFrom;
	}

	@JsonProperty("validFrom")
	public void setValidFrom(String validFrom) {
		this.validFrom = validFrom;
	}

	@JsonProperty("validUntil")
	public String getValidUntil() {
		return validUntil;
	}

	@JsonProperty("validUntil")
	public void setValidUntil(String validUntil) {
		this.validUntil = validUntil;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("value", value).append("country", country).append("channel", channel)
				.append("customerGroup", customerGroup).append("validFroms", validFrom).append("validUntil", validUntil)
				.toString();
	}

	public String getUniqueKey() {
		final StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(sku.replace("://", "").replaceAll(" ", "").replaceAll("/", "_"));
		stringBuffer.append("_").append(value.getCurrencyCode());
		if (StringUtils.isNotBlank(country)) {
			stringBuffer.append("_co").append(country);
		}
		if (channel != null) {
			stringBuffer.append("_ch").append(channel.getId());
		}
		if (customerGroup != null) {
			stringBuffer.append("_cf").append(customerGroup.getId());
		}
		if (validFrom != null) {
			stringBuffer.append("_fd").append(validFrom);
		}
		if (validUntil != null) {
			stringBuffer.append("_ud").append(validUntil);
		}
		return stringBuffer.toString();
	}

	public PriceDraft toDraft() {
		final PriceDraft draft = SphereJsonUtils.readObject(toJsonString(), PriceDraft.class);
		LOG.debug("PriceDraft - {} : {}", getUniqueKey() , draft.getValue());
		LOG.debug("{}", SphereJsonUtils.toPrettyJsonString(draft));
		return draft;
	}
}

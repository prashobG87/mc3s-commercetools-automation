package com.mindcurv.commercetools.automation.pojo.catalog.products;

import java.util.HashMap;
import java.util.Map;

public class AdditionalProperties {

	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	
	public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}

	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

}


package com.mindcurv.commercetools.automation.pojo;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import io.sphere.sdk.models.LocalizedString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "en", "de" })
public class LocalizedStringPojo {

	private static final String NOT_PROVIDED = "NOT PROVIDED";
	@JsonProperty("en")
	private String en;
	@JsonProperty("de")
	private String de;

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public LocalizedStringPojo() {
	}

	/**
	 * 
	 * @param de
	 * @param en
	 */
	public LocalizedStringPojo(String en, String de) {
		super();
		this.en = en;
		this.de = de;
	}

	@JsonProperty("en")
	public String getEn() {
		return StringUtils.isNotBlank(en) ? en : NOT_PROVIDED;
	}

	@JsonProperty("en")
	public void setEn(String en) {
		this.en = en;
	}

	@JsonProperty("de")
	public String getDe() {
		return StringUtils.isNotBlank(de) ? de : NOT_PROVIDED;
	}

	@JsonProperty("de")
	public void setDe(String de) {
		this.de = de;
	}
	
	public LocalizedStringPojo withEn(String en) {
        setEn(en);
        return this;
    }

    public LocalizedStringPojo  withDe(String de) {
        setDe(de);
        return this;
    }

	public LocalizedString getLocalizedString() {
		Map<Locale, String> map = new HashMap<>();
		map.put(Locale.GERMAN, getDe());
		map.put(Locale.ENGLISH, getEn());
		return LocalizedString.of(map);
	}

}

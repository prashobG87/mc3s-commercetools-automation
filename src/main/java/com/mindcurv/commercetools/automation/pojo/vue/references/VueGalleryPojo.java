
package com.mindcurv.commercetools.automation.pojo.vue.references;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "image",
    "pos",
    "typ",
    "lab"
})
public class VueGalleryPojo {

    @JsonProperty("image")
    private String image;
    @JsonProperty("pos")
    private Integer pos;
    @JsonProperty("typ")
    private String typ;
    @JsonProperty("lab")
    private String lab;

    @JsonProperty("image")
    public String getImage() {
        return image;
    }

    @JsonProperty("image")
    public void setImage(String image) {
        this.image = image;
    }

    public VueGalleryPojo withImage(String image) {
        this.image = image;
        return this;
    }

    @JsonProperty("pos")
    public Integer getPos() {
        return pos;
    }

    @JsonProperty("pos")
    public void setPos(Integer pos) {
        this.pos = pos;
    }

    public VueGalleryPojo withPos(Integer pos) {
        this.pos = pos;
        return this;
    }

    @JsonProperty("typ")
    public String getTyp() {
        return typ;
    }

    @JsonProperty("typ")
    public void setTyp(String typ) {
        this.typ = typ;
    }

    public VueGalleryPojo withTyp(String typ) {
        this.typ = typ;
        return this;
    }

    @JsonProperty("lab")
    public String getLab() {
        return lab;
    }

    @JsonProperty("lab")
    public void setLab(String lab) {
        this.lab = lab;
    }

    public VueGalleryPojo withLab(String lab) {
        this.lab = lab;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("image", image).append("pos", pos).append("typ", typ).append("lab", lab).toString();
    }

}

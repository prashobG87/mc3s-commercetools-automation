package com.mindcurv.commercetools.automation.pojo.vue;

import java.time.format.DateTimeFormatter;

public interface VueConstants {
	public static String ENTITY_TYPE_ID_PRODUCT = "4";
	public static String ENTITY_TYPE_ID_CATEGORY = "3";
	
	public static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	
	public static final Integer VISIBILITY_NOTVISIBLE = new Integer(1); 
	public static final Integer VISIBILITY_CATALOG = new Integer(2); 
	public static final Integer VISIBILITY_SEARCH = new Integer(3); 
	public static final Integer VISIBILITY_BOTH = new Integer(4); 
	
	//https://docs.magento.com/m1/ce/user_guide/catalog/product-types.html
	public static final String PRODUCT_TYPE_SIMPLE = "simple"; 
	public static final String PRODUCT_TYPE_GROUPED = "grouped"; 
	public static final String PRODUCT_TYPE_CONFIGURABLE = "configurable"; 
	public static final String PRODUCT_TYPE_VIRTUAL = "virtual"; 
	public static final String PRODUCT_TYPE_BUNDLE = "bundle"; 
	public static final String PRODUCT_TYPE_DOWNLOADABLE = "downloadable"; 
	
}

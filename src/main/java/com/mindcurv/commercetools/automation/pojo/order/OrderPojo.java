
package com.mindcurv.commercetools.automation.pojo.order;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mindcurv.commercetools.automation.pojo.Custom;
import com.mindcurv.commercetools.automation.pojo.GenericTypeReferencePojo;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "orderNumber",
    "orderState",
    "shipmentState",
    "paymentState",
    "state",
    "custom"
})
public class OrderPojo {

    @JsonProperty("id")
    private String id;
    @JsonProperty("orderNumber")
    private String orderNumber;
    @JsonProperty("orderState")
    private String orderState;
    @JsonProperty("shipmentState")
    private String shipmentState;
    @JsonProperty("paymentState")
    private String paymentState;
    @JsonProperty("state")
    private GenericTypeReferencePojo state;
    @JsonProperty("custom")
    private Custom custom;

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    public OrderPojo withId(String id) {
        this.id = id;
        return this;
    }

    @JsonProperty("orderNumber")
    public String getOrderNumber() {
        return orderNumber;
    }

    @JsonProperty("orderNumber")
    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public OrderPojo withOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
        return this;
    }

    @JsonProperty("orderState")
    public String getOrderState() {
        return orderState;
    }

    @JsonProperty("orderState")
    public void setOrderState(String orderState) {
        this.orderState = orderState;
    }

    public OrderPojo withOrderState(String orderState) {
        this.orderState = orderState;
        return this;
    }

    @JsonProperty("shipmentState")
    public String getShipmentState() {
        return shipmentState;
    }

    @JsonProperty("shipmentState")
    public void setShipmentState(String shipmentState) {
        this.shipmentState = shipmentState;
    }

    public OrderPojo withShipmentState(String shipmentState) {
        this.shipmentState = shipmentState;
        return this;
    }

    @JsonProperty("paymentState")
    public String getPaymentState() {
        return paymentState;
    }

    @JsonProperty("paymentState")
    public void setPaymentState(String paymentState) {
        this.paymentState = paymentState;
    }

    public OrderPojo withPaymentState(String paymentState) {
        this.paymentState = paymentState;
        return this;
    }

    @JsonProperty("state")
    public GenericTypeReferencePojo getState() {
        return state;
    }

    @JsonProperty("state")
    public void setState(GenericTypeReferencePojo state) {
        this.state = state;
    }

    public OrderPojo withState(GenericTypeReferencePojo state) {
        this.state = state;
        return this;
    }

    @JsonProperty("custom")
    public Custom getCustom() {
        return custom;
    }

    @JsonProperty("custom")
    public void setCustom(Custom custom) {
        this.custom = custom;
    }

    public OrderPojo withCustom(Custom custom) {
        this.custom = custom;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("orderNumber", orderNumber).append("orderState", orderState).append("shipmentState", shipmentState).append("paymentState", paymentState).append("state", state).append("custom", custom).toString();
    }

}


package com.mindcurv.commercetools.automation.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "typeId",
    "key",
    "id"
})
public class GenericTypeReferencePojo {

    @JsonProperty("typeId")
    private String typeId;
    @JsonProperty("id")
    private String id;
    @JsonProperty("key")
    private String key;
    
    public GenericTypeReferencePojo withTypeId(String typeId) {
        this.typeId = typeId;
        return this;
    }

    public GenericTypeReferencePojo withKey(String key) {
	    setKey(key);
	    return this;
	}

	public GenericTypeReferencePojo withId(String id) {
	    this.id = id;
	    return this;
	}

	@JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }
    @JsonProperty("key")
    public String getKey() {
        return key;
    }

    @JsonProperty("key")
    public void setKey(String key) {
        this.key = key;
    }

    @JsonProperty("typeId")
	public String getTypeId() {
	    return typeId;
	}

	@JsonProperty("typeId")
	public void setTypeId(String typeId) {
	    this.typeId = typeId;
	}

	@Override
    public String toString() {
        return new ToStringBuilder(this).append("typeId", typeId).append("id", id).toString();
    }

}


package com.mindcurv.commercetools.automation.pojo.catalog.products;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "en",
    "de"
})
public class SearchKeywords {

    @JsonProperty("en")
    private List<KeywordText> en = new ArrayList<KeywordText>();
    @JsonProperty("de")
    private List<KeywordText> de = new ArrayList<KeywordText>();

    @JsonProperty("en")
    public List<KeywordText> getEn() {
        return en;
    }

    @JsonProperty("en")
    public void setEn(List<KeywordText> en) {
        this.en = en;
    }

    public SearchKeywords withEn(List<KeywordText> en) {
        this.en = en;
        return this;
    }

    @JsonProperty("de")
    public List<KeywordText> getDe() {
        return de;
    }

    @JsonProperty("de")
    public void setDe(List<KeywordText> de) {
        this.de = de;
    }

    public SearchKeywords withDe(List<KeywordText> de) {
        this.de = de;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("en", en).append("de", de).toString();
    }

}


package com.mindcurv.commercetools.automation.pojo.tax;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mindcurv.commercetools.automation.helper.spreadsheet.ValueHelper;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;
import com.mindcurv.commercetools.automation.pojo.types.product.TypeAttribute;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "name",
    "amount",
    "includedInPrice",
    "country",
    "state"
})
public class TaxRatePojo {
	
	private String taxKey ; 

    @JsonProperty("name")
    private String name;
    @JsonProperty("amount")
    private Double amount;
    @JsonProperty("includedInPrice")
    private Boolean includedInPrice;
    @JsonProperty("country")
    private String country;
    @JsonProperty("state")
    private String state;
    
    public TaxRatePojo withSheetData(final Map<String, Object> value, final List<TypeAttribute> attributes) {
		setTaxKey(ValueHelper.getValueAsString(CsvConstants.TaxRate.TAXKEY_KEY, value));
    	setName(ValueHelper.getValueAsString(CsvConstants.TaxRate.NAME_KEY, value));
		setAmount(ValueHelper.getValueAsDouble(CsvConstants.TaxRate.AMOUNT_KEY, value));
		setCountry(ValueHelper.getValueAsString(CsvConstants.TaxRate.COUNTRY_KEY, value));
		setState(ValueHelper.getValueAsString(CsvConstants.TaxRate.STATE_KEY, value));
		setIncludedInPrice(ValueHelper.getValueAsBoolean(CsvConstants.TaxRate.INCLUDED_IN_PRICE_KEY, value));
		
		return this; 
    }

    public String getTaxKey() {
		return taxKey;
	}

	public void setTaxKey(String taxKey) {
		this.taxKey = taxKey;
	}

	@JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public TaxRatePojo withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("amount")
    public Double getAmount() {
        return amount;
    }

    @JsonProperty("amount")
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public TaxRatePojo withAmount(Double amount) {
        this.amount = amount;
        return this;
    }

    @JsonProperty("includedInPrice")
    public Boolean getIncludedInPrice() {
        return includedInPrice;
    }

    @JsonProperty("includedInPrice")
    public void setIncludedInPrice(Boolean includedInPrice) {
        this.includedInPrice = includedInPrice;
    }

    public TaxRatePojo withIncludedInPrice(Boolean includedInPrice) {
        this.includedInPrice = includedInPrice;
        return this;
    }

    @JsonProperty("country")
    public String getCountry() {
        return country;
    }

    @JsonProperty("country")
    public void setCountry(String country) {
        this.country = country;
    }

    public TaxRatePojo withCountry(String country) {
        this.country = country;
        return this;
    }

    @JsonProperty("state")
    public String getState() {
        return state;
    }

    @JsonProperty("state")
    public void setState(String state) {
        this.state = state;
    }

    public TaxRatePojo withState(String state) {
        this.state = state;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("taxKey", taxKey).append("name", name).append("amount", amount).append("includedInPrice", includedInPrice).append("country", country).append("state", state).toString();
    }

}

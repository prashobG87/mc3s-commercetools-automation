
package com.mindcurv.commercetools.automation.pojo.customobject.products;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "content"
})
public class TablePojo {

    @JsonProperty("content")
    private List<TableEntryPojo> content = new ArrayList<TableEntryPojo>();

    @JsonProperty("content")
    public List<TableEntryPojo> getContent() {
        return content;
    }

    @JsonProperty("content")
    public void setContent(List<TableEntryPojo> content) {
        this.content = content;
    }

    public TablePojo withContent(List<TableEntryPojo> content) {
        this.content = content;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("content", content).toString();
    }

}

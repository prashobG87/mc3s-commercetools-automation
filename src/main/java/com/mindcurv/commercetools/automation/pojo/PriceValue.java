
package com.mindcurv.commercetools.automation.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "type",
    "currencyCode",
    "centAmount",
    "fractionDigits"
})
public class PriceValue {

    @JsonProperty("type")
    private String type = "centPrecision";
    @JsonProperty("currencyCode")
    private String currencyCode;
    @JsonProperty("centAmount")
    private Integer centAmount;
    @JsonProperty("fractionDigits")
    private Integer fractionDigits = Integer.valueOf(2);

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    public PriceValue withType(String type) {
        this.type = type;
        return this;
    }

    @JsonProperty("currencyCode")
    public String getCurrencyCode() {
        return currencyCode;
    }

    @JsonProperty("currencyCode")
    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public PriceValue withCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
        return this;
    }

    @JsonProperty("centAmount")
    public Integer getCentAmount() {
        return centAmount;
    }

    @JsonProperty("centAmount")
    public void setCentAmount(Integer centAmount) {
        this.centAmount = centAmount;
    }

    public PriceValue withCentAmount(Integer centAmount) {
        this.centAmount = centAmount;
        return this;
    }

    @JsonProperty("fractionDigits")
    public Integer getFractionDigits() {
        return fractionDigits;
    }

    @JsonProperty("fractionDigits")
    public void setFractionDigits(Integer fractionDigits) {
        this.fractionDigits = fractionDigits;
    }

    public PriceValue withFractionDigits(Integer fractionDigits) {
        this.fractionDigits = fractionDigits;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", type).append("currencyCode", currencyCode).append("centAmount", centAmount).append("fractionDigits", fractionDigits).toString();
    }

}

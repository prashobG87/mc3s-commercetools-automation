
package com.mindcurv.commercetools.automation.pojo.vue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mindcurv.commercetools.automation.helper.VueHelper;
import com.mindcurv.commercetools.automation.pojo.BasePojo;
import com.mindcurv.commercetools.automation.pojo.vue.references.VueCategoryReference;
import com.mindcurv.commercetools.automation.pojo.vue.references.VueGalleryPojo;
import com.mindcurv.commercetools.automation.pojo.vue.references.VueStockPojo;

import io.sphere.sdk.categories.Category;
import io.sphere.sdk.models.Reference;
import io.sphere.sdk.products.Image;
import io.sphere.sdk.products.Price;
import io.sphere.sdk.products.Product;
import io.sphere.sdk.products.ProductData;
import io.sphere.sdk.products.ProductVariant;
import io.sphere.sdk.products.ProductVariantAvailability;
import io.sphere.sdk.producttypes.ProductType;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id","product_type_name","entity_type_id", "attribute_set_id", "type_id", "sku", "has_options", "required_options",
		"created_at", "updated_at", "status", "visibility", "tax_class_id", "description", "short_description", "name",
		"meta_keyword", "meta_title", "meta_description", "small_image", "options_container", "url_path", "image",
		"thumbnail", "media_gallery", "gallery", "image_label", "small_image_label", "thumbnail_label", "price",
		"special_price", "is_salable", "stock_item",  "category", "stock" })
public class VueProductPojo extends BasePojo {

	@JsonProperty("entity_type_id")
	private String entity_type_id = VueConstants.ENTITY_TYPE_ID_PRODUCT;
	@JsonProperty("attribute_set_id")
	private String attribute_set_id;
	@JsonProperty("type_id")
	private String type_id;
	@JsonProperty("sku")
	private String sku;
	@JsonProperty("has_options")
	private Boolean hasOptions;
	@JsonProperty("required_options")
	private Integer required_options;
	@JsonProperty("created_at")
	private String created_at;
	@JsonProperty("updated_at")
	private String updated_at;
	@JsonProperty("status")
	private Integer status;
	@JsonProperty("visibility")
	private Integer visibility = VueConstants.VISIBILITY_BOTH;
	@JsonProperty("tax_class_id")
	private String tax_class_id;
	@JsonProperty("description")
	private String description;
	@JsonProperty("short_description")
	private String short_description;
	@JsonProperty("name")
	private String name;
	@JsonProperty("meta_keyword")
	private String meta_keyword;
	@JsonProperty("meta_title")
	private String meta_title;
	@JsonProperty("meta_description")
	private String meta_description;
	@JsonProperty("small_image")
	private String small_image;
	@JsonProperty("options_container")
	private String options_container;
	@JsonProperty("url_path")
	private String url_path;
	@JsonProperty("image")
	private String image;
	@JsonProperty("thumbnail")
	private String thumbnail;
	@JsonProperty("media_gallery")
	private List<VueGalleryPojo> media_gallery = new ArrayList<VueGalleryPojo>();
	@JsonProperty("gallery")
	private List<VueGalleryPojo> gallery = new ArrayList<VueGalleryPojo>();
	@JsonProperty("image_label")
	private String image_label;
	@JsonProperty("small_image_label")
	private String small_image_label;
	@JsonProperty("thumbnail_label")
	private String thumbnail_label;
	@JsonProperty("price")
	private Double price;
	@JsonProperty("special_price")
	private Double special_price;
	@JsonProperty("is_salable")
	private Boolean is_salable;
	@JsonProperty("stock_item")
	private Map<String, Object> stock_item;
	@JsonProperty("category")
	private List<VueCategoryReference> category = new ArrayList<VueCategoryReference>();
	@JsonProperty("stock")
	private VueStockPojo stock;

	// custom
	@JsonProperty("product_type_name")
	private String product_type_name;

	public VueProductPojo withProduct(Product product, Map<String, Category> categoryCacheMap,
			Map<String, ProductType> producttypeCacheMap) {
		final ProductData baseProduct = product.getMasterData().getCurrent();
		final ProductVariant masterVariant = baseProduct.getMasterVariant();
		setSku(masterVariant.getSku());

		if (baseProduct.getVariants().isEmpty()) {
			setTypeId(VueConstants.PRODUCT_TYPE_SIMPLE);
			setHasOptions(Boolean.FALSE);
			// "required_options": 0,
		} else {
			setTypeId(VueConstants.PRODUCT_TYPE_CONFIGURABLE);
			setHasOptions(Boolean.TRUE);
		}

		setId(VueHelper.replaceId(product.getId()));
		setCreatedAt(VueConstants.DATE_FORMAT.format(product.getCreatedAt()));
		setUpdatedAt(VueConstants.DATE_FORMAT.format(product.getLastModifiedAt()));
		setTaxClassId(VueHelper.replaceId(product.getTaxCategory().getId()));
		setStatus(1); // ?

		setupStock(masterVariant);
		setupPrices(masterVariant);
		setupMedia(masterVariant);
		final Locale currentLocale = Locale.GERMAN;
		setupCategories(baseProduct, categoryCacheMap, currentLocale);
		setupLocalizedAttributes(baseProduct, currentLocale);

		setupCustomVueAttributes(product, producttypeCacheMap);
		// "gift_message_available": null
		// "options_container": "container1"

		return this;
	}
	
	private void setupLocalizedAttributes(final ProductData productData, final Locale currentLocale) {
		setName(productData.getName().get(currentLocale));
		setUrlPath(productData.getSlug().get(currentLocale));
		if (productData.getMetaTitle() != null) {
			setMetaTitle(productData.getMetaTitle().get(currentLocale));
		}
		if (productData.getMetaTitle() != null) {
			setMetaDescription(productData.getMetaDescription().get(currentLocale));
		}
		if (productData.getMetaKeywords() != null) {
			setMetaKeyword(productData.getMetaKeywords().get(currentLocale));
		}
		if (productData.getDescription() != null) {
			setDescription(productData.getDescription().get(currentLocale));
		}
		setShortDescription("N/A:shortDescription");
	}

	private void setupPrices(final ProductVariant variant) {
		for (Price price : variant.getPrices()) {
			if (getPrice() == null) {
				setPrice(price.getValue().getNumber().doubleValue());
			}
		}
	}

	private void setupMedia(final ProductVariant variant) {
		for (final Image image : variant.getImages()) {
			setImage(image.getUrl());
			setImageLabel(image.getLabel());
			setSmallImage(image.getUrl());
			setSmallImageLabel(image.getLabel());
			setThumbnail(image.getUrl());
			setThumbnailLabel(image.getLabel());
			break; // only the first
		}
		setGallery(Collections.emptyList());
		setMediaGallery(Collections.emptyList());
		setStockItem(Collections.emptyMap());
	}

	private void setupCategories(final ProductData productData, Map<String, Category> categoryCacheMap,
			final Locale currentLocale) {
		final List<VueCategoryReference> vueCategories = new ArrayList<>();
		for (Reference<Category> category : productData.getCategories()) {
			final String categoryId = VueHelper.replaceId(category.getId());
			final VueCategoryReference categoryPojo = new VueCategoryReference().withCategoryId(categoryId);
			final Category categoryFromCache = categoryCacheMap.get(categoryId);

			if (categoryFromCache != null) {
				final String categoryName = categoryFromCache.getName().get(currentLocale);
				categoryPojo.withName(categoryName);
			}
			vueCategories.add(categoryPojo);
		}
		setCategory(vueCategories);
	}

	private void setupStock(final ProductVariant variant) {
		final ProductVariantAvailability availability = variant.getAvailability();
		final Boolean isOnStock = availability == null ? Boolean.FALSE : availability.isOnStock();
		setStock(new VueStockPojo().withIsInStock(isOnStock));
		boolean isSalableToSet = (getPrice() != null && isOnStock.booleanValue());
		setIsSalable(Boolean.valueOf(isSalableToSet));
	}
	private void setupCustomVueAttributes(Product product, Map<String, ProductType> producttypeCacheMap) {
		final Reference<ProductType> productTypeReference = product.getProductType();
		if (productTypeReference != null) {
			ProductType productType = producttypeCacheMap.get(VueHelper.replaceId(productTypeReference.getId()));
			if (productType != null) {
				setProductTypeName(productType.getKey());
			}
		}
	}

	

	@JsonProperty("entity_type_id")
	public String getEntityTypeId() {
		return entity_type_id;
	}

	@JsonProperty("entity_type_id")
	public void setEntityTypeId(String entityTypeId) {
		this.entity_type_id = entityTypeId;
	}

	public VueProductPojo withEntityTypeId(String entityTypeId) {
		this.entity_type_id = entityTypeId;
		return this;
	}

	@JsonProperty("attribute_set_id")
	public String getAttributeSetId() {
		return attribute_set_id;
	}

	@JsonProperty("attribute_set_id")
	public void setAttributeSetId(String attributeSetId) {
		this.attribute_set_id = attributeSetId;
	}

	public VueProductPojo withAttributeSetId(String attributeSetId) {
		this.attribute_set_id = attributeSetId;
		return this;
	}

	@JsonProperty("type_id")
	public String getTypeId() {
		return type_id;
	}

	@JsonProperty("type_id")
	public void setTypeId(String typeId) {
		this.type_id = typeId;
	}

	public VueProductPojo withTypeId(String typeId) {
		this.type_id = typeId;
		return this;
	}

	@JsonProperty("sku")
	public String getSku() {
		return sku;
	}

	@JsonProperty("sku")
	public void setSku(String sku) {
		this.sku = sku;
	}

	public VueProductPojo withSku(String sku) {
		this.sku = sku;
		return this;
	}

	@JsonProperty("has_options")
	public Boolean getHasOptions() {
		return hasOptions;
	}

	@JsonProperty("has_options")
	public void setHasOptions(Boolean hasOptions) {
		this.hasOptions = hasOptions;
	}

	public VueProductPojo withHasOptions(Boolean hasOptions) {
		this.hasOptions = hasOptions;
		return this;
	}

	@JsonProperty("required_options")
	public Integer getRequiredOptions() {
		return required_options;
	}

	@JsonProperty("required_options")
	public void setRequiredOptions(Integer requiredOptions) {
		this.required_options = requiredOptions;
	}

	public VueProductPojo withRequiredOptions(Integer requiredOptions) {
		this.required_options = requiredOptions;
		return this;
	}

	@JsonProperty("created_at")
	public String getCreatedAt() {
		return created_at;
	}

	@JsonProperty("created_at")
	public void setCreatedAt(String createdAt) {
		this.created_at = createdAt;
	}

	public VueProductPojo withCreatedAt(String createdAt) {
		this.created_at = createdAt;
		return this;
	}

	@JsonProperty("updated_at")
	public String getUpdatedAt() {
		return updated_at;
	}

	@JsonProperty("updated_at")
	public void setUpdatedAt(String updatedAt) {
		this.updated_at = updatedAt;
	}

	public VueProductPojo withUpdatedAt(String updatedAt) {
		this.updated_at = updatedAt;
		return this;
	}

	@JsonProperty("status")
	public Integer getStatus() {
		return status;
	}

	@JsonProperty("status")
	public void setStatus(Integer status) {
		this.status = status;
	}

	public VueProductPojo withStatus(Integer status) {
		this.status = status;
		return this;
	}

	@JsonProperty("visibility")
	public Integer getVisibility() {
		return visibility;
	}

	@JsonProperty("visibility")
	public void setVisibility(Integer visibility) {
		this.visibility = visibility;
	}

	public VueProductPojo withVisibility(Integer visibility) {
		this.visibility = visibility;
		return this;
	}

	@JsonProperty("tax_class_id")
	public String getTaxClassId() {
		return tax_class_id;
	}

	@JsonProperty("tax_class_id")
	public void setTaxClassId(String taxClassId) {
		this.tax_class_id = taxClassId;
	}

	public VueProductPojo withTaxClassId(String taxClassId) {
		this.tax_class_id = taxClassId;
		return this;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("description")
	public void setDescription(String description) {
		this.description = description;
	}

	public VueProductPojo withDescription(String description) {
		this.description = description;
		return this;
	}

	@JsonProperty("short_description")
	public String getShortDescription() {
		return short_description;
	}

	@JsonProperty("short_description")
	public void setShortDescription(String shortDescription) {
		this.short_description = shortDescription;
	}

	public VueProductPojo withShortDescription(String shortDescription) {
		this.short_description = shortDescription;
		return this;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	public VueProductPojo withName(String name) {
		this.name = name;
		return this;
	}

	@JsonProperty("meta_keyword")
	public String getMetaKeyword() {
		return meta_keyword;
	}

	@JsonProperty("meta_keyword")
	public void setMetaKeyword(String metaKeyword) {
		this.meta_keyword = metaKeyword;
	}

	public VueProductPojo withMetaKeyword(String metaKeyword) {
		this.meta_keyword = metaKeyword;
		return this;
	}

	@JsonProperty("meta_title")
	public String getMetaTitle() {
		return meta_title;
	}

	@JsonProperty("meta_title")
	public void setMetaTitle(String metaTitle) {
		this.meta_title = metaTitle;
	}

	public VueProductPojo withMetaTitle(String metaTitle) {
		this.meta_title = metaTitle;
		return this;
	}

	@JsonProperty("meta_description")
	public String getMetaDescription() {
		return meta_description;
	}

	@JsonProperty("meta_description")
	public void setMetaDescription(String metaDescription) {
		this.meta_description = metaDescription;
	}

	public VueProductPojo withMetaDescription(String metaDescription) {
		this.meta_description = metaDescription;
		return this;
	}

	@JsonProperty("small_image")
	public String getSmallImage() {
		return small_image;
	}

	@JsonProperty("small_image")
	public void setSmallImage(String smallImage) {
		this.small_image = smallImage;
	}

	public VueProductPojo withSmallImage(String smallImage) {
		this.small_image = smallImage;
		return this;
	}

	@JsonProperty("options_container")
	public String getOptionsContainer() {
		return options_container;
	}

	@JsonProperty("options_container")
	public void setOptionsContainer(String optionsContainer) {
		this.options_container = optionsContainer;
	}

	public VueProductPojo withOptionsContainer(String optionsContainer) {
		this.options_container = optionsContainer;
		return this;
	}

	@JsonProperty("url_path")
	public String getUrlPath() {
		return url_path;
	}

	@JsonProperty("url_path")
	public void setUrlPath(String urlPath) {
		this.url_path = urlPath;
	}

	public VueProductPojo withUrlPath(String urlPath) {
		this.url_path = urlPath;
		return this;
	}

	@JsonProperty("image")
	public String getImage() {
		return image;
	}

	@JsonProperty("image")
	public void setImage(String image) {
		this.image = image;
	}

	public VueProductPojo withImage(String image) {
		this.image = image;
		return this;
	}

	@JsonProperty("thumbnail")
	public String getThumbnail() {
		return thumbnail;
	}

	@JsonProperty("thumbnail")
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public VueProductPojo withThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
		return this;
	}

	@JsonProperty("media_gallery")
	public List<VueGalleryPojo> getMediaGallery() {
		return media_gallery;
	}

	@JsonProperty("media_gallery")
	public void setMediaGallery(List<VueGalleryPojo> mediaGallery) {
		this.media_gallery = mediaGallery;
	}

	public VueProductPojo withMediaGallery(List<VueGalleryPojo> mediaGallery) {
		this.media_gallery = mediaGallery;
		return this;
	}

	@JsonProperty("gallery")
	public List<VueGalleryPojo> getGallery() {
		return gallery;
	}

	@JsonProperty("gallery")
	public void setGallery(List<VueGalleryPojo> gallery) {
		this.gallery = gallery;
	}

	public VueProductPojo withGallery(List<VueGalleryPojo> gallery) {
		this.gallery = gallery;
		return this;
	}

	@JsonProperty("image_label")
	public String getImageLabel() {
		return image_label;
	}

	@JsonProperty("image_label")
	public void setImageLabel(String imageLabel) {
		this.image_label = imageLabel;
	}

	public VueProductPojo withImageLabel(String imageLabel) {
		this.image_label = imageLabel;
		return this;
	}

	@JsonProperty("small_image_label")
	public String getSmallImageLabel() {
		return small_image_label;
	}

	@JsonProperty("small_image_label")
	public void setSmallImageLabel(String smallImageLabel) {
		this.small_image_label = smallImageLabel;
	}

	public VueProductPojo withSmallImageLabel(String smallImageLabel) {
		this.small_image_label = smallImageLabel;
		return this;
	}

	@JsonProperty("thumbnail_label")
	public String getThumbnailLabel() {
		return thumbnail_label;
	}

	@JsonProperty("thumbnail_label")
	public void setThumbnailLabel(String thumbnailLabel) {
		this.thumbnail_label = thumbnailLabel;
	}

	public VueProductPojo withThumbnailLabel(String thumbnailLabel) {
		this.thumbnail_label = thumbnailLabel;
		return this;
	}

	@JsonProperty("price")
	public Double getPrice() {
		return price;
	}

	@JsonProperty("price")
	public void setPrice(Double price) {
		this.price = price;
	}

	public VueProductPojo withPrice(Double price) {
		this.price = price;
		return this;
	}

	@JsonProperty("special_price")
	public Double getSpecialPrice() {
		return special_price;
	}

	@JsonProperty("special_price")
	public void setSpecialPrice(Double specialPrice) {
		this.special_price = specialPrice;
	}

	public VueProductPojo withSpecialPrice(Double specialPrice) {
		this.special_price = specialPrice;
		return this;
	}

	@JsonProperty("is_salable")
	public Boolean getIsSalable() {
		return is_salable;
	}

	@JsonProperty("is_salable")
	public void setIsSalable(Boolean isSalable) {
		this.is_salable = isSalable;
	}

	public VueProductPojo withIsSalable(Boolean isSalable) {
		this.is_salable = isSalable;
		return this;
	}

	@JsonProperty("stock_item")
	public Map<String, Object> getStockItem() {
		return stock_item;
	}

	@JsonProperty("stock_item")
	public void setStockItem(Map<String, Object> stockItem) {
		this.stock_item = stockItem;
	}

	public VueProductPojo withStockItem(Map<String, Object> stockItem) {
		this.stock_item = stockItem;
		return this;
	}

	@JsonProperty("category")
	public List<VueCategoryReference> getCategory() {
		return category;
	}

	@JsonProperty("category")
	public void setCategory(List<VueCategoryReference> category) {
		this.category = category;
	}

	public VueProductPojo withCategory(List<VueCategoryReference> category) {
		this.category = category;
		return this;
	}

	@JsonProperty("stock")
	public VueStockPojo getStock() {
		return stock;
	}

	@JsonProperty("stock")
	public void setStock(VueStockPojo stock) {
		this.stock = stock;
	}

	public VueProductPojo withStock(VueStockPojo stock) {
		this.stock = stock;
		return this;
	}

	@JsonProperty("product_type_name")
	public String getProductTypeName() {
		return product_type_name;
	}

	@JsonProperty("product_type_name")
	public void setProductTypeName(String productTypeName) {
		this.product_type_name = productTypeName;
	}

	public VueProductPojo withProductTypeName(String productTypeName) {
		this.product_type_name = productTypeName;
		return this;
	}

	

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("entityTypeId", entity_type_id).append("attributeSetId", attribute_set_id)
				.append("typeId", type_id).append("sku", sku).append("hasOptions", hasOptions)
				.append("requiredOptions", required_options).append("createdAt", created_at)
				.append("updatedAt", updated_at).append("status", status).append("visibility", visibility)
				.append("taxClassId", tax_class_id).append("description", description)
				.append("shortDescription", short_description).append("name", name).append("metaKeyword", meta_keyword)
				.append("metaTitle", meta_title).append("metaDescription", meta_description)
				.append("smallImage", small_image).append("optionsContainer", options_container)
				.append("urlPath", url_path).append("image", image).append("thumbnail", thumbnail)
				.append("mediaGallery", media_gallery).append("gallery", gallery).append("imageLabel", image_label)
				.append("smallImageLabel", small_image_label).append("thumbnailLabel", thumbnail_label)
				.append("price", price).append("specialPrice", special_price).append("isSalable", is_salable)
				.append("stockItem", stock_item).append("id", getId()).append("category", category)
				.append("stock", stock).toString();
	}

}

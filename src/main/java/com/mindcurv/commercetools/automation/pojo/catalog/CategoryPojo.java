
package com.mindcurv.commercetools.automation.pojo.catalog;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mindcurv.commercetools.automation.helper.TypeHelper;
import com.mindcurv.commercetools.automation.helper.spreadsheet.ValueHelper;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;
import com.mindcurv.commercetools.automation.pojo.Custom;
import com.mindcurv.commercetools.automation.pojo.GenericTypeReferencePojo;
import com.mindcurv.commercetools.automation.pojo.LocalizedStringPojo;
import com.mindcurv.commercetools.automation.pojo.types.BaseTypePojo;

import io.sphere.sdk.categories.CategoryDraft;
import io.sphere.sdk.json.SphereJsonUtils;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "key", "version", "name", "slug", "description", "parent", "orderHint", "createdAt",
		"externalId", "custom", "assets" })
public class CategoryPojo extends BaseTypePojo {
	private final static Logger LOG = LoggerFactory.getLogger(CategoryPojo.class);

	public static final String MESSAGETYPE = "category";

	@JsonProperty("slug")
	private LocalizedStringPojo slug;
	@JsonProperty("parent")
	private GenericTypeReferencePojo parent;
	@JsonProperty("orderHint")
	private String orderHint;
	@JsonProperty("createdAt")
	private String createdAt;
	@JsonProperty("externalId")
	private String externalId;
	@JsonProperty("assets")
	private List<Object> assets;

	public CategoryPojo() {
		super(); 
		setMessageType(MESSAGETYPE);
	}
			
	public CategoryPojo(Map<String, Object> value) {

		super(value);
		setMessageType(MESSAGETYPE);
		
		externalId = ValueHelper.getValueAsString(CsvConstants.Category.EXTERNALID_KEY, value);
		final String parentKey = ValueHelper.getValueAsString(CsvConstants.Category.PARENT_KEY, value);
		if (StringUtils.isNotBlank(parentKey)) {
			parent = new GenericTypeReferencePojo().withId(parentKey);
		}
		orderHint = ValueHelper.getValueAsString(CsvConstants.Category.ORDERHINT_KEY, value);

		setSlug(TypeHelper.getNameObject(value, CsvConstants.SLUG_DE_KEY,
				CsvConstants.SLUG_EN_KEY));

	}

	public CategoryPojo withId(String id) {
		setId(id);
		return this;
	}

	public CategoryPojo withKey(String key) {
		setKey(key);
		return this;
	}

	@JsonProperty("slug")
	public LocalizedStringPojo getSlug() {
		return slug;
	}

	@JsonProperty("slug")
	public void setSlug(LocalizedStringPojo slug) {
		this.slug = slug;
	}

	public CategoryPojo withSlug(LocalizedStringPojo slug) {
		this.slug = slug;
		return this;
	}

	@JsonProperty("parent")
	public GenericTypeReferencePojo getParent() {
		return parent;
	}

	@JsonProperty("parent")
	public void setParent(GenericTypeReferencePojo parent) {
		this.parent = parent;
	}

	public CategoryPojo withParent(GenericTypeReferencePojo parent) {
		this.parent = parent;
		return this;
	}

	@JsonProperty("orderHint")
	public String getOrderHint() {
		return orderHint;
	}

	@JsonProperty("orderHint")
	public void setOrderHint(String orderHint) {
		this.orderHint = orderHint;
	}

	public CategoryPojo withOrderHint(String orderHint) {
		this.orderHint = orderHint;
		return this;
	}

	@JsonProperty("createdAt")
	public String getCreatedAt() {
		return createdAt;
	}

	@JsonProperty("createdAt")
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public CategoryPojo withCreatedAt(String createdAt) {
		this.createdAt = createdAt;
		return this;
	}

	@JsonProperty("externalId")
	public String getExternalId() {
		return externalId;
	}

	@JsonProperty("externalId")
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public CategoryPojo withExternalId(String externalId) {
		this.externalId = externalId;
		return this;
	}

	public CategoryPojo withCustom(Custom custom) {
		setCustom(custom);
		return this;
	}

	@JsonProperty("assets")
	public List<Object> getAssets() {
		return assets;
	}

	@JsonProperty("assets")
	public void setAssets(List<Object> assets) {
		this.assets = assets;
	}

	public CategoryPojo withAssets(List<Object> assets) {
		this.assets = assets;
		return this;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", getId()).append("key", getKey()).append("name", getName())
				.append("slug", slug).append("description", getDescription()).append("parent", parent)
				.append("orderHint", orderHint).append("createdAt", createdAt).append("externalId", externalId)
				.append("custom", getCustom()).append("assets", assets).toString();
	}

	public CategoryDraft toDraft() {
		final CategoryDraft draft = SphereJsonUtils.readObject(toJsonString(), CategoryDraft.class);
		LOG.debug("CategoryDraft - {}, isRoot?{}", draft.getKey(), (draft.getParent() == null));
		LOG.debug("{}", SphereJsonUtils.toPrettyJsonString(draft));
		return draft;
	}

}

package com.mindcurv.commercetools.automation.pojo.types;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mindcurv.commercetools.automation.helper.TypeHelper;
import com.mindcurv.commercetools.automation.helper.spreadsheet.ValueHelper;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;
import com.mindcurv.commercetools.automation.pojo.DraftPojo;
import com.mindcurv.commercetools.automation.pojo.Custom;
import com.mindcurv.commercetools.automation.pojo.GenericTypeReferencePojo;
import com.mindcurv.commercetools.automation.pojo.LocalizedStringPojo;
import com.mindcurv.commercetools.automation.pojo.types.product.TypeAttribute;

public class BaseTypePojo extends DraftPojo {

	private final static Logger LOG = LoggerFactory.getLogger(BaseTypePojo.class);

	@JsonProperty("name")
	private LocalizedStringPojo name;

	@JsonProperty("description")
	private LocalizedStringPojo description;

	
	@JsonProperty("custom")
	private Custom custom;

	@JsonProperty("custom")
	public Custom getCustom() {
		return custom;
	}

	@JsonProperty("custom")
	public void setCustom(Custom custom) {
		this.custom = custom;
	}

	public BaseTypePojo() {
		super();
	}

	@JsonProperty("name")
	public LocalizedStringPojo getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(LocalizedStringPojo name) {
		this.name = name;
	}

	public BaseTypePojo withName(LocalizedStringPojo name) {
		this.name = name;
		return this;
	}

	@JsonProperty("description")
	public LocalizedStringPojo getDescription() {
		return description;
	}

	@JsonProperty("description")
	public void setDescription(LocalizedStringPojo description) {
		this.description = description;
	}

	public BaseTypePojo withDescription(LocalizedStringPojo description) {
		this.description = description;
		return this;
	}

	public BaseTypePojo(Map<String, Object> value) {
		super(value); 
		setId(ValueHelper.getValueAsString(CsvConstants.KEY_KEY, value));
		setKey(ValueHelper.getValueAsString(CsvConstants.KEY_KEY, value));
		
		setName(TypeHelper.getNameObject(value, CsvConstants.NAME_DE_KEY, CsvConstants.NAME_EN_KEY));
		setDescription(TypeHelper.getNameObject(value, CsvConstants.DESCRIPTION_DE_KEY, CsvConstants.DESCRIPTION_EN_KEY));
		
		final String typeKey = ValueHelper.getValueAsString(CsvConstants.Category.TYPE_KEY, value);
		if (StringUtils.isNotBlank(typeKey)) {
			final GenericTypeReferencePojo type = new GenericTypeReferencePojo()
					.withId(typeKey) //will be translated later
					.withKey(typeKey);
			setCustom(new Custom().withType(type));
		}
	}

	
	@Override
	public void addAttributes(Map<String, Object> value, Map<String, List<TypeAttribute>> attributesMap) {

		if (custom != null && custom.getType() != null) {
			String typeKey = custom.getType().getId(); // key is translated to id in snyc API
			if ( typeKey == null) {
				typeKey = custom.getType().getKey(); //lookup for non sync items
			}
			LOG.debug("{}", attributesMap.keySet());
			final List<TypeAttribute> attributeList = attributesMap.get(typeKey);
			if (attributeList != null && !attributeList.isEmpty()) {
				LOG.debug("Found {} attributes for type {}", attributeList.size(), typeKey);

				Map<String, Object> fields = new HashMap<>();
				for (TypeAttribute typeAttribute : attributeList) {
					Object valueFromSheet = null;
					if (TypeHelper.TYPE_BOOLEAN.equalsIgnoreCase(typeAttribute.getType().getName())) {
						valueFromSheet = ValueHelper.getValueAsBoolean(typeAttribute.getName(), value);
					} else {
						valueFromSheet = ValueHelper.getValue(typeAttribute.getName(), value);
					}

					if (valueFromSheet != null) {
						LOG.debug("attribute {} / {} -> {}", typeAttribute.getName(), typeAttribute.getType().getName(),
								valueFromSheet);
						fields.put(typeAttribute.getName(), valueFromSheet);
					} else {
						LOG.debug("attribute {} / {} -> {}", typeAttribute.getName(), typeAttribute.getType().getName(),
								valueFromSheet);

					}
				}
				if (!fields.isEmpty()) {
					custom.setFields(fields);
				}

			} else {
				LOG.debug("no addditional attributes for type {}", typeKey);
			}
		}
	}

}

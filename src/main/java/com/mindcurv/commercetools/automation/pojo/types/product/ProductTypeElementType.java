
package com.mindcurv.commercetools.automation.pojo.types.product;

import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
    "name",
    "referenceTypeId",
    "values"
})
public class ProductTypeElementType {

    @JsonProperty("name")
    private String name;
    @JsonProperty("referenceTypeId")
    private String referenceTypeId;
    @JsonProperty("values")
    private List<ProductTypeValue> values = null;

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public ProductTypeElementType withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("referenceTypeId")
    public String getReferenceTypeId() {
        return referenceTypeId;
    }

    @JsonProperty("referenceTypeId")
    public void setReferenceTypeId(String referenceTypeId) {
        this.referenceTypeId = referenceTypeId;
    }

    public ProductTypeElementType withReferenceTypeId(String referenceTypeId) {
        this.referenceTypeId = referenceTypeId;
        return this;
    }

    @JsonProperty("values")
    public List<ProductTypeValue> getValues() {
        return values;
    }

    @JsonProperty("values")
    public void setValues(List<ProductTypeValue> values) {
        this.values = values;
    }

    public ProductTypeElementType withValues(List<ProductTypeValue> values) {
        this.values = values;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("name", name).append("referenceTypeId", referenceTypeId).append("values", values).toString();
    }
}


package com.mindcurv.commercetools.automation.pojo.shipping;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mindcurv.commercetools.automation.pojo.DraftPojo;
import com.mindcurv.commercetools.automation.helper.spreadsheet.ValueHelper;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;
import com.mindcurv.commercetools.automation.pojo.types.product.TypeAttribute;

import io.sphere.sdk.json.SphereJsonUtils;
import io.sphere.sdk.zones.ZoneDraft;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "name", "description", "locations" })
public class ShippingZonePojo extends DraftPojo {
	private final static Logger LOG = LoggerFactory.getLogger(ShippingZonePojo.class);

	@JsonProperty("name")
	private String name;
	@JsonProperty("description")
	private String description;
	@JsonProperty("locations")
	private List<ShippingLocation> locations = new ArrayList<ShippingLocation>();

	public ShippingZonePojo withSheetData(final Map<String, Object> value, final List<TypeAttribute> attributes) {
		setName(ValueHelper.getValueAsString(CsvConstants.ShippingZone.NAME_KEY, value));
		setDescription(ValueHelper.getValueAsString(CsvConstants.ShippingZone.DESCRIPTION_KEY, value));
		final String locationsKeys = ValueHelper.getValueAsString(CsvConstants.ShippingZone.LOCATIONS_KEY, value); 
		if ( StringUtils.isNotBlank(locationsKeys)) {
			for ( String locationCountry : locationsKeys.split(CsvConstants.SPLITTOKEN)) {
				final ShippingLocation shippingLocation = new ShippingLocation().withCountry(locationCountry.trim()); 
				locations.add(shippingLocation); 
			}
		}
		return this;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	public ShippingZonePojo withName(String name) {
		this.name = name;
		return this;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("description")
	public void setDescription(String description) {
		this.description = description;
	}

	public ShippingZonePojo withDescription(String description) {
		this.description = description;
		return this;
	}

	@JsonProperty("locations")
	public List<ShippingLocation> getLocations() {
		return locations;
	}

	@JsonProperty("locations")
	public void setLocations(List<ShippingLocation> locations) {
		this.locations = locations;
	}

	public ShippingZonePojo withLocations(List<ShippingLocation> locations) {
		this.locations = locations;
		return this;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("name", name).append("description", description).append("locations", locations).toString();
	}

	public ZoneDraft toDraft() {
		final ZoneDraft draft = SphereJsonUtils.readObject(toJsonString(), ZoneDraft.class);
		LOG.info("ZoneDraft - {}", draft.getName());
		LOG.debug("{}", SphereJsonUtils.toPrettyJsonString(draft));
		return draft;
	}
}

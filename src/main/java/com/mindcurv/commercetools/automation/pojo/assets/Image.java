
package com.mindcurv.commercetools.automation.pojo.assets;


import java.util.Map;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mindcurv.commercetools.automation.helper.spreadsheet.ValueHelper;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "dimensions",
    "label",
    "url"
})
public class Image {

    @JsonProperty("dimensions")
    private ImageDimensions dimensions;
    @JsonProperty("url")
    private String url;
    @JsonProperty("label")
    private String label;
    
    public Image() {
    }
    
    public Image(Map<String, Object> value) {
    	setUrl(ValueHelper.getValueAsString(CsvConstants.ProductImages.URL_KEY, value));
    	setLabel(ValueHelper.getValueAsString(CsvConstants.ProductImages.LABEL_KEY, value));
    	final Integer width = ValueHelper.getValueAsInteger(CsvConstants.ProductImages.WIDTH_KEY, value);
    	final Integer height = ValueHelper.getValueAsInteger(CsvConstants.ProductImages.HEIGHT_KEY, value);
    	if ( width != null && height != null) {
        	setDimensions( new ImageDimensions(width, height));
    	}
    }
    
    public Image(ImageDimensions dimensions, String url) {
        super();
        this.dimensions = dimensions;
        this.url = url;
    }

    @JsonProperty("dimensions")
    public ImageDimensions getDimensions() {
        return dimensions;
    }

    @JsonProperty("dimensions")
    public void setDimensions(ImageDimensions dimensions) {
        this.dimensions = dimensions;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }
    
    public Image withUrl(String url) {
    	setUrl(url);
    	return this; 
    }
    
    
    @JsonProperty("label")
    public String getLabel() {
		return label;
	}

    @JsonProperty("label")
    public void setLabel(String label) {
		this.label = label;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("label", getLabel()).append("url", getUrl()).append("dimensions", getDimensions())
				.toString();
	}
}

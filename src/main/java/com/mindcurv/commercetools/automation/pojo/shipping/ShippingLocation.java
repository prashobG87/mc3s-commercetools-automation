
package com.mindcurv.commercetools.automation.pojo.shipping;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "country", "state" })
public class ShippingLocation {

	@JsonProperty("country")
	private String country;
	@JsonProperty("state")
	private String state;

	@JsonProperty("country")
	public String getCountry() {
		return country;
	}

	@JsonProperty("country")
	public void setCountry(String country) {
		this.country = country;
	}

	public ShippingLocation withCountry(String country) {
		this.country = country;
		return this;
	}

	@JsonProperty("state")
	public String getState() {
		return state;
	}

	@JsonProperty("state")
	public void setState(String state) {
		this.state = state;
	}

	public ShippingLocation withState(String state) {
		this.state = state;
		return this;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("country", country).append("state", state).toString();
	}

}

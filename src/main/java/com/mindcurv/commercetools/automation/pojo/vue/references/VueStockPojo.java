
package com.mindcurv.commercetools.automation.pojo.vue.references;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "is_in_stock"
})
public class VueStockPojo {

    @JsonProperty("is_in_stock")
    private Boolean is_in_stock;

    @JsonProperty("is_in_stock")
    public Boolean getIsInStock() {
        return is_in_stock;
    }

    @JsonProperty("is_in_stock")
    public void setIsInStock(Boolean isInStock) {
        this.is_in_stock = isInStock;
    }

    public VueStockPojo withIsInStock(Boolean isInStock) {
        this.is_in_stock = isInStock;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("isInStock", is_in_stock).toString();
    }

}

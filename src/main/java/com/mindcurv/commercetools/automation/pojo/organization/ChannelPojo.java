
package com.mindcurv.commercetools.automation.pojo.organization;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mindcurv.commercetools.automation.helper.spreadsheet.ValueHelper;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;
import com.mindcurv.commercetools.automation.pojo.Custom;
import com.mindcurv.commercetools.automation.pojo.types.BaseTypePojo;

import io.sphere.sdk.channels.ChannelDraft;
import io.sphere.sdk.json.SphereJsonUtils;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "key", "name", "description", "address", "custom", "geoLocation","roles" })
public class ChannelPojo extends BaseTypePojo {

	private final static Logger LOG = LoggerFactory.getLogger(ChannelPojo.class);

	@JsonProperty("address")
	private AddressPojo address;
	@JsonProperty("geoLocation")
	private GeoLocation geoLocation;
	@JsonProperty("roles")
	private List<String> roles;

	public ChannelPojo(Map<String, Object> value) {
		super(value);

		setKey(ValueHelper.getValueAsString(CsvConstants.KEY_KEY, value));
		//setAddress(new AddressPojo().withSheetData(value));
		final String rolesValue = ValueHelper.getValueAsString(CsvConstants.Channel.ROLES_KEY, value);
		if (StringUtils.isNotBlank(rolesValue)) {
			roles = new ArrayList<>();
			for (String role : rolesValue.split(CsvConstants.SPLITTOKEN)) {
				role = role.trim();
				if (StringUtils.isNotBlank(role)) {
					roles.add(role.trim());
				}
			}
		}
		final String geoValue = ValueHelper.getValueAsString(CsvConstants.Address.GEOCOORDINATES_KEY, value);
		if (StringUtils.isNotBlank(geoValue)) {
			List<Double> coordinates = new ArrayList<>();
			try {
				for (final String geo : geoValue.split(",")) {
					coordinates.add(Double.valueOf(geo));
				}
				if (coordinates.size() == 2) {
					setGeoLocation(new GeoLocation().withCoordinates(coordinates));
				} else {
					LOG.warn("invalid geo coordinates for {}:'{}'", getKey(), geoValue);
				}
			} catch (NumberFormatException e) {
				LOG.warn("invalid geo coordinates for {}:'{}'", getKey(), geoValue);
			}
		}
	}

	public ChannelPojo withKey(String key) {
		setKey(key);
		return this;
	}

	@JsonProperty("address")
	public AddressPojo getAddress() {
		return address;
	}

	@JsonProperty("address")
	public void setAddress(AddressPojo address) {
		this.address = address;
	}

	public ChannelPojo withAddress(AddressPojo address) {
		this.address = address;
		return this;
	}

	public ChannelPojo withCustom(Custom custom) {
		setCustom(custom);
		return this;
	}

	@JsonProperty("geoLocation")
	public GeoLocation getGeoLocation() {
		return geoLocation;
	}

	@JsonProperty("geoLocation")
	public void setGeoLocation(GeoLocation geoLocation) {
		this.geoLocation = geoLocation;
	}

	public ChannelPojo withGeoLocation(GeoLocation geoLocation) {
		this.geoLocation = geoLocation;
		return this;
	}
	

	@JsonProperty("roles")
	public List<String> getRoles() {
		return roles;
	}

	@JsonProperty("roles")
	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("key", getKey()).append("name", getName())
				.append("description", getDescription()).append("address", address).append("custom", getCustom())
				.append("geoLocation", geoLocation).toString();
	}

	public ChannelDraft toDraft() {
		final ChannelDraft draft = SphereJsonUtils.readObject(toJsonString(), ChannelDraft.class);
		LOG.debug("ChannelDraft - {}", draft.getKey());
		LOG.debug("{}", SphereJsonUtils.toPrettyJsonString(draft));
		return draft;
	}

}

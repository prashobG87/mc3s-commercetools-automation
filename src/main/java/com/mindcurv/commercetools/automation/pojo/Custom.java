
package com.mindcurv.commercetools.automation.pojo;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "type", "fields" })
public class Custom {

	@JsonProperty("type")
	private GenericTypeReferencePojo type;
	@JsonProperty("fields")
	private Map<String, Object> fields = new HashMap<>();

	@JsonProperty("type")
	public GenericTypeReferencePojo getType() {
		return type;
	}

	@JsonProperty("type")
	public void setType(GenericTypeReferencePojo type) {
		this.type = type;
	}

	public Custom withType(GenericTypeReferencePojo type) {
		this.type = type;
		return this;
	}

	@JsonProperty("fields")
	public Map<String, Object> getFields() {
		return fields;
	}

	@JsonProperty("fields")
	public void setFields(Map<String, Object> fields) {
		this.fields = fields;
	}

	public Custom withFields(Map<String, Object> fields) {
		this.fields = fields;
		return this;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("type", type).append("fields", fields).toString();
	}

}


package com.mindcurv.commercetools.automation.pojo.customobject.products;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mindcurv.commercetools.automation.pojo.LocalizedStringPojo;

import io.sphere.sdk.models.LocalizedString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "name", "unit", "value" })
public class TableEntryPojo {

	private final static Logger LOG = LoggerFactory.getLogger(TableEntryPojo.class);

	@JsonProperty("id")
	private String id;
	@JsonProperty("name")
	private LocalizedString name;
	@JsonProperty("unit")
	private String unit;
	@JsonProperty("value")
	private String value;

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(String id) {
		this.id = id;
	}

	public TableEntryPojo withId(String id) {
		this.id = id;
		return this;
	}

	@JsonProperty("name")
	public LocalizedString getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(LocalizedString name) {
		this.name = name;
	}

	public TableEntryPojo withName(LocalizedString name) {
		this.name = name;
		return this;
	}

	@JsonProperty("unit")
	public String getUnit() {
		return unit;
	}

	@JsonProperty("unit")
	public void setUnit(String unit) {
		this.unit = unit;
	}

	public TableEntryPojo withUnit(String unit) {
		this.unit = unit;
		return this;
	}

	@JsonProperty("value")
	public String getValue() {
		return value;
	}

	@JsonProperty("value")
	public void setValue(String value) {
		this.value = value;
	}

	public TableEntryPojo withValue(String value) {
		this.value = value;
		return this;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("name", name).append("unit", unit)
				.append("value", value).toString();
	}

	public TableEntryPojo withLineValue(String line) {
		// "id":"1","value": "100","unit": "g","name.de": "Salz","name.en":"Salt"

		final LocalizedStringPojo name = new LocalizedStringPojo();
		final String[] parts = line.split(",");
		for (String part : parts) {
			final String[] values = part.split(":");
			if (values.length >= 2) {
				final String key = values[0].trim().replaceAll("\"", "").trim();
				final String value = values[1].trim().replaceAll("\"", "").trim();
				LOG.debug("\t'{}'->'{}'", key, value);
				if ("id".equals(key)) {
					setId(value);
				} else if ("value".equals(key)) {
					setValue(value);
				} else if ("unit".equals(key)) {
					setUnit(value);
				} else if ("name.de".equals(key)) {
					name.setDe(value);
				} else if ("name.en".equals(key)) {
					name.setEn(value);
				}
			}
		}
		setName(name.getLocalizedString());
		return this;
	}

}

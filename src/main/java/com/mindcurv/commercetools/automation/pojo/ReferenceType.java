
package com.mindcurv.commercetools.automation.pojo;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mindcurv.commercetools.automation.pojo.types.product.ProductTypeElementType;
import com.mindcurv.commercetools.automation.pojo.types.product.ProductTypeValue;

import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
	"id",
    "name",
    "referenceTypeId",
    "values",
    "elementType"
})
public class ReferenceType {
	
	public static final String CART = "cart"; 
	public static final String CART_DISCOUNT = "cart-discount"; 
	public static final String CATEGORY = "category";
	public static final String CATEGORY_DISCOUNT = "cart-discount";
	public static final String CHANNEL = "channel";
	public static final String CUSTOMER = "customer";
	public static final String CUSTOMER_GROUP = "customer-group";
	public static final String DISCOUNT_CODE = "discount-code";
	public static final String KEY_VALUE_DOCUMENT = "key-value-document";
	public static final String PAYMENT = "payment";
	public static final String PRODUCT = "product";
	public static final String PRODUCT_DISCOUNT = "product-discount";
	public static final String PRODUCT_PRICE = "product-price";
	public static final String PRODUCT_TYPE = "product-type";
	public static final String ORDER = "order";
	public static final String SHIPPING_METHOD = "shipping-method";
	public static final String SHOPPING_LIST = "shopping-list";
	public static final String STATE = "state";
	public static final String TAX_CATEGORY = "tax-category";
	public static final String TYPE = "type";
	public static final String ZONE = "zone";


	@JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("referenceTypeId")
    private String referenceTypeId;
    @JsonProperty("values")
    private List<ProductTypeValue> values = new ArrayList<ProductTypeValue>();
    @JsonProperty("elementType")
    private ProductTypeElementType elementType;

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    public ReferenceType withId(String id) {
        this.id = id;
        return this;
    }
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public ReferenceType withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("referenceTypeId")
    public String getReferenceTypeId() {
        return referenceTypeId;
    }

    @JsonProperty("referenceTypeId")
    public void setReferenceTypeId(String referenceTypeId) {
        this.referenceTypeId = referenceTypeId;
    }

    public ReferenceType withReferenceTypeId(String referenceTypeId) {
        this.referenceTypeId = referenceTypeId;
        return this;
    }

    @JsonProperty("values")
    public List<ProductTypeValue> getValues() {
        return values;
    }

    @JsonProperty("values")
    public void setValues(List<ProductTypeValue> values) {
        this.values = values;
    }

    public ReferenceType withValues(List<ProductTypeValue> values) {
        this.values = values;
        return this;
    }

    @JsonProperty("elementType")
    public ProductTypeElementType getElementType() {
        return elementType;
    }

    @JsonProperty("elementType")
    public void setElementType(ProductTypeElementType elementType) {
        this.elementType = elementType;
    }

    public ReferenceType withElementType(ProductTypeElementType elementType) {
        this.elementType = elementType;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("name", name).append("referenceTypeId", referenceTypeId).append("values", values).append("elementType", elementType).toString();
    }

}


package com.mindcurv.commercetools.automation.pojo.catalog.products;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "name",
    "value"
})
public class AttributePojo {
     
	@JsonProperty("name")
	String name;
	@JsonProperty("value")
	Object value;
	
	
	 public AttributePojo() {
	    }
	 
	public AttributePojo(String name, Object value) {
        super();
        this.name = name;
        this.value = value;
       
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }
    
    @JsonProperty("value")
    public Object getValue() {
        return value;
    }

    @JsonProperty("value")
    public void setValue(Object value) {
        this.value = value;
    }
   
}

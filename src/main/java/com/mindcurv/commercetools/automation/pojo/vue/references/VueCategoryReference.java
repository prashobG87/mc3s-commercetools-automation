
package com.mindcurv.commercetools.automation.pojo.vue.references;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "category_id",
    "name"
})
public class VueCategoryReference {

    @JsonProperty("category_id")
    private String category_id;
    @JsonProperty("name")
    private String name;

    @JsonProperty("category_id")
    public String getCategoryId() {
        return category_id;
    }

    @JsonProperty("category_id")
    public void setCategoryId(String categoryId) {
        this.category_id = categoryId;
    }

    public VueCategoryReference withCategoryId(String categoryId) {
        this.category_id = categoryId;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public VueCategoryReference withName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("categoryId", category_id).append("name", name).toString();
    }

}

package com.mindcurv.commercetools.automation.helper.files;

import java.io.File;
import java.io.FileFilter;

import com.mindcurv.commercetools.automation.helper.MediaHelper;

public class ImageFileFilter implements FileFilter {

	@Override
	public boolean accept(File file) {
		if ( file.isDirectory()) {
			return false; 
		} 
		final String pathName = file.getName().toLowerCase();
		String[] nameParts = pathName.split("_"); 
		if ( nameParts.length < 2) {
			return false; 
		}
		
		return MediaHelper.isSupportedMedia(pathName);  
		
	}

}

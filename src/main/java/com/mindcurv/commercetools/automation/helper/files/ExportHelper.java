package com.mindcurv.commercetools.automation.helper.files;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mindcurv.commercetools.automation.pojo.BasePojo;

public class ExportHelper {
	
	final static Logger LOG = LoggerFactory.getLogger(ExportHelper.class);

	public static File prepareExportFolder(final String exportDirectory, final String className) {
		File exportFolder = new File(exportDirectory, className);
		if (!exportFolder.exists()) {
			boolean success = exportFolder.mkdirs();
			if (success) {
				LOG.info("created export directory {} ", exportFolder.getAbsolutePath());
			} else {
				LOG.error("Could export directory {}", exportFolder.getAbsolutePath());
				return null;
			}
		}
		return exportFolder;
	}

	public static File storeToFile(final String exportDirectory, final String clazz, final String jsonString) {
		final File file = new File(exportDirectory, clazz + ".json");
		LOG.info( "store to file '{}'", file.getAbsolutePath());
		try {
			FileUtils.write(file, jsonString, "UTF-8");
			return file; 
		} catch (IOException e) {
			LOG.error("IOException for {}", file.getAbsolutePath());
		}
		return null; 
	}
	
	public static File storeFilesAsZip(String exportDirectory, List<File> files) {
		final File file = new File(exportDirectory, System.currentTimeMillis() + ".zip");
		try {
			FileUtils.write(file, "TODO", "UTF-8");
			return file; 
		} catch (IOException e) {
			LOG.error("IOException for {}", file.getAbsolutePath());
		}
		return null; 
	}

	public static File storeToFile(String exportDirectory, Object pojo) {
		final String clazz = pojo.getClass().getSimpleName().toLowerCase();
		final File directory = prepareExportFolder(exportDirectory, clazz);
		final File file = new File(directory, clazz + "-" + System.currentTimeMillis() + "_" + RandomUtils.nextInt(1000, 9999) + ".json");
		try {
			final String fileContent = (pojo instanceof BasePojo)? ((BasePojo) pojo).toJsonString() : pojo.toString();
			FileUtils.write(file, fileContent, "UTF-8");
			return file; 
		} catch (IOException e) {
			LOG.error("IOException for " + file.getAbsolutePath(), e);
		}
		return null; 
	}

}

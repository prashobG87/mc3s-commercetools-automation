package com.mindcurv.commercetools.automation.helper.spreadsheet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.pojo.BasePojo;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;
import com.mindcurv.commercetools.automation.pojo.organization.AddressPojo;
import com.mindcurv.commercetools.automation.pojo.types.product.TypeAttribute;

public class ExcelHelper {

	private final static Logger LOG = LoggerFactory.getLogger(ExcelHelper.class);

	public static Map<String, List<TypeAttribute>> prepareAttributesListsMap(Workbook workbook,
			final CommercetoolsProject project) {
		final List<Map<String, Object>> attributeLines;
		try {
			attributeLines = processRange(workbook, CsvConstants.Attributes.SHEET, project);
		} catch (IOException e) {
			LOG.error("Exception while accessing Sheet " + CsvConstants.Attributes.SHEET, e);
			return null;
		}

		return ValueHelper.processAttributeLines(attributeLines);
	}

	public static List<Map<String, Object>> processRange(Workbook workbook, final String sheetName,
			final CommercetoolsProject project) throws IOException {

		FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();

		final Sheet sheet = workbook.getSheet(sheetName);
		if (sheet == null) {
			LOG.error("No sheet {} found.", sheetName);
			return null;
		}
		final List<Object> headers = new ArrayList<>();
		final List<Map<String, Object>> values = new ArrayList<>();
		final Iterator<Row> iterator = sheet.iterator();
		while (iterator.hasNext()) {
			final Row currentRow = iterator.next();
			if (currentRow.getRowNum() == 0) {
				prepareHeader(headers, currentRow, evaluator);
				continue;
			} else {
				processValueRow(headers, values, currentRow, evaluator, project);
			}
		}
		LOG.info("prepared {} lines from Sheet '{}'", values.size(), sheetName);
		return values;

	}

	public static Map<String, List<BasePojo>> prepareAddressPojos(Workbook workbook, final String type,
			final CommercetoolsProject project) {
		final Map<String, List<BasePojo>> addressPojoMap = new HashMap<>();

		try {
			if (StringUtils.isNotBlank(type)) {
				for (Map<String, Object> valueLine : processRange(workbook, CsvConstants.Address.SHEET, project)) {
					final AddressPojo addressPojo = new AddressPojo().withSheetData(valueLine);
					final String referenceKey = addressPojo.getReferenceId();
					List<BasePojo> addressesForReference = addressPojoMap.get(referenceKey);
					if (addressesForReference == null) {
						addressesForReference = new ArrayList<>();
						addressPojoMap.put(referenceKey, addressesForReference);
					}
					addressesForReference.add(addressPojo);
				}
			}
		} catch (Exception e) {
			LOG.error("Exception during address parsing", e);
		}
		return addressPojoMap;
	}

	public static void processValueRow(final List<Object> headers, final List<Map<String, Object>> values,
			final Row currentRow, FormulaEvaluator evaluator, final CommercetoolsProject project) {
		final Iterator<Cell> cellIterator = currentRow.iterator();
		final Map<String, Object> valuesMap = new HashMap<>();

		int columnIndex = 0;
		while (cellIterator.hasNext()) {

			if (headers.size() == columnIndex) {
				LOG.debug("{} -> {}", currentRow.getRowNum(), valuesMap);
				break;
			}

			final Cell currentCell = cellIterator.next();
			Object value = getCellValue(currentCell, evaluator);

			if (columnIndex == 0 && value == null) {
				LOG.debug("skipping line {}", currentRow.getRowNum());
				break;
			}
			final String key = (String) headers.get(columnIndex);
			valuesMap.put(key, value);
			columnIndex++;
		}
		if (ValueHelper.checkLineValues(project, valuesMap)) {
			values.add(valuesMap);
		}
	}

	public static void prepareHeader(final List<Object> headers, final Row headerRow, FormulaEvaluator evaluator) {
		final Iterator<Cell> headerRowIterator = headerRow.iterator();
		while (headerRowIterator.hasNext()) {
			final Cell currentCell = headerRowIterator.next();

			// stop at first undefined
			if (CellType.BLANK.equals(currentCell.getCellTypeEnum())) {
				break;
			}
			Object value = getCellValue(currentCell, evaluator);
			headers.add(value);
		}
		LOG.info("header : {} / {}", headers.size(), headers);

	}

	public static Object getCellValue(final Cell cell, final FormulaEvaluator evaluator) {

		Object value = null;

		try {
			CellValue cellValue = evaluator.evaluate(cell);
			if (cellValue != null) {
				final CellType cellTypeEnum = cellValue.getCellTypeEnum();

				if (CellType.STRING.equals(cellTypeEnum)) {
					value = cell.getStringCellValue();
				} else if (CellType.NUMERIC.equals(cellTypeEnum)) {
					if (HSSFDateUtil.isCellDateFormatted(cell)) {
						value = cell.getDateCellValue();
					} else {
						value = Double.valueOf(cell.getNumericCellValue());
					}
				} else if (CellType.STRING.equals(cellTypeEnum)) {
					value = cell.getStringCellValue();
				} else if (CellType.BOOLEAN.equals(cellTypeEnum)) {
					value = Boolean.valueOf(cell.getBooleanCellValue());
				} else if (CellType.FORMULA.equals(cellTypeEnum)) {
					LOG.info("{} -> {} -> {}", cell.getCellFormula(), cellTypeEnum, cellValue);
				} else if (CellType.BLANK.equals(cellTypeEnum)) {

				} else if (CellType._NONE.equals(cellTypeEnum)) {

				} else {
					LOG.warn("Unsupported [{},{}]: {}", cell.getRowIndex(), cell.getColumnIndex(), cellTypeEnum);
				}
			}
		} catch (Exception e) {
			LOG.error("Exception in Sheet {} - cell: {}:{} -> {}", cell.getSheet().getSheetName(), cell.getRowIndex(),
					cell.getColumnIndex(), cell.getStringCellValue());
			LOG.error("", e);
		}
		return value;

	}
}

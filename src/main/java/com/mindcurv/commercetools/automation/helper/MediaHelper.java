package com.mindcurv.commercetools.automation.helper;

public class MediaHelper {

	public static boolean isSupportedMedia(final String fileName) {
		if ( fileName.startsWith("__MACOSX")) {
			return false; 
		}
		if ( fileName.endsWith(".jpg") || fileName.endsWith(".jpeg")) {
			return true; 
		}
		if ( fileName.endsWith(".gif")) {
			return true; 
		}
		if ( fileName.endsWith(".png")) {
			return true; 
		}
		return false; 
	}
}

package com.mindcurv.commercetools.automation.helper.spreadsheet;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.Constants;
import com.mindcurv.commercetools.automation.helper.TypeHelper;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;
import com.mindcurv.commercetools.automation.pojo.LocalizedStringPojo;
import com.mindcurv.commercetools.automation.pojo.ReferenceType;
import com.mindcurv.commercetools.automation.pojo.types.product.ProductTypeElementType;
import com.mindcurv.commercetools.automation.pojo.types.product.ProductTypeValue;
import com.mindcurv.commercetools.automation.pojo.types.product.TypeAttribute;

public class ValueHelper {

	private final static Logger LOG = LoggerFactory.getLogger(ValueHelper.class);

	public static Object getValue(String key, Map<String, Object> value) {
		return value.get(key);
	}

	public static String getValueAsString(final String key, Map<String, Object> value) {
		Object object = getValue(key, value);
		if (object instanceof String) {
			final String stringValue = (String) object;
			if (stringValue != null) {
				return stringValue.trim();
			}
		}
		if (object instanceof Date) {
			final Date date = (Date) object;
			return Constants.DATEFORMAT.format(date);
		}
		if (object != null) {
			return object.toString().trim();
		}
		return null;
	}

	public static Boolean getValueAsBoolean(final String key, Map<String, Object> value) {
		Object object = getValue(key, value);
		if (object instanceof Boolean) {
			return (Boolean) object;
		}
		if (object instanceof String) {
			final String stringValue = (String) object;

			if (StringUtils.isNotBlank(stringValue)) {
				return Boolean.valueOf(stringValue);
			}
		}
		return Boolean.FALSE;
	}

	public static Integer getValueAsInteger(String key, Map<String, Object> value) {
		Object object = getValue(key, value);
		if (object instanceof Integer) {
			return (Integer) object;
		}

		if (object instanceof Double) {
			Double doubleValue = (Double) object;
			return Integer.valueOf((int) doubleValue.doubleValue());
		}

		if (object instanceof String) {
			final String stringValue = (String) object;
			if (StringUtils.isNotBlank(stringValue)) {
				return Integer.valueOf(stringValue);
			}
		}
		return null;
	}

	public static Long getValueAsLong(String key, Map<String, Object> value) {
		Object object = getValue(key, value);
		if (object instanceof Long) {
			return (Long) object;
		}
		if (object instanceof Double) {
			return Long.valueOf(((Double) object).longValue());
		}
		if (object instanceof String) {
			final String stringValue = (String) object;

			if (StringUtils.isNotBlank(stringValue)) {
				return Long.valueOf(stringValue);
			}
		}
		return null;
	}

	public static Double getValueAsDouble(String key, Map<String, Object> value) {
		Object object = getValue(key, value);
		if (object instanceof Double) {
			return (Double) object;
		}

		if (object instanceof String) {
			final String stringValue = (String) object;
			if (StringUtils.isNotBlank(stringValue)) {
				return Double.valueOf(stringValue);
			}
		}

		return null;
	}

	public static Date getValueAsDate(String key, Map<String, Object> value) {

		Object object = getValue(key, value);
		if (object instanceof Date) {
			return (Date) object;
		}

		if (object instanceof String) {
			final String stringValue = (String) object;
			if (StringUtils.isNotBlank(stringValue)) {
				try {
					return Constants.DATEFORMAT.parse(stringValue);
				} catch (ParseException e) {
					LOG.error("Could not parse date {} from column {}", stringValue, key);
				}
			}
		}
		return null;
	}

	public static Integer getValueAsCentAmount(String key, Map<String, Object> value) {
		Double priceValueAsDouble = getValueAsDouble(key, value);
		if (priceValueAsDouble != null) {
			return Integer.valueOf((int) (priceValueAsDouble.doubleValue() * 100));
		}
		return null;
	}

	public static List<String> getValueAsList(String key, Map<String, Object> value) {
		final String stringValue = getValueAsString(key, value);
		if (StringUtils.isNotBlank(stringValue)) {
			return Arrays.asList(stringValue.split(CsvConstants.SPLITTOKEN));
		}
		return null;
	}

	public static Map<String, List<TypeAttribute>> processAttributeLines(
			final List<Map<String, Object>> attributeLines) {
		final Map<String, List<TypeAttribute>> attributesListMap = new HashMap<>();

		for (Map<String, Object> value : attributeLines) {

			final String attributeName = ValueHelper.getValueAsString(CsvConstants.Attributes.NAME, value);
			if (StringUtils.isBlank(attributeName)) {
				continue;
			}

			final TypeAttribute attribute = new TypeAttribute().withName(attributeName);

			final ReferenceType productTypeType = new ReferenceType();
			boolean isSet = ValueHelper.getValueAsBoolean(CsvConstants.Attributes.SET, value);
			final String attributeType = ValueHelper.getValueAsString(CsvConstants.Attributes.TYPE, value);
			final String referenceTypeId = ValueHelper.getValueAsString(CsvConstants.Attributes.REFERENCE_TYPE_ID,
					value);
			if (isSet) {
				productTypeType.setName(TypeHelper.PRODUCTTYPE_SET);
				final ProductTypeElementType elementType = new ProductTypeElementType().withName(attributeType);
				if (StringUtils.isNotBlank(referenceTypeId)) {
					elementType.setReferenceTypeId(referenceTypeId);
				}
				productTypeType.setElementType(elementType);
				if (TypeHelper.PRODUCTTYPE_ENUM.equals(attributeType)) {
					elementType.setValues(prepareEnumValues(value));
				}
			} else {
				productTypeType.setName(attributeType);
				if (StringUtils.isNotBlank(referenceTypeId)) {
					productTypeType.setReferenceTypeId(referenceTypeId);
				}
				if (TypeHelper.PRODUCTTYPE_ENUM.equals(attributeType)) {
					productTypeType.setValues(prepareEnumValues(value));
				} else if (TypeHelper.PRODUCTTYPE_LOCALIZED_ENUM.equals(attributeType)) {
					productTypeType.setValues(prepareLocalizedEnumValues(value));
				}
			}

			attribute
					.withAttributeConstraint(
							ValueHelper.getValueAsString(CsvConstants.Attributes.ATTRIBUTE_CONTRAINTS, value))
					.withDisplayGroup(ValueHelper.getValueAsString(CsvConstants.Attributes.DISPLAY_GROUP, value))
					.withInputHint(ValueHelper.getValueAsString(CsvConstants.Attributes.TEXT_INPUT_HINT, value))
					.withIsRequired(ValueHelper.getValueAsBoolean(CsvConstants.Attributes.REQUIRED, value))
					.withIsSearchable(ValueHelper.getValueAsBoolean(CsvConstants.Attributes.SEARCHABLE, value))
					.withLabel(new LocalizedStringPojo(
							ValueHelper.getValueAsString(CsvConstants.Attributes.LABEL_DE, value),
							ValueHelper.getValueAsString(CsvConstants.Attributes.LABEL_EN, value)))
					.withType(productTypeType);

			final String assignedTypes = ValueHelper.getValueAsString(CsvConstants.Attributes.TYPES, value);
			LOG.debug(Constants.MSG_SPACER);
			LOG.debug("adding attribute {}[{}] for type '{}'", attribute.getName(), attribute.getType().getName());

			for (String assignedType : assignedTypes.split(CsvConstants.SPLITTOKEN)) {
				final String key = assignedType.trim();
				LOG.debug("adding to '{}'", key);
				List<TypeAttribute> list = attributesListMap.get(key);
				if (list == null) {
					list = new ArrayList<>();
					attributesListMap.put(key, list);
				}
				list.add(attribute);
			}
		}

		return attributesListMap;
	}

	public static List<ProductTypeValue> prepareEnumValues(Map<String, Object> value) {
		final String enumValues = (String) value.get(CsvConstants.Attributes.ENUM_VALUES);
		List<ProductTypeValue> values = new ArrayList<>();
		for (String enumValue : enumValues.split(CsvConstants.SPLITTOKEN)) {
			values.add(new ProductTypeValue().withKey(enumValue).withLabel(enumValue));
		}
		return values;
	}

	public static List<ProductTypeValue> prepareLocalizedEnumValues(Map<String, Object> value) {
		final String enumValues = (String) value.get(CsvConstants.Attributes.ENUM_VALUES);
		List<ProductTypeValue> values = new ArrayList<>();
		for (String enumValue : enumValues.split(CsvConstants.SPLITTOKEN)) {
			LocalizedStringPojo label = new LocalizedStringPojo(enumValue, enumValue);
			values.add(new ProductTypeValue().withKey(enumValue).withLabel(label));
		}
		return values;
	}
	
	public static boolean checkLineValues (final CommercetoolsProject project, Map<String, Object> values) {
		if ( values == null || values.isEmpty()) {
			return false; 
		}
		if ( values.containsKey(CsvConstants.ESSENTIAL)) {
			final String essential = getValueAsString(CsvConstants.ESSENTIAL, values); 
			if ( StringUtils.isBlank(essential)) {
				return false; 
			}
			boolean essentialBoolean = Boolean.valueOf(essential).booleanValue();
			if ( !essentialBoolean) {
				boolean returnValue = project.getOptions().contains(Constants.ARG_SAMPLE); 
				LOG.debug( "options {} -> essential:{} -> {}", project.getOptions(), essentialBoolean, returnValue);
				return returnValue; 
			}
		}
		return true; 
	}
}

package com.mindcurv.commercetools.automation.helper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mindcurv.commercetools.automation.pojo.vue.VueProductPojo;
import com.mindcurv.commercetools.automation.pojo.vue.references.VueCategoryReference;

import io.sphere.sdk.categories.Category;
import io.sphere.sdk.models.Reference;
import io.sphere.sdk.products.Product;

public class VueHelper {
	
	final static Logger LOG = LoggerFactory.getLogger(VueHelper.class);

	public VueHelper() {
		
	}
	
	public static String replaceId(final String id) {
		return id.replace("-", "_"); 
	}

	public static Map<String, Category> extractRelevantCategories(Map<String, Category> categoryCacheMap, final List<VueProductPojo> productPojoList) {
		final Map<String, Category> categoryList = new HashMap<>(); 
		for (VueProductPojo pojo : productPojoList) {
			for (VueCategoryReference categoryReference : pojo.getCategory()) {
				final String categoryId = categoryReference.getCategoryId();
				final Category categoryFromCache = categoryCacheMap.get(categoryId);
				if (categoryFromCache == null) {
					LOG.warn("no category for ID {} found", categoryId);
				} else {
					categoryList.put(categoryFromCache.getId(), categoryFromCache);
				}
			}
		}
		return categoryList;
	}
	
	public static String listToJsonString(List<?> pojoList) {
		final Gson gsonBuilder = new GsonBuilder().setPrettyPrinting().create();
		return gsonBuilder.toJson(pojoList);
	}
	
	public static Map<String, Category> extractRelevantCategoriesFromProductList(Map<String, Category> categoryCacheMap, final List<Product> productPojoList) {
		final Map<String, Category> categoryList = new HashMap<>(); 
		for (Product pojo : productPojoList) {
			for (Reference<Category> categoryReference : pojo.getMasterData().getCurrent().getCategories()) {
				final String categoryId = categoryReference.getId();
				final Category categoryFromCache = categoryCacheMap.get(categoryId);
				if (categoryFromCache == null) {
					LOG.warn("no category for ID {} found", categoryId);
				} else {
					categoryList.put(categoryFromCache.getId(), categoryFromCache);
				}
			}
		}
		return categoryList;
	}
	
}

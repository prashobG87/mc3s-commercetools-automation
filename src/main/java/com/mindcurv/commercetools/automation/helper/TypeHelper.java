package com.mindcurv.commercetools.automation.helper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mindcurv.commercetools.automation.helper.spreadsheet.ValueHelper;
import com.mindcurv.commercetools.automation.pojo.LocalizedStringPojo;
import com.mindcurv.commercetools.automation.pojo.ReferenceType;
import com.mindcurv.commercetools.automation.pojo.types.BaseTypePojo;
import com.mindcurv.commercetools.automation.pojo.types.product.ProductTypeValue;
import com.mindcurv.commercetools.automation.pojo.types.product.TypeAttribute;

import io.sphere.sdk.client.BlockingSphereClient;
import io.sphere.sdk.queries.QueryPredicate;
import io.sphere.sdk.types.Type;
import io.sphere.sdk.types.queries.TypeQuery;
import io.sphere.sdk.types.queries.TypeQueryBuilder;
import io.sphere.sdk.types.queries.TypeQueryModel;

public class TypeHelper {
	private final static Logger LOG = LoggerFactory.getLogger(TypeHelper.class);
	
	public static final String TYPE_BOOLEAN = "Boolean";
	public static final String TYPE_DATE = "Date";
	public static final String TYPE_DATETIME = "DateTime";
	public static final String TYPE_TIME = "Time";
	public static final String TYPE_ENUM = "Enum";
	public static final String TYPE_LOCALIZED_ENUM = "LocalizedEnum";
	public static final String TYPE_STRING = "String";
	public static final String TYPE_LOCALIZED_STRING = "LocalizedString";
	public static final String TYPE_NUMBER = "Number";
	public static final String TYPE_REFERENCE = "Reference";
	public static final String TYPE_SET = "Set";
	public static final String TYPE_MONEY = "Money";

	public static final String PRODUCTTYPE_BOOLEAN = TYPE_BOOLEAN.toLowerCase();
	public static final String PRODUCTTYPE_DATE = TYPE_DATE.toLowerCase();
	public static final String PRODUCTTYPE_ENUM = "enum";
	public static final String PRODUCTTYPE_LOCALIZED_ENUM = "lenum";
	public static final String PRODUCTTYPE_STRING = "text";
	public static final String PRODUCTTYPE_LOCALIZED_STRING = "ltext";
	public static final String PRODUCTTYPE_NUMBER = TYPE_NUMBER.toLowerCase();
	public static final String PRODUCTTYPE_REFERENCE = TYPE_REFERENCE.toLowerCase();
	public static final String PRODUCTTYPE_SET = TYPE_SET.toLowerCase();

	private static Map<String, String> productType2TypeMapping = new HashMap<>();

	static {
		// number,reference,boolean,text,ltext,money,date,enum
		productType2TypeMapping.put(PRODUCTTYPE_ENUM, TYPE_ENUM);
		productType2TypeMapping.put(PRODUCTTYPE_LOCALIZED_ENUM, TYPE_LOCALIZED_ENUM);
		productType2TypeMapping.put(PRODUCTTYPE_STRING, TYPE_STRING);
		productType2TypeMapping.put(PRODUCTTYPE_LOCALIZED_STRING, TYPE_LOCALIZED_STRING);

		productType2TypeMapping.put(PRODUCTTYPE_BOOLEAN, TYPE_BOOLEAN);
		productType2TypeMapping.put(PRODUCTTYPE_DATE, TYPE_DATE);
		productType2TypeMapping.put(PRODUCTTYPE_NUMBER, TYPE_NUMBER);
		productType2TypeMapping.put(PRODUCTTYPE_REFERENCE, TYPE_REFERENCE);
		productType2TypeMapping.put(PRODUCTTYPE_SET, TYPE_SET);

		productType2TypeMapping.put(TYPE_MONEY, TYPE_MONEY);
		productType2TypeMapping.put(TYPE_DATETIME, TYPE_DATETIME);
		productType2TypeMapping.put(TYPE_TIME, TYPE_TIME);

	}

	public static TypeAttribute toFieldDefinition(TypeAttribute typeAttribute) {

		final TypeAttribute fieldDefinition = new TypeAttribute().withName(typeAttribute.getName())
				.withLabel(new LocalizedStringPojo().withDe(typeAttribute.getName()).withEn(typeAttribute.getName()))
				.withIsRequired(typeAttribute.getIsRequired()).withDisplayGroup(typeAttribute.getDisplayGroup())
				.withInputTip(typeAttribute.getInputTip());

		fieldDefinition.setType(new ReferenceType());
		final String typeName = productType2TypeMapping.get(typeAttribute.getType().getName());
		fieldDefinition.getType().setName(typeName);
		if ( TYPE_REFERENCE.equalsIgnoreCase(typeName)) {
			fieldDefinition.getType().setReferenceTypeId(typeAttribute.getType().getReferenceTypeId());
		}
		
		final List<ProductTypeValue> values = typeAttribute.getType().getValues();
		if (values != null && values.isEmpty()) {
			fieldDefinition.getType().setValues(null);
		}
		
		return fieldDefinition;

	}

	public static Map<String, Type> determineTypeIds(final BlockingSphereClient client,
			final List<String> resourceTypeIds) {
		final QueryPredicate<Type> queryPredicate = TypeQueryModel.of().resourceTypeIds().containsAny(resourceTypeIds);
		final TypeQuery query = TypeQueryBuilder.of().limit(500).predicates(queryPredicate).build();
		final List<Type> list = client.executeBlocking(query).getResults();

		final Map<String, Type> typesMap = new HashMap<>();
		for (Type type : list) {
			typesMap.put(type.getKey(), type);
		}
		LOG.info("found {} types for '{}'", typesMap.size(), resourceTypeIds);
		return typesMap;
	}

	public static void translateKeyToTypeId(final Map<String, Type> typesMap, final BaseTypePojo pojo) {
		final String typeId = pojo.getCustom().getType().getId();
		if (StringUtils.isNotBlank(typeId)) {
			final Type type = typesMap.get(typeId);
			if (type != null) {
				pojo.getCustom().getType().setId(type.getId());
			}
		}
	}
	
	public static LocalizedStringPojo getNameObject(Map<String, Object> value, String deKey, String enKey) {
		LocalizedStringPojo name = null;
		final String nameDe = ValueHelper.getValueAsString(deKey, value);
		final String nameEn = ValueHelper.getValueAsString(enKey, value);
		if (nameDe != null || nameEn != null) {
			name = new LocalizedStringPojo();
			name.setDe(nameDe);
			name.setEn(nameEn);
		}
		return name;
	}
}

package com.mindcurv.commercetools.automation.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mindcurv.commercetools.automation.helper.spreadsheet.GDriveHelper;
import com.mindcurv.commercetools.automation.pojo.types.TypePojo;
import com.mindcurv.commercetools.automation.pojo.types.product.ProductTypePojo;
import com.mindcurv.commercetools.automation.pojo.types.product.TypeAttribute;

import io.sphere.sdk.models.LocalizedString;
import io.sphere.sdk.producttypes.ProductTypeDraft;
import io.sphere.sdk.types.TypeDraft;

public class DraftHelper {

	private final static Logger LOG = LoggerFactory.getLogger(DraftHelper.class);

	public DraftHelper() {

	}

	public static LocalizedString getLocalizedString(String name) {
		Map<Locale, String> map = new HashMap<>();
		final String nameToSet = StringUtils.isNotBlank(name) ? name : "N/A";
		map.put(Locale.GERMAN, nameToSet);
		map.put(Locale.ENGLISH, nameToSet);
		return LocalizedString.of(map);
	}

	public static String createLocationKey(String country, String state) {
		final StringBuffer stringBuffer = new StringBuffer();
		if (StringUtils.isNotBlank(country)) {
			stringBuffer.append(country);
		}
		if (StringUtils.isNotBlank(state)) {
			if (stringBuffer.length() > 0) {
				stringBuffer.append("_");
			}
			stringBuffer.append(state);
		}
		return stringBuffer.toString();
	}

	public static boolean checkForUpdate(final Object itemValue, final Object draftValue) {
		boolean returnValue = false;
		if (itemValue == null && draftValue == null) {
			returnValue = false;
		} else if (itemValue == null && draftValue != null) {
			returnValue = true;
		} else if (itemValue != null && draftValue != null && !itemValue.equals(draftValue)) {
			returnValue = true;
		} else {
			LOG.debug("checkForUpdate : else block");
		}
		LOG.debug("check for update item:'{}' - draft':{}' -> {}", itemValue, draftValue, returnValue);
		return returnValue;
	}

	public static String generateContainerKey(final String key) {
		if (StringUtils.isNotBlank(key)) {
			return key.replaceAll("://", "_").replaceAll("/", "_").replaceAll(" ", "_");
		}
		return key;
	}

	public static TypeDraft generateTypeDraft(final Map<String, Object> value, final List<TypeAttribute> attributes) {

		final TypePojo pojo = new TypePojo(value);
		if (attributes != null) {
			if (LOG.isInfoEnabled()) {
				GDriveHelper.printAttributes(pojo.getKey(), attributes);
			}
			final List<TypeAttribute> fieldDefinitions = new ArrayList<>();
			for (TypeAttribute attribute : attributes) {
				fieldDefinitions.add(TypeHelper.toFieldDefinition(attribute));
			}
			if (!fieldDefinitions.isEmpty()) {
				pojo.setFieldDefinitions(fieldDefinitions);
			}
		}
		return pojo.toDraft();
	}

	public static ProductTypeDraft generateProductTypeDraft(final Map<String, Object> value,
			final List<TypeAttribute> attributes) {
		return new ProductTypePojo().withSheetData(value, attributes).toDraft();
	}
}

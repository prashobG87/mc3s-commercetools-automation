package com.mindcurv.commercetools.automation.helper.spreadsheet;

import java.io.FileReader;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.IteratorUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.MemoryDataStoreFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.Sheets.Spreadsheets;
import com.google.api.services.sheets.v4.Sheets.Spreadsheets.Get;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.Constants;
import com.mindcurv.commercetools.automation.integration.gdrive.GDriveSettings;
import com.mindcurv.commercetools.automation.pojo.BasePojo;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;
import com.mindcurv.commercetools.automation.pojo.organization.AddressPojo;
import com.mindcurv.commercetools.automation.pojo.types.product.TypeAttribute;

public class GDriveHelper {

	// https://www.baeldung.com/google-sheets-java-client
	private final static Logger LOG = LoggerFactory.getLogger(GDriveHelper.class);

	public static Credential authorizeOauth() throws IOException, GeneralSecurityException {
		final FileReader in = new FileReader(new java.io.File(GDriveSettings.DEFAULT_GDRIVE_OAUTH_SECRETS_PATH));
		final GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JacksonFactory.getDefaultInstance(), in);

		final List<String> scopes = Arrays.asList(SheetsScopes.SPREADSHEETS_READONLY);

		final GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
				GoogleNetHttpTransport.newTrustedTransport(), JacksonFactory.getDefaultInstance(), clientSecrets,
				scopes).setDataStoreFactory(new MemoryDataStoreFactory()).setAccessType("offline").build();
		final Credential credential = new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver())
				.authorize("user");

		in.close();

		return credential;
	}

	public static Sheets getSheetsServiceOauth() throws IOException, GeneralSecurityException {
		final Credential credential = authorizeOauth();
		LOG.info("GDrive Sheet token {}", credential.getAccessToken());
		return new Sheets.Builder(GoogleNetHttpTransport.newTrustedTransport(), JacksonFactory.getDefaultInstance(),
				credential).setApplicationName(GDriveSettings.DEFAULT_APPLICATION_NAME).build();

	}

	public static Map<String, List<TypeAttribute>> prepareAttributesListsMap(final Spreadsheets sheets,
			final String sheetId, final CommercetoolsProject project) {

		final List<Map<String, Object>> attributeLines;
		try {
			attributeLines = processRange(sheets, sheetId, CsvConstants.Attributes.SHEETNAME, project);

		} catch (IOException e) {
			LOG.error("Exception while accessing Sheet " + CsvConstants.Attributes.SHEETNAME, e);
			return null;
		}

		return ValueHelper.processAttributeLines(attributeLines);
	}

	public static Spreadsheets accessSheets(final String sheetId) throws IOException, GeneralSecurityException {
		final Spreadsheets sheets = getSheetsServiceOauth().spreadsheets();
		LOG.info("Sheets {}", sheets);

		final Get sheet = sheets.get(sheetId);

		LOG.info("Sheet {}", sheet.getSpreadsheetId());
		return sheets;
	}

	public static List<Map<String, Object>> processRange(final Spreadsheets sheets, final String sheetId,
			final String range, CommercetoolsProject project) throws IOException {

		ValueRange response = sheets.values().get(sheetId, range).execute();
		List<List<Object>> responseValues = response.getValues();

		if (responseValues == null || responseValues.isEmpty()) {
			LOG.error("No data found.");
		} else {
			final List<Map<String, Object>> values = new ArrayList<>();
			final List<Object> headers = new ArrayList<>();

			int counter = 0;
			for (List<Object> row : responseValues) {
				LOG.debug("{} : {} - {}", counter, row.size(), row);
				final List<Object> valuesList = IteratorUtils.toList(row.iterator());

				if (headers.isEmpty()) {
					headers.addAll(valuesList);
				} else {
					final Map<String, Object> valuesMap = new HashMap<>();
					int valueIndex = 0;
					for (Object valueObject : valuesList) {
						final String key = (String) headers.get(valueIndex);
						if (isValidKey(key)) {
							valuesMap.put(key, valueObject);
						}
						valueIndex++;
					}
					boolean validLine = ValueHelper.checkLineValues(project, valuesMap);
					if (validLine) {
						values.add(valuesMap);
					}
				}
				counter++;
			}
			return values;

		}
		return null;

	}

	public static Map<String,List<BasePojo>> prepareAddressPojos(final Spreadsheets sheets, final String sheetId,
			final String type, CommercetoolsProject project) {
		final Map<String,List<BasePojo>> addressPojoMap = new HashMap<>(); 
		try {
			if (StringUtils.isNotBlank(type)) {
				for (Map<String, Object> valueLine : processRange(sheets, sheetId, CsvConstants.Address.SHEETNAME,
						project)) {
					AddressPojo addressPojo = new AddressPojo().withSheetData(valueLine);
					final String referenceKey = addressPojo.getReferenceId();
					List<BasePojo> addressesForReference = addressPojoMap.get(referenceKey); 
					if (addressesForReference == null) {
						addressesForReference = new ArrayList<>();
						addressPojoMap.put(referenceKey, addressesForReference); 
					}
					addressesForReference.add(addressPojo); 
				}
			}
		} catch (Exception e) {
			LOG.error("Exception during address parsing", e);
		}
		return addressPojoMap;
	}

	private static boolean isValidKey(final String key) {
		return !key.startsWith("#");
	}

	public static void printAttributesMap(final Map<String, List<TypeAttribute>> attributesMap) {
		if (LOG.isInfoEnabled()) {
			for (String key : attributesMap.keySet()) {
				LOG.info(Constants.MSG_SPACER);
				final List<TypeAttribute> attributesList = attributesMap.get(key);
				printAttributes(key, attributesList);
			}
		}
	}

	public static void printAttributes(String key, final List<TypeAttribute> attributesList) {
		if (LOG.isInfoEnabled()) {
			LOG.info("Attributes for '{}': {} attributes defined", key, attributesList.size());
			for (TypeAttribute typeAttribute : attributesList) {

				final String typeInfo = typeAttribute.getType().getName();
				LOG.info("\t{}[{}]", typeAttribute.getName(), typeInfo);
			}
		}
	}

}

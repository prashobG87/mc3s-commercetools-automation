package com.mindcurv.commercetools.automation.services.load.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.commercetools.sync.inventories.InventorySync;
import com.commercetools.sync.inventories.InventorySyncOptions;
import com.commercetools.sync.inventories.InventorySyncOptionsBuilder;
import com.commercetools.sync.inventories.helpers.InventorySyncStatistics;
import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.integration.SyncStatistics;
import com.mindcurv.commercetools.automation.services.load.AbstractLoadService;

import io.sphere.sdk.inventory.InventoryEntryDraft;

public class InventoryLoadService extends AbstractLoadService<InventoryEntryDraft> {

	private final static Logger LOG = LoggerFactory.getLogger(InventoryLoadService.class);

	public InventoryLoadService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	@Override
	protected void init() {
		super.init();
		
		setTypeParameterClass(InventoryEntryDraft.class);
	}

	@Override
	public SyncStatistics executeSync(final List<InventoryEntryDraft> drafts) {

		final SyncStatistics syncStatistics = new SyncStatistics();
		if (validForSync(drafts)) {
			final InventorySyncOptions syncOptions = InventorySyncOptionsBuilder.of(getClient())
					.errorCallback(LOG::error).warningCallback(LOG::warn).build();

			final InventorySync sync = new InventorySync(syncOptions);
			final InventorySyncStatistics syncStatisticsStage = sync.sync(drafts).toCompletableFuture().join();

			syncStatistics.setValues(syncStatisticsStage);
		}
		LOG.info("finished inventory sync:{}", syncStatistics);
		return syncStatistics;
	}

}

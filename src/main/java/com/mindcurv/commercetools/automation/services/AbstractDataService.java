package com.mindcurv.commercetools.automation.services;

import io.sphere.sdk.client.SphereClient;

public interface AbstractDataService {
	
	boolean setupData(SphereClient client);

	boolean cleanup(SphereClient client);

	void init(SphereClient client);

}

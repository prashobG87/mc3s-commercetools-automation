package com.mindcurv.commercetools.automation.services.query.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.services.query.QueryService;

import io.sphere.sdk.carts.Cart;
import io.sphere.sdk.carts.queries.CartQueryBuilder;
import io.sphere.sdk.carts.queries.CartQueryModel;
import io.sphere.sdk.queries.PagedQueryResult;
import io.sphere.sdk.queries.QueryPredicate;

public class CartQueryService extends QueryService<Cart> {

	private final static Logger LOG = LoggerFactory.getLogger(CartQueryService.class);

	public CartQueryService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	@Override
	public List<Cart> getQueryResult(final Map<String, Object> params) {

		final List<Cart> result = new ArrayList<>();
		Long limit = params.containsKey(PARAM_LIMIT) ? (Long) params.get(PARAM_LIMIT) : DEFAULT_LIMIT;
		final CartQueryBuilder queryBuilder = CartQueryBuilder.of().fetchTotal(true);
		queryBuilder.limit(limit);

		final List<QueryPredicate<Cart>> predicates = preparePredicates(params);
		if (!predicates.isEmpty()) {
			queryBuilder.predicates(predicates);
		}

		long page = 1;
		boolean breakLoop = false;
		while (!breakLoop) {
			long offset = (page - 1) * limit;
			queryBuilder.offset(offset);
			final PagedQueryResult<Cart> pagedQueryResult = getClient().executeBlocking(queryBuilder.build());
			final List<Cart> pageResult = pagedQueryResult.getResults();
			if (pagedQueryResult.getTotal() == 0) {
				breakLoop = true;
			} else {
				result.addAll(pageResult);
				LOG.debug("found {} Carts on page {} of {} pages [{} entries]", pageResult.size(), page,
						pagedQueryResult.getTotalPages(), pagedQueryResult.getTotal());
				if (page == pagedQueryResult.getTotalPages()) {
					breakLoop = true;
				}
				page++;
			}
		}
		LOG.debug("returning total: {} Carts", result.size());
		return result;
	}

	@Override
	public List<QueryPredicate<Cart>> preparePredicates(final Map<String, Object> params) {
		final List<QueryPredicate<Cart>> predicates = new ArrayList<>();
		if (params.containsKey(PARAM_ID)) {
			final String id = (String) params.get(PARAM_ID);
			predicates.add(CartQueryModel.of().id().is(id));
		}

		return predicates;
	}

	@Override
	protected boolean supportsByKeyQuery() {
		return false;
	}
}

package com.mindcurv.commercetools.automation.services.setup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.services.sheets.v4.Sheets.Spreadsheets;
import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.helper.spreadsheet.ExcelHelper;
import com.mindcurv.commercetools.automation.helper.spreadsheet.GDriveHelper;
import com.mindcurv.commercetools.automation.pojo.BasePojo;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;
import com.mindcurv.commercetools.automation.pojo.types.product.TypeAttribute;
import com.mindcurv.commercetools.automation.services.CommercetoolsBaseService;
import com.mindcurv.commercetools.automation.services.query.impl.ProductTypeQueryService;

import io.sphere.sdk.commands.UpdateAction;
import io.sphere.sdk.products.Product;
import io.sphere.sdk.products.ProductVariant;
import io.sphere.sdk.products.attributes.Attribute;
import io.sphere.sdk.products.attributes.AttributeDraft;
import io.sphere.sdk.products.commands.updateactions.SetAttribute;
import io.sphere.sdk.producttypes.ProductType;

public abstract class SetupService extends CommercetoolsBaseService {
	
	private final static Logger LOG = LoggerFactory.getLogger(SetupService.class);
	
	private ProductTypeQueryService productTypeQueryService; 
	
	private Map<String, List<BasePojo>> associatedPojos = new HashMap<>(); 
	
	public SetupService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	abstract public boolean cleanupData() throws Exception;

	abstract protected boolean setupValues(List<Map<String, Object>> valueLines, final Map<String, List<TypeAttribute>> attributesMap);
	
	abstract public String getSheetname();
	
	public boolean setup(Workbook workbook, final Map<String, List<TypeAttribute>> attributesMap) {

		final List<Map<String, Object>> valueLines;
		try {
			valueLines = getValueLinesFromExcel(workbook);
		} catch (IOException e) {
			LOG.error("Exception while accessing Workbook '" + getSheetname() + "'", e);
			return false;
		}
		return setupValues(valueLines, attributesMap);
	}
	
	public boolean setup(String sheetId, Spreadsheets sheets, final Map<String, List<TypeAttribute>> attributesMap) {

		final List<Map<String, Object>> valueLines;
		try {
			valueLines = getValueLinesFromGoogleSheet(sheets, sheetId);
		} catch (IOException e) {
			LOG.error("Exception while accessing Google Sheet '" + getSheetname() + "'", e);
			return false;
		}

		return setupValues(valueLines, attributesMap);

	}
	
	public Map<String, List<BasePojo>> getAssociatedPojos() {
		return associatedPojos;
	}

	public void setAssociatedPojos(Map<String, List<BasePojo>> associatedPojos) {
		this.associatedPojos = associatedPojos;
	}

	protected List<Map<String, Object>> getValueLinesFromExcel(Workbook workbook) throws IOException {
		return ExcelHelper.processRange(workbook, getSheetname(), getCommercetoolsProject());
	}

	protected List<Map<String, Object>> getValueLinesFromGoogleSheet(Spreadsheets sheets, String sheetId) throws IOException {
		return GDriveHelper.processRange(sheets, sheetId, getSheetname() + CsvConstants.SHEET_RANGE, getCommercetoolsProject());
	}
	
	protected void attachObjectToReferencedItem(final Object referencedItem) {
		LOG.info( "referencedItem {}", referencedItem);
		
		String referencedtype = null; 
		if (referencedItem instanceof Product) {
			Product product = (Product) referencedItem;
			if ( product.getProductType() != null) {
				final ProductType productType = productTypeQueryService.getById(product.getProductType().getId()); 
				if ( productType != null) {
					referencedtype = productType.getKey(); 
				}
			} else {
				//TypeQuery
			}
		}
		
		if (StringUtils.isNotBlank(referencedtype)) {
			LOG.info( "Type {}", referencedtype);
		} else {
			LOG.warn("Unsupported referencedItem {}", referencedItem);
		}
//			if (referencedtype.equals("product")) {
//				final ProductQueryService queryService = (ProductQueryService) getQueryConfigurationMap()
//						.get(referencedtype + "QueryService");
//				LOG.info("QueryService {}", queryService);
//				final String referenceditem = ValueHelper
//						.getValueAsString(CsvConstants.CustomObject.REFERENCEDITEM_KEY, value);
//				final Product product = queryService.getBySku(referenceditem);
//				if ( product == null) {
//					LOG.error("no product with SKU {}  found ", referenceditem);
//				} else {
//					LOG.info("Product key {} v{}", product.getKey(), product.getVersion());
//					List<UpdateAction<Product>> updateActions = prepareUpdateActions(product, referenceditem, container, customObjectId); 
//					if ( !updateActions.isEmpty()) {
//						final Product result = getCommercetoolsProject().getClient().executeBlocking(ProductUpdateCommand.of(product, updateActions));
//						LOG.info( "Update result {}", result.getKey(), result.getVersion());
//					}
//				}
//				
//		} else {
//				// category
//				// channel
//				// customer
//				// customer-group
//				// cart
//				// product-price
//				// order
//				// line-item
//				// custom-line-item
//				// inventory-entry
//				// product-type
		
				// asset
				// cart-discount
				// discount-code
  				// payment
 				// payment-interface-interaction
 				// shopping-list
 				// shopping-list-text-line-item
 				// review
 				
//			}
//		}
	}
	
	protected List<UpdateAction<Product>> prepareUpdateActions(final Product referencedItem, final String sku, final String container,
			final String customObjectId) {
		
		//get custom type
		//try to find attributes with keyvalue document
		//seach for container / key for product 
		// attach if not present 
		
		
		LOG.info("Updating attribute '{}' for sku {}", container, sku);
		final List<UpdateAction<Product>> actions = new ArrayList<>();
		for (ProductVariant variant : referencedItem.getMasterData().getStaged().getAllVariants()) {
			boolean addToActions = false; 
			
			if ( variant.getSku().equals(sku)) {
				final Attribute existingValue = variant.getAttribute(container); 
				if (existingValue == null) {
					addToActions = true; 
				} else {
					final String id = existingValue.getValueAsJsonNode().get("id").toString();
					LOG.info( "existingValue {}", id);
					if ( !id.equals(customObjectId)) {
						addToActions = true; 
					}
				}
			}
			if ( addToActions) {
				final Map<String, String> map = new HashMap<>();
				map.put("id", customObjectId);
				map.put("typeId", "key-value-document");
				
				actions.add(SetAttribute.of(variant.getId(), AttributeDraft.of(container, map)));
				LOG.info( "Update Variant {} : {} ", variant.getSku() , variant.getId());
			} else {
				LOG.debug( "Skip Variant {} : {} ", variant.getSku() , variant.getId());
			}
		}
		return actions;
	}
}

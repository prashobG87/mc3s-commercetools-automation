package com.mindcurv.commercetools.automation.services.reports.datasource;

import java.util.ArrayList;
import java.util.List;

import net.sf.jasperreports.engine.JRDataSource;

public abstract class BaseJRDataSource<T> implements JRDataSource {

	
	private List<T> elements = new ArrayList<>();

	public List<T> getElements() {
		return elements;
	}

	public void setElements(List<T> elements) {
		this.elements = elements;
	}
}

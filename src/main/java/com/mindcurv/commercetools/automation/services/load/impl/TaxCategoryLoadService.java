package com.mindcurv.commercetools.automation.services.load.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.integration.SyncStatistics;
import com.mindcurv.commercetools.automation.services.load.AbstractLoadService;
import com.mindcurv.commercetools.automation.services.query.impl.TaxCategoryQueryService;

import io.sphere.sdk.commands.UpdateAction;
import io.sphere.sdk.taxcategories.TaxCategory;
import io.sphere.sdk.taxcategories.TaxCategoryDraft;
import io.sphere.sdk.taxcategories.TaxRate;
import io.sphere.sdk.taxcategories.TaxRateDraft;
import io.sphere.sdk.taxcategories.commands.TaxCategoryCreateCommand;
import io.sphere.sdk.taxcategories.commands.TaxCategoryUpdateCommand;
import io.sphere.sdk.taxcategories.commands.updateactions.AddTaxRate;
import io.sphere.sdk.taxcategories.commands.updateactions.ChangeName;
import io.sphere.sdk.taxcategories.commands.updateactions.RemoveTaxRate;
import io.sphere.sdk.taxcategories.commands.updateactions.ReplaceTaxRate;
import io.sphere.sdk.taxcategories.commands.updateactions.SetDescription;

public class TaxCategoryLoadService extends AbstractLoadService<TaxCategoryDraft> {

	private final static Logger LOG = LoggerFactory.getLogger(TaxCategoryLoadService.class);

	private TaxCategoryQueryService taxCategoryQueryService;

	public TaxCategoryLoadService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	@Override
	protected void init() {
		super.init();
		setTypeParameterClass(TaxCategoryDraft.class);
		
		final CommercetoolsProject commercetoolsProjectBean = getCommercetoolsProject();
		if (commercetoolsProjectBean != null) {
			taxCategoryQueryService = new TaxCategoryQueryService(commercetoolsProjectBean);
		}
	}

	@Override
	public SyncStatistics executeSync(final List<TaxCategoryDraft> drafts) {
		final SyncStatistics syncStatistics = new SyncStatistics();
		if (validForSync(drafts)) {
			for (TaxCategoryDraft draft : drafts) {
				syncStatistics.incrementProcessed();

				TaxCategory result = null;
				final TaxCategory taxCategory = taxCategoryQueryService.getByKey(draft.getKey());
				if (taxCategory == null) {
					final TaxCategoryCreateCommand command = TaxCategoryCreateCommand.of(draft);
					result = getClient().executeBlocking(command);
					syncStatistics.incrementCreated();

				} else {
					final List<UpdateAction<TaxCategory>> updateActions = new ArrayList<>();
					if (!draft.getName().equals(taxCategory.getName())) {
						updateActions.add(ChangeName.of(draft.getName()));
					}
					if (!draft.getDescription().equals(taxCategory.getDescription())) {
						updateActions.add(SetDescription.of(draft.getDescription()));
					}

					final Map<String, TaxRate> existingTaxRates = new HashMap<>();
					final Map<String, TaxRate> ratesToBeRemoved = new HashMap<>();
					for (TaxRate taxRate : taxCategory.getTaxRates()) {
						existingTaxRates.put(taxRate.getName(), taxRate);
					}
					ratesToBeRemoved.putAll(existingTaxRates);
					for (TaxRateDraft taxRateDraft : draft.getTaxRates()) {
						final TaxRate taxRate = existingTaxRates.get(taxRateDraft.getName());
						if (taxRate == null) {
							updateActions.add(AddTaxRate.of(taxRateDraft));
						} else {
							updateActions.add(ReplaceTaxRate.of(taxRate.getId(), taxRateDraft));
							ratesToBeRemoved.remove(taxRate.getName());
						}
					}
					for (Map.Entry<String, TaxRate> entry : ratesToBeRemoved.entrySet()) {
						updateActions.add(RemoveTaxRate.of(entry.getValue().getId()));
					}
					if (!updateActions.isEmpty()) {
						final TaxCategoryUpdateCommand command = TaxCategoryUpdateCommand.of(taxCategory,
								updateActions);
						result = getClient().executeBlocking(command);
						syncStatistics.incrementUpdated();
					}
				}
				LOG.info("Result for TaxCategoryDraft {}: {}", draft.getKey(), result);

			}
		}
		return syncStatistics;
	}

	@Required
	public void setTaxCategoryQueryService(TaxCategoryQueryService taxCategoryQueryService) {
		this.taxCategoryQueryService = taxCategoryQueryService;
	}

}


package com.mindcurv.commercetools.automation.services.setup.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;
import com.mindcurv.commercetools.automation.pojo.catalog.InventoryPojo;
import com.mindcurv.commercetools.automation.pojo.types.product.TypeAttribute;
import com.mindcurv.commercetools.automation.services.load.impl.InventoryLoadService;
import com.mindcurv.commercetools.automation.services.query.impl.InventoryQueryService;
import com.mindcurv.commercetools.automation.services.setup.SetupService;

import io.sphere.sdk.inventory.InventoryEntry;
import io.sphere.sdk.inventory.InventoryEntryDraft;
import io.sphere.sdk.inventory.commands.InventoryEntryDeleteCommand;
import io.sphere.sdk.inventory.queries.InventoryEntryQuery;
import io.sphere.sdk.inventory.queries.InventoryEntryQueryBuilder;

public class InventorySetupService extends SetupService {

	private final static Logger LOG = LoggerFactory.getLogger(InventorySetupService.class);

	private InventoryLoadService inventoryLoadService;
	private InventoryQueryService inventoryQueryService;

	public InventorySetupService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	@Override
	protected void init() {
		super.init();

		final CommercetoolsProject commercetoolsProjectBean = getCommercetoolsProject();
		if (commercetoolsProjectBean != null) {
			if (inventoryLoadService == null) {
				inventoryLoadService = new InventoryLoadService(commercetoolsProjectBean);
			}
			if (inventoryQueryService == null) {
				inventoryQueryService = new InventoryQueryService(commercetoolsProjectBean);
			}
		}

	}

	public boolean cleanupData() {
		LOG.info("cleanup InventoryEntry items");
		InventoryEntryQuery query = InventoryEntryQueryBuilder.of().limit(500).build();
		for (InventoryEntry inventoryEntry : getClient().executeBlocking(query).getResults()) {

			final InventoryEntry result = getClient().executeBlocking(InventoryEntryDeleteCommand.of(inventoryEntry));
			LOG.info("Removed InventoryEntry {}", result);
		}
		return true;
	}

	@Override
	protected boolean setupValues(List<Map<String, Object>> valueLines, Map<String, List<TypeAttribute>> attributesMap) {
		final List<InventoryEntryDraft> drafts = new ArrayList<>();
		for (Map<String, Object> value : valueLines) {
			final InventoryPojo pojo = new InventoryPojo(value);
			if (StringUtils.isNotBlank(pojo.getSku())) {
				drafts.add(pojo.toDraft());
			}
		}
		inventoryLoadService.sync(drafts);
		return true;
	}

	@Override
	public String getSheetname() {
		return CsvConstants.Inventory.SHEET;
	}

	public static Logger getLog() {
		return LOG;
	}

	@Required
	public void setInventoryLoadService(InventoryLoadService inventoryLoadService) {
		this.inventoryLoadService = inventoryLoadService;
	}

	@Required
	public void setInventoryQueryService(InventoryQueryService inventoryQueryService) {
		this.inventoryQueryService = inventoryQueryService;
	}

}

package com.mindcurv.commercetools.automation.services.query.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.services.query.QueryService;

import io.sphere.sdk.queries.PagedQueryResult;
import io.sphere.sdk.queries.QueryPredicate;
import io.sphere.sdk.types.Type;
import io.sphere.sdk.types.queries.TypeQueryBuilder;
import io.sphere.sdk.types.queries.TypeQueryModel;

@Configuration
public class TypeQueryService extends QueryService<Type> {

	private final static Logger LOG = LoggerFactory.getLogger(TypeQueryService.class);

	public TypeQueryService(CommercetoolsProject commercetoolsProject ) {
		super(commercetoolsProject);
	}

	@Override
	public List<Type> getQueryResult(Map<String, Object> params) {
		final List<Type> result = new ArrayList<>();
		Long limit = params.containsKey(PARAM_LIMIT) ? (Long) params.get(PARAM_LIMIT) : DEFAULT_LIMIT;
		final TypeQueryBuilder queryBuilder = TypeQueryBuilder.of().fetchTotal(true);
		queryBuilder.limit(limit);

		final List<QueryPredicate<Type>> predicates = preparePredicates(params);
		if (!predicates.isEmpty()) {
			queryBuilder.predicates(predicates);
		}

		long page = 1;
		boolean breakLoop = false;
		while (!breakLoop) {
			long offset = (page - 1) * limit;
			queryBuilder.offset(offset);
			final PagedQueryResult<Type> pagedQueryResult = getClient().executeBlocking(queryBuilder.build());
			final List<Type> pageResult = pagedQueryResult.getResults();
			if (pagedQueryResult.getTotal() == 0) {
				breakLoop = true;
			} else {
				result.addAll(pageResult);
				LOG.debug("found {} Types on page {} of {} pages [{} entries]", pageResult.size(), page,
						pagedQueryResult.getTotalPages(), pagedQueryResult.getTotal());
				if (page == pagedQueryResult.getTotalPages()) {
					breakLoop = true;
				}
				page++;
			}
		}
		LOG.debug("returning total: {} Types", result.size());
		return result;
	}

	@Override
	public List<QueryPredicate<Type>> preparePredicates(Map<String, Object> params) {
		final List<QueryPredicate<Type>> predicates = new ArrayList<>();
		if (params.containsKey(PARAM_ID)) {
			final String id = (String) params.get(PARAM_ID);
			predicates.add(TypeQueryModel.of().id().is(id));
		}
		if (params.containsKey(PARAM_KEY)) {
			final String key = (String) params.get(PARAM_KEY);
			predicates.add(TypeQueryModel.of().key().is(key));
		}
		return predicates;
	}

	@Override
	protected boolean supportsByKeyQuery() {
		return true;
	}
}

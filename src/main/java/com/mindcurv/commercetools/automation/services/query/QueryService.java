package com.mindcurv.commercetools.automation.services.query;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.services.CommercetoolsBaseService;

import io.sphere.sdk.queries.QueryPredicate;

@Configuration
public abstract class QueryService<T> extends CommercetoolsBaseService {

	private final static Logger LOG = LoggerFactory.getLogger(QueryService.class);

	public static final String PARAM_LIMIT = "limit";
	public static final String PARAM_ID = "id";
	public static final String PARAM_KEY = "key";
	public static final String PARAM_KEYLIST = "keyList";

	public static final Long DEFAULT_LIMIT = Long.valueOf(20);
	public static final Long ALL_LIMIT = Long.valueOf(100);

	public QueryService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	public abstract List<T> getQueryResult(final Map<String, Object> params);

	public abstract List<QueryPredicate<T>> preparePredicates(final Map<String, Object> params);

	protected abstract boolean supportsByKeyQuery();

	public List<T> getAllEntries() {
		final Map<String, Object> params = new HashMap<>();
		params.put(PARAM_LIMIT, Long.valueOf(500));
		return getQueryResult(params);
	}

	public T getSingleResult(final Map<String, Object> params) {
		List<T> result = getQueryResult(params);
		if (result != null) {
			LOG.debug("Found {} items", result.size());
			if (!result.isEmpty()) {
				return result.get(0);
			}
		}
		return null;
	}

	public T getById(final String id) {

		final Map<String, Object> params = new HashMap<>();
		params.put(PARAM_ID, id);
		return getSingleResult(params);
	}

	public T getByKey(final String key) {
		if (supportsByKeyQuery()) {
			final Map<String, Object> params = new HashMap<>();
			params.put(PARAM_KEY, key);
			return getSingleResult(params);
		}
		throw new UnsupportedOperationException("Method getByKey() is not supported for this type");
	}

	public List<T> getByKeys(final List<String> keyList) {
		if (supportsByKeyQuery()) {
			final Map<String, Object> params = new HashMap<>();
			params.put(PARAM_KEYLIST, keyList);
			params.put(PARAM_LIMIT, DEFAULT_LIMIT);
			return getQueryResult(params);
		}
		throw new UnsupportedOperationException("Method getByKey() is not supported for this type");
	}

	public void printResult(final T result) {
		if (result != null) {
			LOG.debug("found {}", result);
		} else {
			LOG.warn("no result found");
		}
	}
}

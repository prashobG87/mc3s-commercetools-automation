package com.mindcurv.commercetools.automation.services.reports.datasource.impl;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mindcurv.commercetools.automation.services.reports.datasource.BaseJRDataSource;

import io.sphere.sdk.orders.Order;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

/**
 * 
 * https://community.jaspersoft.com/wiki/how-create-and-use-jrdatasource-adapter
 * 
 * @author dirkpaschke
 *
 */

public class OrdersJRDataSource extends BaseJRDataSource<Order> {

	private final static Logger LOG = LoggerFactory.getLogger(OrdersJRDataSource.class);

	private int counter = -1;

	public OrdersJRDataSource() {

	}

	@Override
	public Object getFieldValue(JRField jrField) throws JRException {
		Order order = getElements().get(counter);
		Object returnValue = null;
		if (jrField.getName().equalsIgnoreCase("id")) {
			returnValue = order.getId();
		} else if (jrField.getName().equalsIgnoreCase("orderNumber")
				&& StringUtils.isNotBlank(order.getOrderNumber())) {
			returnValue = order.getOrderNumber();
		} else if (jrField.getName().equalsIgnoreCase("customerEmail")) {
			returnValue = order.getCustomerEmail();
		}
		LOG.debug("getFieldValue ({}); -> {}", jrField.getName(), returnValue);
		return returnValue;
	}

	@Override
	public boolean next() throws JRException {
		if (counter < getElements().size() - 1) {
			counter++;
			return true;
		}
		return false;
	}

	public static JRDataSource getDataSource() {
		return new OrdersJRDataSource();
	}

	public String toString() {
		StringBuffer stringBuffer = new StringBuffer(super.toString());
		stringBuffer.append(" -> ").append(getElements().size()).append(" elements");
		return stringBuffer.toString();
	}

}


package com.mindcurv.commercetools.automation.services.setup.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.pojo.types.product.TypeAttribute;
import com.mindcurv.commercetools.automation.services.query.impl.ProductTypeQueryService;
import com.mindcurv.commercetools.automation.services.setup.SetupService;

import io.sphere.sdk.producttypes.ProductType;
import io.sphere.sdk.producttypes.commands.ProductTypeDeleteCommand;

public class ProductTypeSetupService extends SetupService {

	private final static Logger LOG = LoggerFactory.getLogger(ProductTypeSetupService.class);

	private ProductTypeQueryService productTypeQueryService;

	public ProductTypeSetupService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	@Override
	protected void init() {
		super.init();

		final CommercetoolsProject commercetoolsProjectBean = getCommercetoolsProject();
		if (commercetoolsProjectBean != null) {
			if (productTypeQueryService == null) {
				productTypeQueryService = new ProductTypeQueryService(commercetoolsProjectBean);
			}
		}
	}

	@Override
	public boolean cleanupData() {
		LOG.info("cleanup ProductType items");
		for (ProductType productType : productTypeQueryService.getAllEntries()) {
			ProductTypeDeleteCommand command = ProductTypeDeleteCommand.of(productType);
			productType = getClient().executeBlocking(command);
			LOG.info("Removed ProductType {}", productType);
		}
		return true;
	}

	@Override
	protected boolean setupValues(List<Map<String, Object>> valueLines, Map<String, List<TypeAttribute>> attributesMap) {
		LOG.warn("setupValues() not implemented()");
		return false;
	}

	@Override
	public String getSheetname() {
		LOG.warn("getSheetname() not implemented()");
		return null;
	}

}

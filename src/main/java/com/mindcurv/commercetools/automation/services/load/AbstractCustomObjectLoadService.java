package com.mindcurv.commercetools.automation.services.load;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.services.CommercetoolsBaseService;

import io.sphere.sdk.customobjects.CustomObject;
import io.sphere.sdk.customobjects.CustomObjectDraft;
import io.sphere.sdk.customobjects.commands.CustomObjectUpsertCommand;

public abstract class AbstractCustomObjectLoadService<T> extends CommercetoolsBaseService {

	private Class<T> typeParameterClass;

	public AbstractCustomObjectLoadService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject); 
	}

	@SuppressWarnings("unchecked")
	public CustomObject<T> syncCustomObject(final String container, final String key, final Object customobject) {
		return createCustomObject(container, key, (T) customobject);
	}

	abstract public CustomObject<T> createCustomObject(final String container, final String key, final T customobject);

	public Class<T> getTypeParameterClass() {
		return typeParameterClass;
	}

	protected void setTypeParameterClass(Class<T> typeParameterClass) {
		this.typeParameterClass = typeParameterClass;
	}

	protected CustomObject<T> sync(String container, final String fullKey, T customobject) {
		final CustomObjectDraft<T> draft = CustomObjectDraft.ofUnversionedUpsert(container, fullKey, customobject,
				typeParameterClass);
		final CustomObjectUpsertCommand<T> createCommand = CustomObjectUpsertCommand.of(draft);
		return getClient().executeBlocking(createCommand);
	}
}

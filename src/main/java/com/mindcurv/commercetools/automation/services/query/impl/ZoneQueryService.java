package com.mindcurv.commercetools.automation.services.query.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.services.query.QueryService;

import io.sphere.sdk.queries.PagedQueryResult;
import io.sphere.sdk.queries.QueryPredicate;
import io.sphere.sdk.zones.Zone;
import io.sphere.sdk.zones.queries.ZoneQueryBuilder;
import io.sphere.sdk.zones.queries.ZoneQueryModel;

public class ZoneQueryService extends QueryService<Zone> {

	private final static Logger LOG = LoggerFactory.getLogger(ZoneQueryService.class);

	public static final String PARAM_NAME = "name";

	public ZoneQueryService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	@Override
	public List<Zone> getQueryResult(Map<String, Object> params) {
		final List<Zone> result = new ArrayList<>();
		Long limit = params.containsKey(PARAM_LIMIT) ? (Long) params.get(PARAM_LIMIT) : DEFAULT_LIMIT;
		final ZoneQueryBuilder queryBuilder = ZoneQueryBuilder.of().fetchTotal(true);
		queryBuilder.limit(limit);

		final List<QueryPredicate<Zone>> predicates = preparePredicates(params);
		if (!predicates.isEmpty()) {
			queryBuilder.predicates(predicates);
		}

		long page = 1;
		boolean breakLoop = false;
		while (!breakLoop) {
			long offset = (page - 1) * limit;
			queryBuilder.offset(offset);
			final PagedQueryResult<Zone> pagedQueryResult = getClient().executeBlocking(queryBuilder.build());
			final List<Zone> pageResult = pagedQueryResult.getResults();
			if (pagedQueryResult.getTotal() == 0) {
				breakLoop = true;
			} else {
				result.addAll(pageResult);
				LOG.debug("found {} Zones on page {} of {} pages [{} entries]", pageResult.size(), page,
						pagedQueryResult.getTotalPages(), pagedQueryResult.getTotal());
				if (page == pagedQueryResult.getTotalPages()) {
					breakLoop = true;
				}
				page++;
			}
		}
		LOG.debug("returning total: {} Zones", result.size());
		return result;
	}

	@Override
	public List<QueryPredicate<Zone>> preparePredicates(final Map<String, Object> params) {
		final List<QueryPredicate<Zone>> predicates = new ArrayList<>();
		if (params.containsKey(PARAM_ID)) {
			final String id = (String) params.get(PARAM_ID);
			predicates.add(ZoneQueryModel.of().id().is(id));
		}
		if (params.containsKey(PARAM_NAME)) {
			final String name = (String) params.get(PARAM_NAME);
			predicates.add(ZoneQueryModel.of().name().is(name));
		}
		return predicates;
	}

	public Zone getByName(String name) {
		final Map<String, Object> params = new HashMap<>();
		params.put(PARAM_NAME, name);
		return getSingleResult(params);
	}

	@Override
	protected boolean supportsByKeyQuery() {
		return false;
	}
}

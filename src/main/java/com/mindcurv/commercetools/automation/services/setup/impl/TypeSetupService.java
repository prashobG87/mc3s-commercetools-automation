package com.mindcurv.commercetools.automation.services.setup.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.google.api.services.sheets.v4.Sheets.Spreadsheets;
import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.helper.DraftHelper;
import com.mindcurv.commercetools.automation.helper.spreadsheet.ExcelHelper;
import com.mindcurv.commercetools.automation.helper.spreadsheet.GDriveHelper;
import com.mindcurv.commercetools.automation.helper.spreadsheet.ValueHelper;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;
import com.mindcurv.commercetools.automation.pojo.types.product.TypeAttribute;
import com.mindcurv.commercetools.automation.services.load.impl.ProductTypeLoadService;
import com.mindcurv.commercetools.automation.services.load.impl.TypeLoadService;
import com.mindcurv.commercetools.automation.services.setup.SetupService;

import io.sphere.sdk.models.WithKey;
import io.sphere.sdk.producttypes.ProductTypeDraft;
import io.sphere.sdk.types.Type;
import io.sphere.sdk.types.TypeDraft;
import io.sphere.sdk.types.commands.TypeDeleteCommand;
import io.sphere.sdk.types.queries.TypeQuery;

public class TypeSetupService extends SetupService {

	private final static Logger LOG = LoggerFactory.getLogger(TypeSetupService.class);

	private static final String WILDCARD = "*";
	private static final String PRODUCTTYPE_WILDCARD = "product-type" + WILDCARD;
	private static final String TYPE_WILDCARD = "type" + WILDCARD;

	private ProductTypeLoadService productTypeLoadService;

	private TypeLoadService typeLoadService;

	public TypeSetupService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);

	}

	@Override
	protected void init() {
		super.init();
		final CommercetoolsProject commercetoolsProjectBean = getCommercetoolsProject();
		if (commercetoolsProjectBean != null) {
			productTypeLoadService = new ProductTypeLoadService(commercetoolsProjectBean);
			typeLoadService = new TypeLoadService(commercetoolsProjectBean);
		}
	}

	public Map<String, List<TypeAttribute>> setup(Workbook workbook) throws IOException {
		final Map<String, List<TypeAttribute>> attributesListMap = ExcelHelper.prepareAttributesListsMap(workbook,
				getCommercetoolsProject());

		final Map<String, WithKey> draftMap = prepareTypes(workbook, attributesListMap);

		return setupValues(attributesListMap, draftMap);
	}

	public Map<String, List<TypeAttribute>> setup(final String sheetId, final Spreadsheets sheets,
			final CommercetoolsProject project) throws IOException {

		final Map<String, List<TypeAttribute>> attributesListMap = GDriveHelper.prepareAttributesListsMap(sheets,
				sheetId, project);

		final Map<String, WithKey> draftMap = prepareTypes(sheets, sheetId, attributesListMap);

		return setupValues(attributesListMap, draftMap);
	}

	@Override
	protected boolean setupValues(List<Map<String, Object>> valueLines,
			Map<String, List<TypeAttribute>> attributesMap) {
		LOG.warn("setupValues() not implemented()");
		return false;
	}

	public boolean cleanupData() {
		LOG.info("Cleanup Type items");

		for (Type type : getClient().executeBlocking(TypeQuery.of().withLimit(500)).getResults()) {
			TypeDeleteCommand command = TypeDeleteCommand.of(type);
			type = getClient().executeBlocking(command);
			LOG.info("Removed Type {}", type);
		}
		return true;

	}

	@Override
	public String getSheetname() {
		return CsvConstants.Type.SHEET;
	}

	private Map<String, WithKey> prepareTypes(final Spreadsheets sheets, final String sheetId,
			final Map<String, List<TypeAttribute>> attributesListMap) throws IOException {

		final List<Map<String, Object>> typeLines = GDriveHelper.processRange(sheets, sheetId,
				CsvConstants.Type.SHEETNAME, getCommercetoolsProject());

		return processTypeLines(attributesListMap, typeLines);
	}

	private Map<String, WithKey> prepareTypes(Workbook workbook, Map<String, List<TypeAttribute>> attributesListMap)
			throws IOException {
		final List<Map<String, Object>> typeLines = ExcelHelper.processRange(workbook, CsvConstants.Type.SHEET,
				getCommercetoolsProject());

		return processTypeLines(attributesListMap, typeLines);
	}

	private Map<String, WithKey> processTypeLines(final Map<String, List<TypeAttribute>> attributesListMap,
			final List<Map<String, Object>> typeLines) {
		final Map<String, WithKey> draftMap = new HashMap<>();
		for (Map<String, Object> value : typeLines) {

			final String type = ValueHelper.getValueAsString(CsvConstants.TYPE_KEY, value);
			if (StringUtils.isBlank(type)) {
				continue;
			}
			final String key = ValueHelper.getValueAsString(CsvConstants.KEY_KEY, value);

			final List<TypeAttribute> attributes = new ArrayList<>();
			if (attributesListMap.containsKey(WILDCARD)) {
				final List<TypeAttribute> wildcardAttributes = attributesListMap.get(WILDCARD);
				LOG.info("adding {} wildcard attribute(s)", wildcardAttributes.size());
				attributes.addAll(wildcardAttributes);
			}

			boolean isProductType = type.equals("product-type");
			if (isProductType) {
				if (attributesListMap.containsKey(PRODUCTTYPE_WILDCARD)) {
					final List<TypeAttribute> wildcardAttributes = attributesListMap.get(PRODUCTTYPE_WILDCARD);
					LOG.info("adding {} product type wildcard attribute(s)", wildcardAttributes.size());
					attributes.addAll(wildcardAttributes);
				}
			} else {
				if (attributesListMap.containsKey(TYPE_WILDCARD)) {
					final List<TypeAttribute> wildcardAttributes = attributesListMap.get(TYPE_WILDCARD);
					LOG.info("adding {} type wildcard attribute(s)", wildcardAttributes.size());
					attributes.addAll(wildcardAttributes);
				}
			}
			if (attributesListMap.containsKey(key)) {
				final List<TypeAttribute> specificAttributes = attributesListMap.get(key);
				LOG.info("adding {} attribute(s)", specificAttributes.size());
				attributes.addAll(specificAttributes);
			}

			final WithKey draft = isProductType ? DraftHelper.generateProductTypeDraft(value, attributes)
					: DraftHelper.generateTypeDraft(value, attributes);
			if (draft != null) {
				draftMap.put(draft.getKey(), draft);
			}
		}
		LOG.info("prepared drafts:{}", draftMap.size());
		return draftMap;
	}

	private Map<String, List<TypeAttribute>> setupValues(final Map<String, List<TypeAttribute>> attributesListMap,
			final Map<String, WithKey> draftMap) {
		final List<ProductTypeDraft> productDrafts = new ArrayList<>();
		final List<TypeDraft> typeDrafts = new ArrayList<>();

		for (Map.Entry<String, WithKey> entry : draftMap.entrySet()) {
			if (entry.getValue() instanceof ProductTypeDraft) {
				productDrafts.add((ProductTypeDraft) entry.getValue());
			} else if (entry.getValue() instanceof TypeDraft) {
				typeDrafts.add((TypeDraft) entry.getValue());
			}
		}
		if (!productDrafts.isEmpty()) {
			productTypeLoadService.sync(productDrafts);
		}

		if (!typeDrafts.isEmpty()) {
			typeLoadService.sync(typeDrafts);
		}

		return attributesListMap;
	}

	@Required
	public void setProductTypeLoadService(ProductTypeLoadService productTypeLoadService) {
		this.productTypeLoadService = productTypeLoadService;
	}

	@Required
	public void setTypeLoadService(TypeLoadService typeLoadService) {
		this.typeLoadService = typeLoadService;
	}

}

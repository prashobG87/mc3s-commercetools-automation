package com.mindcurv.commercetools.automation.services.load.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.fasterxml.jackson.databind.JsonNode;
import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.helper.DraftHelper;
import com.mindcurv.commercetools.automation.integration.SyncStatistics;
import com.mindcurv.commercetools.automation.services.load.AbstractLoadService;
import com.mindcurv.commercetools.automation.services.query.impl.ChannelQueryService;
import com.mindcurv.commercetools.automation.services.query.impl.TypeQueryService;

import io.sphere.sdk.channels.Channel;
import io.sphere.sdk.channels.ChannelDraft;
import io.sphere.sdk.channels.ChannelDraftBuilder;
import io.sphere.sdk.channels.ChannelRole;
import io.sphere.sdk.channels.commands.ChannelCreateCommand;
import io.sphere.sdk.channels.commands.ChannelUpdateCommand;
import io.sphere.sdk.channels.commands.updateactions.AddRoles;
import io.sphere.sdk.channels.commands.updateactions.ChangeDescription;
import io.sphere.sdk.channels.commands.updateactions.ChangeKey;
import io.sphere.sdk.channels.commands.updateactions.RemoveRoles;
import io.sphere.sdk.channels.commands.updateactions.SetAddress;
import io.sphere.sdk.channels.commands.updateactions.SetCustomField;
import io.sphere.sdk.channels.commands.updateactions.SetCustomType;
import io.sphere.sdk.channels.commands.updateactions.SetGeoLocation;
import io.sphere.sdk.client.SphereRequest;
import io.sphere.sdk.commands.UpdateAction;
import io.sphere.sdk.types.CustomFieldsDraft;
import io.sphere.sdk.types.CustomFieldsDraftBuilder;
import io.sphere.sdk.types.Type;

public class ChannelLoadService extends AbstractLoadService<ChannelDraft> {

	private final static Logger LOG = LoggerFactory.getLogger(ChannelLoadService.class);

	private TypeQueryService typeQueryService;
	private ChannelQueryService channelQueryService;

	public ChannelLoadService(final CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	@Override
	protected void init() {
		super.init();

		setTypeParameterClass(ChannelDraft.class);

		final CommercetoolsProject commercetoolsProjectBean = getCommercetoolsProject();
		if (commercetoolsProjectBean != null) {
			typeQueryService = new TypeQueryService(commercetoolsProjectBean);
			channelQueryService = new ChannelQueryService(commercetoolsProjectBean);
		}
	}

	@Override
	public SyncStatistics executeSync(List<ChannelDraft> drafts) {
		final SyncStatistics syncStatistics = new SyncStatistics();

		if (validForSync(drafts)) {

			for (ChannelDraft draft : drafts) {
				syncStatistics.incrementProcessed();
				draft = replaceTypeId(draft);
				final Channel channel = channelQueryService.getByKey(draft.getKey());
				SphereRequest<Channel> command = null;
				if (channel == null) {
					command = ChannelCreateCommand.of(draft);
				} else {
					final List<UpdateAction<Channel>> actions = new ArrayList<>();
					if (DraftHelper.checkForUpdate(channel.getDescription(), draft.getDescription())) {
						actions.add(ChangeDescription.of(draft.getDescription()));
					}
					if (DraftHelper.checkForUpdate(channel.getKey(), draft.getKey())) {
						actions.add(ChangeKey.of(draft.getKey()));
					}
					if (DraftHelper.checkForUpdate(channel.getName(), draft.getName())) {
						actions.add(io.sphere.sdk.channels.commands.updateactions.ChangeName.of(draft.getName()));
					}
					if (DraftHelper.checkForUpdate(channel.getGeoLocation(), draft.getGeoLocation())) {
						actions.add(SetGeoLocation.of(draft.getGeoLocation()));
					}

					if (DraftHelper.checkForUpdate(channel.getAddress(), draft.getAddress())) {
						actions.add(SetAddress.of(draft.getAddress()));
					}

					if (DraftHelper.checkForUpdate(channel.getCustom(), draft.getCustom())) {
						if (draft.getCustom() != null && draft.getCustom().getType() != null) {
							String typeId = draft.getCustom().getType().getId();
							actions.add(SetCustomType.ofTypeIdAndJson(typeId, null));
						}
					}
					if ( draft.getCustom() != null) {
						for (Map.Entry<String, JsonNode> field : draft.getCustom().getFields().entrySet()) {
							LOG.debug("field {} -{} ", field.getKey(), field.getValue());
							actions.add(SetCustomField.ofObject(field.getKey(), field.getValue()));
						}
					}
					if (DraftHelper.checkForUpdate(channel.getRoles(), draft.getRoles())) {
						final Set<ChannelRole> removeRoles = new HashSet<>();
						final Set<ChannelRole> addRoles = new HashSet<>();
						for (ChannelRole channelRole : channel.getRoles()) {
							if (!draft.getRoles().contains(channelRole)) {
								removeRoles.add(channelRole);
							}
						}
						for (ChannelRole channelRole : draft.getRoles()) {
							if (!channel.getRoles().contains(channelRole)) {
								addRoles.add(channelRole);
							}
						}
						if (!addRoles.isEmpty()) {
							actions.add(AddRoles.of(addRoles));
						}
						if (!removeRoles.isEmpty()) {
							actions.add(RemoveRoles.of(removeRoles));
						}
						// SetRoles.class
					}

					if (!actions.isEmpty()) {
						command = ChannelUpdateCommand.of(channel, actions);
					}

				}
				if (command != null) {
					Long version = channel == null ? Long.valueOf(0) : channel.getVersion();
					final Channel result = getClient().executeBlocking(command);
					if (result != null) {
						LOG.debug("channel result {}, version:{} -> {}", result.getKey(), version, result.getVersion());
					}
				}

			}
		}
		LOG.info("finished channel sync:{}", syncStatistics);
		return syncStatistics;
	}

	private ChannelDraft replaceTypeId(ChannelDraft draft) {
		final CustomFieldsDraft customFieldsDraft = draft.getCustom();
		if (customFieldsDraft != null && customFieldsDraft.getType() != null) {
			final String customTypeKey = customFieldsDraft.getType().getKey();
			final Type type = typeQueryService.getByKey(customTypeKey);
			if (type != null) {
				final ChannelDraftBuilder draftBuilder = ChannelDraftBuilder.of(draft);
				final CustomFieldsDraft custom = CustomFieldsDraftBuilder.ofTypeId(type.getId())
						.fields(customFieldsDraft.getFields()).build();
				draftBuilder.custom(custom);
				draft = draftBuilder.build();
			}
		}
		return draft;
	}

	@Required
	public void setTypeQueryService(TypeQueryService typeQueryService) {
		this.typeQueryService = typeQueryService;
	}

	@Required
	public void setChannelQueryService(ChannelQueryService channelQueryService) {
		this.channelQueryService = channelQueryService;
	}

}

package com.mindcurv.commercetools.automation.services.setup.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.services.sheets.v4.Sheets.Spreadsheets;
import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.helper.spreadsheet.ExcelHelper;
import com.mindcurv.commercetools.automation.helper.spreadsheet.GDriveHelper;
import com.mindcurv.commercetools.automation.helper.spreadsheet.ValueHelper;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;
import com.mindcurv.commercetools.automation.pojo.assets.Image;
import com.mindcurv.commercetools.automation.pojo.catalog.products.PricePojo;
import com.mindcurv.commercetools.automation.pojo.catalog.products.ProductPojo;
import com.mindcurv.commercetools.automation.pojo.catalog.products.VariantPojo;
import com.mindcurv.commercetools.automation.pojo.types.product.TypeAttribute;
import com.mindcurv.commercetools.automation.services.PublicationService;
import com.mindcurv.commercetools.automation.services.load.impl.MediaLoadService;
import com.mindcurv.commercetools.automation.services.load.impl.PriceLoadService;
import com.mindcurv.commercetools.automation.services.load.impl.ProductLoadService;
import com.mindcurv.commercetools.automation.services.query.impl.ProductQueryService;
import com.mindcurv.commercetools.automation.services.setup.SetupService;

import io.sphere.sdk.products.PriceDraft;
import io.sphere.sdk.products.Product;
import io.sphere.sdk.products.ProductDraft;
import io.sphere.sdk.products.commands.ProductDeleteCommand;

public class ProductSetupService extends SetupService {

	private final static Logger LOG = LoggerFactory.getLogger(ProductSetupService.class);
	private ProductQueryService productQueryService;
	private PublicationService publicationService;
	private ProductLoadService productLoadService;
	private MediaLoadService mediaLoadService;
	private PriceLoadService priceLoadService;

	public ProductSetupService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	@Override
	protected void init() {
		super.init();

		final CommercetoolsProject commercetoolsProjectBean = getCommercetoolsProject();
		if (commercetoolsProjectBean != null) {
			if (productQueryService == null) {
				productQueryService = new ProductQueryService(commercetoolsProjectBean);
			}
			if (publicationService == null) {
				publicationService = new PublicationService(commercetoolsProjectBean);
			}
			if (productLoadService == null) {
				productLoadService = new ProductLoadService(commercetoolsProjectBean);
			}
			if (mediaLoadService == null) {
				mediaLoadService = new MediaLoadService(commercetoolsProjectBean);
			}
			if (priceLoadService == null) {
				priceLoadService = new PriceLoadService(commercetoolsProjectBean);
			}
		}
	}

	@Override
	public boolean cleanupData() {
		LOG.info("cleanupProducts start");

		for (Product product : productQueryService.getAllProductsByPublicationStatus(true)) {
			product = publicationService.unpublishProduct(product);
			LOG.debug("unpublished product {}", product.getKey());

		}
		LOG.info("finished depublication, starting removal");

		for (Product product : productQueryService.getAllProductsByPublicationStatus(false)) {
			product = getClient().executeBlocking(ProductDeleteCommand.of(product));
			LOG.debug("deleted product {}", product.getKey());
		}
		LOG.info("cleanupProducts finished");
		return true;

	}

	@Override
	public boolean setup(Workbook workbook, final Map<String, List<TypeAttribute>> attributesMap) {
		final List<Map<String, Object>> valueLines;
		final List<Map<String, Object>> variantLines;
		final List<Map<String, Object>> imageLines;
		final List<Map<String, Object>> priceLines;
		try {
			valueLines = getValueLinesFromExcel(workbook);
			variantLines = ExcelHelper.processRange(workbook, CsvConstants.Variant.SHEET, getCommercetoolsProject());
			imageLines = ExcelHelper.processRange(workbook, CsvConstants.ProductImages.SHEET, getCommercetoolsProject());
			priceLines = ExcelHelper.processRange(workbook, CsvConstants.Price.SHEET, getCommercetoolsProject());

		} catch (IOException e) {
			LOG.error("Exception while accessing Sheet " + getSheetname(), e);
			return false;
		}
		return processLines(attributesMap, valueLines, variantLines, imageLines, priceLines);
	}

	@Override
	public boolean setup(String sheetId, Spreadsheets sheets, final Map<String, List<TypeAttribute>> attributesMap) {
		final List<Map<String, Object>> valueLines;
		final List<Map<String, Object>> variantLines;
		final List<Map<String, Object>> imageLines;
		final List<Map<String, Object>> priceLines;
		try {
			valueLines = getValueLinesFromGoogleSheet(sheets, sheetId);
			variantLines = GDriveHelper.processRange(sheets, sheetId, CsvConstants.Variant.SHEET, getCommercetoolsProject());
			imageLines = GDriveHelper.processRange(sheets, sheetId, CsvConstants.ProductImages.SHEET, getCommercetoolsProject());
			priceLines = GDriveHelper.processRange(sheets, sheetId, CsvConstants.Price.SHEET, getCommercetoolsProject());

		} catch (IOException e) {
			LOG.error("Exception while accessing Sheet " + getSheetname(), e);
			return false;
		}

		return processLines(attributesMap, valueLines, variantLines, imageLines, priceLines);
	}

	private boolean processLines(final Map<String, List<TypeAttribute>> attributesMap,
			final List<Map<String, Object>> valueLines, final List<Map<String, Object>> variantLines,
			final List<Map<String, Object>> imageLines, final List<Map<String, Object>> priceLines) {
		final Map<String, ProductPojo> pojoMap = new HashMap<>();

		for (Map<String, Object> value : valueLines) {
			final ProductPojo pojo = new ProductPojo(value);
			pojoMap.put(pojo.getKey(), pojo);
		}
		for (Map<String, Object> value : variantLines) {
			final VariantPojo variantPojo = new VariantPojo(value);
			final ProductPojo baseProductPojo = pojoMap.get(variantPojo.getBaseProduct());
			if (baseProductPojo != null) {
				if (baseProductPojo.isCustomType()) {
					final String typeKey = baseProductPojo.getProductType().getId();
					baseProductPojo.setVariantAttributes(value, variantPojo, attributesMap.get(typeKey));
				}
				baseProductPojo.addVariant(variantPojo);
			}
		}

		setupSameForAllAttributes(attributesMap, pojoMap, valueLines);

		int imageCounter = 0;
		for (Map<String, Object> value : imageLines) {
			final String baseProductKey = ValueHelper.getValueAsString(CsvConstants.ProductImages.BASE_PRODUCT_KEY,
					value);
			final String variantKey = ValueHelper.getValueAsString(CsvConstants.ProductImages.VARIANTKEY_KEY, value);
			final ProductPojo baseProductPojo = pojoMap.get(baseProductKey);
			if (baseProductPojo != null) {
				final VariantPojo variant = baseProductPojo.getVariantByKey(variantKey);
				if (variant != null) {
					final Image image = new Image(value);
					LOG.debug("added Image {} to {}", image, variant.getKey());
					variant.addImage(image);
					imageCounter++;
				}
			}
		}
		LOG.info("added {} image lines", imageCounter);

		final List<ProductDraft> drafts = new ArrayList<>();
		for (Map.Entry<String, ProductPojo> entry : pojoMap.entrySet()) {
			drafts.add(entry.getValue().toDraft());
		}
		if (!drafts.isEmpty()) {
			productLoadService.sync(drafts);
		}
		mediaLoadService.sync();

		return setupPrices(priceLines);
	}

	private void setupSameForAllAttributes(final Map<String, List<TypeAttribute>> attributesMap,
			final Map<String, ProductPojo> pojoMap, final List<Map<String, Object>> rangeForBaseProduct) {
		for (Map<String, Object> value : rangeForBaseProduct) {
			final String key = ValueHelper.getValueAsString(CsvConstants.KEY_KEY, value);
			final ProductPojo baseProductPojo = pojoMap.get(key);
			if (baseProductPojo.isCustomType()) {
				baseProductPojo.addAttributes(value, attributesMap);
			}
		}
	}

	private boolean setupPrices(List<Map<String, Object>> priceLines) {

		Map<String, List<PriceDraft>> draftMap = new HashMap<>();
		for (Map<String, Object> value : priceLines) {
			final PricePojo pojo = new PricePojo(value);
			List<PriceDraft> list = draftMap.get(pojo.getSku());
			if (list == null) {
				list = new ArrayList<>();
				draftMap.put(pojo.getSku(), list);
			}
			if (StringUtils.isNotBlank(pojo.getSku())) {
				list.add(pojo.toDraft());
			} else {
				LOG.warn("skipping pojo, sku is null:{}", pojo);
			}
		}
		if (!draftMap.isEmpty()) {
			priceLoadService.executeSync(draftMap);
		}
		return true;

	}

	@Override
	protected boolean setupValues(List<Map<String, Object>> valueLines, Map<String, List<TypeAttribute>> attributesMap) {
		LOG.warn("setupValues() not implemented()");
		return false;
	}

	@Override
	public String getSheetname() {
		return CsvConstants.Product.SHEET;
	}

	public void setProductQueryService(ProductQueryService productQueryService) {
		this.productQueryService = productQueryService;
	}

	public void setPublicationService(PublicationService publicationService) {
		this.publicationService = publicationService;
	}

	public void setProductLoadService(ProductLoadService productLoadService) {
		this.productLoadService = productLoadService;
	}

	public void setMediaLoadService(MediaLoadService mediaLoadService) {
		this.mediaLoadService = mediaLoadService;
	}

	public void setPriceLoadService(PriceLoadService priceLoadService) {
		this.priceLoadService = priceLoadService;
	}

}

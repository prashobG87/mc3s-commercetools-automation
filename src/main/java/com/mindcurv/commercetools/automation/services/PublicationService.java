package com.mindcurv.commercetools.automation.services;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.Constants;

import io.sphere.sdk.commands.UpdateAction;
import io.sphere.sdk.products.Product;
import io.sphere.sdk.products.ProductVariant;
import io.sphere.sdk.products.PublishScope;
import io.sphere.sdk.products.attributes.AttributeDraft;
import io.sphere.sdk.products.commands.ProductUpdateCommand;
import io.sphere.sdk.products.commands.updateactions.Publish;
import io.sphere.sdk.products.commands.updateactions.SetAttribute;
import io.sphere.sdk.products.commands.updateactions.Unpublish;

public class PublicationService extends CommercetoolsBaseService {

	private final static Logger LOG = LoggerFactory.getLogger(PublicationService.class);

	public PublicationService(final CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	public List<Product> publishProducts(List<Product> products) {
		final List<Product> publishedProducts = new ArrayList<>();
		for (Product product : products) {
			final Product publishProduct = publishProduct(product);
			if (publishProduct == null) {
				LOG.warn("unable to publish {}", product.getKey());
			} else {
				publishedProducts.add(publishProduct);
			}
		}
		LOG.info("Publication done for {} products", publishedProducts.size());
		return publishedProducts;
	}

	public Product publishProduct(final Product product) {
		return publishProduct(product, PublishScope.ALL); 
	}

	public Product publishPrices(final Product product) {
		return publishProduct(product, PublishScope.PRICES); 
	}
	
	public Product unpublishProduct(Product product) {
		if (product != null) {
			final ProductUpdateCommand command = ProductUpdateCommand.of(product, Unpublish.of());
			final Product result = getClient().executeBlocking(command);
			LOG.debug("unpublicationResult: {}, version {}", product.getKey(), product.getVersion());
			return result;
		}
		LOG.warn("no product provided");
		return null;
	}

	private Product publishProduct(Product product, final PublishScope scope) {
		if (product != null) {
			final List<UpdateAction<Product>> actions = new ArrayList<>();
			actions.add(Publish.ofScope(scope));
			final ProductUpdateCommand command = ProductUpdateCommand.of(product, actions);
			product = getClient().executeBlocking(command);
			LOG.debug("publicationResult: {}, version {}", product.getKey(), product.getVersion());
		} else {
			LOG.warn("no product provided");
		}
		return product;
	}

	protected void addPublicationIdIfMissing(final Product product, final List<UpdateAction<Product>> actions) {
		for (ProductVariant variant : product.getMasterData().getStaged().getAllVariants()) {
			if (variant.getAttribute(Constants.ATT_PUBLICATIONID) == null) {
				final StringBuffer stringBuffer = new StringBuffer();
				stringBuffer.append(System.currentTimeMillis());
				stringBuffer.append(variant.getId());
				final String publicationId = stringBuffer.toString();
				actions.add(SetAttribute.of(variant.getId(),
						AttributeDraft.of(Constants.ATT_PUBLICATIONID, Long.valueOf(publicationId))));
				LOG.info("created new publicationId {}", publicationId);
			}
		}
	}

}

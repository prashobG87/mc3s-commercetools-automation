package com.mindcurv.commercetools.automation.services.load.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.commercetools.sync.producttypes.ProductTypeSync;
import com.commercetools.sync.producttypes.ProductTypeSyncOptions;
import com.commercetools.sync.producttypes.ProductTypeSyncOptionsBuilder;
import com.commercetools.sync.producttypes.helpers.ProductTypeSyncStatistics;
import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.integration.SyncStatistics;
import com.mindcurv.commercetools.automation.services.load.AbstractLoadService;

import io.sphere.sdk.producttypes.ProductTypeDraft;

public class ProductTypeLoadService extends AbstractLoadService<ProductTypeDraft> {

	private final static Logger LOG = LoggerFactory.getLogger(ProductTypeLoadService.class);

	public ProductTypeLoadService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	@Override
	protected void init() {
		super.init();
		setTypeParameterClass(ProductTypeDraft.class);
	}

	@Override
	public SyncStatistics executeSync(List<ProductTypeDraft> drafts) {
		final SyncStatistics syncStatistics = new SyncStatistics();
		if (validForSync(drafts)) {
			final ProductTypeSyncOptions syncOptions = ProductTypeSyncOptionsBuilder.of(getClient())
					.errorCallback(LOG::error).warningCallback(LOG::warn).build();
			final ProductTypeSync sync = new ProductTypeSync(syncOptions);
			final ProductTypeSyncStatistics syncStatisticsStage = sync.sync(drafts).toCompletableFuture().join();
			LOG.info("finished sync:{}", syncStatisticsStage.getReportMessage());

			syncStatistics.setValues(syncStatisticsStage);
		}
		return syncStatistics;
	}
}

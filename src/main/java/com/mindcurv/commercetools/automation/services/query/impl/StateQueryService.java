package com.mindcurv.commercetools.automation.services.query.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.services.query.QueryService;

import io.sphere.sdk.queries.PagedQueryResult;
import io.sphere.sdk.queries.QueryPredicate;
import io.sphere.sdk.states.State;
import io.sphere.sdk.states.queries.StateQueryBuilder;
import io.sphere.sdk.states.queries.StateQueryModel;

public class StateQueryService extends QueryService<State> {

	private final static Logger LOG = LoggerFactory.getLogger(StateQueryService.class);
	public static final String PARAM_NAMELIST = "namelist";
	
	public StateQueryService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	@Override
	public List<State> getQueryResult(final Map<String, Object> params) {

		final List<State> result = new ArrayList<>();
		Long limit = params.containsKey(PARAM_LIMIT) ? (Long) params.get(PARAM_LIMIT) : DEFAULT_LIMIT;
		final StateQueryBuilder queryBuilder = StateQueryBuilder.of().fetchTotal(true);
		queryBuilder.limit(limit);

		final List<QueryPredicate<State>> predicates = preparePredicates(params);
		if (!predicates.isEmpty()) {
			queryBuilder.predicates(predicates);
		}

		long page = 1;
		boolean breakLoop = false;
		while (!breakLoop) {
			long offset = (page - 1) * limit;
			queryBuilder.offset(offset);
			final PagedQueryResult<State> pagedQueryResult = getClient().executeBlocking(queryBuilder.build());
			final List<State> pageResult = pagedQueryResult.getResults();
			if (pagedQueryResult.getTotal() == 0) {
				breakLoop = true;
			} else {
				result.addAll(pageResult);
				LOG.debug("found {} States on page {} of {} pages [{} entries]", pageResult.size(), page,
						pagedQueryResult.getTotalPages(), pagedQueryResult.getTotal());
				if (page == pagedQueryResult.getTotalPages()) {
					breakLoop = true;
				}
				page++;
			}
		}
		LOG.debug("returning total: {} States", result.size());
		return result;
	}

	@Override
	public List<QueryPredicate<State>> preparePredicates(final Map<String, Object> params) {
		final List<QueryPredicate<State>> predicates = new ArrayList<>();
		if (params.containsKey(PARAM_ID)) {
			final String id = (String) params.get(PARAM_ID);
			predicates.add(StateQueryModel.of().id().is(id));
		}
		if (params.containsKey(PARAM_KEY)) {
			final String key = (String) params.get(PARAM_KEY);
			predicates.add(StateQueryModel.of().key().is(key));
		}
		if (params.containsKey(PARAM_NAMELIST)) {
			@SuppressWarnings("unchecked")
			final List<String> list = (List<String>) params.get(PARAM_NAMELIST);
			predicates.add(StateQueryModel.of().key().isIn(list));
		}
		return predicates;
	}

	@Override
	protected boolean supportsByKeyQuery() {
		return true;
	}

	public List<State> getByStatusName(List<String> openOrdersStatusList) {
		if (openOrdersStatusList != null) {
			final Map<String, Object> params = new HashMap<>();
			params.put(PARAM_LIMIT, Long.valueOf(1));
			params.put(PARAM_NAMELIST, openOrdersStatusList);
			return getQueryResult(params); 
		}
		return Collections.emptyList();
	}
}

package com.mindcurv.commercetools.automation.services.setup.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.Constants;
import com.mindcurv.commercetools.automation.helper.DraftHelper;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;
import com.mindcurv.commercetools.automation.pojo.types.product.TypeAttribute;
import com.mindcurv.commercetools.automation.pojo.types.status.StatusPojo;
import com.mindcurv.commercetools.automation.services.query.impl.StateQueryService;
import com.mindcurv.commercetools.automation.services.setup.SetupService;

import io.sphere.sdk.models.LocalizedString;
import io.sphere.sdk.models.Reference;
import io.sphere.sdk.projects.Project;
import io.sphere.sdk.projects.commands.ProjectUpdateCommand;
import io.sphere.sdk.projects.commands.updateactions.ChangeMessagesEnabled;
import io.sphere.sdk.projects.queries.ProjectGet;
import io.sphere.sdk.states.State;
import io.sphere.sdk.states.StateDraft;
import io.sphere.sdk.states.StateDraftBuilder;
import io.sphere.sdk.states.StateType;
import io.sphere.sdk.states.commands.StateCreateCommand;
import io.sphere.sdk.states.commands.StateDeleteCommand;
import io.sphere.sdk.states.commands.StateUpdateCommand;
import io.sphere.sdk.states.commands.updateactions.SetTransitions;

public class ProjectSetupService extends SetupService {

	private final static Logger LOG = LoggerFactory.getLogger(ProjectSetupService.class);

	private StateQueryService stateQueryService;

	public ProjectSetupService(final CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	@Override
	protected void init() {
		super.init();
		final CommercetoolsProject commercetoolsProjectBean = getCommercetoolsProject();
		if (commercetoolsProjectBean != null) {
			if (stateQueryService == null) {
				stateQueryService = new StateQueryService(commercetoolsProjectBean);
			}
		}
	}

	public boolean cleanupData() {

		LOG.info(Constants.MSG_SPACER);
		LOG.info("Clean up OrderState items");
		final List<State> states = new ArrayList<>();
		for (State state : stateQueryService.getAllEntries()) {
			if (state.getTransitions() != null && !state.getTransitions().isEmpty()) {
				final StateUpdateCommand updateCommand = StateUpdateCommand.of(state,
						SetTransitions.of(new HashSet<>()));
				final State result = getClient().executeBlocking(updateCommand);
				LOG.info("result from setting transition to empty list : {} - {}", result.getType(), result.getKey());
				states.add(result);
			}
		}
		for (State state : states) {
			if (checkForBuiltInState(state)) {
				LOG.info("Skipping built in state {} - {}", state.getType(), state.getKey());
				continue;
			}
			final StateDeleteCommand command = StateDeleteCommand.of(state);
			final State result = getClient().executeBlocking(command);
			LOG.info("Deleted state {} - {}", result.getType(), result.getKey());
			getStatesMap().remove(result.getKey());
		}
		return true;
	}

	private boolean checkForBuiltInState(State state) {
		return "Initial".equals(state.getKey()) && StateType.LINE_ITEM_STATE.equals(state.getType());
	}

	@Override
	protected boolean setupValues(List<Map<String, Object>> valueLines, Map<String, List<TypeAttribute>> attributesMap) {

		setupSettings();

		final Map<String, List<String>> orderStatesDefinitionMap = new HashMap<>();
		final Map<String, List<String>> lineItemStatesDefinitionMap = new HashMap<>();

		for (Map<String, Object> value : valueLines) {
			StatusPojo statusPojo = new StatusPojo(value);
			if (StateType.LINE_ITEM_STATE.equals(statusPojo.getType())) {
				lineItemStatesDefinitionMap.put(statusPojo.getKey(), statusPojo.getTransistions());
			} else {
				orderStatesDefinitionMap.put(statusPojo.getKey(), statusPojo.getTransistions());
			}
		}
		setupStates(orderStatesDefinitionMap, StateType.ORDER_STATE);
		setupStates(lineItemStatesDefinitionMap, StateType.LINE_ITEM_STATE);

		return true;
	}

	@Override
	public String getSheetname() {
		return CsvConstants.Status.SHEET;
	}

	public void setupStates(Map<String, List<String>> statesDefinitionMap, StateType stateType) {
		LOG.info("Setup {} States", statesDefinitionMap.size());

		final Map<String, State> statesMap = getStatesMap();

		final List<StateDraft> drafts = prepareDrafts(statesDefinitionMap, stateType);

		for (StateDraft draft : drafts) {
			final String stateKey = draft.getKey();
			if (statesMap.containsKey(stateKey)) {
				LOG.info("State {} already present", stateKey);
			} else {
				StateCreateCommand createCommand = StateCreateCommand.of(draft);
				final State result = getClient().executeBlocking(createCommand);
				statesMap.put(stateKey, result);
				LOG.info("State {} created", result);
			}
		}

		for (String stateKey : statesDefinitionMap.keySet()) {
			final Set<Reference<State>> statesToBeSet = new HashSet<>();
			for (String transitionState : statesDefinitionMap.get(stateKey)) {
				final State state = statesMap.get(transitionState);
				if (state != null) {
					statesToBeSet.add(state.toReference());
				} else {
					LOG.warn("Could not find state {}", transitionState);
				}
			}
			if (!statesToBeSet.isEmpty()) {

				Set<Reference<State>> existingTransitions = statesMap.get(stateKey).getTransitions();
				final List<String> checkList = new ArrayList<>();
				if (existingTransitions != null) {
					for (Reference<State> state : existingTransitions) {
						checkList.add(state.getKey());
					}
				}
				boolean needsUpdate = false;
				if (statesToBeSet.size() != checkList.size()) {
					LOG.info("found different tranistion states found {} / to set {}", checkList.size(),
							statesToBeSet.size());
					needsUpdate = true;
				} else {
					for (Reference<State> transitionToSet : statesToBeSet) {
						if (checkList.contains(transitionToSet.getKey())) {
							checkList.remove(transitionToSet.getKey());
						}
					}
					if (checkList.size() > 0) {
						LOG.info("found {} remaining tranistion states", checkList.size());
						needsUpdate = true;
					}
				}
				if (needsUpdate) {
					try {
						StateUpdateCommand updateCommand = StateUpdateCommand.of(statesMap.get(stateKey),
								SetTransitions.of(statesToBeSet));
						State result = getClient().executeBlocking(updateCommand);
						statesMap.put(result.getKey(), result);
						LOG.info("Updated State transitions {}", result);

					} catch (Exception e) {
						if (StringUtils.isNotBlank(e.getMessage())
								&& e.getMessage().contains("'transitions' has no changes")) {
							LOG.info("no changes in transitions for State {}", stateKey);
						} else {
							LOG.error("could not update state ", e);
						}
					}
				}
			}

		}
	}

	public State getOrderState1(final String requestedState) {
		return getStatesMap().get(requestedState);
	}

	private void setupSettings() {
		LOG.info(Constants.MSG_SPACER);
		LOG.info("Setup Project settings");
		Project project = getClient().executeBlocking(ProjectGet.of());
		if (!project.getMessages().isEnabled().booleanValue()) {
			final ProjectUpdateCommand command = ProjectUpdateCommand.of(project,
					ChangeMessagesEnabled.of(Boolean.TRUE));
			project = getClient().executeBlocking(command);
			LOG.info("enabled messages ({}) for project {}", project.getMessages().isEnabled(), project.getKey());
		} else {
			LOG.info("messages already enabled ({}) for project {}", project.getMessages().isEnabled(),
					project.getKey());
		}
	}

	private Map<String, State> getStatesMap() {
		final Map<String, State> statesMap = new HashMap<>();
		for (State state : stateQueryService.getAllEntries()) {
			statesMap.put(state.getKey(), state);
		}
		LOG.debug("found {} states", statesMap.size());
		return statesMap;
	}

	private List<StateDraft> prepareDrafts(Map<String, List<String>> statesDefinitionMap, StateType stateType) {
		final List<StateDraft> drafts = new ArrayList<>();
		for (String stateKey : statesDefinitionMap.keySet()) {

			final String prefix = StateType.LINE_ITEM_STATE.equals(stateType) ? "Line item state" : "Order state";
			final LocalizedString name = DraftHelper.getLocalizedString(stateKey.replace("_", " "));
			final LocalizedString description = DraftHelper.getLocalizedString(prefix + " '" + stateKey + "'");
			final StateDraft draft = StateDraftBuilder.of(stateKey, stateType).name(name).description(description)
					.initial(drafts.isEmpty()).build();
			drafts.add(draft);
		}
		return drafts;
	}

	@Required
	public void setStateQueryService(StateQueryService stateQueryService) {
		this.stateQueryService = stateQueryService;
	}
}

package com.mindcurv.commercetools.automation.services.load.impl.customobject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.pojo.customobject.products.TablePojo;
import com.mindcurv.commercetools.automation.services.load.AbstractCustomObjectLoadService;

import io.sphere.sdk.customobjects.CustomObject;

public class CustomObjectLoadTableService extends AbstractCustomObjectLoadService<TablePojo> {

	private final static Logger LOG = LoggerFactory.getLogger(CustomObjectLoadTableService.class);

	public CustomObjectLoadTableService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}
	
	@Override
	protected void init() {
		super.init();
		setTypeParameterClass(TablePojo.class);
	}
	

	@Override
	public CustomObject<TablePojo> createCustomObject(String container, String key, TablePojo customobject) {
		LOG.debug("creating custom object with container {} and key {} ", container, key);

		final CustomObject<TablePojo> customObject = sync(container, key, customobject);

		LOG.info("CustomObject: {} - {} -> {}", customObject.getContainer(), customObject.getKey(), customObject.getValue());
		return customObject;
	}

}

package com.mindcurv.commercetools.automation.services.load.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.helper.DraftHelper;
import com.mindcurv.commercetools.automation.integration.SyncStatistics;
import com.mindcurv.commercetools.automation.services.load.AbstractLoadService;
import com.mindcurv.commercetools.automation.services.query.impl.ShippingMethodQueryService;
import com.mindcurv.commercetools.automation.services.query.impl.TaxCategoryQueryService;
import com.mindcurv.commercetools.automation.services.query.impl.ZoneQueryService;

import io.sphere.sdk.client.ErrorResponseException;
import io.sphere.sdk.commands.UpdateAction;
import io.sphere.sdk.json.SphereJsonUtils;
import io.sphere.sdk.models.Reference;
import io.sphere.sdk.models.ResourceIdentifier;
import io.sphere.sdk.shippingmethods.ShippingMethod;
import io.sphere.sdk.shippingmethods.ShippingMethodDraft;
import io.sphere.sdk.shippingmethods.ShippingRate;
import io.sphere.sdk.shippingmethods.ZoneRate;
import io.sphere.sdk.shippingmethods.commands.ShippingMethodCreateCommand;
import io.sphere.sdk.shippingmethods.commands.ShippingMethodUpdateCommand;
import io.sphere.sdk.shippingmethods.commands.updateactions.AddShippingRate;
import io.sphere.sdk.shippingmethods.commands.updateactions.AddZone;
import io.sphere.sdk.shippingmethods.commands.updateactions.ChangeIsDefault;
import io.sphere.sdk.shippingmethods.commands.updateactions.ChangeName;
import io.sphere.sdk.shippingmethods.commands.updateactions.ChangeTaxCategory;
import io.sphere.sdk.shippingmethods.commands.updateactions.RemoveShippingRate;
import io.sphere.sdk.shippingmethods.commands.updateactions.RemoveZone;
import io.sphere.sdk.shippingmethods.commands.updateactions.SetDescription;
import io.sphere.sdk.taxcategories.TaxCategory;
import io.sphere.sdk.zones.Location;
import io.sphere.sdk.zones.Zone;
import io.sphere.sdk.zones.ZoneDraft;
import io.sphere.sdk.zones.commands.ZoneCreateCommand;
import io.sphere.sdk.zones.commands.ZoneUpdateCommand;
import io.sphere.sdk.zones.commands.updateactions.AddLocation;
import io.sphere.sdk.zones.commands.updateactions.RemoveLocation;

public class ShippingMethodLoadService extends AbstractLoadService<ShippingMethodDraft> {

	private final static Logger LOG = LoggerFactory.getLogger(ShippingMethodLoadService.class);

	private ShippingMethodQueryService shippingMethodQueryService;
	private ZoneQueryService zoneQueryService;;
	private TaxCategoryQueryService taxCategoryQueryService;

	public ShippingMethodLoadService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	@Override
	protected void init() {
		super.init();

		setTypeParameterClass(ShippingMethodDraft.class);

		final CommercetoolsProject commercetoolsProjectBean = getCommercetoolsProject();
		if (commercetoolsProjectBean != null) {
			shippingMethodQueryService = new ShippingMethodQueryService(commercetoolsProjectBean);
			zoneQueryService = new ZoneQueryService(commercetoolsProjectBean);
			taxCategoryQueryService = new TaxCategoryQueryService(commercetoolsProjectBean);
		}
	}

	@Override
	public SyncStatistics syncStrings(List<String> input) {
		final List<ShippingMethodDraft> drafts = new ArrayList<>();
		for (String draftString : input) {
			drafts.add(SphereJsonUtils.readObject(draftString, ShippingMethodDraft.class));
		}
		return sync(drafts);
	}

	@Override
	public SyncStatistics executeSync(final List<ShippingMethodDraft> drafts) {
	
		final SyncStatistics syncStatistics = new SyncStatistics();
		if (validForSync(drafts)) {
			for (ShippingMethodDraft draft : drafts) {
				syncStatistics.incrementProcessed();
	
				
				ShippingMethod shippingMethod = shippingMethodQueryService.getByName(draft.getName());
				if (shippingMethod == null) {
					final ShippingMethodCreateCommand command = ShippingMethodCreateCommand.of(draft);
					shippingMethod = getClient().executeBlocking(command);
					syncStatistics.incrementCreated();
	
				} else {
final ResourceIdentifier<TaxCategory> draftTaxCategory = draft.getTaxCategory();
					
					final String key = StringUtils.isNotBlank(draftTaxCategory.getKey()) ? draftTaxCategory.getKey() : draftTaxCategory.getId();
					
					final TaxCategory taxCategory = taxCategoryQueryService.getByKey(key);
					
					final List<UpdateAction<ShippingMethod>> updateActions = new ArrayList<>();
					if (shippingMethod.getName() == null || !draft.getName().equals(shippingMethod.getName())) {
						updateActions.add(ChangeName.of(draft.getName()));
					}
					if (shippingMethod.getDescription() == null
							|| !draft.getDescription().equals(shippingMethod.getDescription())) {
						updateActions.add(SetDescription.of(draft.getDescription()));
					}
					if (shippingMethod.isDefault() == null || !draft.isDefault().equals(shippingMethod.isDefault())) {
						updateActions.add(ChangeIsDefault.of(draft.isDefault()));
					}
	
					if (shippingMethod.getTaxCategory() == null || taxCategory != null
							&& !taxCategory.getId().equals(shippingMethod.getTaxCategory().getId())) {
						updateActions.add(ChangeTaxCategory.of(taxCategory));
					}
					final List<UpdateAction<ShippingMethod>> zoneRelatedActions = processZones(draft, shippingMethod);
					final List<UpdateAction<ShippingMethod>> zoneRelatedRemoveActions = new ArrayList<>();
	
					for (UpdateAction<ShippingMethod> action : zoneRelatedActions) {
						if (action instanceof RemoveShippingRate) {
							zoneRelatedRemoveActions.add(action);
						} else {
							updateActions.add(action);
						}
					}
					if (!zoneRelatedRemoveActions.isEmpty()) {
						try {
							final ShippingMethodUpdateCommand command = ShippingMethodUpdateCommand.of(shippingMethod,
									zoneRelatedRemoveActions);
							shippingMethod = getClient().executeBlocking(command);
						} catch (ErrorResponseException e) {
							LOG.error("Exception during ShippingMethod Remove Rates Command for "
									+ shippingMethod.getName() + ":" + e.getMessage(), e);
						}
					}
					if (!updateActions.isEmpty()) {
						try {
							final ShippingMethodUpdateCommand command = ShippingMethodUpdateCommand.of(shippingMethod,
									updateActions);
							shippingMethod = getClient().executeBlocking(command);
							syncStatistics.incrementUpdated();
						} catch (ErrorResponseException e) {
							LOG.error("Exception during ShippingMethod Update Command for " + shippingMethod.getName()
									+ ":" + e.getMessage(), e);
							syncStatistics.incrementFailed();
						}
					}
				}
				LOG.info("Result for ShippingMethodDraft {}: {}", draft.getKey(), shippingMethod);
			}
		}
		return syncStatistics;
	
	}

	public void syncZones(final List<ZoneDraft> drafts) {
		if (drafts.isEmpty()) {
			LOG.warn("no ZoneDraft items available.");
			return;
		}
		LOG.info("number of ZoneDraft to be created: {}", drafts.size());
		for (ZoneDraft draft : drafts) {
			Zone zone = zoneQueryService.getByName(draft.getName());
			if (zone == null) {
				final ZoneCreateCommand command = ZoneCreateCommand.of(draft);
				zone = getClient().executeBlocking(command);
				LOG.info("Create Zone result:{}", zone.getLastModifiedAt());
			} else {

				final List<UpdateAction<Zone>> updateActions = new ArrayList<>();

				if (DraftHelper.checkForUpdate(zone.getName(), draft.getName())) {
					updateActions.add(io.sphere.sdk.zones.commands.updateactions.ChangeName.of(draft.getName()));
				}
				if (DraftHelper.checkForUpdate(zone.getDescription(), draft.getDescription())) {
					updateActions
							.add(io.sphere.sdk.zones.commands.updateactions.SetDescription.of(draft.getDescription()));
				}

				final Map<String, Location> locationsToBeRemoved = new HashMap<>();
				final Map<String, Location> existingLocations = new HashMap<>();

				for (Location location : zone.getLocations()) {
					existingLocations.put(createLocationKey(location), location);
				}
				locationsToBeRemoved.putAll(existingLocations);

				for (Location locationFromDraft : draft.getLocations()) {
					final String locationKey = createLocationKey(locationFromDraft);
					final Location existingLocation = existingLocations.get(locationKey);
					if (existingLocation == null) {
						updateActions.add(AddLocation.of(locationFromDraft));
					}
					locationsToBeRemoved.remove(locationKey);
				}
				for (Map.Entry<String, Location> entry : locationsToBeRemoved.entrySet()) {
					updateActions.add(RemoveLocation.of(entry.getValue()));
				}
				try {
					if (!updateActions.isEmpty()) {
						final ZoneUpdateCommand command = ZoneUpdateCommand.of(zone, updateActions);
						Long versionBefore = zone.getVersion();
						zone = getClient().executeBlocking(command);
						LOG.info("Update Zone result: {} -> {}", versionBefore, zone.getVersion());
					} else {
						LOG.info("no Zone update required");
					}
				} catch (Exception e) {
					LOG.error("Exception during update of zone ", e);
				}
			}
		}
	}

	private List<UpdateAction<ShippingMethod>> processZones(ShippingMethodDraft draft, ShippingMethod shippingMethod) {
		final Map<String, Reference<Zone>> zonesToBeRemoved = new HashMap<>();
		final Map<String, Reference<Zone>> existingZones = new HashMap<>();
		for (Reference<Zone> zone : shippingMethod.getZones()) {
			LOG.info("found zone {}", zone.getId());
			existingZones.put(zone.getId(), zone);

		}
		zonesToBeRemoved.putAll(existingZones);

		List<UpdateAction<ShippingMethod>> updateActions = new ArrayList<>();
		for (ZoneRate zoneRateFromFraft : draft.getZoneRates()) {
			final String zoneId = zoneRateFromFraft.getZone().getId();
			final Zone shippingMethodZone = zoneQueryService.getById(zoneId);

			if (shippingMethodZone == null) {
				LOG.warn("Zone with ID {} not found for ShippingMethod {}", zoneId, draft.getName());
				continue;
			}

			final String shippingMethodZoneName = shippingMethodZone.getName();
			if (existingZones.get(zoneId) == null) {
				updateActions.add(AddZone.of(shippingMethodZone));
				existingZones.put(shippingMethodZoneName, shippingMethodZone.toReference());
			} else {
				LOG.info("Zone {} [{}] already present", shippingMethodZoneName, zoneId);
			}
			zonesToBeRemoved.remove(zoneId);

			final Map<String, ShippingRate> shippingRatesToBeRemoved = new HashMap<>();
			final Map<String, ShippingRate> existingShippingRates = new HashMap<>();

			shippingRatesToBeRemoved.putAll(existingShippingRates);
			for (ShippingRate shippingRate : shippingMethod.getShippingRatesForZone(shippingMethodZone)) {
				existingShippingRates.put(createShippingRateKey(shippingMethodZoneName, shippingRate), shippingRate);
			}

			for (ShippingRate shippingRate : zoneRateFromFraft.getShippingRates()) {
				final String shippingRateKey = createShippingRateKey(shippingMethodZoneName, shippingRate);
				final ShippingRate existingShippingRate = existingShippingRates.get(shippingRateKey);
				updateActions.add(AddShippingRate.of(shippingRate, shippingMethodZone));
				if (existingShippingRate == null) {
					existingShippingRates.put(shippingRateKey, shippingRate);
				} else {
					LOG.info("ShippingRate {} already present: ", shippingRateKey);
					updateActions.add(RemoveShippingRate.of(existingShippingRate, shippingMethodZone));
				}
				shippingRatesToBeRemoved.remove(shippingRateKey);
			}

			for (Map.Entry<String, ShippingRate> entry : shippingRatesToBeRemoved.entrySet()) {
				updateActions.add(RemoveShippingRate.of(entry.getValue(), shippingMethodZone));
			}
		}
		for (Map.Entry<String, Reference<Zone>> entry : zonesToBeRemoved.entrySet()) {
			updateActions.add(RemoveZone.of(entry.getValue()));
		}
		return updateActions;
	}

	private String createShippingRateKey(String zoneKey, ShippingRate shippingRate) {
		if (shippingRate == null) {
			return null;
		}
		final StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(zoneKey).append("_").append(shippingRate.getPrice().getCurrency().getCurrencyCode());
		return stringBuffer.toString();

	}

	private String createLocationKey(Location location) {
		if (location == null) {
			return null;
		}
		final String country = location.getCountry() == null ? "" : location.getCountry().getAlpha2();
		final String state = location.getState() == null ? "" : location.getState();
		return DraftHelper.createLocationKey(country, state);
	}

	@Required
	public void setShippingMethodQueryService(ShippingMethodQueryService shippingMethodQueryService) {
		this.shippingMethodQueryService = shippingMethodQueryService;
	}

	@Required
	public void setZoneQueryService(ZoneQueryService zoneQueryService) {
		this.zoneQueryService = zoneQueryService;
	}

	@Required
	public void setTaxCategoryQueryService(TaxCategoryQueryService taxCategoryQueryService) {
		this.taxCategoryQueryService = taxCategoryQueryService;
	}

}

package com.mindcurv.commercetools.automation.services.query.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.services.query.QueryDateRange;
import com.mindcurv.commercetools.automation.services.query.QueryService;

import io.sphere.sdk.orders.Order;
import io.sphere.sdk.orders.OrderState;
import io.sphere.sdk.orders.queries.OrderQueryBuilder;
import io.sphere.sdk.orders.queries.OrderQueryModel;
import io.sphere.sdk.queries.PagedQueryResult;
import io.sphere.sdk.queries.QueryPredicate;
import io.sphere.sdk.states.State;

public class OrderQueryService extends QueryService<Order> {

	private final static Logger LOG = LoggerFactory.getLogger(OrderQueryService.class);

	public static final String PARAM_STATE = "State";
	public static final String PARAM_ORDERSTATE = "orderState";
	public static final String PARAM_DATERANGE = "dateRange";
	public static final String PARAM_ORDERNUMBER = "orderNumber";
	public static final String PARAM_STATELIST = "statelist";
	
	private List<String> openOrdersStatusList; 
	private StateQueryService stateQueryService; 

	public OrderQueryService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	@Override
	protected void init() {
		super.init();
		if ( getCommercetoolsProject() != null) {
			if ( stateQueryService == null) {
				stateQueryService= new StateQueryService(getCommercetoolsProject()); 
			}
		}
	}
	public List<Order> getQueryResult(final Map<String, Object> params) {

		Long limit = params.containsKey(PARAM_LIMIT) ? (Long) params.get(PARAM_LIMIT) : DEFAULT_LIMIT;
		final OrderQueryBuilder queryBuilder = OrderQueryBuilder.of().fetchTotal(true).limit(limit);

		final List<QueryPredicate<Order>> predicates = preparePredicates(params);
		if (!predicates.isEmpty()) {
			queryBuilder.predicates(predicates);
		}
		long page = 1;
		boolean breakLoop = false;
		Long totalResults = Long.valueOf(0);
		final List<Order> result = new ArrayList<>();
		while (!breakLoop) {
			long offset = (page - 1) * limit;
			queryBuilder.offset(offset);
			final PagedQueryResult<Order> pagedQueryResult = getClient().executeBlocking(queryBuilder.build());
			final List<Order> pageResult = pagedQueryResult.getResults();
			if (pagedQueryResult.getTotal() == 0) {
				breakLoop = true;
			} else {
				result.addAll(pageResult);
				LOG.debug("found {} Orders on page {} of {} pages [{} entries]", pageResult.size(), page,
						pagedQueryResult.getTotalPages(), pagedQueryResult.getTotal());
				if (page == pagedQueryResult.getTotalPages()) {
					breakLoop = true;
				}
				page++;
			}
			totalResults = pagedQueryResult.getTotal();
		}
		LOG.debug("returning Orders: total: {}, expected {}", result.size(), totalResults);
		return result;
	}

	public List<QueryPredicate<Order>> preparePredicates(final Map<String, Object> params) {
		final List<QueryPredicate<Order>> predicates = new ArrayList<>();
		if (params.containsKey(PARAM_ID)) {
			final String orderId = (String) params.get(PARAM_ID);
			predicates.add(OrderQueryModel.of().id().is(orderId));
		}

		if (params.containsKey(PARAM_ORDERSTATE)) {
			final OrderState orderState = (OrderState) params.get(PARAM_ORDERSTATE);
			predicates.add(OrderQueryModel.of().orderState().is(orderState));
		}
		if (params.containsKey(PARAM_STATELIST)) {
			@SuppressWarnings("unchecked")
			final List<State> statelist = (List<State>) params.get(PARAM_STATELIST);
			predicates.add(OrderQueryModel.of().state().isIn(statelist));
		}

		if (params.containsKey(PARAM_STATE)) {
			final State state = (State) params.get(PARAM_STATE);
			predicates.add(OrderQueryModel.of().state().is(state));
		}

		if (params.containsKey(PARAM_DATERANGE)) {
			final QueryDateRange dateRange = (QueryDateRange) params.get(PARAM_DATERANGE);
			predicates.add(OrderQueryModel.of().createdAt().isGreaterThan(dateRange.getStartZonedDateTime()));
			predicates.add(OrderQueryModel.of().createdAt().isLessThanOrEqualTo(dateRange.getEndZonedDateTime()));
		}
		if (params.containsKey(PARAM_ORDERNUMBER)) {
			final String orderNumber = (String) params.get(PARAM_ORDERNUMBER);
			predicates.add(OrderQueryModel.of().orderNumber().is(orderNumber));
		}
		return predicates;
	}

	public List<Order> getByOrderState(OrderState orderState) {
		final Map<String, Object> params = new HashMap<>();
		params.put(PARAM_LIMIT, Long.valueOf(500));
		params.put(PARAM_ORDERSTATE, orderState);
		return getQueryResult(params);
	}

	public Order getByOrderNumber(final String orderNumber) {
		final Map<String, Object> params = new HashMap<>();
		params.put(PARAM_LIMIT, Long.valueOf(1));
		params.put(PARAM_ORDERNUMBER, orderNumber);
		return getSingleResult(params);
	}

	public List<Order> getOrdersForExport() {
		return getByOrderState(OrderState.OPEN);
	}

	public List<Order> getOrdersToBeUpdated() {
		final Map<String, Object> params = new HashMap<>();
		params.put(PARAM_LIMIT, Long.valueOf(500));
		final List<State> stateList = stateQueryService.getByStatusName(openOrdersStatusList); 
		if ( stateList.isEmpty()) {
			return Collections.emptyList(); 
		}
		params.put(PARAM_STATELIST, stateList);
		return getQueryResult(params);
	}

	@Override
	protected boolean supportsByKeyQuery() {
		return false;
	}

	@Required
	public void setOpenOrdersStatusList(List<String> openOrdersStatusList) {
		this.openOrdersStatusList = openOrdersStatusList;
	}

	@Required
	public void setStateQueryService(StateQueryService stateQueryService) {
		this.stateQueryService = stateQueryService;
	}
}

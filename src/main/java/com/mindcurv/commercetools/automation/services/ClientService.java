package com.mindcurv.commercetools.automation.services;

import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.sphere.sdk.client.BlockingSphereClient;
import io.sphere.sdk.client.SphereClient;
import io.sphere.sdk.client.SphereClientConfig;
import io.sphere.sdk.client.SphereClientFactory;

public class ClientService {

	private final static Logger LOG = LoggerFactory.getLogger(ClientService.class);

	private static BlockingSphereClient blockingSphereClient;

	public static File getConnectionPropertiesFile() throws IOException {
		File directory = new File("secrets/");
		File[] files = directory.listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(".properties");
			}
		});
		LOG.debug("found {} properties files", files.length);
		if (files.length > 1) {
			throw new IOException(
					"no unique connection.properties found for project  in " + directory.getAbsolutePath());
		}
		final File file = files[0];
		LOG.info("Using {}", file.getAbsolutePath());
		return file;
	}

	public static Properties getConnectionProperties() throws IOException {

		final Properties prop = new Properties();
		prop.load(new FileReader(getConnectionPropertiesFile()));
		return prop;

	}

	public static Properties getConnectionProperties(final String project) throws IOException {
		File propsFile = new File("secrets/" + project + ".properties");
		LOG.debug("connection file:" + propsFile.getAbsolutePath() + "? " + propsFile.exists());
		if (propsFile.exists()) {
			final Properties prop = new Properties();
			prop.load(new FileReader(propsFile));
			return prop;
		}
		throw new IOException("no connection.properties found for project " + project);
	}

	/**
	 * Creates a blocking sphere client
	 * 
	 * @return Sphere client
	 * @throws IOException
	 */
	public static BlockingSphereClient createBlockingSphereClient(final String project) throws IOException {
		return createBlockingSphereClient(getConnectionProperties(project));
	}

	public static BlockingSphereClient createBlockingSphereClient(Properties connectionProperties) {
		if (blockingSphereClient == null) {
			final String apiUrl = connectionProperties.getProperty("apiUrl");
			final String authUrl = connectionProperties.getProperty("authUrl");

			final SphereClientConfig clientConfig = SphereClientConfig.ofProperties(connectionProperties, "")
					.withApiUrl(apiUrl).withAuthUrl(authUrl);
			final SphereClientFactory factory = SphereClientFactory.of();
			final SphereClient asyncSphereClient = factory.createClient(clientConfig);
			blockingSphereClient = BlockingSphereClient.of(asyncSphereClient, 20, TimeUnit.SECONDS);
		}
		return blockingSphereClient;
	}
}
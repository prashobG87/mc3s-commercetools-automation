package com.mindcurv.commercetools.automation.services.load.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.commercetools.sync.categories.CategorySync;
import com.commercetools.sync.categories.CategorySyncOptions;
import com.commercetools.sync.categories.CategorySyncOptionsBuilder;
import com.commercetools.sync.categories.helpers.CategorySyncStatistics;
import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.integration.SyncStatistics;
import com.mindcurv.commercetools.automation.services.load.AbstractLoadService;

import io.sphere.sdk.categories.CategoryDraft;

public class CategoryLoadService extends AbstractLoadService<CategoryDraft> {

	private final static Logger LOG = LoggerFactory.getLogger(CategoryLoadService.class);

	public CategoryLoadService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	@Override
	protected void init() {
		super.init();

		setTypeParameterClass(CategoryDraft.class);
	}

	@Override
	public SyncStatistics executeSync(final List<CategoryDraft> drafts) {
		final SyncStatistics syncStatistics = new SyncStatistics();
		if (validForSync(drafts)) {
			final CategorySyncOptions syncOptions = CategorySyncOptionsBuilder.of(getClient())
					.errorCallback(LOG::error).warningCallback(LOG::warn).build();
			final CategorySync sync = new CategorySync(syncOptions);
			CategorySyncStatistics syncStatisticsStage = sync.sync(drafts).toCompletableFuture().join();
			LOG.info("sync statistic:{}", syncStatisticsStage.getReportMessage());
			syncStatistics.setValues(syncStatisticsStage);
		}
		return syncStatistics;
	}
}

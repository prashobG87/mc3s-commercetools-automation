package com.mindcurv.commercetools.automation.services.setup;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.Constants;

public class CleanupService {

	private final static Logger LOG = LoggerFactory.getLogger(CleanupService.class);

	private CommercetoolsProject commercetoolsProject;

	private List<SetupService> setupServices;

	public CleanupService(CommercetoolsProject commercetoolsProject) {
		this.commercetoolsProject = commercetoolsProject;
	}

	public List<SetupService> getSetupServices() {
		return setupServices;
	}

	public void setSetupServices(List<SetupService> setupServices) {
		this.setupServices = setupServices;
	}

	public CleanupService() {
		super();
	}

	public boolean executeCleanup() {

		try {
			LOG.info(Constants.MSG_SPACER);
			LOG.info("Cleaning up project {}", commercetoolsProject.getProjectKey());

			final Date startDate = new Date();
			for (SetupService service : getSetupServices()) {
				LOG.info(Constants.MSG_SPACER);
				try {
					service.cleanupData();
				} catch (Exception e) {
					LOG.error("Exception during cleanup of type " + service.getClass().getSimpleName(), e);
				}
			}
			final Date endDate = new Date();
			LOG.info("Cleanup duration: {}s", (endDate.getTime() - startDate.getTime()) / 1000);
			LOG.info(Constants.MSG_SPACER);

		} catch (Exception e) {
			LOG.error("Exception", e);
		}
		return false;

	}

}

package com.mindcurv.commercetools.automation.services.query.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.services.query.QueryService;

import io.sphere.sdk.inventory.InventoryEntry;
import io.sphere.sdk.inventory.queries.InventoryEntryQueryBuilder;
import io.sphere.sdk.inventory.queries.InventoryEntryQueryModel;
import io.sphere.sdk.queries.PagedQueryResult;
import io.sphere.sdk.queries.QueryPredicate;

public class InventoryQueryService extends QueryService<InventoryEntry> {

	private final static Logger LOG = LoggerFactory.getLogger(InventoryQueryService.class);
	private static final String PARAM_SKU = "sku";

	public InventoryQueryService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	@Override
	public List<InventoryEntry> getQueryResult(final Map<String, Object> params) {

		final List<InventoryEntry> result = new ArrayList<>();
		Long limit = params.containsKey(PARAM_LIMIT) ? (Long) params.get(PARAM_LIMIT) : DEFAULT_LIMIT;
		final InventoryEntryQueryBuilder queryBuilder = InventoryEntryQueryBuilder.of().fetchTotal(true);
		queryBuilder.limit(limit);

		final List<QueryPredicate<InventoryEntry>> predicates = preparePredicates(params);
		if (!predicates.isEmpty()) {
			queryBuilder.predicates(predicates);
		}

		long page = 1;
		boolean breakLoop = false;
		while (!breakLoop) {
			long offset = (page - 1) * limit;
			queryBuilder.offset(offset);
			final PagedQueryResult<InventoryEntry> pagedQueryResult = getClient().executeBlocking(queryBuilder.build());
			final List<InventoryEntry> pageResult = pagedQueryResult.getResults();
			if (pagedQueryResult.getTotal() == 0) {
				breakLoop = true;
			} else {
				result.addAll(pageResult);
				LOG.debug("found {} Carts on page {} of {} pages [{} entries]", pageResult.size(), page,
						pagedQueryResult.getTotalPages(), pagedQueryResult.getTotal());
				if (page == pagedQueryResult.getTotalPages()) {
					breakLoop = true;
				}
				page++;
			}
		}
		LOG.debug("returning total: {} Carts", result.size());
		return result;
	}

	@Override
	public List<QueryPredicate<InventoryEntry>> preparePredicates(final Map<String, Object> params) {
		final List<QueryPredicate<InventoryEntry>> predicates = new ArrayList<>();
		if (params.containsKey(PARAM_ID)) {
			final String id = (String) params.get(PARAM_ID);
			predicates.add(InventoryEntryQueryModel.of().id().is(id));
		}
		
		if (params.containsKey(PARAM_SKU)) {
			final String sku = (String) params.get(PARAM_SKU);
			predicates.add(InventoryEntryQueryModel.of().sku().is(sku));
		}

		return predicates;
	}
	
	public List<InventoryEntry> getBySku(final String sku) {
		
		Map<String, Object> params = new HashMap<>();
		params.put(PARAM_SKU, sku); 
		return getQueryResult(params );
	}

	@Override
	protected boolean supportsByKeyQuery() {
		return false;
	}
}

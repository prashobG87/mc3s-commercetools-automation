package com.mindcurv.commercetools.automation.services.setup.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.google.api.services.sheets.v4.Sheets.Spreadsheets;
import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.helper.TypeHelper;
import com.mindcurv.commercetools.automation.helper.spreadsheet.ExcelHelper;
import com.mindcurv.commercetools.automation.helper.spreadsheet.GDriveHelper;
import com.mindcurv.commercetools.automation.pojo.BasePojo;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;
import com.mindcurv.commercetools.automation.pojo.organization.AddressPojo;
import com.mindcurv.commercetools.automation.pojo.organization.customer.CustomerPojo;
import com.mindcurv.commercetools.automation.pojo.types.product.TypeAttribute;
import com.mindcurv.commercetools.automation.services.load.impl.CustomerLoadService;
import com.mindcurv.commercetools.automation.services.query.impl.CustomerQueryService;
import com.mindcurv.commercetools.automation.services.setup.SetupService;

import io.sphere.sdk.customers.Customer;
import io.sphere.sdk.customers.CustomerDraft;
import io.sphere.sdk.customers.commands.CustomerDeleteCommand;
import io.sphere.sdk.types.Type;

/**
 * This class provides operations to work with {@link Customer}s.
 */
public class CustomerSetupService extends SetupService {

	private final static Logger LOG = LoggerFactory.getLogger(CustomerSetupService.class);

	private CustomerQueryService customerQueryService;
	private CustomerLoadService customerLoadService;

	public CustomerSetupService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	@Override
	protected void init() {
		super.init();
		final CommercetoolsProject commercetoolsProjectBean = getCommercetoolsProject();
		if (commercetoolsProjectBean != null) {
			if (customerQueryService == null) {
				customerQueryService = new CustomerQueryService(commercetoolsProjectBean);
			}
			if (customerLoadService == null) {
				customerLoadService = new CustomerLoadService(commercetoolsProjectBean);
			}
		}
	}

	@Override
	public boolean cleanupData() {
		LOG.info("cleanup Customer items");

		for (Customer customer : customerQueryService.getAllEntries()) {
			CustomerDeleteCommand command = CustomerDeleteCommand.of(customer);
			customer = getClient().executeBlocking(command);
			LOG.info("Removed customer {}", customer);
		}

		return true;
	}

	@Override
	public boolean setup(String sheetId, Spreadsheets sheets, Map<String, List<TypeAttribute>> attributesMap) {
		setAssociatedPojos(GDriveHelper.prepareAddressPojos(sheets, sheetId,
				CsvConstants.Address.REFERENCE_TYPE_CUSTOMER, getCommercetoolsProject()));
		return super.setup(sheetId, sheets, attributesMap);
	}
	@Override
	public boolean setup(Workbook workbook, Map<String, List<TypeAttribute>> attributesMap) {
		setAssociatedPojos(ExcelHelper.prepareAddressPojos(workbook, CsvConstants.Address.REFERENCE_TYPE_CUSTOMER, getCommercetoolsProject()));
		return super.setup(workbook, attributesMap);
	}
	
	@Override
	protected boolean setupValues(List<Map<String, Object>> valueLines,
			Map<String, List<TypeAttribute>> attributesMap) {
		final List<CustomerDraft> drafts = new ArrayList<>();
		final Map<String, Type> typesMap = TypeHelper.determineTypeIds(getClient(), Arrays.asList("customer"));

		final Map<String, List<BasePojo>> associatedPojos = getAssociatedPojos();

		for (Map<String, Object> value : valueLines) {
			final CustomerPojo pojo = new CustomerPojo(value);

			processAddresses(associatedPojos, pojo);

			if (pojo.getCustom() != null) {
				pojo.addAttributes(value, attributesMap);
				TypeHelper.translateKeyToTypeId(typesMap, pojo);
			}

			drafts.add(pojo.toDraft());
		}
		if (!drafts.isEmpty()) {
			customerLoadService.sync(drafts);
		}
		return true;
	}

	private void processAddresses(final Map<String, List<BasePojo>> associatedPojos, CustomerPojo pojo) {
		final String pojoKey = pojo.getCustomerNumber(); 
		if (associatedPojos.containsKey(pojoKey)) {
			final List<BasePojo> addressList = (List<BasePojo>) associatedPojos.get(pojoKey);
			if (addressList != null) {
				for (BasePojo basePojo : addressList) {
					final AddressPojo addressPojo = (AddressPojo) basePojo;
					pojo.getAddresses().add(addressPojo);
				}
			}
		}
	}

	@Override
	public String getSheetname() {
		return CsvConstants.Customer.SHEET;
	}

	@Required
	public void setCustomerQueryService(CustomerQueryService customerQueryService) {
		this.customerQueryService = customerQueryService;
	}

	@Required
	public void setCustomerLoadService(CustomerLoadService customerLoadService) {
		this.customerLoadService = customerLoadService;
	}

}

package com.mindcurv.commercetools.automation.services.load.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.Constants;
import com.mindcurv.commercetools.automation.helper.files.ImageFileFilter;
import com.mindcurv.commercetools.automation.integration.SyncStatistics;
import com.mindcurv.commercetools.automation.services.ClientService;
import com.mindcurv.commercetools.automation.services.PublicationService;
import com.mindcurv.commercetools.automation.services.load.AbstractLoadService;
import com.mindcurv.commercetools.automation.services.query.impl.ProductQueryService;

import io.sphere.sdk.products.Image;
import io.sphere.sdk.products.Product;
import io.sphere.sdk.products.ProductVariant;
import io.sphere.sdk.products.commands.ProductImageUploadCommand;
import io.sphere.sdk.products.commands.ProductUpdateCommand;
import io.sphere.sdk.products.commands.updateactions.SetImageLabel;

public class MediaLoadService extends AbstractLoadService<File> {

	private final static Logger LOG = LoggerFactory.getLogger(MediaLoadService.class);

	private PublicationService publicationService;
	private ProductQueryService productQueryService;

	public MediaLoadService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	@Override
	protected void init() {
		super.init();

		setTypeParameterClass(File.class);

		final CommercetoolsProject commercetoolsProjectBean = getCommercetoolsProject();
		if (commercetoolsProjectBean != null) {

			if (publicationService == null) {
				publicationService = new PublicationService(commercetoolsProjectBean);
			}
			if (productQueryService == null) {
				productQueryService = new ProductQueryService(commercetoolsProjectBean);
			}
		}
	}

	@Override
	public SyncStatistics syncStrings(List<String> filepathList) {
		List<File> files = new ArrayList<>();
		for (String filePath : filepathList) {
			files.add(new File(filePath));
		}
		return executeSync(files);
	}

	@Override
	public SyncStatistics executeSync(List<File> files) {
		final SyncStatistics syncStatistics = new SyncStatistics();
		LOG.info(Constants.MSG_SPACER);

		final List<String> productSkuList = new ArrayList<>();

		for (File uploadImageFile : files) {
			syncStatistics.incrementProcessed();
			try {
				final String uploadFileName = uploadImageFile.getName();
				final String sku = extractSkuFromFilename(uploadFileName);

				final Product product = productQueryService.getBySku(sku);
				if (product != null) {
					uploadImageFile(productSkuList, uploadImageFile, uploadFileName, sku, product);
					syncStatistics.incrementUpdated();
				} else {
					LOG.info("no product found for sku {})", sku);
					syncStatistics.incrementFailed();
				}
			} catch (Exception e) {
				LOG.error("Exception during image assignment of " + uploadImageFile.getAbsolutePath(), e);
				syncStatistics.incrementFailed();
			}

		}
		LOG.info(Constants.MSG_SPACER);

		return syncStatistics;

	}

	public SyncStatistics sync() {
		File imageDirectory = determineImageDirectory();
		if (imageDirectory == null) {
			LOG.error("No valid image directory configured");
		}
		if (imageDirectory.exists()) {
			List<File> files = Arrays.asList(imageDirectory.listFiles(new ImageFileFilter()));
			return executeSync(files);
		}
		return new SyncStatistics();
	}

	private void uploadImageFile(final List<String> productSkuList, File uploadImageFile, final String uploadFileName,
			final String sku, Product product) {
		if (checkIfUploadIsRequired(uploadFileName, product)) {
			LOG.info("upload required for {} and sku {}", uploadFileName, sku);

			final ProductImageUploadCommand command = ProductImageUploadCommand
					.ofMasterVariant(uploadImageFile, product.getId()).withFilename(uploadFileName);
			product = getClient().executeBlocking(command);
			final ProductVariant masterVariant = product.getMasterData().getStaged().getMasterVariant();
			final String uploadFileNameWithoutSuffix = getFileNameWithoutSuffix(uploadFileName);

			for (Image image : product.getMasterData().getStaged().getMasterVariant().getImages()) {
				if (StringUtils.isBlank(image.getLabel())) {
					LOG.info("Setting new label {} for image {}", uploadFileNameWithoutSuffix, image.getUrl());
					final SetImageLabel imageLabelAction = SetImageLabel.of(masterVariant.getId(), image.getUrl(),
							uploadFileNameWithoutSuffix, Boolean.TRUE);
					product = getClient().executeBlocking(ProductUpdateCommand.of(product, imageLabelAction));

					productSkuList.add(product.getMasterData().getStaged().getMasterVariant().getSku());
				}

			}
			LOG.info("Updated product {}", product.getKey());
			publicationService.publishProduct(product);

		} else {
			LOG.debug("no upload required for {} and sku {}", uploadFileName, sku);
		}
	}

	private String getFileNameWithoutSuffix(final String uploadFileName) {
		return uploadFileName.substring(0, uploadFileName.lastIndexOf("."));
	}

	private boolean checkIfUploadIsRequired(final String uploadFileName, final Product product) {

		final String uploadFileNameWithoutSuffix = getFileNameWithoutSuffix(uploadFileName);
		final ProductVariant masterVariant = product.getMasterData().getStaged().getMasterVariant();
		LOG.info("checking file {} for variant sku {}", uploadFileNameWithoutSuffix, masterVariant.getSku());

		boolean needsUpload = true;

		final List<Image> images = masterVariant.getImages();
		if (images.isEmpty()) {
			LOG.info("no images found at variant sku {}", masterVariant.getSku());
			return true;
		} else {
			for (Image image : images) {
				LOG.info("Checking {} , {}, URL {}", masterVariant.getSku(), image.getLabel(), image.getUrl());
				if (StringUtils.contains(image.getUrl(), uploadFileNameWithoutSuffix)) {
					LOG.info("image {} already uploaded for variant sku {} (url:{}/{})", uploadFileName,
							masterVariant.getSku(), uploadFileNameWithoutSuffix, image.getUrl());
					needsUpload = false;
					break;
				}

				if (uploadFileName.equals(image.getLabel())) {
					LOG.info("image {} already uploaded for variant sku {} (label:{})", uploadFileName,
							masterVariant.getSku(), image.getLabel());
					needsUpload = false;
					break;
				}
				if (StringUtils.isEmpty(image.getLabel())) {
					LOG.warn("variant sku {} -> image without label {}", masterVariant.getSku(), image.getUrl());
				}
			}
		}
		return needsUpload;
	}

	private String extractSkuFromFilename(String name) {

		final String[] nameParts = name.split("_");
		final StringBuffer stringBuffer = new StringBuffer();
		// mindcurv_010_product.png -> SKU = mindcurv://010
		if (nameParts.length >= 3) {
			stringBuffer.append(nameParts[0]).append("://").append(nameParts[1]);

			// mindcurv_010_01_product.png -> SKU = mindcurv://010/1
			if (nameParts.length > 3) {
				stringBuffer.append("/").append(nameParts[2]);
			}
		}
		final String sku = stringBuffer.toString();
		LOG.info("File '{}' -> SKU '{}'", name, sku);
		return sku;
	}

	private File determineImageDirectory() {
		try {
			final String projectKey = getClient().getConfig().getProjectKey();
			final Properties props = ClientService.getConnectionProperties(projectKey);
			final File imageDirectory = new File(props.getProperty(Constants.PROPS_PRODUCTIMAGEDIR, "syncData/images"));
			LOG.info("imageDirectory:{}", imageDirectory.getAbsolutePath());
			if (!imageDirectory.exists()) {
				LOG.warn("No file found {}, try to create", imageDirectory.getAbsolutePath());
				boolean success = imageDirectory.mkdirs();
				if (imageDirectory.exists()) {
					return imageDirectory;
				}
				LOG.warn("Wasn't able to create success, {}", success, imageDirectory.getAbsolutePath());
				return null;
			} else if (!imageDirectory.isDirectory()) {
				LOG.warn("{} is not a directory", imageDirectory.getAbsolutePath());
				return null;
			}
			return imageDirectory;
		} catch (Exception e) {
			LOG.error("Exception", e);
		}
		return null;
	}

	public void setPublicationService(PublicationService publicationService) {
		this.publicationService = publicationService;
	}

	public void setProductQueryService(ProductQueryService productQueryService) {
		this.productQueryService = productQueryService;
	}

}

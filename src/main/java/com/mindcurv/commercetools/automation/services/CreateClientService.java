package com.mindcurv.commercetools.automation.services;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mindcurv.commercetools.automation.services.load.impl.ProductLoadService;

import io.sphere.sdk.client.BlockingSphereClient;
import io.sphere.sdk.client.SphereClient;
import io.sphere.sdk.client.SphereClientConfig;
import io.sphere.sdk.client.SphereClientFactory;
import io.sphere.sdk.projects.Project;
import io.sphere.sdk.projects.queries.ProjectGet;

public class CreateClientService  {
	private final static Logger LOG = LoggerFactory.getLogger(ProductLoadService.class);
	protected BlockingSphereClient client = null;

	public CreateClientService(final Properties context) {

		try {

			final String clientId = context.getProperty(SphereClientConfig.PROPERTIES_KEY_CLIENT_ID_SUFFIX);
			final String clientSecret = context
					.getProperty(context.getProperty(SphereClientConfig.PROPERTIES_KEY_CLIENT_SECRET_SUFFIX));
			final String projectKey = context
					.getProperty(context.getProperty(SphereClientConfig.PROPERTIES_KEY_PROJECT_KEY_SUFFIX));

			final String apiUrl = context.getProperty(SphereClientConfig.PROPERTIES_KEY_API_URL_SUFFIX);
			final String authUrl = context.getProperty(SphereClientConfig.PROPERTIES_KEY_AUTH_URL_SUFFIX);

			LOG.info("creating client with projectKey {}, clientId {}, clientSecret provided {}, apiUrl {}, authUrl {}",
					projectKey, clientId, StringUtils.isNotBlank(clientSecret), apiUrl, authUrl);

			
			LOG.info( "{}", context);
			
			final SphereClientConfig clientConfig = SphereClientConfig.ofProperties(context, "")
					.withApiUrl(apiUrl)
					.withAuthUrl(authUrl);

			final SphereClientFactory factory = SphereClientFactory.of();
			final SphereClient asyncSphereClient = factory.createClient(clientConfig);
			
			client = BlockingSphereClient.of(asyncSphereClient, 20, TimeUnit.SECONDS);

			LOG.info("created client {}", client);
			System.out.println(client);
			
			System.out.println( printProjectInfo(client) );
		} catch (Exception e) {
			LOG.error("Exception during client setup");
		}
	}
	protected Project printProjectInfo(final BlockingSphereClient client) {
		final Project projectResponse = client.executeBlocking(ProjectGet.of());
	
		LOG.info("Project info {}", projectResponse);
		return projectResponse;
	}
	public SphereClient getClient() {
		return client;
	}

}

package com.mindcurv.commercetools.automation.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

import com.mindcurv.commercetools.automation.CommercetoolsProject;

import io.sphere.sdk.client.BlockingSphereClient;
import io.sphere.sdk.projects.Project;
import io.sphere.sdk.projects.queries.ProjectGet;

/**
 * Abstract base class for services.
 */

@Configuration
public abstract class CommercetoolsBaseService {

	private final static Logger LOG = LoggerFactory.getLogger(CommercetoolsBaseService.class);

	private BlockingSphereClient client = null;

	private CommercetoolsProject commercetoolsProject;

	public CommercetoolsBaseService(CommercetoolsProject commercetoolsProject) {
		if (commercetoolsProject == null) {
			LOG.error("commercetoolsProject is null");
		} else {
			this.commercetoolsProject = commercetoolsProject;
			this.client = getCommercetoolsProject().getClient();
			init();
		}
	}

	protected void init() {
		if (commercetoolsProject == null) {
			LOG.error("commercetoolsProjectBean is null");
		} 
	}

	public CommercetoolsProject getCommercetoolsProject() {
		return commercetoolsProject;
	}

	public void setCommercetoolsProject(CommercetoolsProject commercetoolsProject) {
		this.commercetoolsProject = commercetoolsProject;
		if ( commercetoolsProject != null) {
			init(); 
		}
	}

	public BlockingSphereClient getClient() {
		if (client != null) {
			return client;
		}

		if (getCommercetoolsProject() == null) {
			LOG.error("no CommercetoolsProject configured");
		} else {
			client = getCommercetoolsProject().getClient();
		}
		if (client == null) {
			LOG.error("no client available");
		}
		return client;
	}

	protected Project printProjectInfo(final BlockingSphereClient client) {
		final Project projectResponse = client.executeBlocking(ProjectGet.of());

		LOG.info("Project info {}", projectResponse);
		return projectResponse;
	}
}

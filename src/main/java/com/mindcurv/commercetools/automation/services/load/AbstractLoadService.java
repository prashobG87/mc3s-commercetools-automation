package com.mindcurv.commercetools.automation.services.load;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.integration.SyncStatistics;
import com.mindcurv.commercetools.automation.services.CommercetoolsBaseService;

import io.sphere.sdk.json.SphereJsonUtils;

public abstract class AbstractLoadService<T> extends CommercetoolsBaseService {
	
	private final static Logger LOG = LoggerFactory.getLogger(AbstractLoadService.class);

	private Class<T> typeParameterClass; 
	
	protected AbstractLoadService(CommercetoolsProject commercetoolsProject ) {
		super(commercetoolsProject);
	}
	
	abstract public SyncStatistics executeSync(List<T> drafts);
	
	public SyncStatistics syncStrings(List<String> input) {
		final List<T> drafts = new ArrayList<>();
		for (String draftString : input) {
			drafts.add(SphereJsonUtils.readObject(draftString, getTypeParameterClass()));
		}
		return sync(drafts);
	}
	
	public SyncStatistics sync(List<T> drafts) {
		final SyncStatistics syncStatistics = executeSync(drafts); 
		dumpDrafts(drafts);
		return syncStatistics; 
	}
	public void dumpDrafts(List<T> drafts) {
		if ( drafts == null) {
			return; 
		}
		if ( !getCommercetoolsProject().isDumpDraftsEnabled()) {
			LOG.debug( "dump drafts is disabled:{}", getCommercetoolsProject());
			return; 
		}
		final File typeDumpDirectory = new File( getCommercetoolsProject().getDumpDirectory(), getTypeParameterClass().getSimpleName()); 
		
		for (final T draft : drafts) {
			final File targetFile = new File(typeDumpDirectory, getTypeParameterClass().getSimpleName() + "-" + System.currentTimeMillis()+ ".json"); 
			try {
				LOG.debug( "store {} to {}", draft, targetFile.getAbsolutePath());
				FileUtils.writeStringToFile(targetFile, SphereJsonUtils.toPrettyJsonString(draft));
			} catch (IOException e) {
				LOG.error( "IOException while dumping draft", e);
			}
		}
		LOG.info("stored {} drafts to {}", drafts.size(), typeDumpDirectory.getAbsolutePath());
	}
	
	public Class<T> getTypeParameterClass() {
		return typeParameterClass;
	}
	public void setTypeParameterClass(Class<T> typeParameterClass) {
		this.typeParameterClass = typeParameterClass;
	}
	public boolean validForSync(final List<?> drafts) {
		if (getClient() == null) {
			LOG.error("no client defined");
			return false;
		}
		if (drafts == null || drafts.isEmpty()) {
			LOG.warn("no draft items available.");
			return false;
		}
		LOG.info("number of prepared drafts: {}", drafts.size());
		return true; 
	}
	
	public boolean analyzeResult(final SyncStatistics syncStatistics) {
		if ( syncStatistics.getFailed() > 0) {
			LOG.warn( "Sync Statistic contains failures {}", syncStatistics);
			return false; 
		}
		LOG.info( "Sync Statistic : {}", syncStatistics);
		return true;
	}
}

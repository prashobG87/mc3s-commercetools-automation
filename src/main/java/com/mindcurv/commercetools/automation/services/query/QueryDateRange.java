package com.mindcurv.commercetools.automation.services.query;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

public class QueryDateRange {

	private ZonedDateTime startZonedDateTime;
	private ZonedDateTime endZonedDateTime;

	public QueryDateRange(Date startDate, Date endDate) {
		this.startZonedDateTime = toZonedDateTime(startDate);
		this.endZonedDateTime = toZonedDateTime(endDate);
	}

	public ZonedDateTime getStartZonedDateTime() {
		return startZonedDateTime;
	}

	public ZonedDateTime getEndZonedDateTime() {
		return endZonedDateTime;
	}

	public String toString() {
		final StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(this.getClass().getName());
		stringBuffer.append(" ( startDate:").append(startZonedDateTime);
		stringBuffer.append(", endDate:").append(endZonedDateTime).append(")");
		return stringBuffer.toString();
	}
	
	private ZonedDateTime toZonedDateTime(Date utilDate) {
	    if (utilDate == null) {
	      return null;
	    }
	    final ZoneId systemDefault = ZoneId.systemDefault();
	    return ZonedDateTime.ofInstant(utilDate.toInstant(), systemDefault);
	  }

}

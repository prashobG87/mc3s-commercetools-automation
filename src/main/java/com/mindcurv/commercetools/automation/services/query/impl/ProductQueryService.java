package com.mindcurv.commercetools.automation.services.query.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.services.query.QueryService;

import io.sphere.sdk.products.Product;
import io.sphere.sdk.products.ProductProjectionType;
import io.sphere.sdk.products.queries.ProductQuery;
import io.sphere.sdk.products.queries.ProductQueryBuilder;
import io.sphere.sdk.products.queries.ProductQueryModel;
import io.sphere.sdk.producttypes.ProductType;
import io.sphere.sdk.queries.PagedQueryResult;
import io.sphere.sdk.queries.QueryPredicate;

public class ProductQueryService extends QueryService<Product> {

	private final static Logger LOG = LoggerFactory.getLogger(ProductQueryService.class);
	
	private ProductTypeQueryService productTypeQueryService; 

	public static final String PARAM_PUBLISHED = "published";
	public static final String PARAM_PRODUCTTYPE = "productType";

	public ProductQueryService(CommercetoolsProject commercetoolsProject ) {
		super(commercetoolsProject);
	}
	
	@Override
	public List<Product> getQueryResult(Map<String, Object> params) {
		final List<Product> result = new ArrayList<>();
		Long limit = params.containsKey(PARAM_LIMIT) ? (Long) params.get(PARAM_LIMIT) : DEFAULT_LIMIT;
		final ProductQueryBuilder queryBuilder = ProductQueryBuilder.of().fetchTotal(true);
		queryBuilder.limit(limit);

		final List<QueryPredicate<Product>> predicates = preparePredicates(params);
		if (!predicates.isEmpty()) {
			queryBuilder.predicates(predicates);
		}

		long page = 1;
		boolean breakLoop = false;
		while (!breakLoop) {
			long offset = (page - 1) * limit;
			queryBuilder.offset(offset);
			final PagedQueryResult<Product> pagedQueryResult = getClient().executeBlocking(queryBuilder.build())
				;
			final List<Product> pageResult = pagedQueryResult.getResults();

			if (pagedQueryResult.getTotal() == 0) {
				breakLoop = true;
			} else {
				result.addAll(pageResult);
				LOG.info("found {} Products on page {} of {} pages [{} entries]", pageResult.size(), page,
						pagedQueryResult.getTotalPages(), pagedQueryResult.getTotal());
				if (page == pagedQueryResult.getTotalPages()) {
					breakLoop = true;
				}
				page++;
			}
		}
		LOG.debug("returning total: {} Products", result.size());
		return result;
	}

	@Override
	public List<QueryPredicate<Product>> preparePredicates(Map<String, Object> params) {
		final List<QueryPredicate<Product>> predicates = new ArrayList<>();
		if (params.containsKey(PARAM_ID)) {
			final String id = (String) params.get(PARAM_ID);
			predicates.add(ProductQueryModel.of().id().is(id));
		}
		if (params.containsKey(PARAM_PUBLISHED)) {
			final Boolean published = (Boolean) params.get(PARAM_PUBLISHED);
			predicates.add(ProductQueryModel.of().masterData().published().is(published));
		}
		if (params.containsKey(PARAM_PRODUCTTYPE)) {
			@SuppressWarnings("unchecked")
			final List<String> typeIds = (List<String>) params.get(PARAM_PRODUCTTYPE);
			predicates.add(ProductQueryModel.of().productType().isInIds(typeIds)); 
		}
		return predicates;
	}

	public List<Product> getAllProductsByPublicationStatus(boolean published) {
		final Map<String, Object> params = new HashMap<>();
		params.put(PARAM_PUBLISHED, published);
		return getQueryResult(params);
	}
	
	public List<Product> getAllProductsByProductType(List<String> productTypeKeys) {
		final List<ProductType> productTypes = getProductTypeQueryService().getByKeys(productTypeKeys); 
		if ( productTypes != null && !productTypes.isEmpty()) {
			final List<String> idList = new ArrayList<>(); 
			for ( ProductType productType : productTypes) {
				idList.add(productType.getId()); 
			}
			final Map<String, Object> params = new HashMap<>();
			params.put(PARAM_PRODUCTTYPE, idList); 
			params.put(PARAM_LIMIT, DEFAULT_LIMIT);
			return getQueryResult(params);
		}
		LOG.warn("producttypes {} not present", productTypeKeys);
		return Collections.emptyList(); 
	}
	
	public List<Product> getAllPublishedProductsByProductType(List<String> productTypeKeys) {
		final List<ProductType> productTypes = getProductTypeQueryService().getByKeys(productTypeKeys); 
		if ( productTypes != null && !productTypes.isEmpty()) {
			final List<String> idList = new ArrayList<>(); 
			for ( ProductType productType : productTypes) {
				idList.add(productType.getId()); 
			}
			final Map<String, Object> params = new HashMap<>();
			params.put(PARAM_PRODUCTTYPE, idList); 
			params.put(PARAM_PUBLISHED, Boolean.TRUE); 
			params.put(PARAM_LIMIT, DEFAULT_LIMIT);
			return getQueryResult(params);
		}
		LOG.warn("producttypes {} not present", productTypeKeys);
		return Collections.emptyList(); 
	}

	public Product getBySku(final String sku, ProductProjectionType productProjectionType) {
		final ProductQuery queryBySku = ProductQuery.of().bySku(sku, productProjectionType);

		final List<Product> list = getClient().executeBlocking(queryBySku).getResults();
		if (list != null && list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

	public Product getBySku(final String sku) {
		return getBySku(sku, ProductProjectionType.CURRENT);
	}

	@Override
	protected boolean supportsByKeyQuery() {
		return false;
	}

	public ProductTypeQueryService getProductTypeQueryService() {
		return productTypeQueryService;
	}

	@Required
	public void setProductTypeQueryService(ProductTypeQueryService productTypeQueryService) {
		this.productTypeQueryService = productTypeQueryService;
	}
}

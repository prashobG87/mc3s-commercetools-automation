package com.mindcurv.commercetools.automation.services.setup.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;
import com.mindcurv.commercetools.automation.pojo.organization.customer.CustomerGroupPojo;
import com.mindcurv.commercetools.automation.pojo.types.product.TypeAttribute;
import com.mindcurv.commercetools.automation.services.load.impl.CustomergroupLoadService;
import com.mindcurv.commercetools.automation.services.query.impl.CustomerGroupQueryService;
import com.mindcurv.commercetools.automation.services.setup.SetupService;

import io.sphere.sdk.customergroups.CustomerGroup;
import io.sphere.sdk.customergroups.CustomerGroupDraft;
import io.sphere.sdk.customergroups.commands.CustomerGroupDeleteCommand;
import io.sphere.sdk.customers.Customer;

/**
 * This class provides operations to work with {@link Customer}s.
 */
public class CustomerGroupSetupService extends SetupService {

	private final static Logger LOG = LoggerFactory.getLogger(CustomerGroupSetupService.class);

	private CustomerGroupQueryService customergroupQueryService;
	private CustomergroupLoadService customergroupLoadService;

	public CustomerGroupSetupService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	@Override
	protected void init() {
		super.init();
		final CommercetoolsProject commercetoolsProjectBean = getCommercetoolsProject();
		if (commercetoolsProjectBean != null) {
			if (customergroupQueryService == null) {
				customergroupQueryService = new CustomerGroupQueryService(commercetoolsProjectBean);
			}
			if (customergroupLoadService == null) {
				customergroupLoadService = new CustomergroupLoadService(commercetoolsProjectBean);
			}
		}
	}

	@Override
	public boolean cleanupData() {
		LOG.info("cleanup CustomerGroup items");

		for (CustomerGroup customerGroup : customergroupQueryService.getAllEntries()) {
			CustomerGroupDeleteCommand command = CustomerGroupDeleteCommand.of(customerGroup);
			customerGroup = getClient().executeBlocking(command);
			LOG.info("Removed customerGroup {}", customerGroup);
		}
		return true;
	}

	@Override
	protected boolean setupValues(List<Map<String, Object>> valueLines, Map<String, List<TypeAttribute>> attributesMap) {
		final List<CustomerGroupDraft> drafts = new ArrayList<>();
		for (Map<String, Object> value : valueLines) {
			final CustomerGroupPojo pojo = new CustomerGroupPojo(value);
			if (pojo.getCustom() != null) {
				pojo.addAttributes(value, attributesMap);
			}
			drafts.add(pojo.toDraft());
		}
		if (!drafts.isEmpty()) {
			customergroupLoadService.sync(drafts);
		}
		return true;
	}

	@Override
	public String getSheetname() {
		return CsvConstants.CustomerGroup.SHEET;
	}

	@Required
	public void setCustomergroupQueryService(CustomerGroupQueryService customergroupQueryService) {
		this.customergroupQueryService = customergroupQueryService;
	}

	@Required
	public void setCustomergroupLoadService(CustomergroupLoadService customergroupLoadService) {
		this.customergroupLoadService = customergroupLoadService;
	}

}

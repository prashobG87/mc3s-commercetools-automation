package com.mindcurv.commercetools.automation.services.query.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.services.query.QueryService;

import io.sphere.sdk.customers.Customer;
import io.sphere.sdk.customers.queries.CustomerQueryBuilder;
import io.sphere.sdk.customers.queries.CustomerQueryModel;
import io.sphere.sdk.queries.PagedQueryResult;
import io.sphere.sdk.queries.QueryPredicate;

public class CustomerQueryService extends QueryService<Customer> {

	private final static Logger LOG = LoggerFactory.getLogger(CustomerQueryService.class);

	public static final String PARAM_CUSTOMERNUMBER = "customerNumber";
	public static final String PARAM_EMAIL = "email";

	public CustomerQueryService(CommercetoolsProject commercetoolsProject ) {
		super(commercetoolsProject);
	}

	@Override
	public List<Customer> getQueryResult(Map<String, Object> params) {
		final List<Customer> result = new ArrayList<>();
		Long limit = params.containsKey(PARAM_LIMIT) ? (Long) params.get(PARAM_LIMIT) : DEFAULT_LIMIT;
		final CustomerQueryBuilder queryBuilder = CustomerQueryBuilder.of().fetchTotal(true);
		queryBuilder.limit(limit);

		final List<QueryPredicate<Customer>> predicates = preparePredicates(params);
		if (!predicates.isEmpty()) {
			queryBuilder.predicates(predicates);
		}

		long page = 1;
		boolean breakLoop = false;
		while (!breakLoop) {
			long offset = (page - 1) * limit;
			queryBuilder.offset(offset);
			final PagedQueryResult<Customer> pagedQueryResult = getClient().executeBlocking(queryBuilder.build());
			final List<Customer> pageResult = pagedQueryResult.getResults();
			if (pagedQueryResult.getTotal() == 0) {
				breakLoop = true;
			} else {
				result.addAll(pageResult);
				LOG.debug("found {} CustomerGroups on page {} of {} pages [{} entries]", pageResult.size(), page,
						pagedQueryResult.getTotalPages(), pagedQueryResult.getTotal());
				if (page == pagedQueryResult.getTotalPages()) {
					breakLoop = true;
				}
				page++;
			}
		}
		LOG.debug("returning total: {} Customers", result.size());
		return result;
	}

	@Override
	public List<QueryPredicate<Customer>> preparePredicates(Map<String, Object> params) {
		final List<QueryPredicate<Customer>> predicates = new ArrayList<>();
		if (params.containsKey(PARAM_ID)) {
			final String id = (String) params.get(PARAM_ID);
			predicates.add(CustomerQueryModel.of().id().is(id));
		}
		if (params.containsKey(PARAM_KEY)) {
			final String key = (String) params.get(PARAM_KEY);
			predicates.add(CustomerQueryModel.of().key().is(key));
		}
		if (params.containsKey(PARAM_CUSTOMERNUMBER)) {
			final String customerNumber = (String) params.get(PARAM_CUSTOMERNUMBER);
			predicates.add(CustomerQueryModel.of().customerNumber().is(customerNumber));
		}
		if (params.containsKey(PARAM_EMAIL)) {
			final String email = (String) params.get(PARAM_EMAIL);
			predicates.add(CustomerQueryModel.of().email().is(email));
		}

		return predicates;
	}

	public Customer getByCustomerNumber(String customerNumber) {
		final Map<String, Object> params = new HashMap<>();
		params.put(PARAM_CUSTOMERNUMBER, customerNumber);
		final Customer result = getSingleResult(params);
		printResult(result);
		return result;
	}

	public Customer getByEmail(final String email) {
		final Map<String, Object> params = new HashMap<>();
		params.put(PARAM_EMAIL, email);
		final Customer result = getSingleResult(params);
		printResult(result);
		return result;

	}

	@Override
	protected boolean supportsByKeyQuery() {
		return true;
	}
}

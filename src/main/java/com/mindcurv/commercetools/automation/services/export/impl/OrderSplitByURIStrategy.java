package com.mindcurv.commercetools.automation.services.export.impl;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.mindcurv.commercetools.automation.services.export.AbstractOrderSplitStrategy;
import com.mindcurv.commercetools.automation.services.export.OrderImpExService;
import com.mindcurv.commercetools.automation.services.query.impl.StateQueryService;

import io.sphere.sdk.carts.LineItem;
import io.sphere.sdk.client.BlockingSphereClient;
import io.sphere.sdk.commands.UpdateAction;
import io.sphere.sdk.commands.UpdateActionImpl;
import io.sphere.sdk.models.Address;
import io.sphere.sdk.orders.Delivery;
import io.sphere.sdk.orders.DeliveryItem;
import io.sphere.sdk.orders.Order;
import io.sphere.sdk.orders.OrderState;
import io.sphere.sdk.orders.Parcel;
import io.sphere.sdk.orders.ParcelDraft;
import io.sphere.sdk.orders.ParcelDraftBuilder;
import io.sphere.sdk.orders.TrackingData;
import io.sphere.sdk.orders.commands.OrderUpdateCommand;
import io.sphere.sdk.orders.commands.updateactions.AddDelivery;
import io.sphere.sdk.orders.commands.updateactions.ChangeOrderState;
import io.sphere.sdk.orders.commands.updateactions.SetOrderNumber;
import io.sphere.sdk.orders.commands.updateactions.TransitionState;
import io.sphere.sdk.states.State;

public class OrderSplitByURIStrategy extends AbstractOrderSplitStrategy {

	private final static Logger LOG = LoggerFactory.getLogger(OrderSplitByURIStrategy.class);

	private static final String SPLIT = "-";
	private static final String ORDER_SPLITTED = "ORDER_SPLITTED";
	
	private StateQueryService stateQueryService;

	@Override
	public Map<String, Object> splitOrder(Order order) {
		if (order == null) {
			LOG.error("Order is null");
			return null;
		}
		//TODO Configurable
		if (!OrderState.OPEN.equals(order.getOrderState())) {
			LOG.warn("order {} is not in state {}:", order.getId(), OrderState.OPEN, order.getOrderState());
			return null;
		}
		LOG.info("splitting order {}/{}/{}", order.getId(), order.getOrderState(), order.getState());

		final Map<String, List<LineItem>> splittedLineItemMap = getSplitLineItemMap(order);

		for (String key : splittedLineItemMap.keySet()) {
			order = createDeliveryForKey(order, splittedLineItemMap, key);
		}

		final Map<String, Object> splitMap = new HashMap<>();
		for (Delivery delivery : order.getShippingInfo().getDeliveries()) {

			final Parcel parcel = delivery.getParcels().get(0);
			final String key = parcel.getTrackingData().getProvider();
			splitMap.put(key, delivery);

		}
		List<UpdateAction<Order>> actions = new ArrayList<>();
		final State orderStateSplitted = stateQueryService.getByKey(ORDER_SPLITTED);
		actions.add(ChangeOrderState.of(OrderState.CONFIRMED));
		if (orderStateSplitted != null) {
			actions.add(TransitionState.of(orderStateSplitted));
		}
		if (order.getOrderNumber() == null) {
			final String orderNumber = generateOrderNumber();
			actions.add(SetOrderNumber.of(orderNumber));
		}
		final OrderUpdateCommand orderUpdateCommand = OrderUpdateCommand.of(order, actions);
		order = getClient().executeBlocking(orderUpdateCommand);

		splitMap.put(OrderImpExService.KEY_ORDER, order);

		LOG.info("after split {}/{}/{}/{}", order.getId(), order.getOrderNumber(), order.getOrderState(),
				order.getState());

		return splitMap;
	}

	protected String generateOrderExportMapKey(final String sku) throws URISyntaxException {
		final URI uri = new URI(sku);
		return uri.getScheme();
	}

	protected Map<String, List<LineItem>> getSplitLineItemMap(Order order) {

		final Map<String, List<LineItem>> splittedLineItemMap = new HashMap<>();
		for (LineItem lineItem : order.getLineItems()) {
			try {
				final String sku = lineItem.getVariant().getSku();

				final String mapKey = generateOrderExportMapKey(sku);
				LOG.info("Line Item {} {} -> {}", order.getId(), sku, mapKey);
				List<LineItem> lineItemList = splittedLineItemMap.get(mapKey);
				if (lineItemList == null) {
					lineItemList = new ArrayList<>();
				}
				lineItemList.add(lineItem);
				splittedLineItemMap.put(mapKey, lineItemList);
			} catch (URISyntaxException e) {
				LOG.error("No valid URI", e);
			}
		}
		return splittedLineItemMap;
	}

	private String generateOrderNumber() {
		return "O-" + System.currentTimeMillis() + "-" + RandomUtils.nextInt(1000, 9999);
	}

	private Order createDeliveryForKey(Order order, Map<String, List<LineItem>> splittedLineItemMap, String key) {
		final List<LineItem> lineItems = splittedLineItemMap.get(key);
		LOG.info("processing order {} for {}:{}", order.getId(), key, lineItems);
		String carrier = key;
		String provider = key;
		if (StringUtils.contains(key, SPLIT)) {
			final String[] splitted = key.split(SPLIT);
			provider = splitted[0];
			carrier = splitted[1];
		}
		List<DeliveryItem> deliveryItems = new ArrayList<>();
		for (LineItem lineItem : lineItems) {
			deliveryItems.add(DeliveryItem.of(lineItem));
		}

		final TrackingData trackingData = TrackingData.of().withCarrier(carrier).withProvider(provider)
				.withProviderTransaction(order.getId());

		final ParcelDraft parcelDraft = ParcelDraftBuilder.of().items(deliveryItems).trackingData(trackingData).build();

		Address shippingAddress = order.getShippingAddress();

		final AddDelivery action = AddDelivery.of(deliveryItems, Arrays.asList(parcelDraft))
				.withAddress(shippingAddress);

		final List<UpdateActionImpl<Order>> actions = Arrays.asList(action);

		final OrderUpdateCommand orderUpdateCommand = OrderUpdateCommand.of(order, actions);
		return getClient().executeBlocking(orderUpdateCommand);
	}
	
	private BlockingSphereClient getClient() {
		return stateQueryService.getClient(); 
	}

	public StateQueryService getStateQueryService() {
		return stateQueryService;
	}

	@Required
	public void setStateQueryService(StateQueryService stateQueryService) {
		this.stateQueryService = stateQueryService;
	}
}

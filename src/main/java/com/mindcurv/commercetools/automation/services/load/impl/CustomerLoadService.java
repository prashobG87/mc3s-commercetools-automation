package com.mindcurv.commercetools.automation.services.load.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.fasterxml.jackson.databind.JsonNode;
import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.helper.DraftHelper;
import com.mindcurv.commercetools.automation.integration.SyncStatistics;
import com.mindcurv.commercetools.automation.services.load.AbstractLoadService;
import com.mindcurv.commercetools.automation.services.query.impl.CustomerGroupQueryService;
import com.mindcurv.commercetools.automation.services.query.impl.CustomerQueryService;

import io.sphere.sdk.commands.UpdateAction;
import io.sphere.sdk.customergroups.CustomerGroup;
import io.sphere.sdk.customers.Customer;
import io.sphere.sdk.customers.CustomerDraft;
import io.sphere.sdk.customers.commands.CustomerCreateCommand;
import io.sphere.sdk.customers.commands.CustomerUpdateCommand;
import io.sphere.sdk.customers.commands.updateactions.AddAddress;
import io.sphere.sdk.customers.commands.updateactions.AddBillingAddressId;
import io.sphere.sdk.customers.commands.updateactions.AddShippingAddressId;
import io.sphere.sdk.customers.commands.updateactions.ChangeAddress;
import io.sphere.sdk.customers.commands.updateactions.ChangeEmail;
import io.sphere.sdk.customers.commands.updateactions.SetCompanyName;
import io.sphere.sdk.customers.commands.updateactions.SetCustomType;
import io.sphere.sdk.customers.commands.updateactions.SetCustomerGroup;
import io.sphere.sdk.customers.commands.updateactions.SetCustomerNumber;
import io.sphere.sdk.customers.commands.updateactions.SetDateOfBirth;
import io.sphere.sdk.customers.commands.updateactions.SetDefaultBillingAddress;
import io.sphere.sdk.customers.commands.updateactions.SetDefaultShippingAddress;
import io.sphere.sdk.customers.commands.updateactions.SetExternalId;
import io.sphere.sdk.customers.commands.updateactions.SetFirstName;
import io.sphere.sdk.customers.commands.updateactions.SetKey;
import io.sphere.sdk.customers.commands.updateactions.SetLastName;
import io.sphere.sdk.customers.commands.updateactions.SetLocale;
import io.sphere.sdk.customers.commands.updateactions.SetMiddleName;
import io.sphere.sdk.customers.commands.updateactions.SetSalutation;
import io.sphere.sdk.customers.commands.updateactions.SetTitle;
import io.sphere.sdk.customers.commands.updateactions.SetVatId;
import io.sphere.sdk.models.Address;
import io.sphere.sdk.models.Reference;
import io.sphere.sdk.models.ResourceIdentifier;
import io.sphere.sdk.types.Type;

public class CustomerLoadService extends AbstractLoadService<CustomerDraft> {

	private final static Logger LOG = LoggerFactory.getLogger(CustomerLoadService.class);

	private CustomerQueryService customerQueryService;
	private CustomerGroupQueryService customergroupQueryService;

	public CustomerLoadService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	@Override
	protected void init() {
		super.init();

		setTypeParameterClass(CustomerDraft.class);

		final CommercetoolsProject commercetoolsProjectBean = getCommercetoolsProject();
		if (commercetoolsProjectBean != null) {
			customerQueryService = new CustomerQueryService(commercetoolsProjectBean);
			customergroupQueryService = new CustomerGroupQueryService(commercetoolsProjectBean);
		}
	}

	@Override
	public SyncStatistics executeSync(List<CustomerDraft> drafts) {
		final SyncStatistics syncStatistics = new SyncStatistics();

		if (validForSync(drafts)) {
			for (final CustomerDraft draft : drafts) {
				syncStatistics.incrementProcessed();
				try {

					final List<UpdateAction<Customer>> updateActions = new ArrayList<>();

					Customer customer = customerQueryService.getByCustomerNumber(draft.getCustomerNumber());

					if (customer == null) {
						customer = getClient().executeBlocking(CustomerCreateCommand.of(draft)).getCustomer();
						LOG.info("Created customer {}", customer.getId());
						List<Address> addresses = customer.getAddresses();
						if (!addresses.isEmpty()) {
							final Address address = addresses.get(0);
							updateActions.add(SetDefaultBillingAddress.ofAddress(address));
							updateActions.add(SetDefaultShippingAddress.ofAddress(address));
							updateActions.add(AddBillingAddressId.of(address.getId()));
							updateActions.add(AddShippingAddressId.of(address.getId()));
						}
						syncStatistics.incrementCreated();
					} else {

						if (DraftHelper.checkForUpdate(customer.getKey(), draft.getKey())) {
							updateActions.add(SetKey.of(draft.getKey()));
						}

						if (DraftHelper.checkForUpdate(customer.getEmail(), draft.getEmail())) {
							updateActions.add(ChangeEmail.of(draft.getEmail()));
						}

						if (DraftHelper.checkForUpdate(customer.getName(), draft.getName())) {
							updateActions
									.add(io.sphere.sdk.customers.commands.updateactions.ChangeName.of(draft.getName()));
						}

						if (DraftHelper.checkForUpdate(customer.getCompanyName(), draft.getCompanyName())) {
							updateActions.add(SetCompanyName.of(draft.getCompanyName()));
						}
						if (DraftHelper.checkForUpdate(customer.getCustomerGroup(), draft.getCustomerGroup())) {
							CustomerGroup customerGroup = customergroupQueryService
									.getByKey(draft.getCustomerGroup().getKey());
							if (customerGroup == null) {
								LOG.warn("No customer group found for {}", draft.getCustomerGroup());
							} else {
								updateActions.add(SetCustomerGroup.of(customerGroup));
							}
						}
						if (DraftHelper.checkForUpdate(customer.getCustomerNumber(), draft.getCustomerNumber())) {
							updateActions.add(SetCustomerNumber.of(draft.getCustomerNumber()));
						}
						if (DraftHelper.checkForUpdate(customer.getExternalId(), draft.getExternalId())) {
							updateActions.add(SetExternalId.of(draft.getExternalId()));
						}
						if (DraftHelper.checkForUpdate(customer.getFirstName(), draft.getFirstName())) {
							updateActions.add(SetFirstName.of(draft.getFirstName()));
						}
						if (DraftHelper.checkForUpdate(customer.getMiddleName(), draft.getMiddleName())) {
							updateActions.add(SetMiddleName.of(draft.getMiddleName()));
						}
						if (DraftHelper.checkForUpdate(customer.getLastName(), draft.getLastName())) {
							updateActions.add(SetLastName.of(draft.getFirstName()));
						}
						if (DraftHelper.checkForUpdate(customer.getTitle(), draft.getTitle())) {
							updateActions.add(SetTitle.of(draft.getTitle()));
						}
						if (DraftHelper.checkForUpdate(customer.getSalutation(), draft.getSalutation())) {
							updateActions.add(SetSalutation.of(draft.getSalutation()));
						}
						if (DraftHelper.checkForUpdate(customer.getVatId(), draft.getVatId())) {
							updateActions.add(SetVatId.of(draft.getVatId()));
						}
						if (DraftHelper.checkForUpdate(customer.getLocale(), draft.getLocale())) {
							updateActions.add(SetLocale.of(draft.getLocale()));
						}
						if (DraftHelper.checkForUpdate(customer.getDateOfBirth(), draft.getDateOfBirth())) {
							updateActions.add(SetDateOfBirth.of(draft.getDateOfBirth()));
						}
						final Reference<Type> customerCustomType = customer.getCustom() == null ? null
								: customer.getCustom().getType();

						if (draft.getCustom() != null && draft.getCustom().getType() != null) {
							final ResourceIdentifier<Type> draftCustomType = draft.getCustom().getType();
							if (customerCustomType != null && !customerCustomType.equals(draftCustomType)) {
								LOG.warn("Different custom type already set customer: {} / draft: {} .",
										customerCustomType, draftCustomType);
							} else {
								if (customerCustomType == null) {
									updateActions.add(SetCustomType.ofTypeKeyAndJson(draftCustomType.getKey(), null));
								}
								for (Entry<String, JsonNode> field : draft.getCustom().getFields().entrySet()) {
									updateActions.add(io.sphere.sdk.customers.commands.updateactions.SetCustomField
											.ofJson(field.getKey(), field.getValue()));
								}
							}
						}
						final Address address = getCustomerDefaultAddress(customer);
						final Address draftAddress = getDefaultAddressFromDraft(draft);
						
						if (address == null) {
							updateActions.add(AddAddress.of(draftAddress));
						} else {
							updateActions.add(ChangeAddress.of(address.getId(), draftAddress));
						}
						if (!updateActions.isEmpty()) {
							syncStatistics.incrementUpdated();
						}
					}

					if (updateActions.isEmpty()) {
						LOG.info("no additonal updqte for customer required {}", customer.getId());
					} else {
						customer = getClient().executeBlocking(CustomerUpdateCommand.of(customer, updateActions));
						LOG.info("Updated customer {}", customer.getId());
					}

				} catch (Exception e) {
					LOG.error("Exception during customer creation", e);
				}

			}
		}
		LOG.info("finished customer sync:{}", syncStatistics);
		return syncStatistics;
	}

	private Address getDefaultAddressFromDraft(final CustomerDraft draft) {
		return draft.getAddresses().isEmpty() ? null
				: draft.getAddresses().get(0);
	}

	private Address getCustomerDefaultAddress(Customer customer) {
		// TODO Default address?
		final Address address = customer.getAddresses().isEmpty() ? null
				: customer.getAddresses().get(0);
		return address;
	}

	@Required
	public void setCustomerQueryService(CustomerQueryService customerQueryService) {
		this.customerQueryService = customerQueryService;
	}

	@Required
	public void setCustomergroupQueryService(CustomerGroupQueryService customergroupQueryService) {
		this.customergroupQueryService = customergroupQueryService;
	}
}

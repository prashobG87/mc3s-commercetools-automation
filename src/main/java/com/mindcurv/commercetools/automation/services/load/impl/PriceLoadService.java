package com.mindcurv.commercetools.automation.services.load.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.integration.SyncStatistics;
import com.mindcurv.commercetools.automation.pojo.catalog.products.PricePojo;
import com.mindcurv.commercetools.automation.services.PublicationService;
import com.mindcurv.commercetools.automation.services.load.AbstractLoadService;
import com.mindcurv.commercetools.automation.services.query.impl.ChannelQueryService;
import com.mindcurv.commercetools.automation.services.query.impl.CustomerGroupQueryService;
import com.mindcurv.commercetools.automation.services.query.impl.ProductQueryService;

import io.sphere.sdk.channels.Channel;
import io.sphere.sdk.customergroups.CustomerGroup;
import io.sphere.sdk.json.SphereJsonUtils;
import io.sphere.sdk.models.Reference;
import io.sphere.sdk.products.Price;
import io.sphere.sdk.products.PriceDraft;
import io.sphere.sdk.products.PriceDraftBuilder;
import io.sphere.sdk.products.Product;
import io.sphere.sdk.products.commands.ProductUpdateCommand;
import io.sphere.sdk.products.commands.updateactions.AddPrice;
import io.sphere.sdk.products.commands.updateactions.ChangePrice;

public class PriceLoadService extends AbstractLoadService<PriceDraft> {

	private final static Logger LOG = LoggerFactory.getLogger(PriceLoadService.class);

	private ChannelQueryService channelQueryService;
	private ProductQueryService productQueryService;
	private PublicationService publicationService;
	private CustomerGroupQueryService customerGroupQueryService;

	public PriceLoadService(final CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	@Override
	protected void init() {
		super.init();

		setTypeParameterClass(PriceDraft.class);

		final CommercetoolsProject commercetoolsProjectBean = getCommercetoolsProject();
		if (commercetoolsProjectBean != null) {
			if (productQueryService == null) {
				productQueryService = new ProductQueryService(commercetoolsProjectBean);
			}
			if (publicationService == null) {
				publicationService = new PublicationService(commercetoolsProjectBean);
			}
			if (channelQueryService == null) {
				channelQueryService = new ChannelQueryService(commercetoolsProjectBean);
			}
			if (customerGroupQueryService == null) {
				customerGroupQueryService = new CustomerGroupQueryService(commercetoolsProjectBean);
			}
		}
	}

	@Override
	public SyncStatistics syncStrings(List<String> input) {
		final Map<String, List<PriceDraft>> draftMap = new HashMap<>();
		for (String draftString : input) {
			final PricePojo pojo = SphereJsonUtils.readObject(draftString, PricePojo.class);
			final String sku = pojo.getSku();
			List<PriceDraft> drafts = draftMap.get(sku);
			if (drafts == null) {
				drafts = new ArrayList<>();
				draftMap.put(sku, drafts);
			}
			drafts.add(pojo.toDraft());
		}
		return executeSync(draftMap);
	}

	@Override
	public SyncStatistics executeSync(List<PriceDraft> drafts) {
		throw new UnsupportedOperationException("Draft sync needs a sku, not implemented");
	}

	public SyncStatistics executeSync(Map<String, List<PriceDraft>> draftMap) {
		final SyncStatistics syncStatistics = new SyncStatistics();
		for (Map.Entry<String, List<PriceDraft>> entry : draftMap.entrySet()) {
			syncStatistics.incrementProcessed();
			final String key = entry.getKey();
			Product product = productQueryService.getBySku(key);
			if (product != null) {
				final Long version = product.getVersion();
				product = sync(product, entry.getValue());
				if (!version.equals(product.getVersion())) {
					syncStatistics.incrementUpdated();
				}
			} else {
				syncStatistics.incrementFailed();
				LOG.error("no Product found with SKU {}", key);
			}
		}
		LOG.info("finished sync:{}", syncStatistics);
		return syncStatistics;
	}

	public Product sync(Product product, final List<PriceDraft> drafts) {

		LOG.debug("Update {} price(s) for '{}'", drafts.size(), product.getKey());
		final Map<String, Price> existingPriceMap = new HashMap<>();

		List<Price> prices = product.getMasterData().getStaged().getMasterVariant().getPrices();
		for (Price price : prices) {
			final String generatedUniqueKey = generateUniqueKey(price);
			LOG.debug("Product {}, existing price [{}]: {} country {} channel: {}", product.getKey(),
					generatedUniqueKey, price.getValue(), price.getCountry(), price.getChannel());
			existingPriceMap.put(generatedUniqueKey, price);

		}
		final Map<String, CustomerGroup> groupMap = customerGroupQueryService.prepareGroupMap();

		for (PriceDraft draft : drafts) {
			final PriceDraftBuilder builder = PriceDraftBuilder.of(draft);
			final Reference<Channel> draftChannel = draft.getChannel();
			if (draft.getCustomerGroup() != null) {

				final String groupKey = draft.getCustomerGroup().getId();
				final CustomerGroup group = groupMap.get(groupKey);
				if (group == null) {
					LOG.warn("no matching customer group found for {}", groupKey);
				} else {
					builder.customerGroup(group);
				}
			}

			if (draftChannel != null) {
				final Channel channel = channelQueryService.getByKey(draftChannel.getId());
				if (channel == null) {
					LOG.error("no channel available for {}", draftChannel.getId());
					continue;
				}
				LOG.debug("channel for key {}: {}", draftChannel.getId(), channel.getId());
				builder.channel(channel);
			}

			final PriceDraft finalPriceDraft = builder.build();
			final String draftUniqueKey = PriceLoadService.generateUniqueKey(finalPriceDraft);
			final Price price = existingPriceMap.get(draftUniqueKey);
			LOG.debug("PriceDraft to import  {} - {}", draftUniqueKey, finalPriceDraft);
			if (price == null) {
				LOG.debug("creating price {}", draftUniqueKey);
				product = createPrice(product, finalPriceDraft);
			} else {
				LOG.debug("updating price {}", draftUniqueKey);
				product = updatePrice(product, price, finalPriceDraft);
			}

		}
		try {
			return publicationService.publishProduct(product);
		} catch (Exception e) {
			LOG.info("Publication of prices for {} failed", product.getKey());
		}
		return product;
	}

	public static String generateUniqueKey(PriceDraft draft) {
		String channel = draft.getChannel() == null ? "" : draft.getChannel().getId();
		String country = draft.getCountry() == null ? "" : draft.getCountry().getAlpha2();
		String currencyCode = draft.getValue().getCurrency().getCurrencyCode();
		String customerGroup = draft.getCustomerGroup() == null ? "" : draft.getCustomerGroup().getId();
		String validFrom = draft.getValidFrom() == null ? "" : draft.getValidFrom().toString();
		String validUntil = draft.getValidUntil() == null ? "" : draft.getValidUntil().toString();
		return generateUniquePriceKey(channel, country, currencyCode, customerGroup, validFrom, validUntil);
	}

	public static String generateUniqueKey(Price price) {
		PriceDraft priceDraft = PriceDraftBuilder.of(price).build();
		return generateUniqueKey(priceDraft);

	}

	public static String generateUniquePriceKey(String channel, String country, String currencyCode,
			String customerGroup, String validFrom, String validUntil) {
		final StringBuffer stringBuffer = new StringBuffer();
		if (StringUtils.isNotBlank(currencyCode)) {
			stringBuffer.append(currencyCode);
		}
		if (StringUtils.isNotBlank(country)) {
			stringBuffer.append("_co").append(country);
		}
		if (channel != null) {
			stringBuffer.append("_ch").append(channel);
		}
		if (customerGroup != null) {
			stringBuffer.append("_cf").append(customerGroup);
		}
		if (validFrom != null) {
			stringBuffer.append("_fd").append(validFrom);
		}
		if (validUntil != null) {
			stringBuffer.append("_ud").append(validUntil);
		}
		return stringBuffer.toString();
	}

	private Product createPrice(final Product product, PriceDraft finalDraft) {

		try {
			final AddPrice action = AddPrice.of(1, finalDraft);
			final ProductUpdateCommand command = ProductUpdateCommand.of(product, action);
			return getClient().executeBlocking(command);
		} catch (Exception e) {
			LOG.error("Exception during price creation", e);
		}
		return product;
	}

	private Product updatePrice(final Product product, final Price oldPrice, final PriceDraft finalDraft) {

		try {
			final ChangePrice action = ChangePrice.of(oldPrice, finalDraft);
			final ProductUpdateCommand command = ProductUpdateCommand.of(product, action);
			return getClient().executeBlocking(command);
		} catch (Exception e) {
			LOG.error("Exception during price update", e);
		}
		return product;
	}

	@Required
	public void setChannelQueryService(ChannelQueryService channelQueryService) {
		this.channelQueryService = channelQueryService;
	}

	@Required
	public void setProductQueryService(ProductQueryService productQueryService) {
		this.productQueryService = productQueryService;
	}

	@Required
	public void setPublicationService(PublicationService publicationService) {
		this.publicationService = publicationService;
	}

	@Required
	public void setCustomerGroupQueryService(CustomerGroupQueryService customerGroupQueryService) {
		this.customerGroupQueryService = customerGroupQueryService;
	}
}

package com.mindcurv.commercetools.automation.services.setup.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.annotation.Configuration;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.helper.DraftHelper;
import com.mindcurv.commercetools.automation.helper.spreadsheet.ValueHelper;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;
import com.mindcurv.commercetools.automation.pojo.customobject.products.TableEntryPojo;
import com.mindcurv.commercetools.automation.pojo.customobject.products.TablePojo;
import com.mindcurv.commercetools.automation.pojo.types.product.TypeAttribute;
import com.mindcurv.commercetools.automation.services.load.AbstractCustomObjectLoadService;
import com.mindcurv.commercetools.automation.services.load.impl.customobject.CustomObjectLoadTableService;
import com.mindcurv.commercetools.automation.services.setup.SetupService;

import io.sphere.sdk.customobjects.CustomObject;

@Configuration
public class CustomObjectSetupService extends SetupService {

	private final static Logger LOG = LoggerFactory.getLogger(CustomObjectSetupService.class);

	private Map<String, AbstractCustomObjectLoadService<?>> customObjectLoadServices;

	public CustomObjectSetupService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	@Override
	protected void init() {
		super.init();

		if (customObjectLoadServices == null) {
			customObjectLoadServices = new HashMap<>();
			customObjectLoadServices.put("customObjectLoad" + TablePojo.class.getSimpleName() + "Service",
					new CustomObjectLoadTableService(getCommercetoolsProject()));
		}
	}

	@Override
	public boolean cleanupData() {
		return true;
	}

	@Override
	protected boolean setupValues(List<Map<String, Object>> valueLines,
			Map<String, List<TypeAttribute>> attributesMap) {
		if (valueLines == null) {
			LOG.error("no lines provided");
		} else {
			try {
				for (Map<String, Object> value : valueLines) {
					final String clazz = ValueHelper.getValueAsString(CsvConstants.CustomObject.CLASS_KEY, value);
					if (StringUtils.isBlank(clazz)) {
						LOG.debug("Skipping line {}", value);
						continue;
					}
					Class<?> classFromSheet = Class.forName(clazz);
					final String mapKey = "customObjectLoad" + classFromSheet.getSimpleName() + "Service";
					final AbstractCustomObjectLoadService<?> service = customObjectLoadServices.get(mapKey);
					if (service == null) {
						LOG.warn("Unspported class '{}' / {}", clazz, mapKey);
						LOG.warn("valid keys are '{}'", customObjectLoadServices.keySet());
						continue;
					}
					
					final String container = ValueHelper.getValueAsString(CsvConstants.CustomObject.CONTAINER_KEY,
							value);
					final String containerKey = DraftHelper.generateContainerKey(
							ValueHelper.getValueAsString(CsvConstants.CustomObject.KEY_KEY, value));
					
					if (TablePojo.class.getName().equals(clazz)) {
						LOG.info("Processing {} {} {}", container, containerKey, TablePojo.class.getSimpleName() );
						final CustomObject<?> object = service.syncCustomObject(container, containerKey,
								processTablePojo(value));
						LOG.info("Custom Object: {}", object);
					} else {
						LOG.warn("Unspported class '{}'", clazz);
					}
				}
				return true;
			} catch (Exception e) {
				LOG.error("Exception", e);
			}
		}
		return false;
	}

	private TablePojo processTablePojo(Map<String, Object> value) {
		final String payload = ValueHelper.getValueAsString(CsvConstants.CustomObject.VALUE_KEY, value);

		LOG.debug("Payload {}", payload);
		final List<TableEntryPojo> list = new ArrayList<>(); // .asList(tableEntries);
		try {
			final BufferedReader in = new BufferedReader(new StringReader(payload));
			String line;

			while ((line = in.readLine()) != null) {
				LOG.debug("Line {}", line);
				list.add(new TableEntryPojo().withLineValue(line));
			}
		} catch (IOException e) {
			LOG.error("IOException during parsing", e);
		}

		return new TablePojo().withContent(list);
	}

	@Override
	public String getSheetname() {
		return CsvConstants.CustomObject.SHEET;
	}

	@Required
	public void setCustomObjectLoadServices(Map<String, AbstractCustomObjectLoadService<?>> customObjectLoadServices) {
		this.customObjectLoadServices = customObjectLoadServices;
	}

}

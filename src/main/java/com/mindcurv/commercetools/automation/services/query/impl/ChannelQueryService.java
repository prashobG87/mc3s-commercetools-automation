package com.mindcurv.commercetools.automation.services.query.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.services.query.QueryService;

import io.sphere.sdk.channels.Channel;
import io.sphere.sdk.channels.queries.ChannelQueryBuilder;
import io.sphere.sdk.channels.queries.ChannelQueryModel;
import io.sphere.sdk.products.ProductProjection;
import io.sphere.sdk.queries.PagedQueryResult;
import io.sphere.sdk.queries.QueryPredicate;

/**
 * This class provides search operations for {@link ProductProjection}s.
 */
public class ChannelQueryService extends QueryService<Channel> {

	private final static Logger LOG = LoggerFactory.getLogger(ChannelQueryService.class);

	public ChannelQueryService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	@Override
	public List<Channel> getQueryResult(final Map<String, Object> params) {

		final List<Channel> result = new ArrayList<>();
		Long limit = params.containsKey(PARAM_LIMIT) ? (Long) params.get(PARAM_LIMIT) : DEFAULT_LIMIT;
		final ChannelQueryBuilder queryBuilder = ChannelQueryBuilder.of().fetchTotal(true);
		queryBuilder.limit(limit);

		final List<QueryPredicate<Channel>> predicates = preparePredicates(params);
		if (!predicates.isEmpty()) {
			queryBuilder.predicates(predicates);
		}

		long page = 1;
		boolean breakLoop = false;
		while (!breakLoop) {
			long offset = (page - 1) * limit;
			queryBuilder.offset(offset);
			final PagedQueryResult<Channel> pagedQueryResult = getClient().executeBlocking(queryBuilder.build());
			final List<Channel> pageResult = pagedQueryResult.getResults();
			if (pagedQueryResult.getTotal() == 0) {
				breakLoop = true;
			} else {
				result.addAll(pageResult);
				LOG.debug("found {} Channels on page {} of {} pages [{} entries]", pageResult.size(), page,
						pagedQueryResult.getTotalPages(), pagedQueryResult.getTotal());
				if (page == pagedQueryResult.getTotalPages()) {
					breakLoop = true;
				}
				page++;
			}
		}
		LOG.debug("returning total: {} Carts", result.size());
		return result;
	}

	@Override
	public List<QueryPredicate<Channel>> preparePredicates(final Map<String, Object> params) {
		final List<QueryPredicate<Channel>> predicates = new ArrayList<>();
		if (params.containsKey(PARAM_ID)) {
			final String channelId = (String) params.get(PARAM_ID);
			predicates.add(ChannelQueryModel.of().id().is(channelId));
		}
		if (params.containsKey(PARAM_KEY)) {
			final String key = (String) params.get(PARAM_KEY);
			predicates.add(ChannelQueryModel.of().key().is(key));
		}
		return predicates;
	}

	@Override
	protected boolean supportsByKeyQuery() {
		return true;
	}
}

package com.mindcurv.commercetools.automation.services.query.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.services.query.QueryService;

import io.sphere.sdk.categories.Category;
import io.sphere.sdk.categories.queries.CategoryQueryBuilder;
import io.sphere.sdk.categories.queries.CategoryQueryModel;
import io.sphere.sdk.products.ProductProjection;
import io.sphere.sdk.queries.PagedQueryResult;
import io.sphere.sdk.queries.QueryPredicate;

/**
 * This class provides search operations for {@link ProductProjection}s.
 */
public class CategoryQueryService extends QueryService<Category> {

	private final static Logger LOG = LoggerFactory.getLogger(CategoryQueryService.class);

	public static final String PARAM_PARENT_PRESENT = "parentPresent";

	public CategoryQueryService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	@Override
	public List<Category> getQueryResult(final Map<String, Object> params) {

		final List<Category> result = new ArrayList<>();
		Long limit = params.containsKey(PARAM_LIMIT) ? (Long) params.get(PARAM_LIMIT) : DEFAULT_LIMIT;
		final CategoryQueryBuilder queryBuilder = CategoryQueryBuilder.of().fetchTotal(true);
		queryBuilder.limit(limit);

		final List<QueryPredicate<Category>> predicates = preparePredicates(params);
		if (!predicates.isEmpty()) {
			queryBuilder.predicates(predicates);
		}

		long page = 1;
		boolean breakLoop = false;
		while (!breakLoop) {
			long offset = (page - 1) * limit;
			queryBuilder.offset(offset);
			final PagedQueryResult<Category> pagedQueryResult = getClient().executeBlocking(queryBuilder.build());
			final List<Category> pageResult = pagedQueryResult.getResults();

			if (pagedQueryResult.getTotal() == 0) {
				breakLoop = true;
			} else {
				result.addAll(pageResult);
				LOG.info("found {} Carts on page {} of {} pages [{} entries]", pageResult.size(), page,
						pagedQueryResult.getTotalPages(), pagedQueryResult.getTotal());
				if (page == pagedQueryResult.getTotalPages()) {
					breakLoop = true;
				}
				page++;
			}
		}
		LOG.debug("returning total: {} Categories", result.size());
		return result;
	}

	@Override
	public List<QueryPredicate<Category>> preparePredicates(final Map<String, Object> params) {
		final List<QueryPredicate<Category>> predicates = new ArrayList<>();
		if (params.containsKey(PARAM_ID)) {
			final String id = (String) params.get(PARAM_ID);
			predicates.add(CategoryQueryModel.of().id().is(id));
		}
		if (params.containsKey(PARAM_KEY)) {
			final String key = (String) params.get(PARAM_KEY);
			predicates.add(CategoryQueryModel.of().key().is(key));
		}
		if (params.containsKey(PARAM_PARENT_PRESENT)) {
			final Boolean parentPresent = (Boolean) params.get(PARAM_PARENT_PRESENT);
			final QueryPredicate<Category> predicate = parentPresent.booleanValue()
					? CategoryQueryModel.of().parent().isPresent() : CategoryQueryModel.of().parent().isNotPresent();
			predicates.add(predicate);
		}
		return predicates;
	}

	public List<Category> getAllRootCategories() {
		final Map<String, Object> params = new HashMap<>();
		params.put(PARAM_PARENT_PRESENT, Boolean.FALSE);
		final List<Category> result = getQueryResult(params);
		LOG.debug("Found {} root categories", result);
		return result;
	}

	@Override
	protected boolean supportsByKeyQuery() {
		return true;
	}
}

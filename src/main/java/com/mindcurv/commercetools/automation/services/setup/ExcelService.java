package com.mindcurv.commercetools.automation.services.setup;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.Constants;
import com.mindcurv.commercetools.automation.pojo.types.product.TypeAttribute;
import com.mindcurv.commercetools.automation.services.setup.impl.TypeSetupService;

public class ExcelService {
	private final static Logger LOG = LoggerFactory.getLogger(ExcelService.class);

	private CommercetoolsProject commercetoolsProject;

	private List<SetupService> setupServices;

	private File excelFile;

	public ExcelService(CommercetoolsProject commercetoolsProject) {
		this.commercetoolsProject = commercetoolsProject;
	}

	public List<SetupService> getSetupServices() {
		return setupServices;
	}

	@Required
	public void setSetupServices(List<SetupService> setupServices) {
		this.setupServices = setupServices;
	}

	public File getExcelFile() {
		return excelFile;
	}

	@Required
	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public CommercetoolsProject getCommercetoolsProject() {
		return commercetoolsProject;
	}

	public boolean executeSync() {
		try {

			boolean success = false;
			final File file = getExcelFile();
			try {

				LOG.info("processing setup excel file {}", file);

				final FileInputStream excelFile = new FileInputStream(file);
				final Workbook workbook = new XSSFWorkbook(excelFile);

				final Date startDate = new Date();
				Map<String, List<TypeAttribute>> attributesMap = Collections.emptyMap();

				for (SetupService setupService : setupServices) {
					LOG.info(Constants.MSG_SPACER);
					if (setupService instanceof TypeSetupService) {
						attributesMap = ((TypeSetupService) setupService).setup(workbook);
					}
					setupService.setup(workbook, attributesMap);
				}

				final Date endDate = new Date();
				LOG.info("Setup duration: {}s", (endDate.getTime() - startDate.getTime()) / 1000);
				LOG.info(Constants.MSG_SPACER);

				workbook.close();

			} catch (FileNotFoundException e) {
				LOG.error("FileNotFoundException for file " + file.getName(), e);
			} catch (IOException e) {
				LOG.error("IOException for file " + file.getName(), e);
			} catch (Exception e) {
				LOG.error("Exception for file " + file.getName(), e);
			}
			if (success) {
				LOG.info("processed {}", file.getAbsolutePath());
			} else {
				LOG.error("finished setup with errors for file '" + file.getAbsolutePath() + "'");
			}

			return true;

		} catch (Exception e) {
			LOG.error("Exception", e);
		}
		return false;
	}

}

package com.mindcurv.commercetools.automation.services.setup.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.google.api.services.sheets.v4.Sheets.Spreadsheets;
import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.helper.spreadsheet.ExcelHelper;
import com.mindcurv.commercetools.automation.helper.spreadsheet.GDriveHelper;
import com.mindcurv.commercetools.automation.pojo.BasePojo;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;
import com.mindcurv.commercetools.automation.pojo.organization.AddressPojo;
import com.mindcurv.commercetools.automation.pojo.organization.ChannelPojo;
import com.mindcurv.commercetools.automation.pojo.types.product.TypeAttribute;
import com.mindcurv.commercetools.automation.services.load.impl.ChannelLoadService;
import com.mindcurv.commercetools.automation.services.query.impl.ChannelQueryService;
import com.mindcurv.commercetools.automation.services.setup.SetupService;

import io.sphere.sdk.channels.Channel;
import io.sphere.sdk.channels.ChannelDraft;
import io.sphere.sdk.channels.commands.ChannelDeleteCommand;
import io.sphere.sdk.channels.commands.ChannelUpdateCommand;
import io.sphere.sdk.channels.commands.updateactions.SetCustomField;
import io.sphere.sdk.commands.UpdateAction;
import io.sphere.sdk.customers.Customer;

/**
 * This class provides operations to work with {@link Customer}s.
 */
public class ChannelSetupService extends SetupService {

	private final static Logger LOG = LoggerFactory.getLogger(ChannelSetupService.class);
	private ChannelLoadService channelLoadService;
	private ChannelQueryService channelQueryService;

	public ChannelSetupService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	@Override
	protected void init() {
		super.init();
		final CommercetoolsProject commercetoolsProjectBean = getCommercetoolsProject();
		if (commercetoolsProjectBean != null) {
			if (channelQueryService == null) {
				channelQueryService = new ChannelQueryService(commercetoolsProjectBean);
			}
			if (channelLoadService == null) {
				channelLoadService = new ChannelLoadService(commercetoolsProjectBean);
			}
		}
	}

	public boolean cleanupData() {
		LOG.info("cleanup Channel items");
		final List<Channel> allChannels = channelQueryService.getAllEntries();
		for (Channel channel : allChannels) {
			ChannelDeleteCommand command = ChannelDeleteCommand.of(channel);
			channel = getClient().executeBlocking(command);
			LOG.info("Removed channel {}", channel);
		}
		return true;
	}
	
	@Override
	public boolean setup(String sheetId, Spreadsheets sheets, Map<String, List<TypeAttribute>> attributesMap) {
		setAssociatedPojos(GDriveHelper.prepareAddressPojos(sheets, sheetId, CsvConstants.Address.REFERENCE_TYPE_CHANNEL, getCommercetoolsProject()));
		return super.setup(sheetId, sheets, attributesMap);
	}
	
	@Override
	public boolean setup(Workbook workbook, Map<String, List<TypeAttribute>> attributesMap) {
		setAssociatedPojos(ExcelHelper.prepareAddressPojos(workbook, CsvConstants.Address.REFERENCE_TYPE_CHANNEL, getCommercetoolsProject()));
		return super.setup(workbook, attributesMap);
	}
	

	@Override
	protected boolean setupValues(List<Map<String, Object>> valueLines,
			Map<String, List<TypeAttribute>> attributesList) {
		final List<ChannelDraft> drafts = new ArrayList<>();
		
		final Map<String, List<BasePojo>> associatedPojos = getAssociatedPojos(); 
		
		for (Map<String, Object> value : valueLines) {
			ChannelPojo pojo = new ChannelPojo(value);
			
			processAddress(associatedPojos, pojo);
			
			if (pojo.getCustom() != null) {
				pojo.addAttributes(value, attributesList);
				
			}
			
			drafts.add(pojo.toDraft());
		}
		if (!drafts.isEmpty()) {
			channelLoadService.sync(drafts);
		}
		return true;
	}

	private void processAddress(final Map<String, List<BasePojo>> associatedPojos, ChannelPojo pojo) {
		final String pojoKey = pojo.getKey();
		if ( associatedPojos.containsKey(pojoKey)) {
			final List<BasePojo> addressList = (List<BasePojo>) associatedPojos.get(pojoKey); 
			if ( addressList != null && addressList.size() > 0 ) {
				final AddressPojo addressPojo = (AddressPojo) addressList.get(0); 
				pojo.setAddress(addressPojo);
			}
		}
	}

	@Override
	public String getSheetname() {
		return CsvConstants.Channel.SHEET;
	}

	public Channel updateCustomFields(final String key, final Map<String, Object> customFieldValues) {
		Channel channel = channelQueryService.getByKey(key);
		if (channel != null) {
			final List<UpdateAction<Channel>> updateActions = new ArrayList<>();
			for (Map.Entry<String, Object> entry : customFieldValues.entrySet()) {
				updateActions.add(SetCustomField.ofObject(entry.getKey(), entry.getValue()));
			}
			channel = getClient().executeBlocking(ChannelUpdateCommand.of(channel, updateActions));
			LOG.info("Update field result {}", channel);
		}
		return channel;
	}

	public String replaceChannelIds(final String jsonString, String[] channelKeyArray, String[] channelIdArray) {
		return StringUtils.replaceEach(jsonString, channelKeyArray, channelIdArray);
	}

	@Required
	public void setChannelLoadService(ChannelLoadService channelLoadService) {
		this.channelLoadService = channelLoadService;
	}

	@Required
	public void setChannelQueryService(ChannelQueryService channelQueryService) {
		this.channelQueryService = channelQueryService;
	}

}

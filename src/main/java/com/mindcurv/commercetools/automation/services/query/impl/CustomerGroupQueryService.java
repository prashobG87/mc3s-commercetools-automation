package com.mindcurv.commercetools.automation.services.query.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.services.query.QueryService;

import io.sphere.sdk.customergroups.CustomerGroup;
import io.sphere.sdk.customergroups.queries.CustomerGroupQueryBuilder;
import io.sphere.sdk.customergroups.queries.CustomerGroupQueryModel;
import io.sphere.sdk.queries.PagedQueryResult;
import io.sphere.sdk.queries.QueryPredicate;

public class CustomerGroupQueryService extends QueryService<CustomerGroup> {

	private final static Logger LOG = LoggerFactory.getLogger(CustomerGroupQueryService.class);

	public CustomerGroupQueryService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	@Override
	public List<CustomerGroup> getQueryResult(Map<String, Object> params) {
		final List<CustomerGroup> result = new ArrayList<>();
		Long limit = params.containsKey(PARAM_LIMIT) ? (Long) params.get(PARAM_LIMIT) : DEFAULT_LIMIT;
		final CustomerGroupQueryBuilder queryBuilder = CustomerGroupQueryBuilder.of().fetchTotal(true);
		queryBuilder.limit(limit);

		final List<QueryPredicate<CustomerGroup>> predicates = preparePredicates(params);
		if (!predicates.isEmpty()) {
			queryBuilder.predicates(predicates);
		}

		long page = 1;
		boolean breakLoop = false;
		while (!breakLoop) {
			long offset = (page - 1) * limit;
			queryBuilder.offset(offset);
			final PagedQueryResult<CustomerGroup> pagedQueryResult = getClient().executeBlocking(queryBuilder.build());
			final List<CustomerGroup> pageResult = pagedQueryResult.getResults();
			if (pagedQueryResult.getTotal() == 0) {
				breakLoop = true;
			} else {
				result.addAll(pageResult);
				LOG.debug("found {} CustomerGroups on page {} of {} pages [{} entries]", pageResult.size(), page,
						pagedQueryResult.getTotalPages(), pagedQueryResult.getTotal());
				if (page == pagedQueryResult.getTotalPages()) {
					breakLoop = true;
				}
				page++;
			}
		}
		LOG.debug("returning total: {} CustomerGroups", result.size());
		return result;
	}

	@Override
	public List<QueryPredicate<CustomerGroup>> preparePredicates(Map<String, Object> params) {
		final List<QueryPredicate<CustomerGroup>> predicates = new ArrayList<>();
		if (params.containsKey(PARAM_ID)) {
			final String id = (String) params.get(PARAM_ID);
			predicates.add(CustomerGroupQueryModel.of().id().is(id));
		}
		if (params.containsKey(PARAM_KEY)) {
			final String key = (String) params.get(PARAM_KEY);
			predicates.add(CustomerGroupQueryModel.of().key().is(key));
		}
		return predicates;
	}

	public Map<String, CustomerGroup> prepareGroupMap() {
		final Map<String, CustomerGroup> map = new HashMap<>();
		for (CustomerGroup customerGroup : getAllEntries()) {
			map.put(customerGroup.getKey(), customerGroup);
		}
		return map;
	}

	@Override
	protected boolean supportsByKeyQuery() {
		return true;
	}
}

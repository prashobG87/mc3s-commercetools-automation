package com.mindcurv.commercetools.automation.services.query.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.services.query.QueryService;

import io.sphere.sdk.products.attributes.AttributeDefinition;
import io.sphere.sdk.products.attributes.RichReferenceAttributeType;
import io.sphere.sdk.producttypes.ProductType;
import io.sphere.sdk.producttypes.queries.ProductTypeQueryBuilder;
import io.sphere.sdk.producttypes.queries.ProductTypeQueryModel;
import io.sphere.sdk.queries.PagedQueryResult;
import io.sphere.sdk.queries.QueryPredicate;

public class ProductTypeQueryService extends QueryService<ProductType> {

	private final static Logger LOG = LoggerFactory.getLogger(ProductTypeQueryService.class);

	public ProductTypeQueryService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	@Override
	public List<ProductType> getQueryResult(Map<String, Object> params) {
		final List<ProductType> result = new ArrayList<>();
		Long limit = params.containsKey(PARAM_LIMIT) ? (Long) params.get(PARAM_LIMIT) : DEFAULT_LIMIT;
		final ProductTypeQueryBuilder queryBuilder = ProductTypeQueryBuilder.of().fetchTotal(true);
		queryBuilder.limit(limit);

		final List<QueryPredicate<ProductType>> predicates = preparePredicates(params);
		if (!predicates.isEmpty()) {
			queryBuilder.predicates(predicates);
		}

		long page = 1;
		boolean breakLoop = false;
		while (!breakLoop) {
			long offset = (page - 1) * limit;
			queryBuilder.offset(offset);
			final PagedQueryResult<ProductType> pagedQueryResult = getClient().executeBlocking(queryBuilder.build());
			final List<ProductType> pageResult = pagedQueryResult.getResults();

			if (pagedQueryResult.getTotal() == 0) {
				breakLoop = true;
			} else {
				result.addAll(pageResult);
				LOG.info("found {} ProductTypes on page {} of {} pages [{} entries]", pageResult.size(), page,
						pagedQueryResult.getTotalPages(), pagedQueryResult.getTotal());
				if (page == pagedQueryResult.getTotalPages()) {
					breakLoop = true;
				}
				page++;
			}
		}
		LOG.debug("returning total: {} Products", result.size());
		return result;
	}

	@Override
	public List<QueryPredicate<ProductType>> preparePredicates(Map<String, Object> params) {
		final List<QueryPredicate<ProductType>> predicates = new ArrayList<>();
		if (params.containsKey(PARAM_ID)) {
			final String id = (String) params.get(PARAM_ID);
			predicates.add(ProductTypeQueryModel.of().id().is(id));
		}
		if (params.containsKey(PARAM_KEY)) {
			final String key = (String) params.get(PARAM_KEY);
			predicates.add(ProductTypeQueryModel.of().key().is(key));
		}
		if (params.containsKey(PARAM_KEYLIST)) {
			@SuppressWarnings("unchecked")
			final List<String> keyList = (List<String>) params.get(PARAM_KEYLIST);
			if (!keyList.isEmpty()) {
				if (!keyList.contains("*")) {
					predicates.add(ProductTypeQueryModel.of().key().isIn(keyList));
				}
			}
		}
		return predicates;
	}

	@Override
	protected boolean supportsByKeyQuery() {
		return true;
	}

	public List<AttributeDefinition> getAttributeListForProductType(final ProductType productType) {
		final List<AttributeDefinition> list = new ArrayList<>();
		if (productType != null) {
			for (AttributeDefinition attributeDefinition : productType.getAttributes()) {
				LOG.debug("attribute {} - {}", attributeDefinition.getName(), attributeDefinition.getAttributeType());
				list.add(attributeDefinition);
			}
		}
		return list;
	}

	public List<AttributeDefinition> getAttributeListForKeyValueDocuments(final ProductType productType) {
		final List<AttributeDefinition> list = new ArrayList<>();
		for (AttributeDefinition attributeDefinition : getAttributeListForProductType(productType)) { 
			if ( attributeDefinition.getAttributeType() instanceof RichReferenceAttributeType) {
				RichReferenceAttributeType<?> richReferenceAttributeType = (RichReferenceAttributeType<?>) attributeDefinition.getAttributeType(); 
				if ("key-value-document".equals(richReferenceAttributeType.getReferenceTypeId())) {
					list.add(attributeDefinition);
				} 
			}
		}
		LOG.debug( "found {} key-value-document attributes for {}", list.size(), productType.getKey());
		return list;
	}
	
	public List<AttributeDefinition> getAttributeListForProductType(final ProductType productType,
			final Class<?> clazz) {
		final List<AttributeDefinition> list = new ArrayList<>();
		for (AttributeDefinition attributeDefinition : getAttributeListForProductType(productType)) {
			if (attributeDefinition.getAttributeType().getClass().getSimpleName().equals(clazz.getSimpleName())) {
				list.add(attributeDefinition);
			} else {
				LOG.debug("not matching attributetype {} - {} - {}", attributeDefinition.getName(),
						attributeDefinition.getAttributeType().getClass().getSimpleName(), clazz.getSimpleName());
			}
		}
		LOG.debug( "found {} attributes with type {}", list.size(), clazz);
		return list;
	}
}

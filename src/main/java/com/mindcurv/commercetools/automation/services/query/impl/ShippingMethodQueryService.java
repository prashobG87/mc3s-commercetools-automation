package com.mindcurv.commercetools.automation.services.query.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.services.query.QueryService;

import io.sphere.sdk.queries.PagedQueryResult;
import io.sphere.sdk.queries.QueryPredicate;
import io.sphere.sdk.shippingmethods.ShippingMethod;
import io.sphere.sdk.shippingmethods.queries.ShippingMethodQueryBuilder;
import io.sphere.sdk.shippingmethods.queries.ShippingMethodQueryModel;

public class ShippingMethodQueryService extends QueryService<ShippingMethod> {

	private final static Logger LOG = LoggerFactory.getLogger(ShippingMethodQueryService.class);

	public static final String PARAM_NAME = "name";
	public static final String PARAM_DEFAULT = "default";

	public ShippingMethodQueryService(CommercetoolsProject commercetoolsProject ) {
		super(commercetoolsProject);
	}

	@Override
	public List<ShippingMethod> getQueryResult(Map<String, Object> params) {
		final List<ShippingMethod> result = new ArrayList<>();
		Long limit = params.containsKey(PARAM_LIMIT) ? (Long) params.get(PARAM_LIMIT) : DEFAULT_LIMIT;
		final ShippingMethodQueryBuilder queryBuilder = ShippingMethodQueryBuilder.of().fetchTotal(true);
		queryBuilder.limit(limit);

		final List<QueryPredicate<ShippingMethod>> predicates = preparePredicates(params);
		if (!predicates.isEmpty()) {
			queryBuilder.predicates(predicates);
		}

		long page = 1;
		boolean breakLoop = false;
		while (!breakLoop) {
			long offset = (page - 1) * limit;
			queryBuilder.offset(offset);
			final PagedQueryResult<ShippingMethod> pagedQueryResult = getClient().executeBlocking(queryBuilder.build());
			final List<ShippingMethod> pageResult = pagedQueryResult.getResults();
			if (pagedQueryResult.getTotal() == 0) {
				breakLoop = true;
			} else {
				result.addAll(pageResult);
				LOG.debug("found {} ShippingMethods on page {} of {} pages [{} entries]", pageResult.size(), page,
						pagedQueryResult.getTotalPages(), pagedQueryResult.getTotal());
				if (page == pagedQueryResult.getTotalPages()) {
					breakLoop = true;
				}
				page++;
			}
		}
		LOG.debug("returning total: {} ShippingMethods", result.size());
		return result;
	}

	@Override
	public List<QueryPredicate<ShippingMethod>> preparePredicates(final Map<String, Object> params) {
		final List<QueryPredicate<ShippingMethod>> predicates = new ArrayList<>();
		if (params.containsKey(PARAM_ID)) {
			final String id = (String) params.get(PARAM_ID);
			predicates.add(ShippingMethodQueryModel.of().id().is(id));
		}
		if (params.containsKey(PARAM_KEY)) {
			final String key = (String) params.get(PARAM_KEY);
			predicates.add(ShippingMethodQueryModel.of().key().is(key));
		}

		if (params.containsKey(PARAM_NAME)) {
			final String name = (String) params.get(PARAM_NAME);
			predicates.add(ShippingMethodQueryModel.of().name().is(name));
		}

		if (params.containsKey(PARAM_DEFAULT)) {
			final Boolean defaultValue = (Boolean) params.get(PARAM_DEFAULT);
			predicates.add(ShippingMethodQueryModel.of().isDefault().is(defaultValue));
		}
		return predicates;
	}

	public ShippingMethod getByName(String name) {

		final Map<String, Object> params = new HashMap<>();
		params.put(PARAM_NAME, name);
		return getSingleResult(params);
	}

	public ShippingMethod getDefaultShippingMethod() {
		final Map<String, Object> params = new HashMap<>();
		params.put(PARAM_DEFAULT, Boolean.TRUE);
		return getSingleResult(params);
	}

	@Override
	protected boolean supportsByKeyQuery() {
		return true;
	}
}

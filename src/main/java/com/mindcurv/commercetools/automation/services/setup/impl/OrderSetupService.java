package com.mindcurv.commercetools.automation.services.setup.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.pojo.types.product.TypeAttribute;
import com.mindcurv.commercetools.automation.services.query.impl.OrderQueryService;
import com.mindcurv.commercetools.automation.services.setup.SetupService;

import io.sphere.sdk.carts.Cart;
import io.sphere.sdk.commands.UpdateActionImpl;
import io.sphere.sdk.orders.Order;
import io.sphere.sdk.orders.commands.OrderDeleteCommand;
import io.sphere.sdk.orders.commands.OrderFromCartCreateCommand;
import io.sphere.sdk.orders.commands.OrderUpdateCommand;

public class OrderSetupService extends SetupService {

	private final static Logger LOG = LoggerFactory.getLogger(OrderSetupService.class);

	private OrderQueryService orderQueryService;

	public OrderSetupService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	@Override
	protected void init() {
		super.init();
		final CommercetoolsProject commercetoolsProjectBean = getCommercetoolsProject();
		if (commercetoolsProjectBean != null) {
			if ( orderQueryService == null) {
				orderQueryService = new OrderQueryService(commercetoolsProjectBean);
			}
		}
	}

	@Override
	public boolean cleanupData() throws Exception {
		LOG.info("cleanup Order items");

		for (Order order : orderQueryService.getAllEntries()) {
			final OrderDeleteCommand command = OrderDeleteCommand.of(order);
			order = getClient().executeBlocking(command);
			LOG.info("Removed order {} / {}", order.getOrderNumber(), order.getId());
		}

		return true;

	}

	public Order createOrderFromCart(final Cart cart) {
		return getClient().executeBlocking(OrderFromCartCreateCommand.of(cart));
	}

	public Order updateOrder(final Order order, List<UpdateActionImpl<Order>> actions) {
		if (!actions.isEmpty()) {
			final OrderUpdateCommand updateCommand = OrderUpdateCommand.of(order, actions);
			return getClient().executeBlocking(updateCommand);
		}
		LOG.warn("no update actions");
		return order;
	}

	@Override
	protected boolean setupValues(List<Map<String, Object>> valueLines,
			Map<String, List<TypeAttribute>> attributesMap) {
		LOG.warn("setupValues() not implemented()");
		return false;
	}

	@Override
	public String getSheetname() {
		return null;
	}

	@Required
	public void setOrderQueryService(OrderQueryService orderQueryService) {
		this.orderQueryService = orderQueryService;
	}

}

package com.mindcurv.commercetools.automation.services.setup.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.money.CurrencyUnit;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.helper.spreadsheet.ValueHelper;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;
import com.mindcurv.commercetools.automation.pojo.types.product.TypeAttribute;
import com.mindcurv.commercetools.automation.services.query.impl.CartQueryService;
import com.mindcurv.commercetools.automation.services.query.impl.CustomerQueryService;
import com.mindcurv.commercetools.automation.services.query.impl.ProductQueryService;
import com.mindcurv.commercetools.automation.services.query.impl.ShippingMethodQueryService;
import com.mindcurv.commercetools.automation.services.query.impl.StateQueryService;
import com.mindcurv.commercetools.automation.services.setup.SetupService;

import io.sphere.sdk.carts.Cart;
import io.sphere.sdk.carts.CartDraft;
import io.sphere.sdk.carts.CartDraftBuilder;
import io.sphere.sdk.carts.InventoryMode;
import io.sphere.sdk.carts.LineItemDraft;
import io.sphere.sdk.carts.TaxCalculationMode;
import io.sphere.sdk.carts.commands.CartCreateCommand;
import io.sphere.sdk.carts.commands.CartDeleteCommand;
import io.sphere.sdk.carts.commands.CartUpdateCommand;
import io.sphere.sdk.carts.commands.updateactions.AddLineItem;
import io.sphere.sdk.commands.UpdateActionImpl;
import io.sphere.sdk.customers.Customer;
import io.sphere.sdk.models.Address;
import io.sphere.sdk.models.DefaultCurrencyUnits;
import io.sphere.sdk.orders.Order;
import io.sphere.sdk.orders.OrderState;
import io.sphere.sdk.orders.PaymentState;
import io.sphere.sdk.orders.ShipmentState;
import io.sphere.sdk.orders.commands.updateactions.ChangeOrderState;
import io.sphere.sdk.orders.commands.updateactions.ChangePaymentState;
import io.sphere.sdk.orders.commands.updateactions.ChangeShipmentState;
import io.sphere.sdk.orders.commands.updateactions.SetCustomType;
import io.sphere.sdk.orders.commands.updateactions.TransitionState;
import io.sphere.sdk.products.Product;
import io.sphere.sdk.shippingmethods.ShippingMethod;

public class CartSetupService extends SetupService {

	private final static Logger LOG = LoggerFactory.getLogger(CartSetupService.class);
	private CartQueryService cartQueryService;
	private CustomerQueryService customerQueryService;
	private ShippingMethodQueryService shippingMethodQueryService;
	private ProductQueryService productQueryService;
	private OrderSetupService orderSetupService;
	private StateQueryService stateQueryService;

	public CartSetupService(final CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	@Override
	protected void init() {
		super.init();
		final CommercetoolsProject commercetoolsProjectBean = getCommercetoolsProject();
		if (commercetoolsProjectBean != null) {
			if (cartQueryService == null) {
				cartQueryService = new CartQueryService(commercetoolsProjectBean);
			}
			if (customerQueryService == null) {
				customerQueryService = new CustomerQueryService(commercetoolsProjectBean);
			}
			if (shippingMethodQueryService == null) {
				shippingMethodQueryService = new ShippingMethodQueryService(commercetoolsProjectBean);
			}
			if (productQueryService == null) {
				productQueryService = new ProductQueryService(commercetoolsProjectBean);
			}
			if (orderSetupService == null) {
				orderSetupService = new OrderSetupService(commercetoolsProjectBean);
			}
			if (stateQueryService == null) {
				stateQueryService = new StateQueryService(commercetoolsProjectBean);
			}
		}
	}

	public boolean cleanupData() throws Exception {
		LOG.info("cleanup Cart items");
		for (Cart cart : cartQueryService.getAllEntries()) {
			final CartDeleteCommand command = CartDeleteCommand.of(cart);
			cart = getClient().executeBlocking(command);
			LOG.info("Removed Cart {}", cart);
		}
		return true;
	}

	@Override
	public String getSheetname() {
		return CsvConstants.Order.SHEET;
	}

	@Override
	protected boolean setupValues(List<Map<String, Object>> valueLines,
			Map<String, List<TypeAttribute>> attributesMap) {
		for (Map<String, Object> value : valueLines) {
			final String customerNumber = ValueHelper.getValueAsString(CsvConstants.Order.CUSTOMER_NUMBER_KEY, value);
			final Customer customer = StringUtils.isNotBlank(customerNumber)
					? customerQueryService.getByCustomerNumber(customerNumber) : null;

			if (customer == null) {
				LOG.warn("no valid customer found for customerNumber '{}'", customerNumber);
			} else {

				final CartDraft draft = createCartDraft(customer, value);
				if (draft == null) {
					LOG.warn("no cart draft provided");
					continue;
				} else {
					final CartCreateCommand command = CartCreateCommand.of(draft);
					Cart cart = getClient().executeBlocking(command);
					LOG.debug("created cart {} [{}]", cart.getId(), cart.getVersion());
					final String entries = ValueHelper.getValueAsString(CsvConstants.Order.ENTRIES_KEY, value);
					for (String entry : entries.split(CsvConstants.SPLITTOKEN)) {
						entry = entry.trim();
						final String[] entryInfo = entry.split("-");
						if (entry.length() < 2) {
							LOG.warn("entry has invalid format {}", entry);
						} else {
							final Long quantity = Long.valueOf(entryInfo[0]);
							final String sku = entryInfo[1];
							cart = addProductToCart(sku, cart, quantity);
							LOG.debug("added '{}' to cart {} [{}]", sku, cart.getId(), cart.getVersion());
						}
					}

					Order order = orderSetupService.createOrderFromCart(cart);
					LOG.debug("Order: {} [{}]", order.getId(), order.getVersion());
					List<UpdateActionImpl<Order>> actions = new ArrayList<>();

					actions.add(ChangePaymentState.of(PaymentState.PENDING));
					actions.add(ChangeShipmentState.of(ShipmentState.READY));
					actions.add(ChangeOrderState.of(OrderState.OPEN));

					final String type = ValueHelper.getValueAsString(CsvConstants.Order.TYPE_KEY, value);
					if (StringUtils.isNotBlank(type)) {
						actions.add(SetCustomType.ofTypeKeyAndJson(type, null));
					}
					final String status = ValueHelper.getValueAsString(CsvConstants.Order.STATUS_KEY, value);
					if (StringUtils.isNotBlank(status)) {
						actions.add(TransitionState.of(stateQueryService.getByKey(status)));
					}
					order = orderSetupService.updateOrder(order, actions);
					LOG.info("updated order {} [{}]", order.getId(), order.getVersion());

				}

			}
		}
		return true;
	}

	private CartDraft createCartDraft(Customer customer, Map<String, Object> value) {

		final String shippingMethodKey = ValueHelper.getValueAsString(CsvConstants.Order.SHIPPING_METHOD_KEY, value);
		final ShippingMethod shippingMethod = shippingMethodQueryService.getByName(shippingMethodKey);

		if (shippingMethod == null) {
			LOG.warn("no shipping method available for name '{}'", shippingMethodKey);
			return null;
		}

		final Address billingAddress = customer.getDefaultBillingAddress() != null ? customer.getDefaultBillingAddress()
				: customer.getAddresses().get(0);
		final Address shippingAddress = customer.getDefaultShippingAddress() != null
				? customer.getDefaultShippingAddress() : billingAddress;

		if (billingAddress == null || shippingAddress == null) {
			LOG.warn("no addresses for customer {} available");
			return null;
		}

		final String currency = ValueHelper.getValueAsString(CsvConstants.Order.CURRENCY_KEY, value);
		final CurrencyUnit currencyUnit = DefaultCurrencyUnits.USD.getCurrencyCode().equals(currency)
				? DefaultCurrencyUnits.USD : DefaultCurrencyUnits.EUR;

		final CartDraftBuilder draftBuilder = CartDraftBuilder.of(currencyUnit).billingAddress(billingAddress)
				.country(billingAddress.getCountry()).customerEmail(billingAddress.getEmail())
				.shippingMethod(shippingMethod).shippingAddress(shippingAddress);

		final String inventoryModeValue = ValueHelper.getValueAsString(CsvConstants.Order.INVENTORY_MODE_KEY, value);
		final InventoryMode inventoryMode = StringUtils.isNotBlank(inventoryModeValue)
				? InventoryMode.valueOf(inventoryModeValue) : InventoryMode.NONE;
		draftBuilder.inventoryMode(inventoryMode);

		final String taxCalcValue = ValueHelper.getValueAsString(CsvConstants.Order.TAX_CALCULATION_MODE_KEY, value);
		final TaxCalculationMode taxCalculationMode = StringUtils.isNotBlank(taxCalcValue)
				? TaxCalculationMode.valueOf(taxCalcValue) : TaxCalculationMode.LINE_ITEM_LEVEL;
		draftBuilder.taxCalculationMode(taxCalculationMode);

		return draftBuilder.build();
	}

	public Cart addProductToCart(final String sku, final Cart cart, Long quantity) {
		final Product product = productQueryService.getBySku(sku);
		return addProductToCart(product, cart, quantity);
	}

	public Cart addProductToCart(final Product product, final Cart cart, Long quantity) {
		if (product != null) {
			final String productId = product.getId();
			final Integer masterVariantId = product.getMasterData().getCurrent().getMasterVariant().getId();
			final LineItemDraft lineItemDraft = LineItemDraft.of(productId, masterVariantId, quantity);
			final AddLineItem action = AddLineItem.of(lineItemDraft);
			final CartUpdateCommand command = CartUpdateCommand.of(cart, action);
			return getClient().executeBlocking(command);
		}
		LOG.warn("Product is null, can not be added to cart");
		return cart;
	}

	@Required
	public void setCartQueryService(CartQueryService cartQueryService) {
		this.cartQueryService = cartQueryService;
	}

	@Required
	public void setCustomerQueryService(CustomerQueryService customerQueryService) {
		this.customerQueryService = customerQueryService;
	}

	@Required
	public void setShippingMethodQueryService(ShippingMethodQueryService shippingMethodQueryService) {
		this.shippingMethodQueryService = shippingMethodQueryService;
	}

	@Required
	public void setProductQueryService(ProductQueryService productQueryService) {
		this.productQueryService = productQueryService;
	}

	@Required
	public void setOrderSetupService(OrderSetupService orderSetupService) {
		this.orderSetupService = orderSetupService;
	}

	@Required
	public void setStateQueryService(StateQueryService stateQueryService) {
		this.stateQueryService = stateQueryService;
	}

}

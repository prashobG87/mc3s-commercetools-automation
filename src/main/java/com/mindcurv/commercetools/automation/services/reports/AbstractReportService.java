package com.mindcurv.commercetools.automation.services.reports;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.annotation.Configuration;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.services.CommercetoolsBaseService;
import com.mindcurv.commercetools.automation.services.query.QueryService;
import com.mindcurv.commercetools.automation.services.reports.datasource.BaseJRDataSource;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;

@Configuration
public abstract class AbstractReportService<T> extends CommercetoolsBaseService {

	private final static Logger LOG = LoggerFactory.getLogger(AbstractReportService.class);

	public static final String JRXML_SUFFIX = ".jrxml";
	public static final String PARAM_REPORTNAME = "reportName";
	public static final String PARAM_REPORTFORMAT = "reportFormat";
	public static final String PARAM_REPORTTARGETDIRECTORY = "reportTargetDirectory";

	public static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMddHHmmssSSS");

	private String reportTemplateDirectory;
	
	private QueryService<T> queryService; 

	public AbstractReportService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	public abstract File getReport(Map<String, Object> params);

	protected abstract JasperPrint prepareReport(BaseJRDataSource<T> jrDataSource, Map<String, Object> reportParams) throws JRException;
	
	public QueryService<T> getQueryService() {
		return queryService;
	}

	@Required
	public void setQueryService(QueryService<T> queryService) {
		this.queryService = queryService;
	}

	@Required
	public void setReportTemplateDirectory(String reportTemplateDirectory) {
		this.reportTemplateDirectory = reportTemplateDirectory;
	}

	public String getReportTemplateDirectory() {
		return reportTemplateDirectory;
	}

	public File createReportFile(final File targetDirectory, final String reportName, final ReportFormat reportFormat) {
		final File destFile = new File(targetDirectory,
				reportName + "-" + DATE_FORMAT.format(new Date()) + "." + reportFormat.name().toLowerCase());
		return destFile;
	}

	protected File generateReportFromDataSource(BaseJRDataSource<T> jrDataSource, final Map<String, Object> params) {

		final String reportName = getReportName(params);
		if (reportName == null) {
			return null;
		}
		final ReportFormat reportFormat = getReportFormat(params);
		if (reportFormat == null) {
			return null;
		}
		final File reportTargetDirectory = getTargetDirectoryAsFile(params);
		if (reportTargetDirectory == null) {
			return null;
		}
		final File destFile = createReportFile(reportTargetDirectory, reportName, reportFormat);
		try {
			
			
			final JasperPrint print = prepareReport(jrDataSource, params);
			if (ReportFormat.PDF.equals(reportFormat)) {
				JasperExportManager.exportReportToPdfFile(print, destFile.getAbsolutePath());
			} else if (ReportFormat.HTML.equals(reportFormat)) {
				JasperExportManager.exportReportToHtmlFile(print, destFile.getAbsolutePath());
			}
			if (destFile.exists()) {
				return destFile;
			}
			LOG.error("generated file {} doesn't exist", destFile.getAbsolutePath());
		} catch (Exception e) {
			LOG.error("Jasper Exception", e);
		}
		return null;
	}

	protected ReportFormat getReportFormat(Map<String, Object> params) {
		if (!params.containsKey(PARAM_REPORTFORMAT)) {
			LOG.error("No reportFormat provided");
			return null;
		}
		final Object reportFormat = params.get(PARAM_REPORTFORMAT);
		if (reportFormat == null || !(reportFormat instanceof ReportFormat)) {
			LOG.error("No valid reportFormat provided");
			return null;
		}
		return (ReportFormat) reportFormat;
	}

	protected String getReportName(Map<String, Object> params) {
		final String reportName = (String) params.get(PARAM_REPORTNAME);
		if (StringUtils.isBlank(reportName)) {
			LOG.error("No reportName provided");
		}
		return reportName;
	}

	public File getTargetDirectoryAsFile(Map<String, Object> params) {
		if ( !params.containsKey(PARAM_REPORTTARGETDIRECTORY)) {
			LOG.error("no param with key {}", PARAM_REPORTTARGETDIRECTORY);
			return null; 
		}
		final String targetDirectory = (String) params.get(PARAM_REPORTTARGETDIRECTORY);
		if (StringUtils.isBlank(targetDirectory)) {
			LOG.error("param with key {} is blank", PARAM_REPORTTARGETDIRECTORY);
			return null;
		}
		File reportTargetDirectory = new File(targetDirectory);
		if (!reportTargetDirectory.exists()) {
			reportTargetDirectory.mkdirs();
		}
		if (!reportTargetDirectory.exists()) {
			LOG.error("no directory '{}' available", reportTargetDirectory.getAbsolutePath());
			return null;
		}
		if (!reportTargetDirectory.isDirectory()) {
			LOG.error("'{}' is not a directory", reportTargetDirectory.getAbsolutePath());
			return null;
		}
		return reportTargetDirectory;
	} 
}

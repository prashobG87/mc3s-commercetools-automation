package com.mindcurv.commercetools.automation.services.export;

import java.util.Map;

import io.sphere.sdk.orders.Order;

public abstract class AbstractOrderSplitStrategy {

	public abstract Map<String, Object> splitOrder(Order order);

}

package com.mindcurv.commercetools.automation.services.load.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.commercetools.sync.products.ProductSync;
import com.commercetools.sync.products.ProductSyncOptions;
import com.commercetools.sync.products.ProductSyncOptionsBuilder;
import com.commercetools.sync.products.helpers.ProductSyncStatistics;
import com.fasterxml.jackson.databind.JsonNode;
import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.helper.DraftHelper;
import com.mindcurv.commercetools.automation.integration.SyncStatistics;
import com.mindcurv.commercetools.automation.services.PublicationService;
import com.mindcurv.commercetools.automation.services.load.AbstractLoadService;
import com.mindcurv.commercetools.automation.services.query.impl.ProductQueryService;
import com.mindcurv.commercetools.automation.services.query.impl.ProductTypeQueryService;

import io.sphere.sdk.commands.UpdateAction;
import io.sphere.sdk.customobjects.CustomObject;
import io.sphere.sdk.customobjects.queries.CustomObjectByKeyGet;
import io.sphere.sdk.products.Product;
import io.sphere.sdk.products.ProductDraft;
import io.sphere.sdk.products.ProductProjectionType;
import io.sphere.sdk.products.ProductVariant;
import io.sphere.sdk.products.attributes.AttributeConstraint;
import io.sphere.sdk.products.attributes.AttributeDefinition;
import io.sphere.sdk.products.attributes.AttributeDraft;
import io.sphere.sdk.products.attributes.RichReferenceAttributeType;
import io.sphere.sdk.products.commands.ProductUpdateCommand;
import io.sphere.sdk.products.commands.updateactions.SetAttribute;
import io.sphere.sdk.producttypes.ProductType;

public class ProductLoadService extends AbstractLoadService<ProductDraft> {

	private final static Logger LOG = LoggerFactory.getLogger(ProductLoadService.class);

	private ProductQueryService productQueryService;
	private PublicationService publicationService;
	private ProductTypeQueryService productTypeQueryService;

	public ProductLoadService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	@Override
	protected void init() {
		super.init();

		setTypeParameterClass(ProductDraft.class);

		final CommercetoolsProject commercetoolsProjectBean = getCommercetoolsProject();
		if (commercetoolsProjectBean != null) {
			if (publicationService == null) {
				publicationService = new PublicationService(commercetoolsProjectBean);
			}
			if (productQueryService == null) {
				productQueryService = new ProductQueryService(commercetoolsProjectBean);
			}
			if (productTypeQueryService == null) {
				productTypeQueryService = new ProductTypeQueryService(commercetoolsProjectBean);
			}
		}
	}

	@Override
	public SyncStatistics executeSync(final List<ProductDraft> drafts) {
		final SyncStatistics syncStatistics = new SyncStatistics();
		if (validForSync(drafts)) {
			final ProductSyncOptions syncOptions = ProductSyncOptionsBuilder.of(getClient()).errorCallback(LOG::error)
					.warningCallback(LOG::warn).build();

			final ProductSync sync = new ProductSync(syncOptions);
			LOG.info("start sync of {} drafts", drafts.size());
			ProductSyncStatistics syncStatisticsStage = sync.sync(drafts).toCompletableFuture().join();
			LOG.info("finished sync:{}", syncStatisticsStage.getReportMessage());
			syncStatistics.setValues(syncStatisticsStage);
			attachCustomObjects(drafts);
			publicationService.publishProducts(productQueryService.getAllProductsByPublicationStatus(false));
		}
		return syncStatistics;
	}

	public void publishProducts(List<String> skuList) {

		for (String sku : skuList) {
			final Product product = productQueryService.getBySku(sku, ProductProjectionType.STAGED);
			if (product != null) {
				LOG.debug("try to publish product {}", product.getKey());
				publicationService.publishProduct(product);
			} else {
				LOG.warn("no product with sku {} found for publication", sku);
			}
		}
	}

	public void publishProductDrafts(List<ProductDraft> drafts) {

		final List<String> skuList = new ArrayList<>();
		for (ProductDraft draft : drafts) {
			skuList.add(draft.getMasterVariant().getSku());
		}
		publishProducts(skuList);
	}

	private void attachCustomObjects(List<ProductDraft> drafts) {
	
		final Map<String, ProductType> productTypeMap = new HashMap<>();
		for (ProductType productType : productTypeQueryService.getAllEntries()) {
			productTypeMap.put(productType.getKey(), productType);
		}
		for (ProductDraft draft : drafts) {
			if (draft.getProductType() != null) {
				final ProductType productType = productTypeMap.get(draft.getProductType().getId()); 
				final List<AttributeDefinition> attributes = productTypeQueryService
						.getAttributeListForKeyValueDocuments(productType);
				for (AttributeDefinition attribute : attributes) {
					final String attributeName = attribute.getName();
	
					final RichReferenceAttributeType<?> richReferenceAttributeType = (RichReferenceAttributeType<?>) attribute
							.getAttributeType();
					LOG.info("RichReferenceAttributeType {}->{}", richReferenceAttributeType.getReferenceTypeId(),
							attributeName);
	
					final Product product = productQueryService.getBySku(draft.getMasterVariant().getSku());
					final ProductVariant masterVariant = product.getMasterData().getStaged().getMasterVariant();
					
					//Attribute already set. // TODO check for modification
					if ( masterVariant.getAttribute(attributeName) != null) {
						continue; 
					}
	
					final String masterContainerKey = DraftHelper.generateContainerKey(masterVariant.getSku());
					final Map<String, String> masterVariantMap = prepareCustomObjectMap(attributeName,
							masterContainerKey);
	
					final List<UpdateAction<Product>> updateActions = new ArrayList<>();
					if (!masterVariantMap.isEmpty()) {
						updateActions.add(prepareUpdateAction(masterVariant.getId(), masterVariantMap, attributeName));
					}
					for (ProductVariant productVariant : product.getMasterData().getStaged().getAllVariants()) {
						if (productVariant.getId().equals(masterVariant.getId())) {
							continue; // already covered;
						}
						if (attribute.getAttributeConstraint().equals(AttributeConstraint.SAME_FOR_ALL)) {
							// TODO fix , doesn't work at the moment.
							if (!masterVariantMap.isEmpty()) {
								updateActions.add(
										prepareUpdateAction(productVariant.getId(), masterVariantMap, attributeName));
							}
						} else {
							final String variantContainerKey = DraftHelper.generateContainerKey(productVariant.getSku());
							final Map<String, String> variantMap = prepareCustomObjectMap(attributeName, variantContainerKey);
							if (!variantMap.isEmpty()) {
								updateActions .add(prepareUpdateAction(productVariant.getId(), variantMap, attributeName));
							}
						}
					}
					if (!updateActions.isEmpty()) {
						setNewAttributesByList(product, updateActions, attributeName);
					}
	
				}
			}
		}
	
	}

	private Map<String, String> prepareCustomObjectMap(final String container, final String containerKey) {
		final Map<String, String> map = new HashMap<>();
		final CustomObjectByKeyGet<JsonNode> fetch = CustomObjectByKeyGet.ofJsonNode(container, containerKey);
		final CustomObject<JsonNode> customObject = getCommercetoolsProject().getClient().executeBlocking(fetch);
		if (customObject != null) {
			LOG.info("CustomObject {} for {} / {}", customObject, container, containerKey);
			map.put("id", customObject.getId());
			map.put("typeId", "key-value-document");
		}
		return map;
	}

	private UpdateAction<Product> prepareUpdateAction(Integer variantId, Object attributeValue,
			final String attributeName) {
		final AttributeDraft attribute = AttributeDraft.of(attributeName, attributeValue);
		return SetAttribute.of(variantId, attribute);
	}

	public Product setNewAttributesByList(Product product, List<UpdateAction<Product>> actions,
			final String attributeName) {
		final ProductUpdateCommand command = ProductUpdateCommand.of(product, actions);
		return getClient().executeBlocking(command);
	}

	public Product setNewAttribute(Product product, Object attributeValue, final String attributeName) {
		final Integer variantId = product.getMasterData().getStaged().getMasterVariant().getId();
		final UpdateAction<Product> action = prepareUpdateAction(variantId, attributeValue, attributeName);
		return setNewAttributesByList(product, Arrays.asList(action), attributeName);
	}

	public Product removeAttribute(Product product, final String attributeName) {
		return setNewAttribute(product, null, attributeName);
	}

	@Required
	public void setProductQueryService(ProductQueryService productQueryService) {
		this.productQueryService = productQueryService;
	}

	@Required
	public void setPublicationService(PublicationService publicationService) {
		this.publicationService = publicationService;
	}

	@Required
	public void setProductTypeQueryService(ProductTypeQueryService productTypeQueryService) {
		this.productTypeQueryService = productTypeQueryService;
	}

}

package com.mindcurv.commercetools.automation.services.setup.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.annotation.Configuration;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;
import com.mindcurv.commercetools.automation.pojo.shipping.ShippingZonePojo;
import com.mindcurv.commercetools.automation.pojo.types.product.TypeAttribute;
import com.mindcurv.commercetools.automation.services.load.impl.ShippingMethodLoadService;
import com.mindcurv.commercetools.automation.services.setup.SetupService;

import io.sphere.sdk.zones.ZoneDraft;

@Configuration
public class ZoneSetupService extends SetupService {

	private final static Logger LOG = LoggerFactory.getLogger(ZoneSetupService.class);

	private ShippingMethodLoadService shippingMethodLoadService;

	public ZoneSetupService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}
	
	@Override
	protected void init() {
		super.init();
		final CommercetoolsProject commercetoolsProjectBean = getCommercetoolsProject();
		if ( commercetoolsProjectBean != null) {
			shippingMethodLoadService = new ShippingMethodLoadService(commercetoolsProjectBean);
		}
	}

	@Override
	public boolean cleanupData() {
		return true;
	}

	@Override
	protected boolean setupValues(List<Map<String, Object>> valueLines,
			Map<String, List<TypeAttribute>> attributesMap) {
		try {
			final List<ZoneDraft> drafts = new ArrayList<>();

			for (Map<String, Object> value : valueLines) {
				final ShippingZonePojo zonePojo = new ShippingZonePojo().withSheetData(value, Collections.emptyList());
				drafts.add(zonePojo.toDraft());
			}
			if (!drafts.isEmpty()) {
				shippingMethodLoadService.syncZones(drafts);
			}
			return true;
		} catch (Exception e) {
			LOG.error("Exception", e);
		}
		return false;
	}

	@Override
	public String getSheetname() {
		return CsvConstants.ShippingZone.SHEET;
	}

	@Required
	public void setShippingMethodLoadService(ShippingMethodLoadService shippingMethodLoadService) {
		this.shippingMethodLoadService = shippingMethodLoadService;
	}

}

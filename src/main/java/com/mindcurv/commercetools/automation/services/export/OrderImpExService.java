package com.mindcurv.commercetools.automation.services.export;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.helper.DraftHelper;
import com.mindcurv.commercetools.automation.pojo.GenericTypeReferencePojo;
import com.mindcurv.commercetools.automation.pojo.order.OrderPojo;
import com.mindcurv.commercetools.automation.services.CommercetoolsBaseService;
import com.mindcurv.commercetools.automation.services.query.impl.OrderQueryService;
import com.mindcurv.commercetools.automation.services.query.impl.StateQueryService;

import io.sphere.sdk.commands.UpdateAction;
import io.sphere.sdk.orders.Order;
import io.sphere.sdk.orders.OrderState;
import io.sphere.sdk.orders.PaymentState;
import io.sphere.sdk.orders.ShipmentState;
import io.sphere.sdk.orders.commands.OrderUpdateCommand;
import io.sphere.sdk.orders.commands.updateactions.ChangeOrderState;
import io.sphere.sdk.orders.commands.updateactions.ChangePaymentState;
import io.sphere.sdk.orders.commands.updateactions.ChangeShipmentState;
import io.sphere.sdk.orders.commands.updateactions.SetCustomField;
import io.sphere.sdk.orders.commands.updateactions.SetOrderNumber;
import io.sphere.sdk.orders.commands.updateactions.TransitionState;
import io.sphere.sdk.states.State;

public class OrderImpExService extends CommercetoolsBaseService {

	private final static Logger LOG = LoggerFactory.getLogger(OrderImpExService.class);
	public static final String KEY_ORDER = "ORDER";

	private OrderQueryService orderQueryService;
	private StateQueryService stateQueryService;

	private AbstractOrderSplitStrategy orderSplitStrategy;

	public OrderImpExService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	@Override
	protected void init() {
		super.init();
		final CommercetoolsProject commercetoolsProjectBean = getCommercetoolsProject();
		if (commercetoolsProjectBean != null) {
			if (orderQueryService == null) {
				orderQueryService = new OrderQueryService(commercetoolsProjectBean);
			}
			if (stateQueryService == null) {
				stateQueryService = new StateQueryService(commercetoolsProjectBean);
			}
		}
	}

	public Map<String, Object> splitOrder(String orderId) {
		final Order order = orderQueryService.getById(orderId);
		return orderSplitStrategy.splitOrder(order);
	}

	public List<Map<String, Object>> splitOrders() {
		final List<Map<String, Object>> splitList = new ArrayList<>();
		final List<Order> orderList = orderQueryService.getOrdersForExport();
		if (orderList.isEmpty()) {
			LOG.info("no orders for export available");
			return splitList;
		}
		for (Order order : orderList) {
			final Map<String, Object> splitResult = orderSplitStrategy.splitOrder(order);
			LOG.info("splitted order {} into {} parts", order.getId(), splitList.size());
			splitList.add(splitResult);
		}
		return splitList;
	}

	public Order updateOrder(Order order, OrderPojo orderPojo) {
		
		LOG.info("updating order {}", order.getId());

		final List<UpdateAction<Order>> updateActions = new ArrayList<>();
		if (order.getOrderNumber() == null && StringUtils.isNotBlank(orderPojo.getOrderNumber())) {
			updateActions.add(SetOrderNumber.of(orderPojo.getOrderNumber()));
		}
		final GenericTypeReferencePojo stateReference = orderPojo.getState();
		if (stateReference != null) {
			final State state = stateQueryService.getByKey(stateReference.getKey());
			if (state == null) {
				LOG.warn("no state found for key = '{}'", stateReference.getKey());
			} else {
				final String orderStateId = order.getState()==null? null:order.getState().getId(); 
				if (DraftHelper.checkForUpdate(orderStateId, state.getId())){
					updateActions.add(TransitionState.of(state));
					LOG.info("setting State to {}", state.getKey());
				}
			}
		}
		if (DraftHelper.checkForUpdate(order.getOrderState().name(), orderPojo.getOrderState())){
			final OrderState stateToBeSet = OrderState.ofSphereValue(orderPojo.getOrderState());
			updateActions.add(ChangeOrderState.of(stateToBeSet));
			LOG.info("setting OrderState to {}", stateToBeSet);
		}
		if (DraftHelper.checkForUpdate(order.getPaymentState().name(), orderPojo.getPaymentState())){
			final PaymentState stateToBeSet = PaymentState.ofSphereValue(orderPojo.getPaymentState());
			updateActions.add(ChangePaymentState.of(stateToBeSet));
			LOG.info("setting PaymentState to {}", stateToBeSet);
		}
		if (DraftHelper.checkForUpdate(order.getShipmentState().name(), orderPojo.getShipmentState())){
			final ShipmentState stateToBeSet = ShipmentState.ofSphereValue(orderPojo.getShipmentState());
			updateActions.add(ChangeShipmentState.of(stateToBeSet));
			LOG.info("setting ShipmentState to {}", stateToBeSet);
		}

		//Custom fields: 
		if ( orderPojo.getCustom() != null) {
			final String sapOrderId = (String) orderPojo.getCustom().getFields().get("sapOrderId");
			if ( DraftHelper.checkForUpdate(order.getCustom().getFieldAsString("sapOrderId"), sapOrderId)) {
				LOG.info("update sapOrderId {}" , sapOrderId); 
				updateActions.add(SetCustomField.ofObject("sapOrderId", sapOrderId));
			}
		}
		if (!updateActions.isEmpty()) {
			OrderUpdateCommand command = OrderUpdateCommand.of(order, updateActions);
			order = getClient().executeBlocking(command);
			LOG.info("Updated order {}", order.getId());
		}
		return order;
	}

	@Required
	public void setOrderQueryService(OrderQueryService orderQueryService) {
		this.orderQueryService = orderQueryService;
	}

	@Required
	public void setOrderSplitStrategy(AbstractOrderSplitStrategy orderSplitStrategy) {
		this.orderSplitStrategy = orderSplitStrategy;
	}

	@Required
	public void setStateQueryService(StateQueryService stateQueryService) {
		this.stateQueryService = stateQueryService;
	}

	public List<Order> updateOpenOrders() {
		return orderQueryService.getOrdersToBeUpdated(); 
		//TODO trigger update 
	}

}

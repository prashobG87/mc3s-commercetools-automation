package com.mindcurv.commercetools.automation.services.setup.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.google.api.services.sheets.v4.Sheets.Spreadsheets;
import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.helper.spreadsheet.ExcelHelper;
import com.mindcurv.commercetools.automation.helper.spreadsheet.GDriveHelper;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;
import com.mindcurv.commercetools.automation.pojo.shipping.ShippingMethodPojo;
import com.mindcurv.commercetools.automation.pojo.shipping.ShippingZoneRate;
import com.mindcurv.commercetools.automation.pojo.types.product.TypeAttribute;
import com.mindcurv.commercetools.automation.services.load.impl.ShippingMethodLoadService;
import com.mindcurv.commercetools.automation.services.query.impl.ShippingMethodQueryService;
import com.mindcurv.commercetools.automation.services.query.impl.TaxCategoryQueryService;
import com.mindcurv.commercetools.automation.services.query.impl.ZoneQueryService;
import com.mindcurv.commercetools.automation.services.setup.SetupService;

import io.sphere.sdk.shippingmethods.ShippingMethod;
import io.sphere.sdk.shippingmethods.ShippingMethodDraft;
import io.sphere.sdk.shippingmethods.commands.ShippingMethodDeleteCommand;
import io.sphere.sdk.taxcategories.TaxCategory;
import io.sphere.sdk.zones.Zone;

public class ShippingSetupService extends SetupService {

	private final static Logger LOG = LoggerFactory.getLogger(ShippingSetupService.class);

	private ShippingMethodQueryService shippingMethodQueryService;

	private ZoneQueryService zoneQueryService;

	private TaxCategoryQueryService taxCategoryQueryService;

	private ShippingMethodLoadService shippingMethodLoadService;

	public ShippingSetupService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	@Override
	protected void init() {
		super.init();
		final CommercetoolsProject commercetoolsProjectBean = getCommercetoolsProject();
		if (commercetoolsProjectBean != null) {
			shippingMethodQueryService = new ShippingMethodQueryService(commercetoolsProjectBean);
			zoneQueryService = new ZoneQueryService(commercetoolsProjectBean);
			taxCategoryQueryService = new TaxCategoryQueryService(commercetoolsProjectBean);
			shippingMethodLoadService = new ShippingMethodLoadService(commercetoolsProjectBean);
		}
	}

	@Override
	public boolean setup(String sheetId, Spreadsheets sheets, Map<String, List<TypeAttribute>> attributesMap) {

		final List<Map<String, Object>> shippingMethodLines;
		final List<Map<String, Object>> shippingZoneRateLines;
		try {
			shippingMethodLines = GDriveHelper.processRange(sheets, sheetId, CsvConstants.ShippingMethod.SHEETNAME,
					getCommercetoolsProject());
			shippingZoneRateLines = GDriveHelper.processRange(sheets, sheetId, CsvConstants.ShippingZoneRates.SHEETNAME,
					getCommercetoolsProject());

		} catch (IOException e) {
			LOG.error("Exception while accessing Sheet " + getSheetname(), e);
			return false;
		}

		return processLines(shippingMethodLines, shippingZoneRateLines);
	}

	@Override
	public boolean setup(Workbook workbook, Map<String, List<TypeAttribute>> attributesMap) {
		final List<Map<String, Object>> shippingMethodLines;
		final List<Map<String, Object>> shippingZoneRateLines;
		try {
			shippingMethodLines = ExcelHelper.processRange(workbook, CsvConstants.ShippingMethod.SHEET,
					getCommercetoolsProject());
			shippingZoneRateLines = ExcelHelper.processRange(workbook, CsvConstants.ShippingZoneRates.SHEET,
					getCommercetoolsProject());

		} catch (IOException e) {
			LOG.error("Exception while accessing Sheet " + getSheetname(), e);
			return false;
		}

		return processLines(shippingMethodLines, shippingZoneRateLines);
	}

	private boolean processLines(final List<Map<String, Object>> shippingMethodLines,
			final List<Map<String, Object>> shippingZoneRateLines) {
		final Map<String, ShippingMethodPojo> shippingMethodMap = new HashMap<>();

		for (Map<String, Object> value : shippingMethodLines) {
			final ShippingMethodPojo shippingMethodPojo = new ShippingMethodPojo().withSheetData(value,
					Collections.emptyList());
			final String taxCategoryKey = shippingMethodPojo.getTaxCategory().getKey();
			LOG.info("try to find tax category '{}' for {}",taxCategoryKey,  shippingMethodPojo.getName());
			if ( StringUtils.isBlank(taxCategoryKey)) {
				LOG.warn("TaxCategory key not for Shipping Method {}", shippingMethodPojo.getName());
			} else {
				final TaxCategory taxCategory = taxCategoryQueryService.getByKey(taxCategoryKey);
				if (taxCategory == null) {
					LOG.warn("TaxCategory {} not found for Shipping Method {}", taxCategoryKey, shippingMethodPojo.getName());
				} else {
					shippingMethodPojo.getTaxCategory().setId(taxCategory.getId());
				}
			}
			shippingMethodMap.put(shippingMethodPojo.getName(), shippingMethodPojo);
		}

		for (Map<String, Object> value : shippingZoneRateLines) {
			final ShippingZoneRate zoneRate = new ShippingZoneRate().withSheetData(value, Collections.emptyList());
			final ShippingMethodPojo shippingMethodPojo = shippingMethodMap.get(zoneRate.getShippingMethod());
			if (shippingMethodPojo != null) {
				final String zoneName = zoneRate.getZone().getId();
				final Zone zone = zoneQueryService.getByName(zoneName);
				if (zone != null) {
					zoneRate.getZone().setId(zone.getId());
				}
				shippingMethodPojo.getZoneRates().add(zoneRate);
			} else {
				LOG.warn("no Shipping method found for {}", zoneRate.getShippingMethod());
			}
		}

		final List<ShippingMethodDraft> drafts = new ArrayList<>();
		for (Map.Entry<String, ShippingMethodPojo> entry : shippingMethodMap.entrySet()) {
			drafts.add(entry.getValue().toDraft());
		}
		if (!drafts.isEmpty()) {
			shippingMethodLoadService.sync(drafts);

		}
		return true;
	}

	public boolean cleanupData() {
		for (ShippingMethod shippingMethod : shippingMethodQueryService.getAllEntries()) {
			final ShippingMethodDeleteCommand command = ShippingMethodDeleteCommand.of(shippingMethod);
			shippingMethod = getClient().executeBlocking(command);
			LOG.info("Removed ShippingMethod {}", shippingMethod);
		}
		return true;
	}

	@Override
	protected boolean setupValues(List<Map<String, Object>> valueLines,
			Map<String, List<TypeAttribute>> attributesMap) {
		LOG.warn("setupValues() not implemented()");
		return false;
	}

	@Override
	public String getSheetname() {
		return CsvConstants.ShippingMethod.SHEET;
	}

	@Required
	public void setShippingMethodQueryService(ShippingMethodQueryService shippingMethodQueryService) {
		this.shippingMethodQueryService = shippingMethodQueryService;
	}

	@Required
	public void setZoneQueryService(ZoneQueryService zoneQueryService) {
		this.zoneQueryService = zoneQueryService;
	}

	@Required
	public void setTaxCategoryQueryService(TaxCategoryQueryService taxCategoryQueryService) {
		this.taxCategoryQueryService = taxCategoryQueryService;
	}

	@Required
	public void setShippingMethodLoadService(ShippingMethodLoadService shippingMethodLoadService) {
		this.shippingMethodLoadService = shippingMethodLoadService;
	}

}

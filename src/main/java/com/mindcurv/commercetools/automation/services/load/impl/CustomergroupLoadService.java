package com.mindcurv.commercetools.automation.services.load.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.collections4.map.HashedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.helper.DraftHelper;
import com.mindcurv.commercetools.automation.integration.SyncStatistics;
import com.mindcurv.commercetools.automation.services.load.AbstractLoadService;

import io.sphere.sdk.commands.UpdateAction;
import io.sphere.sdk.customergroups.CustomerGroup;
import io.sphere.sdk.customergroups.CustomerGroupDraft;
import io.sphere.sdk.customergroups.commands.CustomerGroupCreateCommand;
import io.sphere.sdk.customergroups.commands.CustomerGroupUpdateCommand;
import io.sphere.sdk.customergroups.commands.updateactions.ChangeName;
import io.sphere.sdk.customergroups.commands.updateactions.SetCustomField;
import io.sphere.sdk.customergroups.queries.CustomerGroupQuery;
import io.sphere.sdk.customergroups.queries.CustomerGroupQueryBuilder;
import io.sphere.sdk.customergroups.queries.CustomerGroupQueryModel;
import io.sphere.sdk.queries.PagedQueryResult;
import io.sphere.sdk.queries.QueryPredicate;
import io.sphere.sdk.types.Type;
import io.sphere.sdk.types.queries.TypeQuery;

public class CustomergroupLoadService extends AbstractLoadService<CustomerGroupDraft> {

	private final static Logger LOG = LoggerFactory.getLogger(CustomergroupLoadService.class);

	public CustomergroupLoadService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	@Override
	protected void init() {
		super.init();

		setTypeParameterClass(CustomerGroupDraft.class);
	}

	@Override
	public SyncStatistics executeSync(List<CustomerGroupDraft> drafts) {

		final SyncStatistics syncStatistics = new SyncStatistics();
		if (validForSync(drafts)) {
			List<QueryPredicate<Type>> queryPredicates = new ArrayList<>();
			List<Type> types = getClient()
					.executeBlocking(TypeQuery.of().withLimit(500).plusPredicates(queryPredicates)).getResults();
			final Map<String, String> typeIdMap = new HashedMap<>();
			for (Type type : types) {
				typeIdMap.put(type.getKey(), type.getId());
			}
			for (CustomerGroupDraft draft : drafts) {
				syncStatistics.incrementProcessed();
				try {
					CustomerGroup customerGroup = null;
					final CustomerGroupQuery query = CustomerGroupQueryBuilder.of()
							.predicates(CustomerGroupQueryModel.of().key().is(draft.getKey())).build();
					final PagedQueryResult<CustomerGroup> result = getClient().executeBlocking(query);
					if (result == null || result.getTotal() == 0) {
						customerGroup = getClient().executeBlocking(CustomerGroupCreateCommand.of(draft));
						LOG.info("created customer group {}", customerGroup.getKey());
						syncStatistics.incrementCreated();
					} else {
						customerGroup = result.getResults().get(0);
					}

					List<UpdateAction<CustomerGroup>> updateActions = new ArrayList<>();

					if (DraftHelper.checkForUpdate(draft.getGroupName(), customerGroup.getName())) {
						updateActions.add(ChangeName.of(draft.getGroupName()));
					}
					if (DraftHelper.checkForUpdate(draft.getKey(), customerGroup.getKey())) {
						updateActions
								.add(io.sphere.sdk.customergroups.commands.updateactions.SetKey.of(draft.getKey()));
					}
					if (draft.getCustom() != null) {
						for (Entry<String, JsonNode> field : draft.getCustom().getFields().entrySet()) {
							updateActions.add(SetCustomField.ofJson(field.getKey(), field.getValue()));
						}
					}

					if (updateActions.isEmpty()) {
						LOG.info("no change for customer group {}", customerGroup.getKey());
					} else {
						CustomerGroup updateResult = getClient()
								.executeBlocking(CustomerGroupUpdateCommand.of(customerGroup, updateActions));
						syncStatistics.incrementUpdated();
						LOG.info("updated customer group {} - {}", updateResult.getKey(), updateResult.getName());
					}

				} catch (Exception e) {
					LOG.error("Exception during customer group creation", e);
					syncStatistics.incrementFailed();
				}
			}
		}
		LOG.info("finished customer group sync:{}", syncStatistics);
		return syncStatistics;
	}
}

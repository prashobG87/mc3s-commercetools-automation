package com.mindcurv.commercetools.automation.services.query.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.services.query.QueryService;

import io.sphere.sdk.products.ProductProjection;
import io.sphere.sdk.queries.PagedQueryResult;
import io.sphere.sdk.queries.QueryPredicate;
import io.sphere.sdk.taxcategories.TaxCategory;
import io.sphere.sdk.taxcategories.queries.TaxCategoryQueryBuilder;
import io.sphere.sdk.taxcategories.queries.TaxCategoryQueryModel;

/**
 * This class provides search operations for {@link ProductProjection}s.
 */
public class TaxCategoryQueryService extends QueryService<TaxCategory> {

	private final static Logger LOG = LoggerFactory.getLogger(TaxCategoryQueryService.class);

	public TaxCategoryQueryService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}
	
	@Override
	public List<TaxCategory> getQueryResult(Map<String, Object> params) {
		final List<TaxCategory> result = new ArrayList<>();
		Long limit = params.containsKey(PARAM_LIMIT) ? (Long) params.get(PARAM_LIMIT) : DEFAULT_LIMIT;
		final TaxCategoryQueryBuilder queryBuilder = TaxCategoryQueryBuilder.of().fetchTotal(true);
		queryBuilder.limit(limit);

		final List<QueryPredicate<TaxCategory>> predicates = preparePredicates(params);
		if (!predicates.isEmpty()) {
			queryBuilder.predicates(predicates);
		}

		long page = 1;
		boolean breakLoop = false;
		while (!breakLoop) {
			long offset = (page - 1) * limit;
			queryBuilder.offset(offset);
			final PagedQueryResult<TaxCategory> pagedQueryResult = getClient().executeBlocking(queryBuilder.build());
			final List<TaxCategory> pageResult = pagedQueryResult.getResults();
			if (pagedQueryResult.getTotal() == 0) {
				breakLoop = true;
			} else {
				result.addAll(pageResult);
				LOG.debug("found {} TaxCategories on page {} of {} pages [{} entries]", pageResult.size(), page,
						pagedQueryResult.getTotalPages(), pagedQueryResult.getTotal());
				if (page == pagedQueryResult.getTotalPages()) {
					breakLoop = true;
				}
				page++;
			}
		}
		LOG.debug("returning total: {} TaxCategories", result.size());
		return result;
	}

	@Override
	public List<QueryPredicate<TaxCategory>> preparePredicates(final Map<String, Object> params) {
		final List<QueryPredicate<TaxCategory>> predicates = new ArrayList<>();
		if (params.containsKey(PARAM_ID)) {
			final String id = (String) params.get(PARAM_ID);
			predicates.add(TaxCategoryQueryModel.of().id().is(id));
		}
		if (params.containsKey(PARAM_KEY)) {
			final String key = (String) params.get(PARAM_KEY);
			predicates.add(TaxCategoryQueryModel.of().key().is(key));
		}
		return predicates;
	}

	@Override
	protected boolean supportsByKeyQuery() {
		return true;
	}

}

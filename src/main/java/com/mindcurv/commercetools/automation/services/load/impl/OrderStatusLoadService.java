package com.mindcurv.commercetools.automation.services.load.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.integration.SyncStatistics;
import com.mindcurv.commercetools.automation.pojo.order.OrderPojo;
import com.mindcurv.commercetools.automation.services.export.OrderImpExService;
import com.mindcurv.commercetools.automation.services.load.AbstractLoadService;
import com.mindcurv.commercetools.automation.services.query.impl.OrderQueryService;

import io.sphere.sdk.orders.Order;

public class OrderStatusLoadService extends AbstractLoadService<OrderPojo> {

	private final static Logger LOG = LoggerFactory.getLogger(OrderStatusLoadService.class);

	private OrderImpExService orderImpExService;

	private OrderQueryService orderQueryService;

	public OrderStatusLoadService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);

	}

	@Override
	protected void init() {
		super.init();
		setTypeParameterClass(OrderPojo.class);
		if (getCommercetoolsProject() != null) {
			if (orderImpExService == null) {
				orderImpExService = new OrderImpExService(getCommercetoolsProject());
			}
			if (orderQueryService == null) {
				orderQueryService = new OrderQueryService(getCommercetoolsProject());
			}
		}

	}

	@Override
	public SyncStatistics executeSync(final List<OrderPojo> drafts) {
		final SyncStatistics syncStatistics = new SyncStatistics();
		if (validForSync(drafts)) {
			for (OrderPojo draft : drafts) {
				syncStatistics.incrementProcessed();
				LOG.info("processing order status message:{}", draft.getOrderNumber());

				final String orderId = draft.getId();
				final String orderNumber = draft.getOrderNumber();
				Order order = null;
				if (StringUtils.isNotBlank(orderId)) {
					order = orderQueryService.getById(orderId);
				} else if (StringUtils.isNotBlank(orderNumber)) {
					order = orderQueryService.getByOrderNumber(orderNumber);
				}
				if (order == null) {
					syncStatistics.incrementFailed();
					LOG.error("No order found for {}/{}", orderId, orderNumber);
				} else {
					Long initialVersion = order.getVersion(); 
					order = orderImpExService.updateOrder(order, draft);
					if ( !initialVersion.equals(order.getVersion())) {
						syncStatistics.incrementUpdated();
					}
				}
			}
		}
		return syncStatistics;
	}

	@Required
	public void setOrderImpExService(OrderImpExService orderImpExService) {
		this.orderImpExService = orderImpExService;
	}

	@Required
	public void setOrderQueryService(OrderQueryService orderQueryService) {
		this.orderQueryService = orderQueryService;
	}

}

package com.mindcurv.commercetools.automation.services.setup.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;
import com.mindcurv.commercetools.automation.pojo.catalog.CategoryPojo;
import com.mindcurv.commercetools.automation.pojo.types.product.TypeAttribute;
import com.mindcurv.commercetools.automation.services.load.impl.CategoryLoadService;
import com.mindcurv.commercetools.automation.services.query.impl.CategoryQueryService;
import com.mindcurv.commercetools.automation.services.setup.SetupService;

import io.sphere.sdk.categories.Category;
import io.sphere.sdk.categories.CategoryDraft;
import io.sphere.sdk.categories.commands.CategoryDeleteCommand;

public class CategorySetupService extends SetupService {
	private final static Logger LOG = LoggerFactory.getLogger(CategorySetupService.class);

	private CategoryQueryService categoryQueryService;
	private CategoryLoadService categoryLoadService;

	public CategorySetupService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	@Override
	protected void init() {
		super.init();
		final CommercetoolsProject commercetoolsProjectBean = getCommercetoolsProject();
		if (commercetoolsProjectBean != null) {
			if (categoryQueryService == null) {
				categoryQueryService = new CategoryQueryService(commercetoolsProjectBean);
			}
			if (categoryLoadService == null) {
				categoryLoadService = new CategoryLoadService(commercetoolsProjectBean);
			}
		}
	}

	
	@Override
	protected boolean setupValues(List<Map<String, Object>> valueLines,
			Map<String, List<TypeAttribute>> attributesMap) {
		
		final List<CategoryDraft> drafts = new ArrayList<>();

		for (Map<String, Object> value : valueLines) {
			final CategoryPojo pojo = new CategoryPojo(value);

			if (pojo.getCustom() != null) {
				pojo.addAttributes(value, attributesMap);
			}
			drafts.add(pojo.toDraft());
		}
		LOG.info("prepared CategoryDraft :{} items", drafts.size());
		if (!drafts.isEmpty()) {
			categoryLoadService.sync(drafts);
		}
		return true;
	}

	public boolean cleanupData() {
		LOG.info("cleanup Category items");
		for (Category category : categoryQueryService.getAllRootCategories()) {
			final CategoryDeleteCommand command = CategoryDeleteCommand.of(category);
			getClient().executeBlocking(command);
		}
		return true;

	}

	@Override
	public String getSheetname() {
		return CsvConstants.Category.SHEET;
	}

	@Required
	public void setCategoryQueryService(CategoryQueryService categoryQueryService) {
		this.categoryQueryService = categoryQueryService;
	}

	@Required
	public void setCategoryLoadService(CategoryLoadService categoryLoadService) {
		this.categoryLoadService = categoryLoadService;
	}

}
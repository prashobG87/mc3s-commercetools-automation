package com.mindcurv.commercetools.automation.services.setup;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.google.api.services.sheets.v4.Sheets.Spreadsheets;
import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.Constants;
import com.mindcurv.commercetools.automation.helper.spreadsheet.GDriveHelper;
import com.mindcurv.commercetools.automation.pojo.types.product.TypeAttribute;
import com.mindcurv.commercetools.automation.services.setup.impl.TypeSetupService;

public class GoogleSheetService {
	private final static Logger LOG = LoggerFactory.getLogger(GoogleSheetService.class);

	private CommercetoolsProject commercetoolsProject;

	private String sheetId;

	private List<SetupService> setupServices;

	public GoogleSheetService(CommercetoolsProject commercetoolsProject) {
		this.commercetoolsProject = commercetoolsProject;
	}

	public String getSheetId() {
		return sheetId;
	}

	@Required
	public void setSheetId(String sheetId) {
		this.sheetId = sheetId;
	}

	public List<SetupService> getSetupServices() {
		return setupServices;
	}

	@Required
	public void setSetupServices(List<SetupService> setupServices) {
		this.setupServices = setupServices;
	}
	
	
	public CommercetoolsProject getCommercetoolsProject() {
		return commercetoolsProject;
	}

	public boolean executeSync() {
		try {
			LOG.info(Constants.MSG_SPACER);

			LOG.info("Loading data into project '{}' with sheet '{}'", getCommercetoolsProject().getProjectKey(), sheetId);

			final Spreadsheets sheets = GDriveHelper.accessSheets(sheetId);
			final Date startDate = new Date();

			Map<String, List<TypeAttribute>> attributesMap = Collections.emptyMap();
			for (SetupService setupService : getSetupServices()) {
				LOG.info(Constants.MSG_SPACER);
				if (setupService instanceof TypeSetupService) {
					attributesMap = ((TypeSetupService) setupService).setup(sheetId, sheets, getCommercetoolsProject());
				}
				setupService.setup(sheetId, sheets, attributesMap);
			}
			final Date endDate = new Date();
			LOG.info("Setup duration: {}s", (endDate.getTime() - startDate.getTime()) / 1000);
			LOG.info(Constants.MSG_SPACER);

			return true;

		} catch (

		Exception e) {
			LOG.error("Exception", e);
		}
		return false;
	}

}

package com.mindcurv.commercetools.automation.services.setup.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.google.api.services.sheets.v4.Sheets.Spreadsheets;
import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.helper.spreadsheet.ExcelHelper;
import com.mindcurv.commercetools.automation.helper.spreadsheet.GDriveHelper;
import com.mindcurv.commercetools.automation.pojo.CsvConstants;
import com.mindcurv.commercetools.automation.pojo.tax.TaxCategoryPojo;
import com.mindcurv.commercetools.automation.pojo.tax.TaxRatePojo;
import com.mindcurv.commercetools.automation.pojo.types.product.TypeAttribute;
import com.mindcurv.commercetools.automation.services.load.impl.TaxCategoryLoadService;
import com.mindcurv.commercetools.automation.services.setup.SetupService;

import io.sphere.sdk.taxcategories.TaxCategory;
import io.sphere.sdk.taxcategories.TaxCategoryDraft;
import io.sphere.sdk.taxcategories.commands.TaxCategoryDeleteCommand;
import io.sphere.sdk.taxcategories.queries.TaxCategoryQuery;

public class TaxSetupService extends SetupService {

	private final static Logger LOG = LoggerFactory.getLogger(TaxSetupService.class);

	private TaxCategoryLoadService taxLoadService;

	public TaxSetupService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	@Override
	protected void init() {
		super.init();

		final CommercetoolsProject commercetoolsProjectBean = getCommercetoolsProject();
		if (commercetoolsProjectBean != null) {
			if (taxLoadService == null) {
				taxLoadService = new TaxCategoryLoadService(commercetoolsProjectBean);
			}
		}
	}

	@Override
	public boolean setup(String sheetId, Spreadsheets sheets, Map<String, List<TypeAttribute>> attributesMap) {

		try {

			List<Map<String, Object>> valueLinesCategories = GDriveHelper.processRange(sheets, sheetId,
					CsvConstants.TaxCategory.SHEETNAME, getCommercetoolsProject());
			final Map<String, TaxCategoryPojo> taxMap = new HashMap<>();
			for (Map<String, Object> value : valueLinesCategories) {
				TaxCategoryPojo pojo = new TaxCategoryPojo().withSheetData(value, Collections.emptyList());
				taxMap.put(pojo.getKey(), pojo);
			}

			List<Map<String, Object>> valueLinesRates = GDriveHelper.processRange(sheets, sheetId,
					CsvConstants.TaxRate.SHEETNAME, getCommercetoolsProject());

			for (Map<String, Object> value : valueLinesRates) {
				TaxRatePojo pojo = new TaxRatePojo().withSheetData(value, Collections.emptyList());
				final TaxCategoryPojo taxPojo = taxMap.get(pojo.getTaxKey());
				if (taxPojo != null) {
					taxPojo.addRate(pojo);
				} else {
					LOG.warn("no tax category {} found [{}]", pojo.getTaxKey(), pojo);
				}
			}
			final List<TaxCategoryDraft> drafts = new ArrayList<>();
			for (Map.Entry<String, TaxCategoryPojo> entry : taxMap.entrySet()) {
				drafts.add(entry.getValue().toDraft());
			}
			taxLoadService.sync(drafts);
			return true;
		} catch (IOException e) {
			LOG.error("Exception", e);
		}
		return false;
	}

	@Override
	public boolean setup(Workbook workbook, Map<String, List<TypeAttribute>> attributesMap) {
		try {

			List<Map<String, Object>> valueLinesCategories = ExcelHelper.processRange(workbook,
					CsvConstants.TaxCategory.SHEET, getCommercetoolsProject());
			final Map<String, TaxCategoryPojo> taxMap = new HashMap<>();
			for (Map<String, Object> value : valueLinesCategories) {
				TaxCategoryPojo pojo = new TaxCategoryPojo().withSheetData(value, Collections.emptyList());
				taxMap.put(pojo.getKey(), pojo);
			}

			List<Map<String, Object>> valueLinesRates = ExcelHelper.processRange(workbook, CsvConstants.TaxRate.SHEET,
					getCommercetoolsProject());

			for (Map<String, Object> value : valueLinesRates) {
				TaxRatePojo pojo = new TaxRatePojo().withSheetData(value, Collections.emptyList());
				final TaxCategoryPojo taxPojo = taxMap.get(pojo.getTaxKey());
				if (taxPojo != null) {
					taxPojo.addRate(pojo);
				} else {
					LOG.warn("no tax category {} found [{}]", pojo.getTaxKey(), pojo);
				}
			}
			final List<TaxCategoryDraft> drafts = new ArrayList<>();
			for (Map.Entry<String, TaxCategoryPojo> entry : taxMap.entrySet()) {
				drafts.add(entry.getValue().toDraft());
			}
			taxLoadService.sync(drafts);
			return true;
		} catch (IOException e) {
			LOG.error("Exception", e);
		}
		return false;
	}

	public boolean cleanupData() {
		for (TaxCategory taxCategory : getClient().executeBlocking(TaxCategoryQuery.of().withLimit(500)).getResults()) {
			try {
				final TaxCategoryDeleteCommand command = TaxCategoryDeleteCommand.of(taxCategory);
				taxCategory = getClient().executeBlocking(command);
				LOG.info("Removed TaxCategory {}", taxCategory);
			} catch (Exception e) {
				LOG.error("Could not remove tax category " + taxCategory.getName(), e);
			}

		}
		return true;

	}

	@Override
	protected boolean setupValues(List<Map<String, Object>> valueLines, Map<String, List<TypeAttribute>> attributesMap) {
		LOG.warn("setupValues() not implemented()");
		return false;
	}

	@Override
	public String getSheetname() {
		return CsvConstants.TaxRate.SHEET;
	}

	@Required
	public void setTaxLoadService(TaxCategoryLoadService taxLoadService) {
		this.taxLoadService = taxLoadService;
	}

}

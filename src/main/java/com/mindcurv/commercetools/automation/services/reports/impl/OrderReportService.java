package com.mindcurv.commercetools.automation.services.reports.impl;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.services.reports.AbstractReportService;
import com.mindcurv.commercetools.automation.services.reports.datasource.BaseJRDataSource;
import com.mindcurv.commercetools.automation.services.reports.datasource.impl.OrdersJRDataSource;

import io.sphere.sdk.orders.Order;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

@Configuration
public class OrderReportService extends AbstractReportService<Order> {

	private final static Logger LOG = LoggerFactory.getLogger(OrderReportService.class);

	public OrderReportService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
	}

	@Override
	public File getReport(Map<String, Object> params) {

		final OrdersJRDataSource jrDataSource = new OrdersJRDataSource();
		final List<Order> result = getQueryService().getQueryResult(params);
		jrDataSource.setElements(result);
		return generateReportFromDataSource(jrDataSource, params);
	}

	@Override
	protected JasperPrint prepareReport(BaseJRDataSource<Order> jrDataSource, Map<String, Object> params)
			throws JRException {
		final String reportName = getReportName(params);
		LOG.info("creating report for {} orders", jrDataSource.getElements().size());
		
		final File templateFile = new File(getReportTemplateDirectory(), reportName + AbstractReportService.JRXML_SUFFIX);
		if (templateFile != null && templateFile.exists()) {
			LOG.debug("Preparing Report {}", reportName);
			final JasperDesign design = JRXmlLoader.load(templateFile);
			final JasperReport report = JasperCompileManager.compileReport(design);
			return JasperFillManager.fillReport(report, params, jrDataSource);
		}
		LOG.warn("Unsupported reportName {}", reportName);
		return null;
	}

}

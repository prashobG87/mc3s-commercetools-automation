package com.mindcurv.commercetools.automation.services.load.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.commercetools.sync.types.TypeSync;
import com.commercetools.sync.types.TypeSyncOptions;
import com.commercetools.sync.types.TypeSyncOptionsBuilder;
import com.commercetools.sync.types.helpers.TypeSyncStatistics;
import com.mindcurv.commercetools.automation.CommercetoolsProject;
import com.mindcurv.commercetools.automation.integration.SyncStatistics;
import com.mindcurv.commercetools.automation.services.load.AbstractLoadService;

import io.sphere.sdk.types.TypeDraft;

public class TypeLoadService extends AbstractLoadService<TypeDraft> {

	private final static Logger LOG = LoggerFactory.getLogger(TypeLoadService.class);

	public TypeLoadService(CommercetoolsProject commercetoolsProject) {
		super(commercetoolsProject);
		setTypeParameterClass(TypeDraft.class);
	}

	@Override
	protected void init() {
		super.init();
		setTypeParameterClass(TypeDraft.class);
	}

	@Override
	public SyncStatistics executeSync(final List<TypeDraft> drafts) {
		final SyncStatistics syncStatistics = new SyncStatistics();
		if (validForSync(drafts)) {
			final TypeSyncOptions syncOptions = TypeSyncOptionsBuilder.of(getClient()).errorCallback(LOG::error)
					.warningCallback(LOG::warn).build();
			final TypeSync sync = new TypeSync(syncOptions);
			TypeSyncStatistics syncStatisticsStage = sync.sync(drafts).toCompletableFuture().join();
			LOG.info("sync statistic:{}", syncStatisticsStage.getReportMessage());
			syncStatistics.setValues(syncStatisticsStage);
		}
		return syncStatistics;
	}

}

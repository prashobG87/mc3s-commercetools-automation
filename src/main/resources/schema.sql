CREATE TABLE `messages` (
  `id` varchar(255) NOT NULL DEFAULT '',
  `type` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `draftdocument` longtext,
  `timestamp` bigint(20) DEFAULT NULL,
  `lastmodified` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
package com.mindcurv.commercetools.automation.impl;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;

import io.sphere.sdk.client.BlockingSphereClient;
import io.sphere.sdk.client.SphereClient;
import io.sphere.sdk.client.SphereClientConfig;
import io.sphere.sdk.client.SphereClientFactory;

public class BaseTest {
    private BlockingSphereClient client;

    @Before
    public void setup() throws IOException {
        client = createSphereClient("/dev.properties");
    }

    protected BlockingSphereClient createSphereClient(String configLocation) throws IOException {
        final Properties properties = new Properties();
        properties.load(getClass().getResourceAsStream(configLocation));

        final SphereClientConfig clientConfig = SphereClientConfig.ofProperties(properties, "");
        final SphereClientFactory factory = SphereClientFactory.of();
        final SphereClient asyncSphereClient = factory.createClient(clientConfig); 
        return BlockingSphereClient.of(asyncSphereClient, 20, TimeUnit.SECONDS);
    }

    @After
    public void teardown() {
        client.close();
    }

    protected BlockingSphereClient client() {
        return client;
    }
}

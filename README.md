# CtpInit

Documentation: see docs/

# Name pattern for static images 
mindcurv_010_product.png -> SKU = mindcurv://010
mindcurv_010_01_product.png -> SKU = mindcurv://010/1

# TODO
## Functions
* Error handling from hybris example
* AWS 
* parallel requests for lists -> see http://commercetools.github.io/commercetools-jvm-sdk/apidocs/io/sphere/sdk/meta/AsyncDocumentation.html#mixing-async-and-sync
##Test
* Unit tests 

## Hardening 
* spring integration mail bug maybe solved in 5.2 (https://jira.spring.io/browse/INT-4299)
* Upgrade to spring boot latest version
* Upgrade spring integration to latest version